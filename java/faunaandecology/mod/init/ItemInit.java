package faunaandecology.mod.init;

import java.util.ArrayList;
import java.util.List;

import faunaandecology.mod.item.ItemModFood;
import faunaandecology.mod.objects.items.ItemBase;
import faunaandecology.mod.objects.items.ItemBowl;
import faunaandecology.mod.objects.items.ItemFuel;
import faunaandecology.mod.objects.items.ItemWoolTuft;
import faunaandecology.mod.objects.items.ItemEggBase;
import faunaandecology.mod.objects.tools.ToolAxe;
import faunaandecology.mod.objects.tools.ToolKnife;
import faunaandecology.mod.objects.tools.ToolPickaxe;
import faunaandecology.mod.objects.tools.ToolShovel;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.common.util.EnumHelper;

public class ItemInit 
{

	public static final List<Item> ITEMS = new ArrayList<Item>();
	
	//Materials
	public static final ToolMaterial TOOL_FLINT = EnumHelper.addToolMaterial("tool_flint", 0, 39, 3.0F, 1.0F, 5);  

	//Resources
	public static final Item FIBER = new ItemBase("fiber");

	public static final Item BONE_MEATY = new ItemBase("bone_meaty");
	
	public static final Item BOWL_WATER = new ItemBowl("bowl_water");
	 
	public static final Item MUSHROOM_STEW_RAW = new ItemModFood("mushroom_stew_raw", 2, 0.25f, Items.BOWL, false, 0.0f);
	public static final Item RABBIT_STEW_RAW = new ItemModFood("rabbit_stew_raw", 4, 0.25f, Items.BOWL, false, 0.0f);
	public static final Item BAT_STEW_RAW = new ItemModFood("bat_stew_raw", 3, 0.25f, Items.BOWL, false, 5.0f,
		    new PotionEffect(Potion.getPotionById(19), 200, 2), //poison
		    new PotionEffect(Potion.getPotionById(17), 350, 3) );
	public static final Item BAT_STEW = new ItemModFood("bat_stew", 13, 0.75f, Items.BOWL, false, 0.0f);
	
	public static final Item SHARDS_BONE = new ItemBase("shards_bone");
	public static final Item TENDON = new ItemBase("tendon");
	public static final Item HAIR_HORSE = new ItemBase("hair_horse");
	
	public static final Item WOOL_TUFT = new ItemWoolTuft("wool_tuft_");
	
	public static final Item PIG_SKIN = new ItemBase("pig_skin");

	public static final Item EGG = new ItemEggBase("egg");
	public static final Item EGG_DUCK = new ItemEggBase("egg_duck");
	
	public static final Item FUR_WHITE = new ItemBase("fur_white");
	public static final Item FUR_BLACK = new ItemBase("fur_black");
	public static final Item FUR_RED = new ItemBase("fur_red");
	public static final Item FUR_BROWN = new ItemBase("fur_brown");
	public static final Item FUR_LIGHT_GRAY = new ItemBase("fur_light_gray");
	public static final Item FUR_DARK_GRAY = new ItemBase("fur_dark_gray");
	//public static final Item PUMPKIN_SLICE = new ItemBase("pumpkin_slice");

	public static final Item TANNIN_POWDER = new ItemBase("tannin_powder");
	
	public static final Item FAT_BOTTLE = new ItemFuel("fat_bottle");
	public static final Item OIL_BOTTLE = new ItemFuel("oil_bottle");
	

	public static final Item BUCKET_CURDLE = new ItemModFood("bucket_curdle", 2, 0.25f, Items.BUCKET, false, 0.0f);

	public static final Item CHEESE_COW = new ItemModFood("cheese_cow", 3, 0.5f, false, 0.0f);

	public static final Item WHEAT_ON_A_STICK = new ItemBase("wheat_on_a_stick").setMaxStackSize(1);
	public static final Item ZOOPEDIA = new ItemBase("zoopedia").setMaxStackSize(1);
	
	public static final Item KNIFE_WOOD = new ToolKnife("knife_wood", Item.ToolMaterial.WOOD);
	public static final Item KNIFE_STONE = new ToolKnife("knife_stone", Item.ToolMaterial.STONE);
	public static final Item KNIFE_IRON = new ToolKnife("knife_iron", Item.ToolMaterial.IRON);
	public static final Item KNIFE_GOLD = new ToolKnife("knife_gold", Item.ToolMaterial.GOLD);
	public static final Item KNIFE_DIAMOND = new ToolKnife("knife_diamond", Item.ToolMaterial.DIAMOND);
	
	//Tools 
	public static final Item KNIFE_FLINT = new ToolKnife("knife_flint", TOOL_FLINT);
	public static final Item AXE_FLINT = new ToolAxe("axe_flint", TOOL_FLINT, 7.0F, -3.1F);
	public static final Item SHOVEL_FLINT = new ToolShovel("shovel_flint", TOOL_FLINT);
	public static final Item PICKAXE_FLINT = new ToolPickaxe("pickaxe_flint", TOOL_FLINT);
	
	public static final Item COOKED_PUMPKIN_SEEDS = new ItemModFood("cooked_pumpkin_seeds", 2, 0.5f, false, 0.0f);


	public static final Item RAW_BOAR_PORKCHOP = new ItemModFood("porkchop_boar_raw", "Wild Boar", 2, 0.18f, false, 0.0f);
	public static final Item JUNGLEFOWL_RAW = new ItemModFood("junglefowl_raw", "Junglefowl", 2, 0.18f, false, 0.0f);
	public static final Item MOUFLON_MEAT = new ItemModFood("mutton_mouflon_raw", "Mouflon", 3, 0.18f, false, 0.0f);
	public static final Item AUROCHS_MEAT = new ItemModFood("beef_aurochs_raw", "Aurochs", 3, 0.18f, false, 0.0f);

	public static final Item MALLARD_RAW = new ItemModFood("mallard_raw", "Mallard", 4, 0.2f, false, 0.3f,
		    new PotionEffect(Potion.getPotionById(9), 300, 1), 
		    new PotionEffect(Potion.getPotionById(19), 300, 1) );
	public static final Item DUCK_RAW = new ItemModFood("duck_raw",  4, 0.2f, false, 0.3f,
		    new PotionEffect(Potion.getPotionById(9), 300, 1), 
		    new PotionEffect(Potion.getPotionById(19), 300, 1) );
	public static final Item DUCK_COOKED = new ItemModFood("duck_cooked", 7, 0.4f, false, 0.3f );
		
	public static final Item WILD_MOOSHROOM_MEAT = new ItemModFood("beef_wild_mooshroom_raw", "Wild Mooshroom", 2, 0.15f, false, 0.3f,
		    new PotionEffect(Potion.getPotionById(9), 300, 1), 
		    new PotionEffect(Potion.getPotionById(19), 300, 1) );
	public static final Item MOOSHROOM_MEAT = new ItemModFood("beef_mooshroom_raw", "Mooshroom", 2, 0.15f, false, 0.2f,
		    new PotionEffect(Potion.getPotionById(9), 300, 1), 
		    new PotionEffect(Potion.getPotionById(19), 300, 1) );
	public static final Item HORSE_MEAT = new ItemModFood("horse_meat", 4, 0.3f, false, 0.0f);
	public static final Item WILD_HORSE_MEAT = new ItemModFood("wild_horse_meat", "Wild Horse", 4, 0.3f, false, 0.2f,
		    new PotionEffect(Potion.getPotionById(9), 300, 1), 
		    new PotionEffect(Potion.getPotionById(17), 300, 1) );
	public static final Item COOKED_HORSE_MEAT = new ItemModFood("cooked_horse_meat", 9, 0.5f, false, 0.0f);
	public static final Item DONKEY_MEAT = new ItemModFood("donkey_meat", 4, 0.3f, false, 0.0f);
	public static final Item WILD_ASS_MEAT = new ItemModFood("wild_ass_meat", "Wild Ass", 4, 0.3f, false,  0.2f,
		    new PotionEffect(Potion.getPotionById(9), 300, 1), 
		    new PotionEffect(Potion.getPotionById(17), 300, 1) );
	public static final Item COOKED_DONKEY_MEAT = new ItemModFood("cooked_donkey_meat", 9, 0.5f, false, 0.0f);
	public static final Item ZEBRA_MEAT = new ItemModFood("zebra_meat", 4, 0.32f, false, 0.2f,
		    new PotionEffect(Potion.getPotionById(9), 300, 1), //naseua
		    new PotionEffect(Potion.getPotionById(17), 300, 1) );//hunger */
	public static final Item QUAGGA_MEAT = new ItemModFood("quagga_meat", "Quagga", 4, 0.3f, false, 0.2f,
		    new PotionEffect(Potion.getPotionById(9), 300, 1), //naseua
		    new PotionEffect(Potion.getPotionById(17), 300, 1) );//hunger */
	public static final Item COOKED_ZEBRA_MEAT = new ItemModFood("cooked_zebra_meat", 9, 0.5f, false, 0.0f);

	public static final Item GUANACO_MEAT = new ItemModFood("guanaco_meat", "Guanaco", 4, 0.3f, false, 0.25f,
		    new PotionEffect(Potion.getPotionById(9), 300, 1), //naseua
		    new PotionEffect(Potion.getPotionById(17), 300, 1) );//hunger */
	public static final Item LLAMA_MEAT = new ItemModFood("llama_meat", 4, 0.3f, false, 0.0f);
	public static final Item COOKED_LLAMA_MEAT = new ItemModFood("cooked_llama_meat", 9, 0.5f, false, 0.0f);
	
	public static final Item RAW_BAT = new ItemModFood("raw_bat", 2, 0.25f, false, 0.5f,
		    new PotionEffect(Potion.getPotionById(19), 200, 2), //poison
		    new PotionEffect(Potion.getPotionById(17), 350, 3) ); //hunger */
	public static final Item COOKED_BAT = new ItemModFood("cooked_bat", 3, 0.45f, false, 0.0f);
	
	public static final Item BONE_MARROW = new ItemModFood("bone_marrow", 2, 0.10f, false, 0.0f);
	public static final Item COOKED_BONE_MARROW = new ItemModFood("cooked_bone_marrow", 3, 0.25f, false, 0.0f);
	
	public static final Item CALAMARI = new ItemModFood("calamari", 2, 0.11f, false, 0.0f);
	public static final Item COOKED_CALAMARI = new ItemModFood("cooked_calamari", 5, 0.69f, false, 0.0f);
	
	
	public static final Item FAT = new ItemModFood("fat", 2, 0.03f, false, 0.3f,
		    new PotionEffect(Potion.getPotionById(9), 300, 1) ); //naseua
	public static final Item TALLOW = new ItemModFood("tallow", 3, 0.14f, false, 0.0f);

	public static final Item RESIN = new ItemBase("resin");
	
	public static final Item RIND_PORK = new ItemModFood("rind_pork", 2, 0.3f, false, 0.0f);
	public static final Item FRIED_EGG = new ItemModFood("fried_egg", 2, 0.3f, false, 0.0f);
	
	public static Item ACORN;
	public static Item ACORN_DARK;
	public static Item ACORN_DYING;
	public static Item ACORN_HELL;
	public static Item ACORN_AUTUMN;
	public static Item ACORN_FLOWERING;
	public static Item BAMBOO_SHOOT;
	public static Item PEAR; 
	public static Item PEACH; 
	public static Item PERSIMMON;
	public static Item PINE_CONE;
	public static Item SPRUCE_CONE;
	public static Item REDWOOD_CONE;
	public static Item FIR_CONE;
	

	public static Item BOP_LOG0;
	public static Item BOP_LOG1;
	public static Item BOP_LOG2;
	public static Item BOP_LOG3;
	

	public static Item TAR_DROP;
	//public static Item BOP_LOG4;
}











