package faunaandecology.mod.init;

import java.util.ArrayList;
import java.util.List;

import faunaandecology.mod.objects.blocks.BlockBase;
import faunaandecology.mod.objects.blocks.BlockCandle;
import faunaandecology.mod.objects.blocks.BlockFeathers;
import faunaandecology.mod.objects.blocks.BlockHide;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockInit 
{


	public static final List<Block> BLOCKS = new ArrayList<Block>();
	public static final Block LEATHER_BLOCK = new BlockBase("leather_block", Material.CLOTH).setHardness(0.6F);
	public static final Block FEATHER_BLOCK = new BlockFeathers("feather_block", Material.CLOTH);
	
	public static final Block HIDE_HORSE_WHITE = new BlockHide("hide_horse_white", Material.WOOD, "White", 8, ItemInit.FUR_WHITE).setHardness(0.3F);
	public static final Block HIDE_HORSE_BLACK = new BlockHide("hide_horse_black", Material.WOOD, "Black", 8, ItemInit.FUR_BLACK).setHardness(0.3F);
	public static final Block HIDE_HORSE_BROWN = new BlockHide("hide_horse_brown", Material.WOOD, "Brown", 8, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_HORSE_CREAMY = new BlockHide("hide_horse_creamy", Material.WOOD, "Creamy", 8, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_HORSE_CHESTNUT = new BlockHide("hide_horse_chestnut", Material.WOOD, "Chestnut", 8, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_HORSE_DARKBROWN = new BlockHide("hide_horse_darkbrown", Material.WOOD, "Dark Brown", 8, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_HORSE_GRAY = new BlockHide("hide_horse_gray", Material.WOOD, "Gray", 8, ItemInit.FUR_LIGHT_GRAY).setHardness(0.3F);
	public static final Block HIDE_MOUFLON_MALE = new BlockHide("hide_mouflon_male", Material.WOOD, "Male", 1, ItemInit.FUR_WHITE, 1, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_MOUFLON_FEMALE = new BlockHide("hide_mouflon_female", Material.WOOD, "Female", 3, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_SHEEP_WHITE = new BlockHide("hide_sheep_white", Material.WOOD, 3, ItemInit.FUR_WHITE).setHardness(0.3F);
	public static final Block HIDE_LLAMA_WHITE = new BlockHide("hide_llama_white", Material.WOOD, 4, ItemInit.FUR_WHITE).setHardness(0.3F);
	public static final Block HIDE_GUANACO = new BlockHide("hide_guanaco", Material.WOOD, 3, ItemInit.FUR_BROWN, 1, ItemInit.FUR_WHITE).setHardness(0.3F);
	public static final Block HIDE_DONKEY = new BlockHide("hide_donkey", Material.WOOD, 6, ItemInit.FUR_LIGHT_GRAY).setHardness(0.3F);
	public static final Block HIDE_ZEBRA = new BlockHide("hide_zebra", Material.WOOD, 3, ItemInit.FUR_WHITE, 3, ItemInit.FUR_BLACK).setHardness(0.3F);
	public static final Block HIDE_WILD_HORSE = new BlockHide("hide_wild_horse", Material.WOOD, 6, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_WILD_ASS = new BlockHide("hide_wild_ass", Material.WOOD, 6, ItemInit.FUR_LIGHT_GRAY).setHardness(0.3F);
	public static final Block HIDE_QUAGGA = new BlockHide("hide_quagga", Material.WOOD, 6, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_COW = new BlockHide("hide_cow", Material.WOOD, 6, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_PIG = new BlockHide("hide_pig", Material.WOOD, 3, ItemInit.PIG_SKIN).setHardness(0.3F);
	public static final Block HIDE_WILD_BOAR = new BlockHide("hide_wild_boar", Material.WOOD, 3, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_MOOSHROOM = new BlockHide("hide_mooshroom", Material.WOOD, 4, ItemInit.FUR_RED).setHardness(0.3F);
	public static final Block PELT_BAT = new BlockHide("pelt_bat", Material.WOOD, 1, ItemInit.FUR_DARK_GRAY).setHardness(0.3F);
	public static final Block PELT_RABBIT = new BlockHide("pelt_rabbit", Material.WOOD, 1, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_SANGA_MALE = new BlockHide("hide_sanga_male", Material.WOOD, "Male", 7, ItemInit.FUR_BLACK).setHardness(0.3F);
	public static final Block HIDE_SANGA_FEMALE = new BlockHide("hide_sanga_female", Material.WOOD, "Female", 7, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_AUROCHS_MALE = new BlockHide("hide_aurochs_male", Material.WOOD, "Male", 8, ItemInit.FUR_BLACK).setHardness(0.3F);
	public static final Block HIDE_AUROCHS_FEMALE = new BlockHide("hide_aurochs_female", Material.WOOD, "Female", 7, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_MUSHROOM_AUROCHS_FEMALE = new BlockHide("hide_mushroom_aurochs_female", Material.WOOD, "Female", 7, ItemInit.FUR_RED).setHardness(0.3F);
	public static final Block HIDE_MUSHROOM_AUROCHS_MALE = new BlockHide("hide_mushroom_aurochs_male", Material.WOOD, "Male", 8, ItemInit.FUR_RED).setHardness(0.3F);
	public static final Block HIDE_SCRUB_AUROCHS_MALE = new BlockHide("hide_scrub_aurochs_male", Material.WOOD, "Male", 8, ItemInit.FUR_BROWN).setHardness(0.3F);
	public static final Block HIDE_SCRUB_AUROCHS_FEMALE = new BlockHide("hide_scrub_aurochs_female", Material.WOOD, "Female", 7, ItemInit.FUR_BROWN).setHardness(0.3F);
	
	public static final Block CANDLE = new BlockCandle("candle", Material.WOOD).setLightLevel(0.625F);
}
