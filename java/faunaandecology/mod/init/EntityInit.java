package faunaandecology.mod.init;


import java.util.Iterator;
import java.util.Set;

import faunaandecology.mod.Main;
import faunaandecology.mod.entity.passive.EntityAfricanAurochs;
import faunaandecology.mod.entity.passive.EntityAurochs;
import faunaandecology.mod.entity.passive.EntityDuck;
import faunaandecology.mod.entity.passive.EntityGuanaco;
import faunaandecology.mod.entity.passive.EntityImprovedChicken;
import faunaandecology.mod.entity.passive.EntityImprovedCow;
import faunaandecology.mod.entity.passive.EntityImprovedDonkey;
import faunaandecology.mod.entity.passive.EntityImprovedHorse;
import faunaandecology.mod.entity.passive.EntityImprovedLlama;
import faunaandecology.mod.entity.passive.EntityImprovedMooshroom;
import faunaandecology.mod.entity.passive.EntityImprovedMule;
import faunaandecology.mod.entity.passive.EntityImprovedPig;
import faunaandecology.mod.entity.passive.EntityImprovedSheep;
import faunaandecology.mod.entity.passive.EntityJungleFowl;
import faunaandecology.mod.entity.passive.EntityMallard;
import faunaandecology.mod.entity.passive.EntityMouflon;
import faunaandecology.mod.entity.passive.EntityQuagga;
import faunaandecology.mod.entity.passive.EntitySanga;
import faunaandecology.mod.entity.passive.EntityWildAss;
import faunaandecology.mod.entity.passive.EntityWildBoar;
import faunaandecology.mod.entity.passive.EntityWildHorse;
import faunaandecology.mod.entity.passive.EntityWildMooshroom;
import faunaandecology.mod.entity.passive.EntityZebra;
import faunaandecology.mod.entity.passive.EntityZonkey;
import faunaandecology.mod.entity.passive.EntityZorse;
import faunaandecology.mod.entity.projectile.EntityImprovedLlamaSpit;
import faunaandecology.mod.renderer.entity.RenderAfricanAurochs;
import faunaandecology.mod.renderer.entity.RenderAurochs;
import faunaandecology.mod.renderer.entity.RenderDonkey;
import faunaandecology.mod.renderer.entity.RenderDuck;
import faunaandecology.mod.renderer.entity.RenderImprovedChicken;
import faunaandecology.mod.renderer.entity.RenderImprovedCow;
import faunaandecology.mod.renderer.entity.RenderImprovedHorse;
import faunaandecology.mod.renderer.entity.RenderImprovedLlama;
import faunaandecology.mod.renderer.entity.RenderImprovedMooshroom;
import faunaandecology.mod.renderer.entity.RenderImprovedMule;
import faunaandecology.mod.renderer.entity.RenderImprovedPig;
import faunaandecology.mod.renderer.entity.RenderImprovedSheep;
import faunaandecology.mod.renderer.entity.RenderJungleFowl;
import faunaandecology.mod.renderer.entity.RenderMallard;
import faunaandecology.mod.renderer.entity.RenderMouflon;
import faunaandecology.mod.renderer.entity.RenderQuagga;
import faunaandecology.mod.renderer.entity.RenderSanga;
import faunaandecology.mod.renderer.entity.RenderWildAss;
import faunaandecology.mod.renderer.entity.RenderWildBoar;
import faunaandecology.mod.renderer.entity.RenderWildHorse;
import faunaandecology.mod.renderer.entity.RenderWildMooshroom;
import faunaandecology.mod.renderer.entity.RenderZebra;
import faunaandecology.mod.renderer.entity.RenderZonkey;
import faunaandecology.mod.renderer.entity.RenderZorse;
import faunaandecology.mod.util.Config;
import faunaandecology.mod.util.Reference;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.monster.EntityPolarBear;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.passive.EntityDonkey;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.passive.EntityLlama;
import net.minecraft.entity.passive.EntityOcelot;
import net.minecraft.entity.passive.EntityParrot;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.passive.EntityRabbit;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.init.Biomes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.SpawnListEntry;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityInit {

    private static int entityId = 1;
    
    public static void init()
    {
    }

    public static void postInit()
    {
    	for (Biome b: ForgeRegistries.BIOMES) 
    	{
        	//clear vanilla spawns
    		if(Config.noVanillaAnimals == true)
    		{
    			Iterator<SpawnListEntry> itr = b.getSpawnableList(EnumCreatureType.CREATURE).iterator();
    			while(itr.hasNext())
    			{
					if ((itr.next().entityClass == EntityPig.class) && Config.enablePigs == true) {
						itr.remove();
					}
    			    // modify actual fooList using itr.remove()
    			}
    			itr = b.getSpawnableList(EnumCreatureType.CREATURE).iterator();
    			while(itr.hasNext())
    			{
					if ((itr.next().entityClass == EntityChicken.class) && Config.enableFowl == true) {
						itr.remove();
					}
    			}
    			itr = b.getSpawnableList(EnumCreatureType.CREATURE).iterator();
    			while(itr.hasNext())
    			{
    				if ((itr.next().entityClass == EntityHorse.class) && Config.enableHorses == true) {
    					itr.remove();
    				}    				

    			}
    			itr = b.getSpawnableList(EnumCreatureType.CREATURE).iterator();
    			while(itr.hasNext())
    			{
    				if ((itr.next().entityClass == EntityDonkey.class) && Config.enableHorses == true) {
    					itr.remove();
    				}
    			}
    			itr = b.getSpawnableList(EnumCreatureType.CREATURE).iterator();
    			while(itr.hasNext())
    			{
    				if ((itr.next().entityClass == EntityLlama.class) && Config.enableLlamas == true) {
    					itr.remove();
    				}
    			}
    			itr = b.getSpawnableList(EnumCreatureType.CREATURE).iterator();
    			while(itr.hasNext())
    			{
    				if ((itr.next().entityClass == EntityCow.class) && Config.enableCattle == true) {
    					itr.remove();
    				}
    			}
    			itr = b.getSpawnableList(EnumCreatureType.CREATURE).iterator();
    			while(itr.hasNext())
    			{
    				if ((itr.next().entityClass == EntitySheep.class) && Config.enableSheep == true) {
						itr.remove();
					}
				}
    			itr = b.getSpawnableList(EnumCreatureType.CREATURE).iterator();
    			while(itr.hasNext())
    			{
    				if ((itr.next().entityClass == EntityPolarBear.class) && Config.enableBears == true) {
						itr.remove();
					}
				}
    			itr = b.getSpawnableList(EnumCreatureType.CREATURE).iterator();
    			while(itr.hasNext())
    			{
    				if ((itr.next().entityClass == EntityOcelot.class) && Config.enableCats == true) {
						itr.remove();
					}
				}
    			itr = b.getSpawnableList(EnumCreatureType.CREATURE).iterator();
    			while(itr.hasNext())
    			{
    				if ((itr.next().entityClass == EntityParrot.class) && Config.enableParrots == true) {
						itr.remove();
					}
				}
    			itr = b.getSpawnableList(EnumCreatureType.CREATURE).iterator();
    			while(itr.hasNext())
    			{
    				if ((itr.next().entityClass == EntityWolf.class) && Config.enableCanines == true) {
						itr.remove();
					}
				}
    			itr = b.getSpawnableList(EnumCreatureType.CREATURE).iterator();
    			while(itr.hasNext())
    			{
    				if ((itr.next().entityClass == EntityRabbit.class) && Config.enableRabbits == true) {
						itr.remove();
					}
				}
    			itr = b.getSpawnableList(EnumCreatureType.CREATURE).iterator();
    			while(itr.hasNext())
    			{
					if ((itr.next().entityClass == EntityPig.class) && Config.enablePigs == true) {
						itr.remove();
					}
    			    // modify actual fooList using itr.remove()
    			}
    			
    		}
						
    		//b.getSpawnableList(EnumCreatureType.AMBIENT).clear();
    		//b.getSpawnableList(EnumCreatureType.WATER_CREATURE).clear();
    		
    		//disable unnatural mobs
    		if(Config.noUnnaturalMobs == true) b.getSpawnableList(EnumCreatureType.MONSTER).clear();
    	}
    	
    	registerEntity("improvedspit", EntityImprovedLlamaSpit.class, entityId++);
    	registerEntity("improvedchicken", EntityImprovedChicken.class, entityId++, 15648123,  16772300);
    	registerEntity("junglefowl", EntityJungleFowl.class, entityId++, 15648123,  16772300);
    	registerEntity("duck", EntityDuck.class, entityId++, 15648123,  16772300);
    	registerEntity("mallard", EntityMallard.class, entityId++, 15648123,  16772300);
    	registerEntity("improvedsheep", EntityImprovedSheep.class, entityId++, 15648123,  16772300);
    	registerEntity("mouflon", EntityMouflon.class, entityId++, 15648123,  16772300);
    	registerEntity("improvedhorse", EntityImprovedHorse.class, entityId++, 15648123,  16772300);
    	registerEntity("improvedllama", EntityImprovedLlama.class, entityId++, 15648123,  16772300);
    	registerEntity("guanaco", EntityGuanaco.class, entityId++, 15648123,  16772300);
    	registerEntity("wildhorse", EntityWildHorse.class, entityId++, 15648123,  16772300);
    	registerEntity("zorse", EntityZorse.class, entityId++, 15648123,  16772300);
    	registerEntity("wildass", EntityWildAss.class, entityId++, 15648123,  16772300);
    	registerEntity("improveddonkey", EntityImprovedDonkey.class, entityId++, 15648123,  16772300);
    	registerEntity("improvedmule", EntityImprovedMule.class, entityId++, 16777800,  16772230);
    	registerEntity("zonkey", EntityZonkey.class, entityId++, 15588123,  16122300);
    	registerEntity("zebra", EntityZebra.class, entityId++, 99999999,  00000000);
    	registerEntity("quagga", EntityQuagga.class, entityId++, 15648123,  16772300);
    	registerEntity("improvedcow", EntityImprovedCow.class, entityId++, 7879212,  15197922);
    	registerEntity("sanga", EntitySanga.class, entityId++, 7879212,  15197922);
    	registerEntity("aurochs", EntityAurochs.class, entityId++, 1116416,  4073990);
    	registerEntity("aurochs_african", EntityAfricanAurochs.class, entityId++, 1116419,  4073995);
    	registerEntity("wildmooshroom", EntityWildMooshroom.class, entityId++, 4063232,  3617839);
    	registerEntity("improvedmooshroom", EntityImprovedMooshroom.class, entityId++, 7798784,  10790307);
    	registerEntity("improvedpig", EntityImprovedPig.class, entityId++, 16357783,  11949383);
    	registerEntity("wildboar", EntityWildBoar.class, entityId++, 11357783,  14949383);
        
		if(Config.enableHorses == true && Config.dynamicBiomes == false)
		{
			EntityRegistry.addSpawn(EntityZebra.class, 3, 3, 7, EnumCreatureType.CREATURE, Biomes.SAVANNA); //change the values to vary the spawn rarity, biome, etc. 
	    	EntityRegistry.addSpawn(EntityQuagga.class, 3, 2, 6, EnumCreatureType.CREATURE, Biomes.SAVANNA_PLATEAU); //change the values to vary the spawn rarity, biome, etc. 
	    	EntityRegistry.addSpawn(EntityZebra.class, 3, 3, 7, EnumCreatureType.CREATURE, Biomes.MUTATED_SAVANNA); //change the values to vary the spawn rarity, biome, etc.
	    	
			if(Config.wildHorses == true)
			{
		    	EntityRegistry.addSpawn(EntityWildHorse.class, 4, 2, 6, EnumCreatureType.CREATURE, Biomes.PLAINS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityWildHorse.class, 4, 2, 6, EnumCreatureType.CREATURE, Biomes.PLAINS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityWildHorse.class, 4, 2, 6, EnumCreatureType.CREATURE, Biomes.ICE_PLAINS); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildHorse.class, 4, 2, 6, EnumCreatureType.CREATURE, Biomes.FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildHorse.class, 4, 2, 6, EnumCreatureType.CREATURE, Biomes.FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildHorse.class, 4, 2, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_FOREST); //change the values to vary the spawn rarity, biome, etc.

		    	EntityRegistry.addSpawn(EntityWildAss.class, 1, 1, 3, EnumCreatureType.CREATURE, Biomes.MUTATED_SAVANNA_ROCK); //change the values to vary the spawn rarity, biome, etc.
			}
			else
			{
		    	EntityRegistry.addSpawn(EntityImprovedHorse.class, 4, 2, 6, EnumCreatureType.CREATURE, Biomes.PLAINS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedHorse.class, 4, 2, 6, EnumCreatureType.CREATURE, Biomes.PLAINS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedHorse.class, 4, 2, 6, EnumCreatureType.CREATURE, Biomes.ICE_PLAINS); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedHorse.class, 4, 2, 6, EnumCreatureType.CREATURE, Biomes.FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedHorse.class, 4, 2, 6, EnumCreatureType.CREATURE, Biomes.FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedHorse.class, 4, 2, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_FOREST); //change the values to vary the spawn rarity, biome, etc.
		    	EntityRegistry.addSpawn(EntityImprovedDonkey.class, 1, 1, 3, EnumCreatureType.CREATURE, Biomes.MUTATED_SAVANNA_ROCK); //change the values to vary the spawn rarity, biome, etc.	
			}
	    	
		}

		if(Config.enableFowl == true && Config.dynamicBiomes == false)
		{
			if(Config.wildFowl == true)
			{
		    	EntityRegistry.addSpawn(EntityJungleFowl.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.JUNGLE); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityJungleFowl.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.JUNGLE_EDGE); //change the values to vary the spawn rarity, biome, etc.								
		    	EntityRegistry.addSpawn(EntityJungleFowl.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_JUNGLE); //change the values to vary the spawn rarity, biome, etc.								
		    	EntityRegistry.addSpawn(EntityJungleFowl.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.JUNGLE_HILLS); //change the values to vary the spawn rarity, biome, etc.								
		    	EntityRegistry.addSpawn(EntityJungleFowl.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_JUNGLE_EDGE); //change the values to vary the spawn rarity, biome, etc.								
			}
			else
			{
		    	EntityRegistry.addSpawn(EntityImprovedChicken.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.JUNGLE); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedChicken.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.JUNGLE_EDGE); //change the values to vary the spawn rarity, biome, etc.								
		    	EntityRegistry.addSpawn(EntityImprovedChicken.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_JUNGLE); //change the values to vary the spawn rarity, biome, etc.								
		    	EntityRegistry.addSpawn(EntityImprovedChicken.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.JUNGLE_HILLS); //change the values to vary the spawn rarity, biome, etc.								
		    	EntityRegistry.addSpawn(EntityImprovedChicken.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_JUNGLE_EDGE); //change the values to vary the spawn rarity, biome, etc.								
			} 
			
			if(Config.wildFowl == true)
			{
		    	EntityRegistry.addSpawn(EntityMallard.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.RIVER); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityMallard.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.SWAMPLAND); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityMallard.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_SWAMPLAND); //change the values to vary the spawn rarity, biome, etc.								
			}
			else
			{
		    	EntityRegistry.addSpawn(EntityDuck.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.RIVER); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityDuck.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.SWAMPLAND); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityDuck.class, 5, 2, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_SWAMPLAND); //change the values to vary the spawn rarity, biome, etc.								
			}
		}
		
		if(Config.enableLlamas == true && Config.dynamicBiomes == false)
		{
			if(Config.wildLlamas == true)
			{
		    	EntityRegistry.addSpawn(EntityGuanaco.class, 3, 2, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_EXTREME_HILLS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityGuanaco.class, 3, 2, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_EXTREME_HILLS_WITH_TREES); //change the values to vary the spawn rarity, biome, etc.								
			}
			else
			{
		    	EntityRegistry.addSpawn(EntityImprovedLlama.class, 3, 2, 6, EnumCreatureType.CREATURE, Biomes.EXTREME_HILLS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedLlama.class, 3, 2, 6, EnumCreatureType.CREATURE, Biomes.EXTREME_HILLS_WITH_TREES); //change the values to vary the spawn rarity, biome, etc.				
		    	EntityRegistry.addSpawn(EntityImprovedLlama.class, 3, 2, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_TAIGA); //change the values to vary the spawn rarity, biome, etc.				
		    	EntityRegistry.addSpawn(EntityImprovedLlama.class, 3, 2, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_TAIGA_COLD); //change the values to vary the spawn rarity, biome, etc.				
			} 
		}
		
		if(Config.enableSheep == true && Config.dynamicBiomes == false)
		{
			if(Config.wildSheep == true)
			{
		    	EntityRegistry.addSpawn(EntityMouflon.class, 4, 2, 7, EnumCreatureType.CREATURE, Biomes.EXTREME_HILLS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityMouflon.class, 4, 2, 7, EnumCreatureType.CREATURE, Biomes.EXTREME_HILLS_WITH_TREES); //change the values to vary the spawn rarity, biome, etc. 
			}
			else		
			{
		    	EntityRegistry.addSpawn(EntityImprovedSheep.class, 4, 2, 7, EnumCreatureType.CREATURE, Biomes.EXTREME_HILLS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedSheep.class, 4, 2, 7, EnumCreatureType.CREATURE, Biomes.EXTREME_HILLS_WITH_TREES); //change the values to vary the spawn rarity, biome, etc. 
			} 
		}
		
		
		if(Config.enablePigs == true && Config.dynamicBiomes == false)
		{
			if(Config.wildPorcines == true)
			{
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.BIRCH_FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.BIRCH_FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.EXTREME_HILLS_WITH_TREES); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.JUNGLE); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.JUNGLE_EDGE); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.JUNGLE_HILLS); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_BIRCH_FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_BIRCH_FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_EXTREME_HILLS_WITH_TREES); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_JUNGLE); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_JUNGLE_EDGE); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MESA_ROCK); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_ROOFED_FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_SWAMPLAND); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_TAIGA); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.ROOFED_FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.TAIGA); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityWildBoar.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.TAIGA_HILLS); //change the values to vary the spawn rarity, biome, etc.
			}
			else
			{
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.BIRCH_FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.BIRCH_FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.EXTREME_HILLS_WITH_TREES); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.JUNGLE); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.JUNGLE_EDGE); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.JUNGLE_HILLS); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_BIRCH_FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_BIRCH_FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_EXTREME_HILLS_WITH_TREES); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_JUNGLE); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_JUNGLE_EDGE); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MESA_ROCK); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_ROOFED_FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_SWAMPLAND); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.MUTATED_TAIGA); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.ROOFED_FOREST); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.TAIGA); //change the values to vary the spawn rarity, biome, etc.
				EntityRegistry.addSpawn(EntityImprovedPig.class, 6, 3, 6, EnumCreatureType.CREATURE, Biomes.TAIGA_HILLS); //change the values to vary the spawn rarity, biome, etc.
			}
		
		}
		
		if(Config.enableCattle == true && Config.dynamicBiomes == false)
		{
			if(Config.wildBovine == true)
			{
				EntityRegistry.addSpawn(EntityAurochs.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.FOREST); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityAurochs.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc.  
		    	EntityRegistry.addSpawn(EntityAurochs.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.MUTATED_FOREST); //change the values to vary the spawn rarity, biome, etc.
		    	EntityRegistry.addSpawn(EntityAurochs.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.BIRCH_FOREST); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityAurochs.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.BIRCH_FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityAurochs.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.MUTATED_BIRCH_FOREST); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityAurochs.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.MUTATED_BIRCH_FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityAurochs.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.TAIGA); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityAurochs.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.TAIGA_HILLS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityAurochs.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.MUTATED_TAIGA); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityAurochs.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.COLD_TAIGA); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityAurochs.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.COLD_TAIGA_HILLS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityAurochs.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.MUTATED_TAIGA_COLD); //change the values to vary the spawn rarity, biome, etc. 
		    	
		    	EntityRegistry.addSpawn(EntityAfricanAurochs.class, 6, 3, 7, EnumCreatureType.CREATURE, Biomes.MUTATED_MESA); //change the values to vary the spawn rarity, biome, etc
		    	
		    	EntityRegistry.addSpawn(EntityWildMooshroom.class, 6, 3, 8, EnumCreatureType.CREATURE, Biomes.MUSHROOM_ISLAND); //change the values to vary the spawn rarity, biome, etc.
			}
			else
			{
				EntityRegistry.addSpawn(EntityImprovedCow.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.FOREST); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedCow.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc.  
		    	EntityRegistry.addSpawn(EntityImprovedCow.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.MUTATED_FOREST); //change the values to vary the spawn rarity, biome, etc.		    	
		    	EntityRegistry.addSpawn(EntityImprovedCow.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.BIRCH_FOREST); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedCow.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.BIRCH_FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedCow.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.MUTATED_BIRCH_FOREST); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedCow.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.MUTATED_BIRCH_FOREST_HILLS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedCow.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.TAIGA); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedCow.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.TAIGA_HILLS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedCow.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.MUTATED_TAIGA); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedCow.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.COLD_TAIGA); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedCow.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.COLD_TAIGA_HILLS); //change the values to vary the spawn rarity, biome, etc. 
		    	EntityRegistry.addSpawn(EntityImprovedCow.class, 6, 2, 7, EnumCreatureType.CREATURE, Biomes.MUTATED_TAIGA_COLD); //change the values to vary the spawn rarity, biome, etc. 

		    	EntityRegistry.addSpawn(EntityImprovedMooshroom.class, 6, 3, 8, EnumCreatureType.CREATURE, Biomes.MUSHROOM_ISLAND); //change the values to vary the spawn rarity, biome, etc. 
			}
	    	
		}
		
    	SpawnListEntry chickenSpawn = new SpawnListEntry(EntityImprovedChicken.class, 5, 2, 6);
    	SpawnListEntry duckSpawn = new SpawnListEntry(EntityDuck.class, 5, 2, 6);
    	SpawnListEntry assSpawn = new SpawnListEntry(EntityImprovedDonkey.class, 4, 1, 3);
    	SpawnListEntry horseSpawn = new SpawnListEntry(EntityImprovedHorse.class, 4, 2, 6);
    	SpawnListEntry zebraSpawn = new SpawnListEntry(EntityZebra.class, 3, 2, 6);
    	SpawnListEntry quaggaSpawn = new SpawnListEntry(EntityQuagga.class, 1, 2, 6);
    	SpawnListEntry cowSpawn = new SpawnListEntry(EntityImprovedCow.class, 6, 2, 7);
    	SpawnListEntry pigSpawn = new SpawnListEntry(EntityImprovedPig.class, 6, 3, 6);
    	SpawnListEntry mooshroomSpawn = new SpawnListEntry(EntityImprovedMooshroom.class, 6, 3, 8);
    	SpawnListEntry sangaSpawn = new SpawnListEntry(EntitySanga.class, 6, 3, 7);
    	SpawnListEntry sheepSpawn = new SpawnListEntry(EntityImprovedSheep.class, 4, 2, 7);
    	SpawnListEntry llamaSpawn = new SpawnListEntry(EntityImprovedLlama.class, 3, 2, 6);
    	
    	if(Config.wildHorses == true) assSpawn =  new SpawnListEntry(EntityWildAss.class, 4, 1, 3);
    	if(Config.wildHorses == true) horseSpawn =  new SpawnListEntry(EntityWildHorse.class, 4, 2, 6);
    	if(Config.wildBovine == true) cowSpawn = new SpawnListEntry(EntityAurochs.class, 6, 2, 7);
    	if(Config.wildBovine == true) mooshroomSpawn = new SpawnListEntry(EntityWildMooshroom.class, 6, 3, 7);
    	if(Config.wildPorcines == true) pigSpawn =  new SpawnListEntry(EntityWildBoar.class, 6, 3, 6);
    	if(Config.wildBovine == true) sangaSpawn = new SpawnListEntry(EntityAfricanAurochs.class, 6, 3, 7);
    	if(Config.wildSheep == true) sheepSpawn = new SpawnListEntry(EntityMouflon.class, 4, 2, 7);
    	if(Config.wildLlamas == true) llamaSpawn = new SpawnListEntry(EntityGuanaco.class, 3, 2, 6);
    	if(Config.wildFowl == true)  chickenSpawn = new SpawnListEntry(EntityJungleFowl.class, 5, 2, 6);
    	if(Config.wildFowl == true)  duckSpawn = new SpawnListEntry(EntityMallard.class, 5, 2, 6);
    	
    	Set<Biome> biomes; //BiomeDictionary.getBiomes(Type.FOREST);

    	if(Config.dynamicBiomes == true)
		{	
    		
    		if(Config.enableFowl == true)
    		{

    	    	//Chickens in jungles
    	    	biomes = BiomeDictionary.getBiomes(Type.JUNGLE);
    	    	for (Biome b: biomes) {
    	    		if (!BiomeDictionary.hasType(b, Type.COLD)) 
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(chickenSpawn);    			
    	    		}
    			}

    	    	//Ducks in swamps and rivers
    	    	biomes = BiomeDictionary.getBiomes(Type.SWAMP);
    	    	for (Biome b: biomes) {
    	    		if (!BiomeDictionary.hasType(b, Type.JUNGLE)) 
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(duckSpawn);    			
    	    		}
    	    		if( (BiomeDictionary.hasType(b, Type.RIVER) ) && (!BiomeDictionary.hasType(b, Type.SNOWY)) )
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(duckSpawn);    			
    	    		}
    			}
    		}
    		
    		if(Config.enableSheep == true)
    		{

    	    	//Sheep in lightly forested mountains
    	    	biomes = BiomeDictionary.getBiomes(Type.MOUNTAIN);
    	    	for (Biome b: biomes) {
    	    		if( (BiomeDictionary.hasType(b, Type.HILLS) || BiomeDictionary.hasType(b, Type.FOREST)) && (!BiomeDictionary.hasType(b, Type.DENSE)) )
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(sheepSpawn);    			
    	    		}
    			}
    		}
    		
    		if(Config.enableLlamas == true)
    		{

    	    	//llamas in sparse unforested mountains
    	    	biomes = BiomeDictionary.getBiomes(Type.MOUNTAIN);
    	    	for (Biome b: biomes) {
    	    		if ( BiomeDictionary.hasType(b, Type.SPARSE) && !BiomeDictionary.hasType(b, Type.FOREST) && !BiomeDictionary.hasType(b, Type.DRY)) 
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(llamaSpawn);    			
    	    		}
    			}
    		}
    		
    		if(Config.enableCattle == true)
    		{
    	    	//Cows in cool forests
    	    	biomes = BiomeDictionary.getBiomes(Type.FOREST);
    	    	for (Biome b: biomes) {
    	    		if(!BiomeDictionary.hasType(b, Type.HOT) && !BiomeDictionary.hasType(b, Type.SAVANNA) && !BiomeDictionary.hasType(b, Type.JUNGLE))
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(cowSpawn);    			
    	    		}
    			}
    	
    	    	//mooshroom with mushrooms
    	    	biomes = BiomeDictionary.getBiomes(Type.MUSHROOM);
    	    	for (Biome b: biomes) {
    	    		  b.getSpawnableList(EnumCreatureType.CREATURE).add(mooshroomSpawn);
    			}

    	    	//mesa with spare vegetation
    	    	biomes = BiomeDictionary.getBiomes(Type.MESA);
    	    	for (Biome b: biomes) {
    	    		if(BiomeDictionary.hasType(b, Type.SPARSE) || BiomeDictionary.hasType(b, Type.FOREST))
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(sangaSpawn);    			
    	    		}
    	    	}
    		}
    		
    		if(Config.enableHorses == true)
    		{

    	    	//quagga  in temperate savanna plateau
    	    	biomes = BiomeDictionary.getBiomes(Type.SAVANNA);
    	    	for (Biome b: biomes) {
    	    		if(!BiomeDictionary.hasType(b, Type.COLD) && !BiomeDictionary.hasType(b, Type.DRY) && BiomeDictionary.hasType(b, Type.RARE))
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(quaggaSpawn);
    	    		}
    			}
    	    	//zebra in temperate savanna
    	    	biomes = BiomeDictionary.getBiomes(Type.SAVANNA);
    	    	for (Biome b: biomes) {
    	    		if(!BiomeDictionary.hasType(b, Type.COLD) && !BiomeDictionary.hasType(b, Type.DRY) && !BiomeDictionary.hasType(b, Type.RARE))
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(zebraSpawn);
    	    		}
    			}
    	  		
    	  		
    	    	//horses in cooler plains
    	    	biomes = BiomeDictionary.getBiomes(Type.PLAINS);
    	    	for (Biome b: biomes) {
    	    		if( !BiomeDictionary.hasType(b, Type.MESA) && !BiomeDictionary.hasType(b, Type.HOT)) 
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(horseSpawn);    			
    	    		}
    			}
    	    	biomes = BiomeDictionary.getBiomes(Type.WASTELAND);
    	    	for (Biome b: biomes) {
    	    		if( BiomeDictionary.hasType(b, Type.COLD) ) 
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(horseSpawn);    			
    	    		}
    			}
    	    	//horses in warm forests
    	    	biomes = BiomeDictionary.getBiomes(Type.FOREST);
    	    	for (Biome b: biomes) {
    	    		if(!BiomeDictionary.hasType(b, Type.COLD))
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(horseSpawn);    			
    	    		}
    			}
    	    	
    	    	//asses in hot, dry hills
    	    	biomes = BiomeDictionary.getBiomes(Type.HILLS);
    	    	for (Biome b: biomes) {
    	    		if(BiomeDictionary.hasType(b, Type.HOT) && BiomeDictionary.hasType(b, Type.DRY)) 
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(assSpawn);    			
    	    		}
    			}
//    	    	
//    	    	
    		}
    		
    		///
    		if(Config.enablePigs == true)
    		{

    	    	//boars in many places
    	    	//non-snowy forests
    	    	biomes = BiomeDictionary.getBiomes(Type.FOREST);
    	    	for (Biome b: biomes) {
    	    		if(!BiomeDictionary.hasType(b, Type.SNOWY))
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(pigSpawn);    			
    	    		}
    			}
    	    	//hilly swamp lands
    	    	biomes = BiomeDictionary.getBiomes(Type.SWAMP);
    	    	for (Biome b: biomes) {
    	    		if(BiomeDictionary.hasType(b, Type.HILLS))
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(pigSpawn);    			
    	    		}
    	    	}
    	    	///non-snowy forests
    	    	biomes = BiomeDictionary.getBiomes(Type.JUNGLE);
    	    	for (Biome b: biomes) {
    	    		if(!BiomeDictionary.hasType(b, Type.SNOWY))
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(pigSpawn);    			
    	    		}
    	    	}
    	    	//mesa with spare vegetation
    	    	biomes = BiomeDictionary.getBiomes(Type.MESA);
    	    	for (Biome b: biomes) {
    	    		if(BiomeDictionary.hasType(b, Type.SPARSE))
    	    		{
    	    		    b.getSpawnableList(EnumCreatureType.CREATURE).add(pigSpawn);    			
    	    		}
    	    	}
    	    	
    		}
	    	//
	    	
		}
    }
    private static void registerEntity(String name, Class<? extends Entity> cls, int id)
    {
        EntityRegistry.registerModEntity(new ResourceLocation(Reference.MODID + ":" + name), cls, name, id++, Main.instance, 80, 3, true);
    }

    private static void registerEntity(String name, Class<? extends Entity> cls, int id, int primaryEggColor, int secondaryEggColor)
    {
        EntityRegistry.registerModEntity(new ResourceLocation(Reference.MODID + ":" + name), cls, name, id++, Main.instance, 80, 3, true, primaryEggColor, secondaryEggColor);
    }
    

    @SideOnly(Side.CLIENT)
    public static void initModels() {
    	RenderingRegistry.registerEntityRenderingHandler(EntityJungleFowl.class, RenderJungleFowl.FACTORY);
       	RenderingRegistry.registerEntityRenderingHandler(EntityImprovedChicken.class, RenderImprovedChicken.FACTORY);
       	RenderingRegistry.registerEntityRenderingHandler(EntityMallard.class, RenderMallard.FACTORY);
       	RenderingRegistry.registerEntityRenderingHandler(EntityDuck.class, RenderDuck.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityImprovedSheep.class, RenderImprovedSheep.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityMouflon.class, RenderMouflon.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityImprovedLlama.class, RenderImprovedLlama.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityWildHorse.class, RenderWildHorse.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityZorse.class, RenderZorse.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityWildAss.class, RenderWildAss.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityImprovedDonkey.class, RenderDonkey.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityQuagga.class, RenderQuagga.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityZebra.class, RenderZebra.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityImprovedMule.class, RenderImprovedMule.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityZonkey.class, RenderZonkey.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityImprovedHorse.class, RenderImprovedHorse.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityImprovedCow.class, RenderImprovedCow.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntitySanga.class, RenderSanga.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityWildMooshroom.class, RenderWildMooshroom.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityImprovedMooshroom.class, RenderImprovedMooshroom.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityAurochs.class, RenderAurochs.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityAfricanAurochs.class, RenderAfricanAurochs.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityImprovedPig.class, RenderImprovedPig.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityWildBoar.class, RenderWildBoar.FACTORY);
    }

}