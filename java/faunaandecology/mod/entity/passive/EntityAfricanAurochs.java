package faunaandecology.mod.entity.passive;

import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import faunaandecology.mod.entity.IEntityAdvancedMilkable;
import faunaandecology.mod.util.Config;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeDictionary.Type;

public class EntityAfricanAurochs extends EntityAurochs
{

    private int parentTamability;
    private float parentMaxHealth;
    private int parentMaxMilk;

    private int groupTimer = 0;
    
	public EntityAfricanAurochs(World worldIn) {
		super(worldIn);
		// TODO Auto-generated constructor stub
	}
	

    @Override
    public String getZoopediaName()
    {
    	return "Scrub Aurochs";
    }
    
   @Override
@Nullable
   public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty, @Nullable IEntityLivingData livingdata)
   {
   	super.onInitialSpawn(difficulty, livingdata);

       int randroll = this.rand.nextInt(2);
		if (Config.enableSexes == true)
		{
	        if (randroll == 1)
			{
	            this.dataManager.set(SEX, Integer.valueOf(1));	
			}
	        else
	        {
	            this.dataManager.set(SEX, Integer.valueOf(0));
	        }
	        randroll = this.rand.nextInt(2);
       }

		
		randroll = this.rand.nextInt(2);
		if (randroll == 1)
		{
   		this.dataManager.set(TAMABILITY, Integer.valueOf(1));
		}
       else
       {
   		this.dataManager.set(TAMABILITY, Integer.valueOf(0));
       }
		this.setHunger(foodMax);
		this.setHappiness(0);
		this.setMaxMilk(MathHelper.clamp(this.rand.nextInt(5) + this.rand.nextInt(6), 3, 9));
		this.setCurMilk(this.getMaxMilk());
		this.setMilkTimer(0);

       if (this.rand.nextInt(5) == 0)
       {
           this.setGrowingAge(childAge);
           this.setOldAge(0);
       }    	

       this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.getModifiedMaxHealth());
       this.setHealth(this.getMaxHealth());
       //new
       this.setupTamedAI();
       return livingdata;
   }

    
    /**
     * Returns randomized max health
     */
    private float getModifiedMaxHealth()
    {
        return 20.0F + this.rand.nextInt(5) + this.rand.nextInt(5);
    }

    
    //cactus immunity
   @Override
   public boolean attackEntityFrom(DamageSource par1DamageSource, float par2)
   {
   	this.setHappiness(this.getHappiness()-50);
   	if ( par1DamageSource == DamageSource.CACTUS ) return false; 
       return super.attackEntityFrom(par1DamageSource, par2);
   }


   //
   @Override
   public EntityCow createChild(EntityAgeable ageable)
   {
   	IEntityAdvancedMilkable entitycow = (IEntityAdvancedMilkable)ageable;

       //Domestication process
       int newTamability = (entitycow.getTamability() + this.getTamability()) / 2;
       int randroll = this.rand.nextInt(2) + 1;
       if (this.isTame() || entitycow.isTame())
       {
       	newTamability += randroll;	
       }
       else
       {
       	newTamability -= randroll;
       }
       newTamability = MathHelper.clamp(newTamability, -5, 5);
       

       IEntityAdvancedMilkable entitynewcow;
       if ( newTamability >= 0  || ((ageable.getClass() == EntitySanga.class)) )
       {
           entitynewcow = new EntitySanga(this.world);
       }
       else
       {
       	entitynewcow = new EntityAfricanAurochs(this.world);
       }
       //check nearby players for taming
       List<EntityPlayer> players = this.world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(9.0D));
		for (Entity e : players)
		{
			if (e != null)
			{
	    		entitynewcow.setOwnerId(e.getPersistentID());
	    		entitynewcow.setTamed(true);
			}
		}
       entitynewcow.setTamability(newTamability);
       
       if (Config.newHorseBreeding == true)
		{
	        
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue();
	        entitynewcow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(Math.round(d1 / 1.5D + (this.rand.nextDouble() * 0.5D)));
	        if (entitynewcow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 30.0D) entitynewcow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(30.0D);
	        if (entitynewcow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() < 20.0D) entitynewcow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0D);
	        
	        entitynewcow.setMaxMilk((this.getMaxMilk() + entitycow.getMaxMilk()) /2);
	        if (this.rand.nextInt(3) == 0) entitynewcow.setMaxMilk(entitynewcow.getMaxMilk()+this.rand.nextInt(3) - 1);
	        entitynewcow.setMaxMilk(MathHelper.clamp(entitynewcow.getMaxMilk(), 3, 6));
	        entitynewcow.setMilkTimer(0);
	        entitynewcow.setCurMilk(0);
       }
		else
		{
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.getModifiedMaxHealth();
	        entitynewcow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(d1 / 3.0D);
		}
       
       EntityCow entityreturncow = (EntityCow) entitynewcow;

		if (Config.enableSexes == true)
		{
			entitynewcow.setSex(Integer.valueOf(this.rand.nextInt(2)));			
		}
		entitynewcow.setHunger(foodMax);
		entitynewcow.setHappiness(0);
       
       return entityreturncow;
   }
   

   @Override
	public EntityCow createChild()
   {
       //Domestication process
       int newTamability = (this.parentTamability + this.getTamability()) / 2;
       int randroll = this.rand.nextInt(2) + 1;
       if (this.isTame())
       {
       	newTamability += randroll;	
       }
       else
       {
       	newTamability -= randroll;
       }
       newTamability = MathHelper.clamp(newTamability, -5, 5);
       

       IEntityAdvancedMilkable entitynewcow;
       if ( newTamability >= 0 )
       {
           entitynewcow = new EntitySanga(this.world);
       }
       else
       {
       	entitynewcow = new EntityAfricanAurochs(this.world);
       }
       //check nearby players for taming
       List<EntityPlayer> players = this.world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(9.0D));
		for (Entity e : players)
		{
			if (e != null)
			{
	    		entitynewcow.setOwnerId(e.getPersistentID());
	    		entitynewcow.setTamed(true);
			}
		}
       entitynewcow.setTamability(newTamability);
       
       EntityCow entityreturncow = (EntityCow) entitynewcow;
       if (Config.newHorseBreeding == true)
		{
	        
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.parentMaxHealth;
	        entityreturncow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(Math.round(d1 / 1.5D + (this.rand.nextDouble() * 0.5D)));
	        //entityreturncow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(d1 / (1.5D + (this.rand.nextDouble() * 1.0D)));
	        if (entityreturncow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 30.0D) entityreturncow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(30.0D);
	        if (entityreturncow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() < 20.0D) entityreturncow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0D);
	        
	        entitynewcow.setMaxMilk((this.getMaxMilk() + this.parentMaxMilk) /2);
	        if (this.rand.nextInt(3) == 0) entitynewcow.setMaxMilk(entitynewcow.getMaxMilk()+this.rand.nextInt(3) - 1);
	        entitynewcow.setMaxMilk(MathHelper.clamp(entitynewcow.getMaxMilk(), 3, 6));
	        entitynewcow.setMilkTimer(0);
	        entitynewcow.setCurMilk(0);
       }
		else
		{
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.parentMaxHealth + this.getModifiedMaxHealth();
	        entityreturncow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(d1 / 3.0D);
		}
       

		if (Config.enableSexes == true)
		{
			entitynewcow.setSex(Integer.valueOf(this.rand.nextInt(2)));			
		}
		entitynewcow.setHunger(foodMax);
		entitynewcow.setHappiness(0);
       
       return entityreturncow;
   }
   
   
   
}
