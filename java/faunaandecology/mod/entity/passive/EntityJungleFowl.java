package faunaandecology.mod.entity.passive;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.collect.Sets;

import faunaandecology.mod.entity.IEntityAdvanced;
import faunaandecology.mod.entity.ai.EntityAIFlyingTempt;
import faunaandecology.mod.entity.ai.EntityAIForager;
import faunaandecology.mod.entity.ai.EntityAIGrainEater;
import faunaandecology.mod.entity.ai.EntityAIHerdMovement;
import faunaandecology.mod.entity.ai.EntityAIImprovedFollowParent;
import faunaandecology.mod.entity.ai.EntityAIImprovedMate;
import faunaandecology.mod.entity.ai.EntityAIItemSearch;
import faunaandecology.mod.init.ItemInit;
import faunaandecology.mod.util.Config;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIPanic;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWanderAvoidWater;
import net.minecraft.entity.ai.EntityAIWanderAvoidWaterFlying;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.ai.EntityMoveHelper;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityFlying;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemMonsterPlacer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.NodeProcessor;
import net.minecraft.pathfinding.PathNavigate;
import net.minecraft.pathfinding.PathNavigateFlying;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.server.management.PreYggdrasilConverter;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;
import net.minecraft.world.storage.loot.LootTableList;

public class EntityJungleFowl extends EntityChicken implements IEntityAdvanced, EntityFlying
{

    //private static final DataParameter<Boolean> SADDLED = EntityDataManager.<Boolean>createKey(EntityImprovedPig.class, DataSerializers.BOOLEAN);
	private static final Set<Item> TEMPTATION_ITEMS = Sets.newHashSet(Items.WHEAT_SEEDS, Items.MELON_SEEDS, Items.PUMPKIN_SEEDS, Items.BEETROOT_SEEDS,
			Items.APPLE, Items.CARROT, Items.WHEAT, Items.BEETROOT, Items.GOLDEN_APPLE, Items.GOLDEN_CARROT, Items.MELON, Items.SPECKLED_MELON);
    public float wingRotation;
    public float destPos;
    public float oFlapSpeed;
    public float oFlap;
    public float wingRotDelta = 1.0F;
        /** The time until the next egg is spawned. */
    public int timeUntilRealEgg;
    public boolean chickenJockey;
	///
    protected PathNavigate flyNavigator;

    private static final DataParameter<Integer> OLD_AGE = EntityDataManager.<Integer>createKey(EntityJungleFowl.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> SEX = EntityDataManager.<Integer>createKey(EntityJungleFowl.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> TAMABILITY = EntityDataManager.<Integer>createKey(EntityJungleFowl.class, DataSerializers.VARINT);
    private static final DataParameter<Float> HUNGER = EntityDataManager.<Float>createKey(EntityJungleFowl.class, DataSerializers.FLOAT);
    private static final DataParameter<Integer> HAPPINESS = EntityDataManager.<Integer>createKey(EntityJungleFowl.class, DataSerializers.VARINT);

    private static final DataParameter<Optional<UUID>> OWNER_UNIQUE_ID = EntityDataManager.<Optional<UUID>>createKey(EntityJungleFowl.class, DataSerializers.OPTIONAL_UNIQUE_ID);
    ///
    
    /** "The higher this value, the more likely the animal is to be tamed." */
    protected int temper;

    //new ai

    private EntityAIAvoidEntity<EntityPlayer> avoidEntity;
    private EntityAIPanic aiPanic;
    private EntityAIAttackMelee aiAttack;
    private EntityAIHerdMovement aiHerd;
    ///
    //private boolean sheared;
    private int groupTimer = 0;
    static public int litterSize = 4;
    static public int pregnancyLength = 15200;
	static public float foodMax = 50.0F;
    static public int ageMax = 480000; //20 'years' //2000 = 1 month
    static public int childAge = -36000; //18 'monthes' 

    private int parentTamability;
    private float parentMaxHealth;

    private int pregnancyTime;
    private int litterSpawned;
    
    public EntityJungleFowl(World worldIn)
    {
        super(worldIn);
        this.setSize(0.6F, 0.7F);
        this.timeUntilRealEgg = this.rand.nextInt(30000) + 30000;
        this.setPathPriority(PathNodeType.WATER, 0.0F);
        this.flyNavigator = this.createFlyNavigator(worldIn);
        ((PathNavigateFlying)this.flyNavigator).setCanFloat(true);
		this.moveHelper = new EntityFowlFlyHelper(this);
    }


    @Override
    public String getZoopediaName()
    {
    	return "Junglefowl";
    }


    @Override
    public float getZoopediaHealth()
    {
    	return this.getHealth();
    }
    
    @Override
    public float getZoopediaMaxHealth()
    {
    	return this.getMaxHealth();
    }
    
    @Override
	protected void initEntityAI()
    {
        //this.tasks.addTask(0, new EntityAISwimming(this));
        this.aiPanic = new EntityAIPanic(this, 1.25D);
        this.aiHerd = new EntityAIHerdMovement(this, 1.25D);
        this.tasks.addTask(1, this.aiPanic);
        this.tasks.addTask(2, this.aiHerd);

        this.tasks.addTask(3, new EntityAIImprovedMate(this, 1.0D, EntityJungleFowl.class));
        this.tasks.addTask(4, new EntityAIFlyingTempt(this, 1.2D, false, TEMPTATION_ITEMS));
        this.tasks.addTask(5, new EntityAIImprovedFollowParent(this, 1.1D, EntityJungleFowl.class));
        this.tasks.addTask(6, new EntityAIForager(this));
        this.tasks.addTask(7, new EntityAIItemSearch(this, 1.0D));
        this.tasks.addTask(8, new EntityAIGrainEater(this));

        this.tasks.addTask(9, new EntityAIWanderAvoidWaterFlying(this, 1.0D));
        
        this.tasks.addTask(10, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
        this.tasks.addTask(11, new EntityAILookIdle(this));
        this.targetTasks.addTask(2, new EntityAIHurtByTarget(this, true, new Class[0]));
    }

    @Override
	protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10.0D);
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.25D);
        this.getAttributeMap().registerAttribute(SharedMonsterAttributes.FLYING_SPEED);
        this.getEntityAttribute(SharedMonsterAttributes.FLYING_SPEED).setBaseValue(0.8D);
        this.getAttributeMap().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE);
        this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(1.0D);
    }
    
    /**
     * Moves the entity based on the specified heading.
     */
    public void moveEntityWithHeading(float strafe, float forward)
    {
        this.stepHeight = 0.5F;

        super.travel(strafe, 0.0F, forward);
    }

    /**
     * For vehicles, the first passenger is generally considered the controller and "drives" the vehicle. For example,
     * Pigs, Horses, and Boats are generally "steered" by the controlling passenger.
     */
    @Override
	@Nullable
    public Entity getControllingPassenger()
    {
        return this.getPassengers().isEmpty() ? null : (Entity)this.getPassengers().get(0);
    }

    @Override
	protected void entityInit()
    {
        super.entityInit();
        this.dataManager.register(OLD_AGE, Integer.valueOf(0));
        this.dataManager.register(SEX, Integer.valueOf(0));
        this.dataManager.register(HUNGER, Float.valueOf(0));
        this.dataManager.register(HAPPINESS, Integer.valueOf(0));
        this.dataManager.register(TAMABILITY, Integer.valueOf(0));
        this.dataManager.register(OWNER_UNIQUE_ID, Optional.<UUID>absent());
		this.setPregnancyTime(0);
		this.setLitterSpawned(0);
    }


    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    @Override
	public void writeEntityToNBT(NBTTagCompound compound)
    {
        super.writeEntityToNBT(compound);
        //compound.setBoolean("Saddle", this.getSaddled());
        compound.setInteger("OldAge", this.getOldAge());
        compound.setInteger("PregnancyTime", this.getPregnancyTime());
        compound.setInteger("Sex", this.getSex());
        compound.setFloat("Hunger", this.getHunger());
        compound.setInteger("Tamability", this.getTamability());
        compound.setInteger("Happiness", this.getHappiness());
        compound.setInteger("Temper", this.getTemper());
        compound.setInteger("ParentTamability", this.parentTamability);
        compound.setFloat("ParentMaxHealth", this.parentMaxHealth);
        compound.setInteger("EggLayTime", this.timeUntilRealEgg);
        
        compound.setInteger("PregnancyTime", this.getPregnancyTime());
        compound.setInteger("LitterSpawned", this.getLitterSpawned());

        if (this.getOwnerId() != null)
        {
            compound.setString("OwnerUUID", this.getOwnerId().toString());
        }
        if (this.getOtherParent() != null)
        {
            compound.setString("ParentUUID", this.getOtherParent().toString());
        }

    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    @Override
	public void readEntityFromNBT(NBTTagCompound compound)
    {
        super.readEntityFromNBT(compound);
        //this.setSaddled(compound.getBoolean("Saddle"));
        this.setOldAge( compound.getInteger("OldAge"));            
        this.setPregnancyTime( compound.getInteger("PregnancyTime"));            
        this.setSex(compound.getInteger("Sex"));
        this.setHunger(compound.getInteger("Hunger"));
        this.setTamability(compound.getInteger("Tamability"));
        this.setHappiness(compound.getInteger("Happiness"));
        this.setTemper(compound.getInteger("Temper"));
        String s;
        this.parentTamability = (compound.getInteger("ParentTamability"));
        this.parentMaxHealth =  (compound.getFloat("ParentMaxHealth"));

        if (compound.hasKey("EggLayTime"))
        {
            this.timeUntilRealEgg = compound.getInteger("EggLayTime");
        }
        
        this.pregnancyTime = (compound.getInteger("PregnancyTime"));
        this.litterSpawned = (compound.getInteger("LitterSpawned"));

        
        if (compound.hasKey("OwnerUUID", 8))
        {
            s = compound.getString("OwnerUUID");
        }
        else
        {
            String s1 = compound.getString("Owner");
            s = PreYggdrasilConverter.convertMobOwnerIfNeeded(this.getServer(), s1);
        }

        if (!s.isEmpty())
        {
            this.setOwnerId(UUID.fromString(s));
        }
        //new
        this.setupTamedAI();
        

        if (compound.hasKey("ParentUUID", 8))
        {
            s = compound.getString("ParentUUID");
        }

        if (!s.isEmpty())
        {
            this.setOtherParent(UUID.fromString(s));
        }
    }

    @Override
    public boolean processInteract(EntityPlayer player, EnumHand hand)
    {
        ItemStack stack = player.getHeldItem(hand);
    	//breeding item overrides
        if (stack != null)
        {
        	if (Config.enableAdvancedStats == true)
        	{
        		if (this.getHunger() < foodMax - 0.5F)
        		{
	        		if  (Config.enableWildBreeding == true)
	        		{
	                    if (this.isBreedingItem(stack)  && !(this.isChild()) && !(this.isInLove()) )
	                    {
	                        this.consumeItemFromStack(player, stack);
	                        return true;
	                    }
	                    else if (this.isBreedingItem(stack))
	                    {
	                        this.consumeItemFromStack(player, stack);
	                        return true;
	                    }
	        		}
	        		else
	        		{
	                    if (this.isBreedingItem(stack) && !(this.isChild()) && !(this.isInLove()) )
	                    {
	                        this.consumeItemFromStack(player, stack);
	                        this.setInLove(player);
	                        return true;
	                    }
	                    else if (this.isBreedingItem(stack))
	                    {
	                        this.consumeItemFromStack(player, stack);
	                        return true;
	                    }
	        		}
        		}
                
                if (this.isEdible(stack))
                {
                	if  (this.getHunger() <  foodMax - 0.5F)
                	{
    	                this.consumeItemFromStack(player, stack);
    	                return true;
                	}
                	else return false;
                }
        	}

            //block vanilla milk harvest
            if (stack.getItem() == Items.BOWL || stack.getItem() == Items.BUCKET)
            {
    			return false;
            }
        }
        
        //override of EntityAgeable to allow varied growth times
        if (stack != null && stack.getItem() == Items.SPAWN_EGG)
        {
            ItemStack itemstack = player.getHeldItem(hand);
            if (!this.world.isRemote)
            {
                Class <? extends Entity > oclass = EntityList.getClass(ItemMonsterPlacer.getNamedIdFrom(itemstack));

                if (oclass != null && this.getClass() == oclass)
                {
                    EntityAgeable entityageable = this.createChild();

                    if (entityageable != null)
                    {
                        entityageable.setGrowingAge(childAge);
                        entityageable.setLocationAndAngles(this.posX, this.posY, this.posZ, 0.0F, 0.0F);
                        this.world.spawnEntity(entityageable);

                        if (stack.hasDisplayName())
                        {
                            entityageable.setCustomNameTag(stack.getDisplayName());
                        }

                        if (!player.capabilities.isCreativeMode)
                        {
                            stack.setCount(stack.getCount() -1);
                        }
                    }
                }
            }
            return true;
        }
    	
        return true;
    }

    /**
     * Drop the equipment for this entity.
     */
    @Override
	protected void dropEquipment(boolean wasRecentlyHit, int lootingModifier)
    {
        super.dropEquipment(wasRecentlyHit, lootingModifier);
    }

    @Override
	@Nullable
    protected ResourceLocation getLootTable()
    {
        return LootTableList.ENTITIES_CHICKEN;
    }


    @Override
	public void fall(float distance, float damageMultiplier)
    {
        super.fall(distance, damageMultiplier);
    }

    @Override
	public EntityChicken createChild(EntityAgeable ageable)
    {
    	IEntityAdvanced entitypig = (IEntityAdvanced)ageable;

        //Domestication process
        int newTamability = (entitypig.getTamability() + this.getTamability()) / 2;
        int randroll = this.rand.nextInt(2) + 1;
        if (this.isTame() || entitypig.isTamed())
        {
        	newTamability += randroll;	
        }
        else
        {
        	newTamability -= randroll;
        }
        newTamability = MathHelper.clamp(newTamability, -5, 5);
        

        IEntityAdvanced entitynewpig;
        if ( newTamability >= 0  || ((ageable.getClass() == EntityImprovedChicken.class) && (this.getClass() == EntityImprovedChicken.class) && (Config.reversion == false) ))
        {
            entitynewpig = new EntityImprovedChicken(this.world);
        }
        else
        {
        	entitynewpig = new EntityJungleFowl(this.world);
        }
        //check nearby players for taming
        List<EntityPlayer> players = this.world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(9.0D));
		for (Entity e : players)
		{
			if (e != null)
			{
	    		entitynewpig.setOwnerId(e.getPersistentID());
	    		entitynewpig.setTamed(true);
			}
		}
        entitynewpig.setTamability(newTamability);

        EntityChicken entityreturnchicken = (EntityChicken) entitynewpig;
        
        if (Config.newHorseBreeding == true)
		{   
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue();
	        entityreturnchicken.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(Math.round(d1 / 1.5D + (this.rand.nextDouble() * 0.5D)));
	        if (entityreturnchicken.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 4.0D) entityreturnchicken.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(15.0D);
	        if (entityreturnchicken.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() < 1.0D) entityreturnchicken.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10.0D);
	        
        }
		else
		{
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.getModifiedMaxHealth();
	        entityreturnchicken.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(d1 / 3.0D);
		}
        
        EntityChicken entityreturnpig = (EntityChicken) entitynewpig;
        

		if (Config.enableSexes == true)
		{
			entitynewpig.setSex(Integer.valueOf(this.rand.nextInt(2)));			
		}
		entitynewpig.setHunger(foodMax);
		entitynewpig.setHappiness(0);
        
        return entityreturnpig;
    }
    
    public EntityChicken createChild()
    {
        //Domestication process
        int newTamability = (this.parentTamability + this.getTamability()) / 2;
        int randroll = this.rand.nextInt(2) + 1;
        if (this.isTame())
        {
        	newTamability += randroll;	
        }
        else
        {
        	newTamability -= randroll;
        }
        newTamability = MathHelper.clamp(newTamability, -5, 5);
        

        IEntityAdvanced entitynewpig;
        if ( newTamability >= 0 || ((this.getClass() == EntityImprovedChicken.class) && (Config.reversion == false) ))//(ageable instanceof EntityAurochs)
        {
            entitynewpig = new EntityImprovedChicken(this.world);
        }
        else
        {
        	entitynewpig = new EntityJungleFowl(this.world);
        }
        
        //check nearby players for taming
        List<EntityPlayer> players = this.world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(9.0D));
		for (Entity e : players)
		{
			if (e != null)
			{
	    		entitynewpig.setOwnerId(e.getPersistentID());
	    		entitynewpig.setTamed(true);
			}
		}
        entitynewpig.setTamability(newTamability);
        
        EntityChicken entityreturnpig = (EntityChicken) entitynewpig;
        if (Config.newHorseBreeding == true)
		{   
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.parentMaxHealth;	        
	        entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(Math.round(Math.round(d1 / 1.5D + (this.rand.nextDouble() * 0.5D))));
	        if (entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 30.0D) entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(30.0D);
	        if (entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() < 20.0D) entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0D);
	        
        }
		else
		{
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.parentMaxHealth + this.getModifiedMaxHealth();
	        entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(d1 / 3.0D);
		}
        
        
        

		if (Config.enableSexes == true)
		{
			entitynewpig.setSex(Integer.valueOf(this.rand.nextInt(2)));			
		}
		
		entitynewpig.setHunger(foodMax);
		entitynewpig.setHappiness(0);
		//entitynewpig.oldAge = 0;
        
        return entityreturnpig;
    }

    /**
     * Checks if the parameter is an item which this animal can be fed to breed it (wheat, carrots or seeds depending on
     * the animal type)
     */
    @Override
	public boolean isBreedingItem(@Nullable ItemStack stack)
    {
        return stack != null && TEMPTATION_ITEMS.contains(stack.getItem());
    }


    ///////////////////////////////////
    //new
    /**
     * Called only once on an entity when first time spawned, via egg, mob spawner, natural spawning etc, but not called
     * when entity is reloaded from nbt. Mainly used for initializing attributes and inventory
     */
    @Override
	@Nullable
    public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty, @Nullable IEntityLivingData livingdata)
    {
    	super.onInitialSpawn(difficulty, livingdata);

        int randroll = this.rand.nextInt(2);
		if (Config.enableSexes == true)
		{
	        if (randroll == 1)
			{
	            this.dataManager.set(SEX, Integer.valueOf(1));	
			}
	        else
	        {
	            this.dataManager.set(SEX, Integer.valueOf(0));
	        }
	        randroll = this.rand.nextInt(2);
        }

		this.setHunger(foodMax);
		this.setHappiness(0);

		if (this.rand.nextInt(5) == 0)
        {
            this.setGrowingAge(childAge);
            this.setOldAge(0);
        }    	

        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.getModifiedMaxHealth());
        this.setHealth(this.getMaxHealth());

        this.setupTamedAI();
        
        this.getOtherParentData(this);
        
        return livingdata;
    }
    /////

    /**
     * Returns true if the mob is currently able to mate with the specified mob.
     */
    @Override
    public boolean canMateWith(EntityAnimal otherAnimal)
    {
        if (otherAnimal == this)
        {
            return false;
        }
        else if (otherAnimal.getClass() == EntityJungleFowl.class || otherAnimal.getClass() == EntityImprovedChicken.class)
        {
        	IEntityAdvanced entitypig = (IEntityAdvanced)otherAnimal;

            if ( this.isInLove() && otherAnimal.isInLove() && (this.getSex() != entitypig.getSex() || (Config.enableSexes == false) ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }
    

    /**
     * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
     * use this to react to sunlight and start to burn.
     */
    @Override
	public void onLivingUpdate()
    {
    	//
        this.oFlap = this.wingRotation;
        this.oFlapSpeed = this.destPos;
        this.destPos = (float)(this.destPos + (this.onGround ? -1 : 4) * 0.3D);
        this.destPos = MathHelper.clamp(this.destPos, 0.0F, 1.0F);

        if (!this.onGround && this.wingRotDelta < 1.0F)
        {
            this.wingRotDelta = 1.0F;
        }

        this.wingRotDelta = (float)(this.wingRotDelta * 0.9D);

        //flight
//        if  (!this.world.isRemote)
//        {
//            if (!this.onGround && this.motionY < 0.0D)
//            {
//            	double drop = this.motionY;
//                this.motionY *= 0.5D;
//               
//                if ( this.isFlying )
//            	{
//                    drop -= this.motionY;
//                	if ( !this.isDescending )
//                	{
//                    	this.moveRelative(0.0F, (float) ( (Math.abs(drop)*this.getFlightPower()*this.rand.nextFloat())), (float) ( (Math.abs(drop)*this.getFlightPower())), 0.99F);
//                    	if (this.rand.nextFloat() > 0.99) this.isDescending = true;
//                	}
//                	else
//                	{
//                    	this.moveRelative(0.0F, (float) ( (Math.abs(drop)*this.rand.nextFloat())), (float) ( (Math.abs(drop)*this.getFlightPower())), 0.99F);
//                	}
//            	}
//            }
//            
//            if (this.onGround || this.isInWater())
//        	{
//            	if ( (this.rand.nextFloat() > 0.999 || this.hurtResistantTime > 0) && !this.isChild() && this.getFlightPower() > 0.0F)
//    			{		
//    	        	this.isFlying = true;
//    	        	this.isDescending = false;
//    	        	this.moveRelative(0.0F, this.getFlightPower(), this.getFlightPower(), 0.99F);
//    			}
//                else if (this.rand.nextFloat() > 0.9)
//                {
//                	 this.isFlying = false;
//                	 this.isDescending = false;
//                }
//        	}
//        }
        //
      
        this.wingRotation += this.wingRotDelta * 2.0F;

        if (!this.isChild() && !this.isChickenJockey() && --this.timeUntilRealEgg <= 0)
        {
        	if (this.getSex() == 0) this.playSound(SoundEvents.ENTITY_CHICKEN_EGG, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
        	if (this.getSex() == 0) this.dropItem(ItemInit.EGG, 1);
            this.timeUntilRealEgg = this.rand.nextInt(30000) + 30000;
        }
        //
    	if (Config.enablePregnancy && this.getPregnancyTime() > 0 && !this.world.isRemote)
    	{
			int p = this.getPregnancyTime();
			if (p >= EntityJungleFowl.pregnancyLength)
			{
				//use alt child gen from stored data on other parent
				EntityAgeable entityageable = this.createChild();
		        final net.minecraftforge.event.entity.living.BabyEntitySpawnEvent event = new net.minecraftforge.event.entity.living.BabyEntitySpawnEvent(this, this, entityageable);
		        final boolean cancelled = net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(event);
		        if (cancelled) {
		            //Reset the "inLove" state for the animals
    	            this.setGrowingAge(this.getChildAge()/-2);
		            this.resetInLove();
		            return;
		        }
	
		        entityageable = event.getChild();
	
		        if (entityageable != null)
		        {
    	            this.setGrowingAge(this.getChildAge()/-2);
		            this.resetInLove();
		            
		            entityageable.setGrowingAge(this.getChildAge());
		            entityageable.setLocationAndAngles(this.posX, this.posY, this.posZ, 0.0F, 0.0F);
		            this.world.spawnEntity(entityageable);
		            Random random = this.getRNG();
	
		            for (int i = 0; i < 7; ++i)
		            {
		                double d0 = random.nextGaussian() * 0.02D;
		                double d1 = random.nextGaussian() * 0.02D;
		                double d2 = random.nextGaussian() * 0.02D;
		                double d3 = random.nextDouble() * this.width * 2.0D - this.width;
		                double d4 = 0.5D + random.nextDouble() * this.height;
		                double d5 = random.nextDouble() * this.width * 2.0D - this.width;
		                this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + d3, this.posY + d4, this.posZ + d5, d0, d1, d2, new int[0]);
		            }
	
		            if (this.world.getGameRules().getBoolean("doMobLoot") && this.isTame())
		            {
		                this.world.spawnEntity(new EntityXPOrb(this.world, this.posX, this.posY, this.posZ, random.nextInt(7) + 1));
		            }
		        }
		        
		        if (this.getLitterSpawned() >= EntityJungleFowl.litterSize)
	        	{
		        	this.setPregnancyTime(0);
		        	this.setLitterSpawned(0);
	        	}
		        else if (this.getLitterSpawned() > 1 && this.getRNG().nextInt(100) < 30)
	        	{
		        	this.setPregnancyTime(0);
		        	this.setLitterSpawned(0);
	        	}
		        else
		        {
		        	this.setLitterSpawned(this.getLitterSpawned()+1);
		        }
			}
			else this.setPregnancyTime(p+1);
    	}

    	super.timeUntilNextEgg = 999;
    	super.onLivingUpdate();
        
		this.groupTimer += 1;

		if (this.groupTimer >= 750)
		{
	    	this.setupTamedAI();
	        Random random = this.getRNG();
	    	if (Config.enableAdvancedStats == true)
	    	{
	    		List<EntityAnimal> list = this.world.<EntityAnimal>getEntitiesWithinAABB(EntityJungleFowl.class, this.getEntityBoundingBox().grow(16.0D));
		        int breedchance = 0;
		        //group size happiness
		        if (list.size() == this.getPreferedGroupSize())
		        {
		        	breedchance = 50;
		        	this.setHappiness(this.getHappiness()+3);
		        }
		        else if (list.size() >= this.getPreferedGroupSize()+2 ) //harder cap
		        {
		        	breedchance = -100;
		        	this.setHappiness(this.getHappiness()-random.nextInt(2)); //new
		        }
		        else if (list.size() <= this.getPreferedGroupSize()-3)
		        {
		        	breedchance = -25;
		        	this.setHappiness(this.getHappiness()-random.nextInt(2)); //new
		        }
		        else
		        {
		        	breedchance = 25;
		        	this.setHappiness(this.getHappiness()+1);
		        }
		        
		        //wild breeding
		        if (list.size() >= 2 && list.size() <= this.getPreferedGroupSize() + 2)
		        {
		        	if (Config.enableWildBreeding == true && !(this.isChild()) && !(this.isInLove()) && (this.getGrowingAge() == 0) )
		        	{
		        		if ( (this.getHappiness() + breedchance) >= random.nextInt(100)) this.setInLove(null);
		        	}
		        }	
	    	}
			this.groupTimer = random.nextInt(10);
		}

        //new
		if (Config.enableAdvancedStats == true && !this.world.isRemote)
		{
        	//aging process
            int i = this.getOldAge();

            ++i;
            this.setOldAge(i);

            if (i == ageMax && Config.enableOldAge == true)
            {
                this.setDead();
            }
        
	        //hungerprocess
	        this.setHunger(this.getHunger() - ((this.getMaxHealth() / 50000)));
	        
	        if (this.getHunger() <= 0 && Config.enableStarvation == true)
	        {
	        	this.damageEntity(DamageSource.STARVE, 1.0F);
	        	this.setHunger(3.0F);
	        	this.setHappiness(this.getHappiness()-10);
	        }
		}

        if (!this.world.isRemote)
        {

            if (this.rand.nextInt(900) == 0 && this.deathTime == 0 && this.getHunger() > 3.0F && this.getHealth() < this.getMaxHealth())
            {
                this.heal(1.0F);
                this.setHunger(this.getHunger()-4.0F);
            }
        }
    }

    
    /**
     * Decreases ItemStack size by one
     */
    @Override
    protected void consumeItemFromStack(EntityPlayer player, ItemStack stack)
    {
    	//new
		if (Config.enableAdvancedStats == true)
		{
			float newHunger = 0;
			int newHappiness = 5;
	    	if (stack.getItem() instanceof ItemFood)
	    	{
	    		ItemFood eatenFood = (ItemFood)stack.getItem();
	    		newHunger = this.getHunger() + eatenFood.getHealAmount(stack) + (eatenFood.getHealAmount(stack)*eatenFood.getSaturationModifier(stack)); 
	    		newHappiness = (int) (this.getHappiness() + eatenFood.getHealAmount(stack) + (eatenFood.getHealAmount(stack)*eatenFood.getSaturationModifier(stack)) );	            
	    		this.setHunger(newHunger);
				this.setHappiness(newHappiness);
	    	}
	    	else
	    	{
				this.eatGrassBonus();
	    	}
	    	
            if (!this.isTame() && this.getTemper() < this.getMaxTemper())
            {
                this.increaseTemper(newHappiness+this.rand.nextInt(5)+5);
                if (this.getTemper() >= this.getMaxTemper())
                {
                	this.setTamedBy(player);
                    double d0 = this.rand.nextGaussian() * 0.025D;
                    double d1 = this.rand.nextGaussian() * 0.025D;
                    double d2 = this.rand.nextGaussian() * 0.025D;
                    this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                }
                else
                {
                    double d0 = this.rand.nextGaussian() * 0.025D;
                    double d1 = this.rand.nextGaussian() * 0.025D;
                    double d2 = this.rand.nextGaussian() * 0.025D;
                    this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
                	this.world.spawnParticle(EnumParticleTypes.VILLAGER_HAPPY, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	
                }
            }
            else
            {
                double d0 = this.rand.nextGaussian() * 0.025D;
                double d1 = this.rand.nextGaussian() * 0.025D;
                double d2 = this.rand.nextGaussian() * 0.025D;
                this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
            	this.world.spawnParticle(EnumParticleTypes.SPELL, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
            }
    	}
    	//
        if (!player.capabilities.isCreativeMode) stack.setCount(stack.getCount() -1);
    }
    
    /**
     * This function applies the benefits of growing back wool and faster growing up to the acting entity. (This
     * function is used in the AIEatGrass)
     */
    @Override
	public void eatGrassBonus()
    {
        this.setHunger(this.getHunger() + 4);
        this.setHappiness(this.getHappiness() + 1);
        return;
    }

    ///////////////////////////////////
    //Getters and setters
    /**
     * Returns randomized max health
     */
    protected float getModifiedMaxHealth()
    {
        return 10.0F + this.rand.nextInt(3) + this.rand.nextInt(4);
    }

    
    @Override
	public int getSex()
    {
        return this.dataManager.get(SEX).intValue();
    }
    
    @Override
	public void setSex(int sexIn)
    {
        this.dataManager.set(SEX, Integer.valueOf(sexIn));
    }

    @Override
	public float getHunger()
    {
        return this.dataManager.get(HUNGER).floatValue();
    }
    
    @Override
	public void setHunger(float hungerIn)
    {
    	//hungerIn = MathHelper.clamp_float(hungerIn, 0.0F, foodMax);
    	if (hungerIn > foodMax) hungerIn = foodMax;
        this.dataManager.set(HUNGER, Float.valueOf(hungerIn));
    }
    
    @Override
	public int getTamability()
    {

        return this.dataManager.get(TAMABILITY).intValue();
    }
    
    @Override
	public void setTamability(int tamabilityIn)
    {
        this.dataManager.set(TAMABILITY, Integer.valueOf(tamabilityIn));
    }

    @Override
	public int getHappiness()
    {
        return this.dataManager.get(HAPPINESS).intValue();
    }
    
    @Override
	public void setHappiness(int happinessIn)
    {
    	happinessIn = MathHelper.clamp(happinessIn, -100, 100);
    	if (happinessIn > 100) happinessIn = 100;
    	if (happinessIn < -100) happinessIn = -100;
        this.dataManager.set(HAPPINESS, Integer.valueOf(happinessIn));
    }


    @Override
	@Nullable
    public UUID getOwnerId()
    {
        return (UUID)((Optional<?>)this.dataManager.get(OWNER_UNIQUE_ID)).orNull();
    }

    @Override
	public void setOwnerId(@Nullable UUID uniqueId)
    {
        this.dataManager.set(OWNER_UNIQUE_ID, Optional.fromNullable(uniqueId));
    }

    @Override
	public int getTemper()
    {
        return this.temper;
    }

    @Override
	public void setTemper(int temperIn)
    {
        this.temper = temperIn;
    }

    @Override
	public int increaseTemper(int p_110198_1_)
    {
        int i = MathHelper.clamp(this.getTemper() + p_110198_1_, 0, this.getMaxTemper());
        this.setTemper(i);
        return i;
    }

    @Override
	public int getMaxTemper()
    {
        return 80 - ( this.getTamability() * 5 );        	
    }

    public boolean isTame()
    {
        return (this.getOwnerId() != null);
    }

    @Override
	public boolean isTamed()
    {
        return (this.getOwnerId() != null);
    }


    @Override
	public void setTamed(boolean tamed)
    {
        //new
        //this.setupTamedAI();
    }


    @Override
	public boolean setTamedBy(EntityPlayer player)
    {
        this.setOwnerId(player.getUniqueID());
        this.setTamed(true);
        return true;
    }

    protected void setupTamedAI()
    {
        if (this.avoidEntity == null)
        {
            this.avoidEntity = new EntityAIAvoidEntity<EntityPlayer>(this, EntityPlayer.class, 16.0F, 1.0D, 2.0D);
        }
        if (this.aiPanic == null)
        {
            this.aiPanic = new EntityAIPanic(this, 1.25D);
        }
        if (this.aiAttack == null)
        {
            this.aiAttack = new EntityAIAttackMelee(this, 1.75D, false);
        }
        if (this.aiHerd == null)
        {
            this.aiHerd =  new EntityAIHerdMovement(this, 1.25D);
        }

        this.tasks.removeTask(this.avoidEntity);
        this.tasks.removeTask(this.aiPanic);
        this.tasks.removeTask(this.aiAttack);
        this.tasks.removeTask(this.aiHerd);
        
        if (this.isChild())
    	{
        	this.tasks.addTask(1, this.aiPanic);
        	return;
    	}
        
        this.tasks.addTask(2, this.aiHerd);
        
        if (!this.isTame())
        {
        	if (this.getHappiness() <= -50)
        	{
        		this.tasks.addTask(6, this.avoidEntity);        		
        	}
            
        	if (this.getSex() == 0)
            {
            	this.tasks.addTask(1, this.aiPanic);
            }
        	else
        	{
            	this.tasks.addTask(1, this.aiAttack);
        	}
        }
    }


    @Override
	public int getOldAge()
    {
        return this.dataManager.get(OLD_AGE).intValue();
    }
    
    @Override
	public void setOldAge(int age)
    {
        this.dataManager.set(OLD_AGE, Integer.valueOf(age));
    }    

	@Override
	public int getChildAge() 
	{
		return EntityJungleFowl.childAge;
	}
	
    @Override
	public float getGrowthSize()
    {
    	if (this.isChild())
    	{
    		float growth = EntityJungleFowl.childAge + this.getOldAge();
	    	float ratio = growth / EntityJungleFowl.childAge;
	    	return 1.0F - ratio;
    	}
    	return 1.0F;
    }
    
    @Override
	public int getPreferedGroupSize()
    {
    	return 4;
    }
    
    @Override
	public float getFoodMax()
    {
        return foodMax;
    }



    public boolean isEdible(ItemStack stack)
    {
    	if (EntityJungleFowl.TEMPTATION_ITEMS.contains(stack.getItem())) return true;
    	return false;
    }
 
    @Override
	public boolean isEdible(EntityItem entityitem)
	{
    	if (EntityJungleFowl.TEMPTATION_ITEMS.contains(entityitem.getItem().getItem())) return true;
    	return false;
	}


    @Override
	public int getPregnancyTime()
    {
        return this.pregnancyTime;
    }
    
    @Override
	public void setPregnancyTime(int age)
    {
        this.pregnancyTime = age;
    }    

    public int getLitterSpawned()
    {
        return this.litterSpawned;
    }
    
    public void setLitterSpawned(int age)
    {
        this.litterSpawned = age;
    }    

    @Nullable
    public UUID getOtherParent()
    {
        return (UUID)((Optional)this.dataManager.get(OWNER_UNIQUE_ID)).orNull();
    }

    public void setOtherParent(@Nullable UUID uniqueId)
    {
        this.dataManager.set(OWNER_UNIQUE_ID, Optional.fromNullable(uniqueId));
    }

    @Override
	public boolean attackEntityAsMob(Entity entityIn)
    {
        float f = (float)this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getAttributeValue();
        int i = 0;

        if (entityIn instanceof EntityLivingBase)
        {
            f += EnchantmentHelper.getModifierForCreature(this.getHeldItemMainhand(), ((EntityLivingBase)entityIn).getCreatureAttribute());
            i += EnchantmentHelper.getKnockbackModifier(this);
        }

        boolean flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), f);

        if (flag)
        {
            if (i > 0 && entityIn instanceof EntityLivingBase)
            {
                ((EntityLivingBase)entityIn).knockBack(this, i * 0.5F, MathHelper.sin(this.rotationYaw * 0.017453292F), (-MathHelper.cos(this.rotationYaw * 0.017453292F)));
                this.motionX *= 0.6D;
                this.motionZ *= 0.6D;
            }

            int j = EnchantmentHelper.getFireAspectModifier(this);

            if (j > 0)
            {
                entityIn.setFire(j * 4);
            }

            if (entityIn instanceof EntityPlayer)
            {
                EntityPlayer entityplayer = (EntityPlayer)entityIn;
                ItemStack itemstack = this.getHeldItemMainhand();
                ItemStack itemstack1 = entityplayer.isHandActive() ? entityplayer.getActiveItemStack() : null;

                if (itemstack != null && itemstack1 != null && itemstack.getItem() instanceof ItemAxe && itemstack1.getItem() == Items.SHIELD)
                {
                    float f1 = 0.25F + EnchantmentHelper.getEfficiencyModifier(this) * 0.05F;

                    if (this.rand.nextFloat() < f1)
                    {
                        entityplayer.getCooldownTracker().setCooldown(Items.SHIELD, 100);
                        this.world.setEntityState(entityplayer, (byte)30);
                    }
                }
            }

            this.applyEnchantments(this, entityIn);
        }

        return flag;
    }

    
    @Override
	public void getOtherParentData(IEntityAdvanced parent)
    {
    	if (parent instanceof EntityJungleFowl)
    	{
    		EntityJungleFowl otherParent = (EntityJungleFowl) parent;
            this.parentTamability = otherParent.getTamability();
            this.parentMaxHealth = otherParent.getMaxHealth();
    	}
    	else if (parent instanceof EntityJungleFowl)
    	{
    		EntityJungleFowl otherParent = (EntityJungleFowl) parent;
            this.parentTamability = otherParent.getTamability();
            this.parentMaxHealth = otherParent.getMaxHealth();
    	}
    	else //failsafe
    	{
            this.parentTamability = this.getTamability();
            this.parentMaxHealth = this.getMaxHealth();
    	}
        return;
    }

    @Override
    public PathNavigate getNavigator()
    {
    	if ( (this.rand.nextFloat() > 1.0F - this.getEntityAttribute(SharedMonsterAttributes.FLYING_SPEED).getAttributeValue() || this.hurtResistantTime > 0) && !this.isChild())
    	{
    		return this.flyNavigator;
    	}
    	else
    	{
    		return this.navigator;    		
    	}
    		
        
    }


    /**
     * Returns new PathNavigateGround instance
     */
    protected PathNavigate createFlyNavigator(World worldIn)
    {
        return new PathNavigateFlying(this, worldIn);
    }

	public class EntityFowlFlyHelper extends EntityMoveHelper
	{

	    public boolean isFlying = false;
	    public float flyTime = 0.0F;
	    
	    public EntityFowlFlyHelper(EntityLiving p_i47418_1_)
	    {
	        super(p_i47418_1_);
	    }

	    public void onUpdateMoveHelper()
	    {
	        if (this.action == EntityMoveHelper.Action.STRAFE)
	        {
	            float f = (float)this.entity.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getAttributeValue();
	            float f1 = (float)this.speed * f;
	            float f2 = this.moveForward;
	            float f3 = this.moveStrafe;
	            float f4 = MathHelper.sqrt(f2 * f2 + f3 * f3);

	            if (f4 < 1.0F)
	            {
	                f4 = 1.0F;
	            }

	            f4 = f1 / f4;
	            f2 = f2 * f4;
	            f3 = f3 * f4;
	            float f5 = MathHelper.sin(this.entity.rotationYaw * 0.017453292F);
	            float f6 = MathHelper.cos(this.entity.rotationYaw * 0.017453292F);
	            float f7 = f2 * f6 - f3 * f5;
	            float f8 = f3 * f6 + f2 * f5;
	            PathNavigate pathnavigate = this.entity.getNavigator();

	            if (pathnavigate != null)
	            {
	                NodeProcessor nodeprocessor = pathnavigate.getNodeProcessor();

	                if (nodeprocessor != null && nodeprocessor.getPathNodeType(this.entity.world, MathHelper.floor(this.entity.posX + (double)f7), MathHelper.floor(this.entity.posY), MathHelper.floor(this.entity.posZ + (double)f8)) != PathNodeType.WALKABLE)
	                {
	                    this.moveForward = 1.0F;
	                    this.moveStrafe = 0.0F;
	                    f1 = f;
	                }
	            }

	            this.entity.setAIMoveSpeed(f1);
	            this.entity.setMoveForward(this.moveForward);
	            this.entity.setMoveStrafing(this.moveStrafe);
	            this.action = EntityMoveHelper.Action.WAIT;
	        }
	        else if (this.action == EntityMoveHelper.Action.MOVE_TO)
	        {
	            this.action = EntityMoveHelper.Action.WAIT;
	            double d0 = this.posX - this.entity.posX;
	            double d1 = this.posZ - this.entity.posZ;
	            double d2 = this.posY - this.entity.posY;
	            double d3 = d0 * d0 + d2 * d2 + d1 * d1;

	            if (d3 < 2.500000277905201E-7D)
	            {
		            this.entity.setNoGravity(false);
		            this.entity.setMoveVertical(0.0F);
		            this.entity.setMoveForward(0.0F);
    	            this.isFlying = false;
	                return;
	            }

	            float f = (float)(MathHelper.atan2(d1, d0) * (180D / Math.PI)) - 90.0F;
	            
	            if ( (d2 > 0.5D || this.isFlying) && !this.entity.isChild() && !(this.flyTime > this.entity.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getAttributeValue()) )
	            {
    	            this.action = EntityMoveHelper.Action.WAIT;
    	            this.isFlying = true;
    	            this.entity.setNoGravity(true);
    	            this.flyTime += 0.001F;

    	            float f1;

    	            if (this.entity.onGround)
    	            {
    	                f1 = (float)(this.speed * this.entity.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getAttributeValue());
    	                this.entity.rotationYaw = this.limitAngle(this.entity.rotationYaw, f, 90.0F);
    		            this.entity.setAIMoveSpeed(f1);
    	            }
    	            else
    	            {
    	                f1 = (float)(this.speed * this.entity.getEntityAttribute(SharedMonsterAttributes.FLYING_SPEED).getAttributeValue());
    	            	this.entity.rotationYaw = this.limitAngle(this.entity.rotationYaw, f, 15.0F);
    		            this.entity.setAIMoveSpeed(f1 * 2.95F);
    	            }
		            this.entity.setMoveVertical(d2 > 0.0D ? f1*2.0F : -1.0F - f1);
		            
    	            return;
	    	        
	            }
	            
	            this.entity.rotationYaw = this.limitAngle(this.entity.rotationYaw, f, 90.0F);
	            this.entity.setAIMoveSpeed((float)(this.speed * this.entity.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getAttributeValue()));

	            if (d2 > (double)this.entity.stepHeight && d0 * d0 + d1 * d1 < (double)Math.max(1.0F, this.entity.width))
	            {
	                this.entity.getJumpHelper().setJumping();
	                this.action = EntityMoveHelper.Action.JUMPING;
	            }
	        }
	        else if (this.action == EntityMoveHelper.Action.JUMPING)
	        {
	            this.entity.setAIMoveSpeed((float)(this.speed * this.entity.getEntityAttribute(SharedMonsterAttributes.FLYING_SPEED).getAttributeValue()));

	            if (this.entity.onGround)
	            {
	                this.action = EntityMoveHelper.Action.WAIT;
	            }
	        }
	        else
	        {
	            this.entity.setNoGravity(false);
	            this.entity.setMoveVertical(0.0F);
	            this.entity.setMoveForward(0.0F);
	            if (this.entity.onGround)
	            {
	            	if (this.flyTime > 0) this.flyTime -= this.entity.getEntityAttribute(SharedMonsterAttributes.FLYING_SPEED).getAttributeValue() / 100;
    	            this.isFlying = false;
	            }
	        }
	    }
	}
    
}