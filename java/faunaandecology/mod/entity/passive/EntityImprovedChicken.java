package faunaandecology.mod.entity.passive;

import java.util.Set;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.collect.Sets;

import faunaandecology.mod.entity.IEntityAdvanced;
import faunaandecology.mod.entity.ai.EntityAIHerdMovement;
import faunaandecology.mod.init.ItemInit;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIPanic;
import net.minecraft.entity.ai.EntityAITempt;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.world.World;

public class EntityImprovedChicken extends EntityJungleFowl implements IEntityAdvanced
{
    //private static final DataParameter<Boolean> SADDLED = EntityDataManager.<Boolean>createKey(EntityImprovedPig.class, DataSerializers.BOOLEAN);
    public float wingRotation;
    public float destPos;
    public float oFlapSpeed;
    public float oFlap;
    public float wingRotDelta = 1.0F;
        /** The time until the next egg is spawned. */
    //public int timeUntilNextEgg;
    public boolean chickenJockey;
    
    ///

	private static final Set<Item> TEMPTATION_ITEMS = Sets.newHashSet(Items.WHEAT_SEEDS, Items.MELON_SEEDS, Items.PUMPKIN_SEEDS, Items.BEETROOT_SEEDS, Items.BREAD,
			Items.APPLE, Items.CARROT, Items.WHEAT, Items.BEETROOT, Items.GOLDEN_APPLE, Items.GOLDEN_CARROT, Items.MELON, Items.SPECKLED_MELON);
	
    private static final DataParameter<Integer> OLD_AGE = EntityDataManager.<Integer>createKey(EntityImprovedChicken.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> SEX = EntityDataManager.<Integer>createKey(EntityImprovedChicken.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> TAMABILITY = EntityDataManager.<Integer>createKey(EntityImprovedChicken.class, DataSerializers.VARINT);
    private static final DataParameter<Float> HUNGER = EntityDataManager.<Float>createKey(EntityImprovedChicken.class, DataSerializers.FLOAT);
    private static final DataParameter<Integer> HAPPINESS = EntityDataManager.<Integer>createKey(EntityImprovedChicken.class, DataSerializers.VARINT);

    private static final DataParameter<Optional<UUID>> OWNER_UNIQUE_ID = EntityDataManager.<Optional<UUID>>createKey(EntityImprovedChicken.class, DataSerializers.OPTIONAL_UNIQUE_ID);
    ///
    
    /** "The higher this value, the more likely the animal is to be tamed." */
    protected int temper;

    //new ai

    private EntityAIAvoidEntity<EntityPlayer> avoidEntity;
    private EntityAITempt aiTempt;
    private EntityAIPanic aiPanic;
    private EntityAIAttackMelee aiAttack;
    private EntityAIHerdMovement aiHerd;
    ///
    //private boolean sheared;
    private int groupTimer = 0;
    static public int litterSize = 4;
    static public int pregnancyLength = 15200;
	static public float foodMax = 50.0F;
    static public int ageMax = 480000; //20 'years' //2000 = 1 month
    static public int childAge = -36000; //18 'monthes' 
    
    private int parentTamability;
    private float parentMaxHealth;

    private int pregnancyTime;
    private int litterSpawned;
    
    public EntityImprovedChicken(World worldIn)
    {
        super(worldIn);
        this.setSize(0.8F, 0.9F);
        this.timeUntilRealEgg = this.rand.nextInt(20000) + 20000;
        this.setPathPriority(PathNodeType.WATER, 0.0F);
    }

    @Override
	protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10.0D);
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.25D);
        this.getEntityAttribute(SharedMonsterAttributes.FLYING_SPEED).setBaseValue(0.3D);
        //this.getAttributeMap().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE);
        this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(1.0D);
    }

    @Override
    public String getZoopediaName()
    {
    	return "Chicken";
    }

    /**
     * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
     * use this to react to sunlight and start to burn.
     */
    @Override
	public void onLivingUpdate()
    {
        if (!this.isChild() && !this.isChickenJockey() && this.timeUntilRealEgg <= 0)
        {
        	if (this.getSex() == 0) this.playSound(SoundEvents.ENTITY_CHICKEN_EGG, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
            if (this.getSex() == 0) this.dropItem(ItemInit.EGG, 1);
            this.timeUntilRealEgg = this.rand.nextInt(20000) + 20000;
        }        

     	super.onLivingUpdate();
    }


    @Override
    public boolean isEdible(ItemStack stack)
    {
    	if (EntityImprovedChicken.TEMPTATION_ITEMS.contains(stack.getItem())) return true;
    	return false;
    }
 
    @Override
	public boolean isEdible(EntityItem entityitem)
	{
    	if (EntityImprovedChicken.TEMPTATION_ITEMS.contains(entityitem.getItem().getItem())) return true;
    	return false;
	}

    /**
     * Checks if the parameter is an item which this animal can be fed to breed it (wheat, carrots or seeds depending on
     * the animal type)
     */
    @Override
    public boolean isBreedingItem(@Nullable ItemStack stack)
    {
        return stack != null && TEMPTATION_ITEMS.contains(stack.getItem());
    }

    /////////////////////

    
}