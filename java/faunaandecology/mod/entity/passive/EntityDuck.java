package faunaandecology.mod.entity.passive;

import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.collect.Sets;

import faunaandecology.mod.init.ItemInit;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.world.World;

public class EntityDuck extends EntityMallard 
{
    public float flightPower = 0.75F;

    private static final Set<Item> TEMPTATION_ITEMS = Sets.newHashSet(Items.WHEAT_SEEDS, Items.MELON_SEEDS, Items.PUMPKIN_SEEDS, Items.BEETROOT_SEEDS, Items.BREAD);
	public EntityDuck(World worldIn) {
		super(worldIn);
		// 
        this.setSize(0.8F, 0.9F);
        this.timeUntilRealEgg = this.rand.nextInt(30000) + 30000;
        this.setPathPriority(PathNodeType.WATER, 0.0F);
	}
	

    @Override
	protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(3.0D);
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.20000000298023224D);
        this.getEntityAttribute(SharedMonsterAttributes.FLYING_SPEED).setBaseValue(0.4D);
    }

    @Override
    public String getZoopediaName()
    {
    	return "Duck";
    }

    /**
     * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
     * use this to react to sunlight and start to burn.
     */
    @Override
	public void onLivingUpdate()
    {
        if (!this.isChild() && !this.isChickenJockey() && --this.timeUntilRealEgg <= 0)
        {
        	if (this.getSex() == 0) this.playSound(SoundEvents.ENTITY_CHICKEN_EGG, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
        	if (this.getSex() == 0) this.dropItem(ItemInit.EGG_DUCK, 1);
            this.timeUntilRealEgg = this.rand.nextInt(30000) + 30000;
        }        

     	super.onLivingUpdate();
    }
    

    @Override
	public boolean isEdible(ItemStack stack)
    {
    	if (EntityDuck.TEMPTATION_ITEMS.contains(stack.getItem())) return true;
    	return false;
    }
 
    @Override
	public boolean isEdible(EntityItem entityitem)
	{
    	if (EntityDuck.TEMPTATION_ITEMS.contains(entityitem.getItem().getItem())) return true;
    	return false;
	}

    /**
     * Checks if the parameter is an item which this animal can be fed to breed it (wheat, carrots or seeds depending on
     * the animal type)
     */
    @Override
    public boolean isBreedingItem(@Nullable ItemStack stack)
    {
        return stack != null && TEMPTATION_ITEMS.contains(stack.getItem());
    }
}
