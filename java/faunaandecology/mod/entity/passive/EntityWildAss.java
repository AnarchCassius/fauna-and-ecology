package faunaandecology.mod.entity.passive;

import javax.annotation.Nullable;

import faunaandecology.mod.entity.IEntityAdvancedHorse;
import faunaandecology.mod.util.Config;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.IJumpingMount;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.ai.attributes.RangedAttribute;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.IInventoryChangedListener;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityWildAss extends EntityImprovedHorse implements IInventoryChangedListener, IJumpingMount, IEntityAdvancedHorse
{
	private static final IAttribute JUMP_STRENGTH = (new RangedAttribute((IAttribute)null, "horse.jumpStrength", 0.7D, 0.0D, 2.0D)).setDescription("Jump Strength").setShouldWatch(true);

    //
    static public int pregnancyLength = 26000;
    static public float foodMax = 80.0F;
    static public int ageMax = 720000; //30 'years'
    static public int childAge = -96000; //2000 = 1 month
    //
    
    private String texturePrefix;
    private final String[] horseTexturesArray = new String[3];
    

    public EntityWildAss(World worldIn)
    {
        super(worldIn);
        this.setSize(1.3964844F, 1.6F);
        this.isImmuneToFire = false;
        this.stepHeight = 1.0F;
        this.initHorseChest();

        //new
        this.setupTamedAI();
    }
    
    @Override
    public String getZoopediaName()
    {
    	return "Wild Ass";
    }

    @Override
	@Nullable
    protected ResourceLocation getLootTable()
    {
        return LootTableList.ENTITIES_DONKEY;
    }

    @Override
	protected SoundEvent getAmbientSound()
    {
        super.getAmbientSound();
        return SoundEvents.ENTITY_DONKEY_AMBIENT;
    }

    @Override
	protected SoundEvent getDeathSound()
    {
        super.getDeathSound();
        return SoundEvents.ENTITY_DONKEY_DEATH;
    }

    @Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
    {
        super.getHurtSound(damageSourceIn);
        return SoundEvents.ENTITY_DONKEY_HURT;
    }

    protected SoundEvent getAngrySound()
    {
        super.getAngrySound();
        return SoundEvents.ENTITY_DONKEY_ANGRY;
    }

    //@SideOnly(Side.CLIENT) 
    private void setHorseTexturePaths()
    {
        String texture = null;
        if (this.isChild())
        	this.horseTexturesArray[0] = "faunaandecology:textures/entity/horse/wild_ass.png";
        else
        	this.horseTexturesArray[0] = "faunaandecology:textures/entity/horse/wild_ass.png";
        this.horseTexturesArray[1] = null;
        this.horseTexturesArray[2] = texture;
        this.texturePrefix = "horse/" + "was" + "was" + texture;
    }
    
    @Override
	@SideOnly(Side.CLIENT)
    public String getHorseTexture()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.texturePrefix;
    }

    @Override
	@SideOnly(Side.CLIENT)
    public String[] getVariantTexturePaths()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.horseTexturesArray;
    }

    @Override
	public int getMaxTemper()
    {
    	return 200 - ( this.getTamability() * 5 );
    }

    @Override
	public float getModifiedMaxHealth()
    {
    	return 14.0F + this.rand.nextInt(7) + this.rand.nextInt(7);
    }

    @Override
	public double getModifiedMovementSpeed()
    {
    	return (0.44999998807907104D + this.rand.nextDouble() * 0.3D + this.rand.nextDouble() * 0.3D + this.rand.nextDouble() * 0.3D) * 0.3D;
    }

    @Override
	public boolean wearsArmor()
    {
        return false;
    }

    @Override public boolean isShearable(ItemStack item, net.minecraft.world.IBlockAccess world, BlockPos pos){ return false; }
    
    /**
     * Called only once on an entity when first time spawned, via egg, mob spawner, natural spawning etc, but not called
     * when entity is reloaded from nbt. Mainly used for initializing attributes and inventory
     */
    @Override
	@Nullable
    public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty, @Nullable IEntityLivingData livingdata)
    {
        //livingdata = super.onInitialSpawn(difficulty, livingdata);
        int i = 1;

        this.setHorseVariant(i | 0 << 8);
            
        int randroll = this.rand.nextInt(2);
		if (Config.enableSexes == true)
		{
	        if (randroll == 1)
			{
	        	this.setSex(Integer.valueOf(1));	
			}
	        else
	        {
	            this.setSex(Integer.valueOf(0));
	        }
	        randroll = this.rand.nextInt(2);
        }
		
		randroll = this.rand.nextInt(2);
        if (randroll == 1)
		{
        	this.setTamability(Integer.valueOf(-4));
		}
        else
        {
    		this.setTamability(Integer.valueOf(-5));
        }

        this.setHunger(foodMax);
		this.setHappiness(0);
		

        if (this.rand.nextInt(5) == 0)
        {
            this.setGrowingAge(childAge);
            this.setOldAge(0);
        }
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.getModifiedMaxHealth());
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(this.getModifiedMovementSpeed());
        this.getEntityAttribute(JUMP_STRENGTH).setBaseValue(this.getModifiedJumpStrength());
        
        this.setHealth(this.getMaxHealth());
        this.setupTamedAI();
        this.setHorseTexturePaths();
        return livingdata;
    }
    
    //////////////////////
    //cactus immunity
   @Override
   public boolean attackEntityFrom(DamageSource par1DamageSource, float par2)
   {
   	this.setHappiness(this.getHappiness()-50);
   	if ( par1DamageSource == DamageSource.CACTUS ) return false; 
       return super.attackEntityFrom(par1DamageSource, par2);
   }

    
}

