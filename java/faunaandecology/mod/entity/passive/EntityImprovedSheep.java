package faunaandecology.mod.entity.passive;

import com.google.common.base.Optional;
import com.google.common.collect.Sets;

import faunaandecology.mod.entity.IEntityAdvanced;
import faunaandecology.mod.entity.IEntityAdvancedMilkable;
import faunaandecology.mod.entity.ai.EntityAIGrainEater;
import faunaandecology.mod.entity.ai.EntityAIHerdMovement;
import faunaandecology.mod.entity.ai.EntityAIImprovedFollowParent;
import faunaandecology.mod.entity.ai.EntityAIImprovedMate;
import faunaandecology.mod.entity.ai.EntityAIItemSearch;
import faunaandecology.mod.init.ItemInit;
import faunaandecology.mod.util.Config;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nullable;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIEatGrass;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIPanic;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAITempt;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.passive.AbstractHorse;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemMonsterPlacer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.server.management.PreYggdrasilConverter;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;

public class EntityImprovedSheep extends EntitySheep implements IEntityAdvancedMilkable
{
    //private static final DataParameter<Boolean> SADDLED = EntityDataManager.<Boolean>createKey(EntityImprovedPig.class, DataSerializers.BOOLEAN);
    
	///
    private static final DataParameter<Byte> NATURAL_COLOR = EntityDataManager.<Byte>createKey(EntityImprovedSheep.class, DataSerializers.BYTE);
    
    private static final DataParameter<Integer> OLD_AGE = EntityDataManager.<Integer>createKey(EntityImprovedSheep.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> SEX = EntityDataManager.<Integer>createKey(EntityImprovedSheep.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> TAMABILITY = EntityDataManager.<Integer>createKey(EntityImprovedSheep.class, DataSerializers.VARINT);
    private static final DataParameter<Float> HUNGER = EntityDataManager.<Float>createKey(EntityImprovedSheep.class, DataSerializers.FLOAT);
    private static final DataParameter<Integer> HAPPINESS = EntityDataManager.<Integer>createKey(EntityImprovedSheep.class, DataSerializers.VARINT);

    private static final DataParameter<Optional<UUID>> OWNER_UNIQUE_ID = EntityDataManager.<Optional<UUID>>createKey(EntityImprovedSheep.class, DataSerializers.OPTIONAL_UNIQUE_ID);
    ///
    private static final DataParameter<Integer> MAXMILK = EntityDataManager.<Integer>createKey(EntityImprovedSheep.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> CURMILK = EntityDataManager.<Integer>createKey(EntityImprovedSheep.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> MILK_TIMER = EntityDataManager.<Integer>createKey(EntityImprovedSheep.class, DataSerializers.VARINT);

    private static final Set<Item> TEMPTATION_ITEMS = Sets.newHashSet(new Item[] {Items.BREAD, Items.WHEAT, Item.getItemFromBlock(Blocks.YELLOW_FLOWER), Items.REEDS});
    private boolean boosting;
    private int boostTime;
    private int totalBoostTime;
    private int sheepTimer;
    
    /** "The higher this value, the more likely the animal is to be tamed." */
    protected int temper;

    //new ai
    //private static final DataParameter<Integer> PREGNANCY_TIME = EntityDataManager.<Integer>createKey(EntityImprovedSheep.class, DataSerializers.VARINT);

    private EntityAIAvoidEntity<EntityPlayer> avoidEntity;
    private EntityAITempt aiTempt;
    private EntityAIPanic aiPanic;
    private EntityAIAttackMelee aiAttack;
    private EntityAIHerdMovement aiHerd;
    private EntityAIEatGrass entityAIEatGrass;
    ///
    //private boolean sheared;
    private int groupTimer = 0;
    static public int litterSize = 2;
    static public int pregnancyLength = 10133;
	static public float foodMax = 50.0F;
    static public int ageMax = 288000; //12 'years' //2000 = 1 month
    static public int childAge = -48000; //24 'monthes' 

    private int parentTamability;
    private float parentMaxHealth;
    private int parentMaxMilk;


    private int pregnancyTime;
    private int litterSpawned;

	private int inLove;
    
    public EntityImprovedSheep(World worldIn)
    {
        super(worldIn);
        this.setSize(0.9F, 0.9F);
    }


    @Override
    public String getZoopediaName()
    {
    	return "Sheep";
    }


    @Override
    public float getZoopediaHealth()
    {
    	return this.getHealth();
    }
    
    @Override
    public float getZoopediaMaxHealth()
    {
    	return this.getMaxHealth();
    }
    
    @Override
	protected void initEntityAI()
    {
    	//super.initEntityAI();
        this.entityAIEatGrass = new EntityAIEatGrass(this);
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.aiPanic = new EntityAIPanic(this, 1.25D);
        this.aiHerd = new EntityAIHerdMovement(this, 1.25D);
        this.tasks.addTask(1, this.aiPanic);
        this.tasks.addTask(2, this.aiHerd);

        this.tasks.addTask(3, new EntityAIImprovedMate(this, 1.0D, EntityImprovedSheep.class));
        this.tasks.addTask(4, new EntityAITempt(this, 1.2D, Items.CARROT_ON_A_STICK, false));
        this.tasks.addTask(4, new EntityAITempt(this, 1.2D, false, TEMPTATION_ITEMS));
        this.tasks.addTask(6, new EntityAIImprovedFollowParent(this, 1.1D, EntityImprovedSheep.class));
        this.tasks.addTask(7, this.entityAIEatGrass);
        this.tasks.addTask(7, new EntityAIItemSearch(this, 1.0D));
        this.tasks.addTask(7, new EntityAIGrainEater(this));

        this.tasks.addTask(8, new EntityAIWander(this, 1.0D));
        this.tasks.addTask(9, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
        this.tasks.addTask(10, new EntityAILookIdle(this));
        this.targetTasks.addTask(2, new EntityAIHurtByTarget(this, true, new Class[0]));
    }

    @Override
    protected void updateAITasks()
    {
        this.sheepTimer = this.entityAIEatGrass.getEatingGrassTimer();

        if (this.getGrowingAge() != 0)
        {
            this.inLove = 0;
        }
    }

    @Override
	protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10.0D);
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.25D);
        this.getAttributeMap().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE);
        this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(2.0D);
    }

    /**
     * For vehicles, the first passenger is generally considered the controller and "drives" the vehicle. For example,
     * Pigs, Horses, and Boats are generally "steered" by the controlling passenger.
     */
    @Override
	@Nullable
    public Entity getControllingPassenger()
    {
        return this.getPassengers().isEmpty() ? null : (Entity)this.getPassengers().get(0);
    }

    /**
     * returns true if all the conditions for steering the entity are met. For pigs, this is true if it is being ridden
     * by a player and the player is holding a carrot-on-a-stick
     */
    @Override
	public boolean canBeSteered()
    {
        Entity entity = this.getControllingPassenger();

        if (!(entity instanceof EntityPlayer))
        {
            return false;
        }
        else
        {
            EntityPlayer entityplayer = (EntityPlayer)entity;
            ItemStack itemstack = entityplayer.getHeldItemMainhand();

            if (itemstack != null && itemstack.getItem() == Items.CARROT_ON_A_STICK)
            {
                return true;
            }
            else
            {
                itemstack = entityplayer.getHeldItemOffhand();
                return itemstack != null && itemstack.getItem() == Items.CARROT_ON_A_STICK;
            }
        }
    }

    @Override
	protected void entityInit()
    {
        super.entityInit();
        this.dataManager.register(NATURAL_COLOR, Byte.valueOf((byte)0));
        this.dataManager.register(OLD_AGE, Integer.valueOf(0));
        this.dataManager.register(SEX, Integer.valueOf(0));
        this.dataManager.register(HUNGER, Float.valueOf(0));
        this.dataManager.register(HAPPINESS, Integer.valueOf(0));
        this.dataManager.register(TAMABILITY, Integer.valueOf(0));
        this.dataManager.register(MAXMILK, Integer.valueOf(0));
        this.dataManager.register(CURMILK, Integer.valueOf(0));
        this.dataManager.register(MILK_TIMER, Integer.valueOf(0));
        this.dataManager.register(OWNER_UNIQUE_ID, Optional.<UUID>absent());
        
        //this.dataManager.register(PREGNANCY_TIME, Integer.valueOf(0));
        
		this.setPregnancyTime(0);
		this.setLitterSpawned(0);
    }

//    public static void registerFixesPig(DataFixer fixer)
//    {
//    	EntityLiving.registerFixesMob(fixer, EntityWildBoar.class);
//    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    @Override
	public void writeEntityToNBT(NBTTagCompound compound)
    {
        super.writeEntityToNBT(compound);
        //compound.setBoolean("Saddle", this.getSaddled());
        compound.setInteger("OldAge", this.getOldAge());
        compound.setInteger("MilkCapacity", this.getCurMilk());
        compound.setInteger("MilkMaxCapacity", this.getMaxMilk());
        compound.setInteger("MilkTimer", this.getMilkTimer());
        compound.setInteger("PregnancyTime", this.getPregnancyTime());
        compound.setInteger("Sex", this.getSex());
        compound.setFloat("Hunger", this.getHunger());
        compound.setInteger("Tamability", this.getTamability());
        compound.setInteger("Happiness", this.getHappiness());
        compound.setInteger("Temper", this.getTemper());
        compound.setInteger("ParentTamability", this.parentTamability);
        compound.setFloat("ParentMaxHealth", this.parentMaxHealth);
        compound.setInteger("ParentMaxMilk", this.parentMaxMilk);

        compound.setInteger("LitterSpawned", this.getLitterSpawned());

        if (this.getOwnerId() != null)
        {
            compound.setString("OwnerUUID", this.getOwnerId().toString());
        }
        if (this.getOtherParent() != null)
        {
            compound.setString("ParentUUID", this.getOtherParent().toString());
        }

    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    @Override
	public void readEntityFromNBT(NBTTagCompound compound)
    {
        super.readEntityFromNBT(compound);
        //this.setSaddled(compound.getBoolean("Saddle"));
        this.setOldAge( compound.getInteger("OldAge"));               
        this.setCurMilk(compound.getInteger("MilkCapacity"));       
        this.setMaxMilk(compound.getInteger("MilkMaxCapacity"));          
        this.setMilkTimer(compound.getInteger("MilkTimer"));       
        this.setPregnancyTime( compound.getInteger("PregnancyTime"));            
        this.setSex(compound.getInteger("Sex"));
        this.setHunger(compound.getInteger("Hunger"));
        this.setTamability(compound.getInteger("Tamability"));
        this.setHappiness(compound.getInteger("Happiness"));
        this.setTemper(compound.getInteger("Temper"));
        String s;
        this.parentTamability = (compound.getInteger("ParentTamability"));
        this.parentMaxHealth =  (compound.getFloat("ParentMaxHealth"));
        this.parentMaxMilk =  (compound.getInteger("ParentMaxMilk"));
        
        this.litterSpawned = (compound.getInteger("LitterSpawned"));

        
        if (compound.hasKey("OwnerUUID", 8))
        {
            s = compound.getString("OwnerUUID");
        }
        else
        {
            String s1 = compound.getString("Owner");
            s = PreYggdrasilConverter.convertMobOwnerIfNeeded(this.getServer(), s1);
        }

        if (!s.isEmpty())
        {
            this.setOwnerId(UUID.fromString(s));
        }
        //new
        this.setupTamedAI();
        

        if (compound.hasKey("ParentUUID", 8))
        {
            s = compound.getString("ParentUUID");
        }

        if (!s.isEmpty())
        {
            this.setOtherParent(UUID.fromString(s));
        }
    }

    @Override
    public boolean processInteract(EntityPlayer player, EnumHand hand)
    {
        ItemStack stack = player.getHeldItem(hand);
        
    	//breeding item overrides
        if (stack != null)
        {
        	if (Config.enableAdvancedStats == true)
        	{
        		if (this.getHunger() < foodMax - 0.5F)
        		{
	        		if  (Config.enableWildBreeding == true)
	        		{
	                    if (this.isBreedingItem(stack)  && !(this.isChild()) && !(this.isInLove()) )
	                    {
	                        this.consumeItemFromStack(player, stack);
	                        return true;
	                    }
	                    else if (this.isBreedingItem(stack))
	                    {
	                        this.consumeItemFromStack(player, stack);
	                        return true;
	                    }
	        		}
	        		else
	        		{
	                    if (this.isBreedingItem(stack) && !(this.isChild()) && !(this.isInLove()) )
	                    {
	                        this.consumeItemFromStack(player, stack);
	                        this.setInLove(player);
	                        return true;
	                    }
	                    else if (this.isBreedingItem(stack))
	                    {
	                        this.consumeItemFromStack(player, stack);
	                        return true;
	                    }
	        		}
        		}
                
                if (this.isEdible(stack))
                {
                	if  (this.getHunger() <  foodMax - 0.5F)
                	{
    	                this.consumeItemFromStack(player, stack);
    	                return true;
                	}
                	else return false;
                }
        	}

            if (stack.getItem() == Items.BUCKET && !this.isChild())
            {
            	if ( this.getSex() == 0 && this.isTame())
            	{
    	        	if (this.getCurMilk() > 2)
    	        	{
    		            player.playSound(SoundEvents.ENTITY_COW_MILK, 1.0F, 1.0F);
    		
    		            this.setCurMilk(this.getCurMilk()-3);
    		            if (stack.getCount() - 1 == 0)
    		            {
    		            	stack.setCount(stack.getCount()-1);
    		                player.setHeldItem(hand, new ItemStack(Items.MILK_BUCKET));
    		            }
    		            else if (!player.inventory.addItemStackToInventory(new ItemStack(Items.MILK_BUCKET)))
    		            {
    		            	stack.setCount(stack.getCount()-1);
    		                player.dropItem(new ItemStack(Items.MILK_BUCKET), false);
    		            }
    		            return true;
    	        	}
            	}
            	else	
            	{
            		player.playSound(SoundEvents.ENTITY_SHEEP_HURT, 1.0F, 1.0F);
            		this.setHappiness(this.getHappiness()-3);
            	}
            	return false;
            }
        	
        }
        
        //override of EntityAgeable to allow varied growth times
        if (stack != null && stack.getItem() == Items.SPAWN_EGG)
        {
            ItemStack itemstack = player.getHeldItem(hand);
            if (!this.world.isRemote)
            {
                Class <? extends Entity > oclass = EntityList.getClass(ItemMonsterPlacer.getNamedIdFrom(itemstack));

                if (oclass != null && this.getClass() == oclass)
                {
                    EntityAgeable entityageable = this.createChild();

                    if (entityageable != null)
                    {
                        entityageable.setGrowingAge(childAge);
                        entityageable.setLocationAndAngles(this.posX, this.posY, this.posZ, 0.0F, 0.0F);
                        this.world.spawnEntity(entityageable);

                        if (stack.hasDisplayName())
                        {
                            entityageable.setCustomNameTag(stack.getDisplayName());
                        }

                        if (!player.capabilities.isCreativeMode)
                        {
                            stack.setCount(stack.getCount() -1);
                        }
                    }
                }
            }
            return true;
        }
        
        if (!super.processInteract(player, hand))
        {
            return false;
        }
        else
        {
            return true;
        }
    }


    @Override
    public java.util.List<ItemStack> onSheared(ItemStack item, net.minecraft.world.IBlockAccess world, BlockPos pos, int fortune)
    {
        this.setSheared(true);
        int i = 1 + this.rand.nextInt(3);

        java.util.List<ItemStack> ret = new java.util.ArrayList<ItemStack>();
        for (int j = 0; j < i; ++j)

        	if (Config.woolSystem)
			{
				ret.add(new ItemStack(ItemInit.WOOL_TUFT, 1 + this.rand.nextInt(3), 15 - this.getFleeceColor().getMetadata()));
			}
			else
            ret.add(new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, this.getFleeceColor().getMetadata()));

        this.playSound(SoundEvents.ENTITY_SHEEP_SHEAR, 1.0F, 1.0F);
        return ret;
    }
	/**
     * Drop the equipment for this entity.
     */
    @Override
	protected void dropEquipment(boolean wasRecentlyHit, int lootingModifier)
    {
        super.dropEquipment(wasRecentlyHit, lootingModifier);

    }

    @Override
	public void fall(float distance, float damageMultiplier)
    {
        super.fall(distance, damageMultiplier);

        if (distance > 5.0F)
        {
            for (EntityPlayer entityplayer : this.getRecursivePassengersByType(EntityPlayer.class))
            {
                //entityplayer.addStat(AchievementList.FLY_PIG);
            }
        }
    }

    /**
     * Moves the entity based on the specified heading.
     */
    public void moveEntityWithHeading(float strafe, float forward)
    {
        Entity entity = this.getPassengers().isEmpty() ? null : (Entity)this.getPassengers().get(0);

        if (this.isBeingRidden() && this.canBeSteered())
        {
            this.rotationYaw = entity.rotationYaw;
            this.prevRotationYaw = this.rotationYaw;
            this.rotationPitch = entity.rotationPitch * 0.5F;
            this.setRotation(this.rotationYaw, this.rotationPitch);
            this.renderYawOffset = this.rotationYaw;
            this.rotationYawHead = this.rotationYaw;
            this.stepHeight = 1.0F;
            this.jumpMovementFactor = this.getAIMoveSpeed() * 0.1F;

            if (this.canPassengerSteer())
            {
                float f = (float)this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getAttributeValue() * 0.225F;

                if (this.boosting)
                {
                    if (this.boostTime++ > this.totalBoostTime)
                    {
                        this.boosting = false;
                    }

                    f += f * 1.15F * MathHelper.sin((float)this.boostTime / (float)this.totalBoostTime * (float)Math.PI);
                }

                this.setAIMoveSpeed(f);
                super.travel(0.0F, 0.0F, 1.0F);
            }
            else
            {
                this.motionX = 0.0D;
                this.motionY = 0.0D;
                this.motionZ = 0.0D;
            }

            this.prevLimbSwingAmount = this.limbSwingAmount;
            double d1 = this.posX - this.prevPosX;
            double d0 = this.posZ - this.prevPosZ;
            float f1 = MathHelper.sqrt(d1 * d1 + d0 * d0) * 4.0F;

            if (f1 > 1.0F)
            {
                f1 = 1.0F;
            }

            this.limbSwingAmount += (f1 - this.limbSwingAmount) * 0.4F;
            this.limbSwing += this.limbSwingAmount;
        }
        else
        {
            this.stepHeight = 0.5F;
            this.jumpMovementFactor = 0.02F;
            super.travel(strafe, 0.0F, forward);
        }
    }

    public boolean boost()
    {
        if (this.boosting)
        {
            return false;
        }
        else
        {
            this.boosting = true;
            this.boostTime = 0;
            this.totalBoostTime = this.getRNG().nextInt(841) + 140;
            return true;
        }
    }

    @Override
	public EntitySheep createChild(EntityAgeable ageable)
    {
    	IEntityAdvancedMilkable entitypig = (IEntityAdvancedMilkable)ageable;

        //Domestication process
        int newTamability = (entitypig.getTamability() + this.getTamability()) / 2;
        int randroll = this.rand.nextInt(2) + 1;
        if (this.isTame() || entitypig.isTame())
        {
        	newTamability += randroll;	
        }
        else
        {
        	newTamability -= randroll;
        }
        newTamability = MathHelper.clamp(newTamability, -5, 5);
        

        EntityImprovedSheep entitynewsheep;
        if ( newTamability >= 0 || ((ageable.getClass() == EntityImprovedSheep.class) && (this.getClass() == EntityImprovedSheep.class) && (Config.reversion == false) ) )//(ageable instanceof EntityAurochs)
        {
        	entitynewsheep = new EntityImprovedSheep(this.world);
        }
        else
        {
        	entitynewsheep = new EntityMouflon(this.world);
        }
        //check nearby players for taming
        List<EntityPlayer> players = this.world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(9.0D));
		for (Entity e : players)
		{
			if (e != null)
			{
				entitynewsheep.setOwnerId(e.getPersistentID());
				entitynewsheep.setTamed(true);
			}
		}
		entitynewsheep.setTamability(newTamability);
        
        if (Config.newHorseBreeding == true)
		{   
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue();
	        entitynewsheep.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(Math.round(d1 / 1.5D + (this.rand.nextDouble() * 0.5D)));
	        if (entitynewsheep.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 15.0D) entitynewsheep.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(15.0D);
	        if (entitynewsheep.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() < 10.0D) entitynewsheep.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10.0D);

	        if (entitynewsheep.getClass() == EntityImprovedSheep.class)
	        {
	            IEntityAdvancedMilkable entitymilksheep = entitynewsheep;

	            entitymilksheep.setMaxMilk((this.getMaxMilk() + entitypig.getMaxMilk()) /2);
		        if (this.rand.nextInt(3) == 0) entitymilksheep.setMaxMilk(entitymilksheep.getMaxMilk()+this.rand.nextInt(3) - 1);
		        entitymilksheep.setMaxMilk(MathHelper.clamp(entitymilksheep.getMaxMilk(), 3, 9));
		        entitymilksheep.setMilkTimer(0);
		        entitymilksheep.setCurMilk(0);	        	
	        }
	        
        }
		else
		{
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.getModifiedMaxHealth();
	        entitynewsheep.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(d1 / 3.0D);
		}
        
        EntitySheep entityreturnpig = entitynewsheep;
        


		if (Config.enableSexes == true)
		{
			entitynewsheep.setSex(Integer.valueOf(this.rand.nextInt(2)));			
		}
		
		entitynewsheep.setHunger(foodMax);
		entitynewsheep.setHappiness(0);
        
        return entityreturnpig;
    }
    
    public EntitySheep createChild()
    {
        //Domestication process
        int newTamability = (this.parentTamability + this.getTamability()) / 2;
        int randroll = this.rand.nextInt(2) + 1;
        if (this.isTame())
        {
        	newTamability += randroll;	
        }
        else
        {
        	newTamability -= randroll;
        }
        newTamability = MathHelper.clamp(newTamability, -5, 5);
        

        IEntityAdvancedMilkable entitynewpig;
        if ( newTamability >= 0  || ((this.getClass() == EntityImprovedSheep.class) && (Config.reversion == false) ))
        {
            entitynewpig = new EntityImprovedSheep(this.world);
        }
        else
        {
        	entitynewpig = new EntityMouflon(this.world);
        }
        
        //check nearby players for taming
        List<EntityPlayer> players = this.world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(9.0D));
		for (Entity e : players)
		{
			if (e != null)
			{
	    		entitynewpig.setOwnerId(e.getPersistentID());
	    		entitynewpig.setTamed(true);
			}
		}
        entitynewpig.setTamability(newTamability);
        
        EntitySheep entityreturnpig = (EntitySheep) entitynewpig;
        if (Config.newHorseBreeding == true)
		{   
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.parentMaxHealth;	        
	        entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(Math.round(Math.round(d1 / 1.5D + (this.rand.nextDouble() * 0.5D))));
	        if (entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 30.0D) entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(30.0D);
	        if (entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() < 20.0D) entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0D);


	        entitynewpig.setMaxMilk((this.getMaxMilk() + this.parentMaxMilk) /2);
	        if (this.rand.nextInt(3) == 0) entitynewpig.setMaxMilk(entitynewpig.getMaxMilk()+this.rand.nextInt(3) - 1);
	        entitynewpig.setMaxMilk(MathHelper.clamp(entitynewpig.getMaxMilk(), 3, 9));
	        entitynewpig.setMilkTimer(0);
	        entitynewpig.setCurMilk(0);
        }
		else
		{
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.parentMaxHealth + this.getModifiedMaxHealth();
	        entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(d1 / 3.0D);
		}
        
        
        if (Config.enableSexes == true)
		{
        	entitynewpig.setSex(Integer.valueOf(this.rand.nextInt(2)));			
		}
		
		entitynewpig.setHunger(foodMax);
		entitynewpig.setHappiness(0);
		//entitynewpig.oldAge = 0;
        
        return entityreturnpig;
    }

    /**
     * Checks if the parameter is an item which this animal can be fed to breed it (wheat, carrots or seeds depending on
     * the animal type)
     */
    @Override
	public boolean isBreedingItem(@Nullable ItemStack stack)
    {
        return stack != null && TEMPTATION_ITEMS.contains(stack.getItem());
    }


    ///////////////////////////////////
    //new
    /**
     * Called only once on an entity when first time spawned, via egg, mob spawner, natural spawning etc, but not called
     * when entity is reloaded from nbt. Mainly used for initializing attributes and inventory
     */
    
    /**
     * Sets the natural wool color of this sheep
     */
    public void setNaturalColor(EnumDyeColor color)
    {
        byte b0 = this.dataManager.get(NATURAL_COLOR).byteValue();
        this.dataManager.set(NATURAL_COLOR, Byte.valueOf((byte)(b0 & 240 | color.getMetadata() & 15)));
    }
    
    /**
     * Gets the natural wool color of this sheep.
     */
    public EnumDyeColor getNaturalColor()
    {
        return EnumDyeColor.byMetadata(this.dataManager.get(NATURAL_COLOR).byteValue() & 15);
    }

    @Override
	@Nullable
    public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty, @Nullable IEntityLivingData livingdata)
    {
    	super.onInitialSpawn(difficulty, livingdata);
    	
    	EnumDyeColor color = getRandomSheepColor(this.world.rand);
    	this.setFleeceColor(color);
    	this.setNaturalColor(color);
    	
        int randroll = this.rand.nextInt(2);
		if (Config.enableSexes == true)
		{
	        if (randroll == 1)
			{
	            this.dataManager.set(SEX, Integer.valueOf(1));	
			}
	        else
	        {
	            this.dataManager.set(SEX, Integer.valueOf(0));
	        }
	        randroll = this.rand.nextInt(2);
        }

		this.setHunger(foodMax);
		this.setHappiness(0);

		this.setMaxMilk(MathHelper.clamp(this.rand.nextInt(4) + this.rand.nextInt(4), 3, 9));
		this.setCurMilk(this.getMaxMilk());
		this.setMilkTimer(0);

		if (this.rand.nextInt(5) == 0)
        {
            this.setGrowingAge(childAge);
            this.setOldAge(0);
        }    	

        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.getModifiedMaxHealth());
        this.setHealth(this.getMaxHealth());

        this.setupTamedAI();
        return livingdata;
    }
    /////

    /**
     * Returns true if the mob is currently able to mate with the specified mob.
     */
    @Override
    public boolean canMateWith(EntityAnimal otherAnimal)
    {
        if (otherAnimal == this)
        {
            return false;
        }
        else if (otherAnimal instanceof EntityImprovedSheep)
        {
        	IEntityAdvanced entitypig = (IEntityAdvanced)otherAnimal;

            if ( this.isInLove() && otherAnimal.isInLove() && (this.getSex() != entitypig.getSex() || (Config.enableSexes == false) ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }
    

    /**
     * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
     * use this to react to sunlight and start to burn.
     */
    @Override
	public void onLivingUpdate()
    {
    	if (Config.enablePregnancy && this.getPregnancyTime() > 0)
    	{
			int p = this.getPregnancyTime();
			if (p >= EntityImprovedSheep.pregnancyLength)
			{
				//use alt child gen from stored data on other parent
				EntityAgeable entityageable = this.createChild();
		        final net.minecraftforge.event.entity.living.BabyEntitySpawnEvent event = new net.minecraftforge.event.entity.living.BabyEntitySpawnEvent(this, this, entityageable);
		        final boolean cancelled = net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(event);
		        if (cancelled) {
		            //Reset the "inLove" state for the animals
		            this.setGrowingAge(this.getChildAge()/-2);
		            this.resetInLove();
		            return;
		        }
	
		        entityageable = event.getChild();
	
		        if (entityageable != null)
		        {
	        		this.setGrowingAge(this.getChildAge()/-2);
		            this.resetInLove();
		            
		            entityageable.setGrowingAge(childAge);
		            entityageable.setLocationAndAngles(this.posX, this.posY, this.posZ, 0.0F, 0.0F);
		            this.world.spawnEntity(entityageable);
		            Random random = this.getRNG();
	
		            for (int i = 0; i < 7; ++i)
		            {
		                double d0 = random.nextGaussian() * 0.02D;
		                double d1 = random.nextGaussian() * 0.02D;
		                double d2 = random.nextGaussian() * 0.02D;
		                double d3 = random.nextDouble() * this.width * 2.0D - this.width;
		                double d4 = 0.5D + random.nextDouble() * this.height;
		                double d5 = random.nextDouble() * this.width * 2.0D - this.width;
		                this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + d3, this.posY + d4, this.posZ + d5, d0, d1, d2, new int[0]);
		            }
	
		            if (this.world.getGameRules().getBoolean("doMobLoot") && this.isTame())
		            {
		                this.world.spawnEntity(new EntityXPOrb(this.world, this.posX, this.posY, this.posZ, random.nextInt(7) + 1));
		            }
		        }
		        
		        if (this.getLitterSpawned() >= EntityImprovedSheep.litterSize)
	        	{
		        	this.setPregnancyTime(0);
		        	this.setLitterSpawned(0);
	        	}
		        else if (this.getLitterSpawned() > 0 && this.getRNG().nextInt(100) < 35)
	        	{
		        	this.setPregnancyTime(0);
		        	this.setLitterSpawned(0);
	        	}
		        else
		        {
		        	this.setLitterSpawned(this.getLitterSpawned()+1);
		        }
			}
			else this.setPregnancyTime(p+1);
    	}

    	
    	super.onLivingUpdate();

		this.setMilkTimer(this.getMilkTimer()+this.getMaxMilk());
		if (this.getMilkTimer() >= 10000)
		{
			if (this.getCurMilk() < this.getMaxMilk())
			{
				this.setCurMilk(this.getCurMilk()+1);	
			}
			this.setMilkTimer(0);
		}
		this.groupTimer += 1;

		if (this.groupTimer >= 750)
		{
	    	this.setupTamedAI();
	        Random random = this.getRNG();
	    	if (Config.enableAdvancedStats == true)
	    	{
	    		List<EntityAnimal> list = this.world.<EntityAnimal>getEntitiesWithinAABB(AbstractHorse.class, this.getEntityBoundingBox().grow(16.0D));
		        int breedchance = 0;
		        //group size happiness
		        if (list.size() == this.getPreferedGroupSize())
		        {
		        	breedchance = 50;
		        	this.setHappiness(this.getHappiness()+3);
		        }
		        else if (list.size() >= this.getPreferedGroupSize()+2 ) //harder cap
		        {
		        	breedchance = -100;
		        	this.setHappiness(this.getHappiness()-random.nextInt(2)); //new
		        }
		        else if (list.size() <= this.getPreferedGroupSize()-3)
		        {
		        	breedchance = -25;
		        	this.setHappiness(this.getHappiness()-random.nextInt(2)); //new
		        }
		        else
		        {
		        	breedchance = 25;
		        	this.setHappiness(this.getHappiness()+1);
		        }
		        
		        if (this.inWater)
		        {
		        	breedchance = -100;
		        }
		        //wild breeding
		        if (list.size() >= 2 && list.size() <= this.getPreferedGroupSize() + 2)
		        {
		        	if (Config.enableWildBreeding == true && !(this.isChild()) && !(this.isInLove()) && (this.getGrowingAge() == 0) )
		        	{
		        		if ( (this.getHappiness() + breedchance) >= random.nextInt(100)) this.setInLove(null);
		        	}
		        }	
	    	}
			this.groupTimer = random.nextInt(10);
		}

        //new
		if (Config.enableAdvancedStats == true)
		{
        	//aging process
            int i = this.getOldAge();

            ++i;
            this.setOldAge(i);

            if (i == ageMax && Config.enableOldAge == true)
            {
                this.setDead();
            }
        
	        //hungerprocess
	        this.setHunger(this.getHunger() - ((this.getMaxHealth() + this.getMaxMilk() ) / 50000) );
	        
	        if (this.getHunger() <= 0 && Config.enableStarvation == true)
	        {
	        	this.damageEntity(DamageSource.STARVE, 1.0F);
	        	this.setHunger(3.0F);
	        	this.setHappiness(this.getHappiness()-10);
	        }
		}

        if (!this.world.isRemote)
        {

            if (this.rand.nextInt(900) == 0 && this.deathTime == 0 && this.getHunger() > 3.0F && this.getHealth() < this.getMaxHealth())
            {
                this.heal(1.0F);
                this.setHunger(this.getHunger()-4.0F);
            }
        }
    }

    /**
     * Decreases ItemStack size by one
     */
    @Override
    protected void consumeItemFromStack(EntityPlayer player, ItemStack stack)
    {
    	//new
		if (Config.enableAdvancedStats == true)
		{
			float newHunger = 0;
			int newHappiness = 5;
	    	if (stack.getItem() instanceof ItemFood)
	    	{
	    		ItemFood eatenFood = (ItemFood)stack.getItem();
	    		newHunger = this.getHunger() + eatenFood.getHealAmount(stack) + (eatenFood.getHealAmount(stack)*eatenFood.getSaturationModifier(stack)); 
	    		newHappiness = (int) (this.getHappiness() + eatenFood.getHealAmount(stack) + (eatenFood.getHealAmount(stack)*eatenFood.getSaturationModifier(stack)) );	            
	    		this.setHunger(newHunger);
				this.setHappiness(newHappiness);
	    	}
	    	else
	    	{
				this.eatGrassBonus();
	    	}
	    	
            if (!this.isTame() && this.getTemper() < this.getMaxTemper())
            {
                this.increaseTemper(newHappiness+this.rand.nextInt(5)+5);
                if (this.getTemper() >= this.getMaxTemper())
                {
                	this.setTamedBy(player);
                    double d0 = this.rand.nextGaussian() * 0.025D;
                    double d1 = this.rand.nextGaussian() * 0.025D;
                    double d2 = this.rand.nextGaussian() * 0.025D;
                    this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                }
                else
                {
                    double d0 = this.rand.nextGaussian() * 0.025D;
                    double d1 = this.rand.nextGaussian() * 0.025D;
                    double d2 = this.rand.nextGaussian() * 0.025D;
                    this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
                	this.world.spawnParticle(EnumParticleTypes.VILLAGER_HAPPY, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	
                }
            }
            else
            {
                double d0 = this.rand.nextGaussian() * 0.025D;
                double d1 = this.rand.nextGaussian() * 0.025D;
                double d2 = this.rand.nextGaussian() * 0.025D;
                this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
            	this.world.spawnParticle(EnumParticleTypes.SPELL, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
            }
    	}
    	//
        if (!player.capabilities.isCreativeMode) stack.setCount(stack.getCount() -1);
    }
    
    /**
     * This function applies the benefits of growing back wool and faster growing up to the acting entity. (This
     * function is used in the AIEatGrass)
     */
    @Override
	public void eatGrassBonus()
    {
        this.setHunger(this.getHunger() + 4);
        this.setHappiness(this.getHappiness() + 2);
        return;
    }

    ///////////////////////////////////
    //Getters and setters
    /**
     * Returns randomized max health
     */
    private float getModifiedMaxHealth()
    {
        return 10.0F + this.rand.nextInt(3) + this.rand.nextInt(4);
    }

    
    @Override
	public int getSex()
    {
        return this.dataManager.get(SEX).intValue();
    }
    
    @Override
	public void setSex(int sexIn)
    {
        this.dataManager.set(SEX, Integer.valueOf(sexIn));
    }

    @Override
	public float getHunger()
    {
        return this.dataManager.get(HUNGER).floatValue();
    }
    
    @Override
	public void setHunger(float hungerIn)
    {
    	//hungerIn = MathHelper.clamp_float(hungerIn, 0.0F, foodMax);
    	if (hungerIn > foodMax) hungerIn = foodMax;
        this.dataManager.set(HUNGER, Float.valueOf(hungerIn));
    }
    
    @Override
	public int getTamability()
    {

        return this.dataManager.get(TAMABILITY).intValue();
    }
    
    @Override
	public void setTamability(int tamabilityIn)
    {
        this.dataManager.set(TAMABILITY, Integer.valueOf(tamabilityIn));
    }

    @Override
	public int getHappiness()
    {
        return this.dataManager.get(HAPPINESS).intValue();
    }
    
    @Override
	public void setHappiness(int happinessIn)
    {
    	happinessIn = MathHelper.clamp(happinessIn, -100, 100);
    	if (happinessIn > 100) happinessIn = 100;
    	if (happinessIn < -100) happinessIn = -100;
        this.dataManager.set(HAPPINESS, Integer.valueOf(happinessIn));
    }


    @Override
	@Nullable
    public UUID getOwnerId()
    {
        return (UUID)((Optional)this.dataManager.get(OWNER_UNIQUE_ID)).orNull();
    }

    @Override
	public void setOwnerId(@Nullable UUID uniqueId)
    {
        this.dataManager.set(OWNER_UNIQUE_ID, Optional.fromNullable(uniqueId));
    }

    @Override
	public int getTemper()
    {
        return this.temper;
    }

    @Override
	public void setTemper(int temperIn)
    {
        this.temper = temperIn;
    }

    @Override
	public int increaseTemper(int p_110198_1_)
    {
        int i = MathHelper.clamp(this.getTemper() + p_110198_1_, 0, this.getMaxTemper());
        this.setTemper(i);
        return i;
    }

    @Override
	public int getMaxTemper()
    {
        return 75 - ( this.getTamability() * 5 );        	
    }

    @Override
    public boolean isTame()
    {
        return (this.getOwnerId() != null);
    }

    @Override
	public boolean isTamed()
    {
        return (this.getOwnerId() != null);
    }


    @Override
	public void setTamed(boolean tamed)
    {
        //new
        //this.setupTamedAI();
    }


    @Override
	public boolean setTamedBy(EntityPlayer player)
    {
        this.setOwnerId(player.getUniqueID());
        this.setTamed(true);
        return true;
    }

    protected void setupTamedAI()
    {
        if (this.avoidEntity == null)
        {
            this.avoidEntity = new EntityAIAvoidEntity(this, EntityPlayer.class, 16.0F, 1.0D, 2.0D);
        }
        if (this.aiPanic == null)
        {
            this.aiPanic = new EntityAIPanic(this, 1.25D);
        }
        if (this.aiAttack == null)
        {
            this.aiAttack = new EntityAIAttackMelee(this, 1.75D, false);
        }
        if (this.aiHerd == null)
        {
            this.aiHerd =  new EntityAIHerdMovement(this, 1.25D);
        }

        this.tasks.removeTask(this.avoidEntity);
        this.tasks.removeTask(this.aiPanic);
        this.tasks.removeTask(this.aiAttack);
        this.tasks.removeTask(this.aiHerd);
        
        if (this.isChild())
    	{
        	this.tasks.addTask(1, this.aiPanic);
        	return;
    	}
        
        this.tasks.addTask(2, this.aiHerd);
        
        if (!this.isTame())
        {
        	if (this.getHappiness() <= -50)
        	{
        		this.tasks.addTask(6, this.avoidEntity);        		
        	}
            
        	EntitySheep nearpig = this.getClosestSheep(this, 16.0D);

        	if (this.getSex() == 0 || (nearpig != null && nearpig.isChild()) )
            {
            	this.tasks.addTask(1, this.aiPanic);
            }
        	else
        	{
            	this.tasks.addTask(1, this.aiAttack);
        	}
        }
    }


    @Nullable
    protected EntitySheep getClosestSheep(Entity entityIn, double distance)
    {
        double d0 = Double.MAX_VALUE;
        Entity entity = null;

        for (Entity entity1 : this.getEntityWorld().<EntityAnimal>getEntitiesWithinAABB(EntitySheep.class, this.getEntityBoundingBox().grow(distance)))
        {
        	if (entity1 != this)
        	{
	            double d1 = entity1.getDistanceSq(entityIn.posX, entityIn.posY, entityIn.posZ);
	
	            if (d1 < d0)
	            {
	                entity = entity1;
	                d0 = d1;
	            }
        	}
        }

        return (EntitySheep)entity;
    }

    @Override
	public int getOldAge()
    {
        return this.dataManager.get(OLD_AGE).intValue();
    }
    
    @Override
	public void setOldAge(int age)
    {
        this.dataManager.set(OLD_AGE, Integer.valueOf(age));
    }    

	@Override
	public int getChildAge() 
	{
		return EntityImprovedSheep.childAge;
	}
	
    @Override
	public float getGrowthSize()
    {
    	if (this.isChild())
    	{
    		float growth = EntityImprovedSheep.childAge + this.getOldAge();
	    	float ratio = growth / EntityImprovedSheep.childAge;
	    	return 1.0F - ratio;
    	}
    	return 1.0F;
    }
    
    @Override
	public int getPreferedGroupSize()
    {
    	return 4;
    }
    
    @Override
	public float getFoodMax()
    {
        return foodMax;
    }


    public boolean isEdible(ItemStack stack)
    {
    	if (EntityImprovedSheep.TEMPTATION_ITEMS.contains(stack.getItem())) return true;
    	return false;
    }
 
    @Override
	public boolean isEdible(EntityItem entityitem)
	{
    	if (EntityImprovedSheep.TEMPTATION_ITEMS.contains(entityitem.getItem().getItem())) return true;
    	return false;
	}
 

    @Override
	public int getPregnancyTime()
    {
        return this.pregnancyTime;
    }
    
    @Override
	public void setPregnancyTime(int age)
    {
        this.pregnancyTime = age;
    }    

    public int getLitterSpawned()
    {
        return this.litterSpawned;
    }
    
    public void setLitterSpawned(int age)
    {
        this.litterSpawned = age;
    }    

    @Nullable
    public UUID getOtherParent()
    {
        return (UUID)((Optional)this.dataManager.get(OWNER_UNIQUE_ID)).orNull();
    }

    public void setOtherParent(@Nullable UUID uniqueId)
    {
        this.dataManager.set(OWNER_UNIQUE_ID, Optional.fromNullable(uniqueId));
    }
    
    @Override
    public boolean attackEntityFrom(DamageSource par1DamageSource, float par2)
    {
    	this.setHappiness(this.getHappiness()-50);
        return super.attackEntityFrom(par1DamageSource, par2);
    }

    @Override
	public boolean attackEntityAsMob(Entity entityIn)
    {
        float f = (float)this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getAttributeValue();
        int i = 0;

        if (entityIn instanceof EntityLivingBase)
        {
            f += EnchantmentHelper.getModifierForCreature(this.getHeldItemMainhand(), ((EntityLivingBase)entityIn).getCreatureAttribute());
            i += EnchantmentHelper.getKnockbackModifier(this);
        }

        boolean flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), f);

        if (flag)
        {
            if (i > 0 && entityIn instanceof EntityLivingBase)
            {
                ((EntityLivingBase)entityIn).knockBack(this, i * 0.5F, MathHelper.sin(this.rotationYaw * 0.017453292F), (-MathHelper.cos(this.rotationYaw * 0.017453292F)));
                this.motionX *= 0.6D;
                this.motionZ *= 0.6D;
            }

            int j = EnchantmentHelper.getFireAspectModifier(this);

            if (j > 0)
            {
                entityIn.setFire(j * 4);
            }

            if (entityIn instanceof EntityPlayer)
            {
                EntityPlayer entityplayer = (EntityPlayer)entityIn;
                ItemStack itemstack = this.getHeldItemMainhand();
                ItemStack itemstack1 = entityplayer.isHandActive() ? entityplayer.getActiveItemStack() : null;

                if (itemstack != null && itemstack1 != null && itemstack.getItem() instanceof ItemAxe && itemstack1.getItem() == Items.SHIELD)
                {
                    float f1 = 0.25F + EnchantmentHelper.getEfficiencyModifier(this) * 0.05F;

                    if (this.rand.nextFloat() < f1)
                    {
                        entityplayer.getCooldownTracker().setCooldown(Items.SHIELD, 100);
                        this.world.setEntityState(entityplayer, (byte)30);
                    }
                }
            }

            this.applyEnchantments(this, entityIn);
        }

        return flag;
    }

    
    @Override
	public void getOtherParentData(IEntityAdvanced parent)
    {
    	if (parent instanceof EntityImprovedSheep)
    	{
    		EntityImprovedSheep otherParent = (EntityImprovedSheep) parent;
            this.parentTamability = otherParent.getTamability();
            this.parentMaxHealth = otherParent.getMaxHealth();
            this.parentMaxMilk = otherParent.getMaxMilk();
    	}
    	else //failsafe
    	{
            this.parentTamability = this.getTamability();
            this.parentMaxHealth = this.getMaxHealth();
            this.parentMaxMilk = this.getMaxMilk();
    	}
        return;
    }
    

    //Species Getters and setters
    @Override
	public int getMaxMilk()
    {
        return this.dataManager.get(MAXMILK).intValue();
    }
    
    @Override
	public void setMaxMilk(int In)
    {
        this.dataManager.set(MAXMILK, Integer.valueOf(In));
    }
    
    @Override
	public void setCurMilk(int In)
    {
        this.dataManager.set(CURMILK, Integer.valueOf(In));
    }
    public int getCurMilk()
    {
        return this.dataManager.get(CURMILK).intValue();
    }
    
    @Override
	public void setMilkTimer(int In)
    {
        this.dataManager.set(MILK_TIMER, Integer.valueOf(In));
    }
    public int getMilkTimer()
    {
        return this.dataManager.get(MILK_TIMER).intValue();
    }

}