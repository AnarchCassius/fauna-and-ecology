package faunaandecology.mod.entity.passive;

import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.base.Optional;

import faunaandecology.mod.entity.IEntityAdvancedHorse;
import faunaandecology.mod.entity.ai.EntityAIHerdMovement;
import faunaandecology.mod.util.Config;
import faunaandecology.mod.util.handlers.SoundsHandler;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.IJumpingMount;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIPanic;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.ai.attributes.RangedAttribute;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventoryChangedListener;
import net.minecraft.item.ItemStack;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityZebra extends EntityImprovedHorse implements IInventoryChangedListener, IJumpingMount, IEntityAdvancedHorse, net.minecraftforge.common.IShearable
{
	private static final IAttribute JUMP_STRENGTH = (new RangedAttribute((IAttribute)null, "horse.jumpStrength", 0.7D, 0.0D, 2.0D)).setDescription("Jump Strength").setShouldWatch(true);
    private static final DataParameter<Optional<UUID>> OWNER_UNIQUE_ID = EntityDataManager.<Optional<UUID>>createKey(EntityZebra.class, DataSerializers.OPTIONAL_UNIQUE_ID);
    //
    static public int pregnancyLength = 25000;
    static public float foodMax = 80.0F;
    static public int ageMax = 720000; //30 'years'
    static public int childAge = -96000; //2000 = 1 month
    //
    
    private String texturePrefix;
    private final String[] horseTexturesArray = new String[3];
    //new ai
    private EntityAIAvoidEntity<EntityPlayer> avoidEntity;
    /** The tempt AI task for this mob, used to prevent taming while it is fleeing. */
    //private EntityAITempt aiTempt;
    private EntityAIPanic aiPanic;
    private EntityAIHerdMovement aiHerd;
    

    public EntityZebra(World worldIn)
    {
        super(worldIn);
        this.setSize(1.3964844F, 1.6F);
        this.isImmuneToFire = false;
        this.stepHeight = 1.0F;
        this.initHorseChest();

        //new
        this.setupTamedAI();
    }
    
    @Override
	protected void entityInit()
    {
        super.entityInit();
        this.dataManager.register(OWNER_UNIQUE_ID, Optional.<UUID>absent());
    }

    
    @Override
    public String getZoopediaName()
    {
    	return "Zebra";
    }

    @Override
	protected SoundEvent getAmbientSound()
    {
        super.getAmbientSound();
        return SoundsHandler.AMBIENT_ZEBRA;
    }

    @Override
	protected SoundEvent getDeathSound()
    {
        super.getDeathSound();
        return SoundsHandler.DEATH_ZEBRA;
    }

    @Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
    {
        super.getHurtSound(damageSourceIn);
        return SoundsHandler.HURT_ZEBRA;
    }

    @Override
	protected SoundEvent getAngrySound()
    {
        super.getAngrySound();
        return SoundsHandler.ANGRY_ZEBRA;
    }

    private void setHorseTexturePaths()
    {
        String texture = null;
        if (this.isChild())
        	this.horseTexturesArray[0] = "faunaandecology:textures/entity/horse/zebra_juvenile.png";
        else
        	this.horseTexturesArray[0] = "faunaandecology:textures/entity/horse/zebra.png";
        this.horseTexturesArray[1] = null;
        this.horseTexturesArray[2] = texture;
        this.texturePrefix = "horse/" + "zeb" + "zeb" + texture;
    }
    
    @Override
    public String getHorseTexture()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.texturePrefix;
    }

    @Override
	@SideOnly(Side.CLIENT)
    public String[] getVariantTexturePaths()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.horseTexturesArray;
    }

    @Override
	public int getMaxTemper()
    {
    	return 200 - ( this.getTamability() * 5 );
    }

    @Override
	public float getModifiedMaxHealth()
    {
    	return 14.0F + this.rand.nextInt(7) + this.rand.nextInt(7);
    }

    @Override
	public double getModifiedMovementSpeed()
    {
    	return (0.44999998807907104D + this.rand.nextDouble() * 0.3D + this.rand.nextDouble() * 0.3D + this.rand.nextDouble() * 0.3D) * 0.3D;
    }

    @Override
	public boolean wearsArmor()
    {
        return false;
    }

    @Override public boolean isShearable(ItemStack item, net.minecraft.world.IBlockAccess world, BlockPos pos){ return false; }
    
    /**
     * Called only once on an entity when first time spawned, via egg, mob spawner, natural spawning etc, but not called
     * when entity is reloaded from nbt. Mainly used for initializing attributes and inventory
     */
    @Override
	@Nullable
    public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty, @Nullable IEntityLivingData livingdata)
    {
        //livingdata = super.onInitialSpawn(difficulty, livingdata);
        int i = 1;

        this.setHorseVariant(i | 0 << 8);
            
        int randroll = this.rand.nextInt(2);
		if (Config.enableSexes == true)
		{
	        if (randroll == 1)
			{
	        	this.setSex(Integer.valueOf(1));	
			}
	        else
	        {
	            this.setSex(Integer.valueOf(0));
	        }
	        randroll = this.rand.nextInt(2);
        }
		
		randroll = this.rand.nextInt(2);
        if (randroll == 1)
		{
        	this.setTamability(Integer.valueOf(-4));
		}
        else
        {
    		this.setTamability(Integer.valueOf(-5));
        }

        this.setHunger(foodMax);
		this.setHappiness(0);
		

        if (this.rand.nextInt(5) == 0)
        {
            this.setGrowingAge(childAge);
            this.setOldAge(0);
        }
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.getModifiedMaxHealth());
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(this.getModifiedMovementSpeed());
        this.getEntityAttribute(JUMP_STRENGTH).setBaseValue(this.getModifiedJumpStrength());
        
        this.setHealth(this.getMaxHealth());
        this.setupTamedAI();
        if (this.world.isRemote)
        {
        	this.setHorseTexturePaths();	
        }
        
        this.getOtherParentData(this);
        return livingdata;
    }
    //////////////////////

    @Override
    public boolean isTame()
    {
        return (this.getOwnerUniqueId() != null);
    }

    @Override
	protected void setupTamedAI()
    {
        if (this.avoidEntity == null)
        {
            this.avoidEntity = new EntityAIAvoidEntity<EntityPlayer>(this, EntityPlayer.class, 16.0F, 1.0D, 2.0D);
        }
        if (this.aiPanic == null)
        {
            this.aiPanic = new EntityAIPanic(this, 1.75D);
        }
        if (this.aiHerd == null)
        {
            this.aiHerd =  new EntityAIHerdMovement(this, 1.25D);
        }

        this.tasks.removeTask(this.avoidEntity);
        this.tasks.removeTask(this.aiPanic);
        this.tasks.removeTask(this.aiHerd);
        
        if (this.isChild())
    	{
        	this.tasks.addTask(1, this.aiPanic);
        	return;
    	}
        
        this.tasks.addTask(2, this.aiHerd);
        
        if (!this.isTame())
        {
        	if (this.getHappiness() <= -50)
        	{
        		this.tasks.addTask(6, this.avoidEntity);        		
        	}
            
        	this.tasks.addTask(1, this.aiPanic);

        }
    }

    @Nullable
    @Override
    public UUID getOwnerUniqueId()
    {
        return (UUID)((Optional<?>)this.dataManager.get(OWNER_UNIQUE_ID)).orNull();
    }

    @Override
    public void setOwnerUniqueId(@Nullable UUID uniqueId)
    {
        this.dataManager.set(OWNER_UNIQUE_ID, Optional.fromNullable(uniqueId));
    }
    
}

