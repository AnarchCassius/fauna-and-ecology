package faunaandecology.mod.entity.passive;

import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;
import com.google.common.collect.Sets;

import faunaandecology.mod.entity.IEntityAdvanced;
import faunaandecology.mod.entity.ai.EntityAIImprovedFollowCaravan;
import faunaandecology.mod.entity.projectile.EntityImprovedLlamaSpit;
import faunaandecology.mod.init.ItemInit;
import faunaandecology.mod.util.Config;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackRanged;
import net.minecraft.entity.ai.EntityAIFollowParent;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIMate;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIPanic;
import net.minecraft.entity.ai.EntityAIRunAroundLikeCrazy;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWanderAvoidWater;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemMonsterPlacer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;


public class EntityImprovedLlama extends EntityImprovedDonkey implements net.minecraftforge.common.IShearable, IRangedAttackMob
{
	private static final Set<Item> TEMPTATION_ITEMS = Sets.newHashSet(Items.WHEAT_SEEDS, Items.MELON_SEEDS, Items.PUMPKIN_SEEDS, Items.BEETROOT_SEEDS,
			Items.BREAD, Items.APPLE, Items.CARROT, Items.WHEAT, Items.BEETROOT, Items.GOLDEN_APPLE, Items.GOLDEN_CARROT, Items.MELON, Items.SPECKLED_MELON,
			Items.SUGAR, Items.REEDS, Item.getItemFromBlock(Blocks.YELLOW_FLOWER), Item.getItemFromBlock(Blocks.CACTUS), Item.getItemFromBlock(Blocks.DEADBUSH));
	
    private static final DataParameter<Integer> DATA_STRENGTH_ID = EntityDataManager.<Integer>createKey(EntityImprovedLlama.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> DATA_COLOR_ID = EntityDataManager.<Integer>createKey(EntityImprovedLlama.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> DATA_VARIANT_ID = EntityDataManager.<Integer>createKey(EntityImprovedLlama.class, DataSerializers.VARINT);
    
    private static final DataParameter<Boolean> SHEARED = EntityDataManager.<Boolean>createKey(EntityImprovedLlama.class, DataSerializers.BOOLEAN);

    static public int pregnancyLength = 22000;
    static public float foodMax = 60.0F;
    static public int ageMax = 600000; //25 'years'
    static public int childAge = -36000; //18 'monthes' 
    
    private boolean didSpit;
    @Nullable
    private EntityImprovedLlama caravanHead;
    @Nullable
    private EntityImprovedLlama caravanTail;

    private String texturePrefix;
    private final String[] horseTexturesArray = new String[3];
    
    public EntityImprovedLlama(World worldIn)
    {
        super(worldIn);
        this.setSize(0.9F, 1.87F);
    }
    
    @Override
    public String getZoopediaName()
    {
    	return "Llama";
    }
    
    @Override
	@SideOnly(Side.CLIENT)
    public String getHorseTexture()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.texturePrefix;
    }

    @Override
	@SideOnly(Side.CLIENT)
    public String[] getVariantTexturePaths()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.horseTexturesArray;
    }

    @SideOnly(Side.CLIENT) 
    private void setHorseTexturePaths()
    {
        int i = 0;
        int j = (i & 255) % 7;
        int k = ((i & 65280) >> 8) % 5;
        String texture = null;
        this.horseTexturesArray[0] = "faunaandecology:textures/entity/horse/llama_white.png";
        this.horseTexturesArray[1] = null;
        this.horseTexturesArray[2] = texture;
        this.texturePrefix = "horse/" + "lla" + "lla" + texture;
    }
    
    private void setStrength(int strengthIn)
    {
        this.dataManager.set(DATA_STRENGTH_ID, Integer.valueOf(Math.max(1, Math.min(5, strengthIn))));
    }

    private void setRandomStrength()
    {
        int i = this.rand.nextFloat() < 0.04F ? 5 : 3;
        this.setStrength(1 + this.rand.nextInt(i));
    }

    public int getStrength()
    {
        return this.dataManager.get(DATA_STRENGTH_ID).intValue();
    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    @Override
	public void writeEntityToNBT(NBTTagCompound compound)
    {
        super.writeEntityToNBT(compound);
        compound.setBoolean("Sheared", this.getSheared());
        compound.setInteger("Variant", this.getVariant());
        compound.setInteger("Strength", this.getStrength());

        if (!this.horseChest.getStackInSlot(1).isEmpty())
        {
            compound.setTag("DecorItem", this.horseChest.getStackInSlot(1).writeToNBT(new NBTTagCompound()));
        }
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    @Override
	public void readEntityFromNBT(NBTTagCompound compound)
    {  
        this.setSheared( compound.getBoolean("Sheared"));
        this.setStrength(compound.getInteger("Strength"));
        super.readEntityFromNBT(compound);
        this.setVariant(compound.getInteger("Variant"));

        if (compound.hasKey("DecorItem", 10))
        {
            this.horseChest.setInventorySlotContents(1, new ItemStack(compound.getCompoundTag("DecorItem")));
        }

        this.updateHorseSlots();
    }

    @Override
	protected void initEntityAI()
    {
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(1, new EntityAIRunAroundLikeCrazy(this, 1.2D));
        this.tasks.addTask(2, new EntityAIImprovedFollowCaravan(this, 2.0999999046325684D));
        this.tasks.addTask(3, new EntityAIAttackRanged(this, 1.25D, 40, 20.0F));
        this.tasks.addTask(3, new EntityAIPanic(this, 1.2D));
        this.tasks.addTask(4, new EntityAIMate(this, 1.0D));
        this.tasks.addTask(5, new EntityAIFollowParent(this, 1.0D));
        this.tasks.addTask(6, new EntityAIWanderAvoidWater(this, 0.7D));
        this.tasks.addTask(7, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
        this.tasks.addTask(8, new EntityAILookIdle(this));
        this.targetTasks.addTask(1, new EntityImprovedLlama.AIHurtByTarget(this));
        this.targetTasks.addTask(2, new EntityImprovedLlama.AIDefendTarget(this));
        this.targetTasks.addTask(3, new EntityAIHurtByTarget(this, true, new Class[0]));        
    }

    @Override
	protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(40.0D);
    }

    @Override
	protected void entityInit()
    {
        super.entityInit();
        this.dataManager.register(SHEARED, Boolean.valueOf(false));
        this.dataManager.register(DATA_STRENGTH_ID, Integer.valueOf(0));
        this.dataManager.register(DATA_COLOR_ID, Integer.valueOf(-1));
        this.dataManager.register(DATA_VARIANT_ID, Integer.valueOf(0));
    }

    public int getVariant()
    {
        return MathHelper.clamp(this.dataManager.get(DATA_VARIANT_ID).intValue(), 0, 3);
    }

    public void setVariant(int variantIn)
    {
        this.dataManager.set(DATA_VARIANT_ID, Integer.valueOf(variantIn));
    }

    @Override
	protected int getInventorySize()
    {
        return this.hasChest() ? 2 + 3 * this.getInventoryColumns() : super.getInventorySize();
    }

    @Override
	public void updatePassenger(Entity passenger)
    {
        if (this.isPassenger(passenger))
        {
            float f = MathHelper.cos(this.renderYawOffset * 0.017453292F);
            float f1 = MathHelper.sin(this.renderYawOffset * 0.017453292F);
            float f2 = 0.3F;
            passenger.setPosition(this.posX + 0.3F * f1, this.posY + this.getMountedYOffset() + passenger.getYOffset(), this.posZ - 0.3F * f);
        }
    }

    /**
     * Returns the Y offset from the entity's position for any entity riding this one.
     */
    @Override
	public double getMountedYOffset()
    {
        return this.height * 0.67D;
    }

    /**
     * returns true if all the conditions for steering the entity are met. For pigs, this is true if it is being ridden
     * by a player and the player is holding a carrot-on-a-stick
     */
    @Override
	public boolean canBeSteered()
    {
        return false;
    }

    @Override
	protected boolean handleEating(EntityPlayer player, ItemStack stack)
    {
        int i = 0;
        int j = 0;
        float f = 0.0F;
        boolean flag = false;
        Item item = stack.getItem();

        if (item == Items.WHEAT)
        {
            i = 10;
            j = 3;
            f = 2.0F;
        }
        else if (item == Item.getItemFromBlock(Blocks.HAY_BLOCK))
        {
            i = 90;
            j = 6;
            f = 10.0F;

            if (this.isTame() && this.getGrowingAge() == 0)
            {
                flag = true;
                this.setInLove(player);
            }
        }

        if (this.getHealth() < this.getMaxHealth() && f > 0.0F)
        {
            this.heal(f);
            flag = true;
        }

        if (this.isChild() && i > 0)
        {
            this.world.spawnParticle(EnumParticleTypes.VILLAGER_HAPPY, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, 0.0D, 0.0D, 0.0D);

            if (!this.world.isRemote)
            {
                this.addGrowth(i);
            }

            flag = true;
        }

        if (j > 0 && (flag || !this.isTame()) && this.getTemper() < this.getMaxTemper())
        {
            flag = true;

            if (!this.world.isRemote)
            {
                this.increaseTemper(j);
            }
        }

        if (flag && !this.isSilent())
        {
            this.world.playSound((EntityPlayer)null, this.posX, this.posY, this.posZ, SoundEvents.ENTITY_LLAMA_EAT, this.getSoundCategory(), 1.0F, 1.0F + (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F);
        }

        return flag;
    }

    /**
     * Dead and sleeping entities cannot move
     */
    @Override
	protected boolean isMovementBlocked()
    {
        return this.getHealth() <= 0.0F || this.isEatingHaystack();
    }

    /**
     * Called only once on an entity when first time spawned, via egg, mob spawner, natural spawning etc, but not called
     * when entity is reloaded from nbt. Mainly used for initializing attributes and inventory
     */
    @Override
	@Nullable
    public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty, @Nullable IEntityLivingData livingdata)
    {
        livingdata = super.onInitialSpawn(difficulty, livingdata);
        this.setRandomStrength();
        int i;

        if (livingdata instanceof EntityImprovedLlama.GroupData)
        {
            i = ((EntityImprovedLlama.GroupData)livingdata).variant;
        }
        else
        {
            i = this.rand.nextInt(4);
            livingdata = new EntityImprovedLlama.GroupData(i);
        }

        this.setVariant(i);
        return livingdata;
    }

    @SideOnly(Side.CLIENT)
    public boolean hasColor()
    {
        return this.getColor() != null;
    }

    @Override
	protected SoundEvent getAngrySound()
    {
        return SoundEvents.ENTITY_LLAMA_ANGRY;
    }

    @Override
	protected SoundEvent getAmbientSound()
    {
        return SoundEvents.ENTITY_LLAMA_AMBIENT;
    }

    @Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
    {
        return SoundEvents.ENTITY_LLAMA_HURT;
    }

    @Override
	protected SoundEvent getDeathSound()
    {
        return SoundEvents.ENTITY_LLAMA_DEATH;
    }

    @Override
	protected void playStepSound(BlockPos pos, Block blockIn)
    {
        this.playSound(SoundEvents.ENTITY_LLAMA_STEP, 0.15F, 1.0F);
    }

    @Override
	protected void playChestEquipSound()
    {
        this.playSound(SoundEvents.ENTITY_LLAMA_CHEST, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
    }

    @Override
	public void makeMad()
    {
        SoundEvent soundevent = this.getAngrySound();

        if (soundevent != null)
        {
            this.playSound(soundevent, this.getSoundVolume(), this.getSoundPitch());
        }
    }

    @Override
	@Nullable
    protected ResourceLocation getLootTable()
    {
        return LootTableList.ENTITIES_LLAMA;
    }

    @Override
	public int getInventoryColumns()
    {
        return this.getStrength();
    }

    @Override
	public boolean wearsArmor()
    {
        return true;
    }

    @Override
	public boolean isArmor(ItemStack stack)
    {
        return stack.getItem() == Item.getItemFromBlock(Blocks.CARPET);
    }

    @Override
	public boolean canBeSaddled()
    {
        return false;
    }

    /**
     * Called by InventoryBasic.onInventoryChanged() on a array that is never filled.
     */
    @Override
	public void onInventoryChanged(IInventory invBasic)
    {
        EnumDyeColor enumdyecolor = this.getColor();
        super.onInventoryChanged(invBasic);
        EnumDyeColor enumdyecolor1 = this.getColor();

        if (this.ticksExisted > 20 && enumdyecolor1 != null && enumdyecolor1 != enumdyecolor)
        {
            this.playSound(SoundEvents.ENTITY_LLAMA_SWAG, 0.5F, 1.0F);
        }
    }

    /**
     * Updates the items in the saddle and armor slots of the horse's inventory.
     */
    @Override
	protected void updateHorseSlots()
    {
        if (!this.world.isRemote)
        {
            super.updateHorseSlots();
            this.setColorByItem(this.horseChest.getStackInSlot(1));
        }
    }

    private void setColor(@Nullable EnumDyeColor color)
    {
        this.dataManager.set(DATA_COLOR_ID, Integer.valueOf(color == null ? -1 : color.getMetadata()));
    }

    private void setColorByItem(ItemStack stack)
    {
        if (this.isArmor(stack))
        {
            this.setColor(EnumDyeColor.byMetadata(stack.getMetadata()));
        }
        else
        {
            this.setColor((EnumDyeColor)null);
        }
    }

    @Nullable
    public EnumDyeColor getColor()
    {
        int i = this.dataManager.get(DATA_COLOR_ID).intValue();
        return i == -1 ? null : EnumDyeColor.byMetadata(i);
    }

    @Override
	public int getMaxTemper()
    {
        return 30;
    }

    @Override
    public boolean processInteract(EntityPlayer player, EnumHand hand)
    {
        ItemStack stack = player.getHeldItem(hand);
        
    	//breeding item overrides
        if (stack != null)
        {
        	if (Config.enableAdvancedStats == true)
        	{
        		if (this.getHunger() < foodMax - 0.5F)
        		{
	        		if  (Config.enableWildBreeding == true)
	        		{
	                    if (this.isBreedingItem(stack)  && !(this.isChild()) && !(this.isInLove()) )
	                    {
	                        this.consumeItemFromStack(player, stack);
	                        return true;
	                    }
	                    else if (this.isBreedingItem(stack))
	                    {
	                        this.consumeItemFromStack(player, stack);
	                        return true;
	                    }
	        		}
	        		else
	        		{
	                    if (this.isBreedingItem(stack) && !(this.isChild()) && !(this.isInLove()) )
	                    {
	                        this.consumeItemFromStack(player, stack);
	                        this.setInLove(player);
	                        return true;
	                    }
	                    else if (this.isBreedingItem(stack))
	                    {
	                        this.consumeItemFromStack(player, stack);
	                        return true;
	                    }
	        		}
        		}
                
                if (this.isEdible(stack))
                {
                	if  (this.getHunger() <  foodMax - 0.5F)
                	{
    	                this.consumeItemFromStack(player, stack);
    	                return true;
                	}
                	else return false;
                }
        	}

        	
        }
        
        //override of EntityAgeable to allow varied growth times
        if (stack != null && stack.getItem() == Items.SPAWN_EGG)
        {
            ItemStack itemstack = player.getHeldItem(hand);
            if (!this.world.isRemote)
            {
                Class <? extends Entity > oclass = EntityList.getClass(ItemMonsterPlacer.getNamedIdFrom(itemstack));

                if (oclass != null && this.getClass() == oclass)
                {
                    EntityAgeable entityageable = this.createChild(this);

                    if (entityageable != null)
                    {
                        entityageable.setGrowingAge(childAge);
                        entityageable.setLocationAndAngles(this.posX, this.posY, this.posZ, 0.0F, 0.0F);
                        this.world.spawnEntity(entityageable);

                        if (stack.hasDisplayName())
                        {
                            entityageable.setCustomNameTag(stack.getDisplayName());
                        }

                        if (!player.capabilities.isCreativeMode)
                        {
                            stack.setCount(stack.getCount() -1);
                        }
                    }
                }
            }
            return true;
        }
        
        if (!super.processInteract(player, hand))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Decreases ItemStack size by one
     */
    @Override
    protected void consumeItemFromStack(EntityPlayer player, ItemStack stack)
    {
    	//new
		if (Config.enableAdvancedStats == true)
		{
			float newHunger = 0;
			int newHappiness = 5;
	    	if (stack.getItem() instanceof ItemFood)
	    	{
	    		ItemFood eatenFood = (ItemFood)stack.getItem();
	    		newHunger = this.getHunger() + eatenFood.getHealAmount(stack) + (eatenFood.getHealAmount(stack)*eatenFood.getSaturationModifier(stack)); 
	    		newHappiness = (int) (this.getHappiness() + eatenFood.getHealAmount(stack) + (eatenFood.getHealAmount(stack)*eatenFood.getSaturationModifier(stack)) );	            
	    		this.setHunger(newHunger);
				this.setHappiness(newHappiness);
	    	}
	    	else
	    	{
				this.eatGrassBonus();
	    	}
	    	
            if (!this.isTame() && this.getTemper() < this.getMaxTemper())
            {
                this.increaseTemper(newHappiness+this.rand.nextInt(5)+5);
                if (this.getTemper() >= this.getMaxTemper())
                {
                	this.setTamedBy(player);
                    double d0 = this.rand.nextGaussian() * 0.025D;
                    double d1 = this.rand.nextGaussian() * 0.025D;
                    double d2 = this.rand.nextGaussian() * 0.025D;
                    this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                }
                else
                {
                    double d0 = this.rand.nextGaussian() * 0.025D;
                    double d1 = this.rand.nextGaussian() * 0.025D;
                    double d2 = this.rand.nextGaussian() * 0.025D;
                    this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
                	this.world.spawnParticle(EnumParticleTypes.VILLAGER_HAPPY, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	
                }
            }
            else
            {
                double d0 = this.rand.nextGaussian() * 0.025D;
                double d1 = this.rand.nextGaussian() * 0.025D;
                double d2 = this.rand.nextGaussian() * 0.025D;
                this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
            	this.world.spawnParticle(EnumParticleTypes.SPELL, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
            }
    	}
    	//
        if (!player.capabilities.isCreativeMode) stack.setCount(stack.getCount() -1);
    }
    
    @Override
	public boolean isEdible(ItemStack stack)
    {
    	if (EntityImprovedLlama.TEMPTATION_ITEMS.contains(stack.getItem())) return true;
    	return false;
    }

    @Override
	public boolean isTamed()
    {
        return (this.getOwnerId() != null);
    }

 
    @Override
	public boolean isEdible(EntityItem entityitem)
	{
    	if (EntityImprovedLlama.TEMPTATION_ITEMS.contains(entityitem.getItem().getItem())) return true;
    	return false;
	}
    
    /**
     * Returns true if the mob is currently able to mate with the specified mob.
     */
    @Override
	public boolean canMateWith(EntityAnimal otherAnimal)
    {
        return otherAnimal != this && otherAnimal instanceof EntityImprovedLlama && this.canMate() && ((EntityImprovedLlama)otherAnimal).canMate();
    }

    @Override
	public EntityImprovedLlama createChild(EntityAgeable ageable)
    {
    	IEntityAdvanced entityllama = (IEntityAdvanced)ageable;

        //Domestication process
        int newTamability = (entityllama.getTamability() + this.getTamability()) / 2;
        int randroll = this.rand.nextInt(2) + 1;
        if (this.isTame() || entityllama.isTamed())
        {
        	newTamability += randroll;	
        }
        else
        {
        	newTamability -= randroll;
        }
        newTamability = MathHelper.clamp(newTamability, -5, 5);
        

        EntityImprovedLlama entitynewllama;
        if ( newTamability >= 0 )//(ageable instanceof EntityAurochs)
        {
        	entitynewllama = new EntityImprovedLlama(this.world);
        }
        else
        {
        	entitynewllama = new EntityGuanaco(this.world);
        }
        //check nearby players for taming
        List<EntityPlayer> players = this.world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(9.0D));
		for (Entity e : players)
		{
			if (e != null)
			{

				entitynewllama.setTamedByPlayer((EntityPlayer) e);
				//entitynewllama.setOwnerUniqueId(e.getPersistentID());
				//entitynewllama.setTamed(true);
			}
		}
		entitynewllama.setTamability(newTamability);
        
		this.setOffspringAttributes(ageable, entitynewllama);
        EntityImprovedLlama EntityImprovedLlama1 = (EntityImprovedLlama)ageable;
        int i = this.rand.nextInt(Math.max(this.getStrength(), EntityImprovedLlama1.getStrength())) + 1;

        if (this.rand.nextFloat() < 0.03F)
        {
            ++i;
        }

        entitynewllama.setStrength(i);
        entitynewllama.setVariant(this.rand.nextBoolean() ? this.getVariant() : EntityImprovedLlama1.getVariant());
        return entitynewllama;
    }

    private void spit(EntityLivingBase target)
    {
        EntityImprovedLlamaSpit entityllamaspit = new EntityImprovedLlamaSpit(this.world, this);
        double d0 = target.posX - this.posX;
        double d1 = target.getEntityBoundingBox().minY + target.height / 3.0F - entityllamaspit.posY;
        double d2 = target.posZ - this.posZ;
        float f = MathHelper.sqrt(d0 * d0 + d2 * d2) * 0.2F;
        entityllamaspit.shoot(d0, d1 + f, d2, 1.5F, 10.0F);
        this.world.playSound((EntityPlayer)null, this.posX, this.posY, this.posZ, SoundEvents.ENTITY_LLAMA_SPIT, this.getSoundCategory(), 1.0F, 1.0F + (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F);
        this.world.spawnEntity(entityllamaspit);
        this.didSpit = true;
    }

    private void setDidSpit(boolean didSpitIn)
    {
        this.didSpit = didSpitIn;
    }

    @Override
	public void fall(float distance, float damageMultiplier)
    {
        int i = MathHelper.ceil((distance * 0.5F - 3.0F) * damageMultiplier);

        if (i > 0)
        {
            if (distance >= 6.0F)
            {
                this.attackEntityFrom(DamageSource.FALL, i);

                if (this.isBeingRidden())
                {
                    for (Entity entity : this.getRecursivePassengers())
                    {
                        entity.attackEntityFrom(DamageSource.FALL, i);
                    }
                }
            }

            IBlockState iblockstate = this.world.getBlockState(new BlockPos(this.posX, this.posY - 0.2D - this.prevRotationYaw, this.posZ));
            Block block = iblockstate.getBlock();

            if (iblockstate.getMaterial() != Material.AIR && !this.isSilent())
            {
                SoundType soundtype = block.getSoundType();
                this.world.playSound((EntityPlayer)null, this.posX, this.posY, this.posZ, soundtype.getStepSound(), this.getSoundCategory(), soundtype.getVolume() * 0.5F, soundtype.getPitch() * 0.75F);
            }
        }
    }

    public void leaveCaravan()
    {
        if (this.caravanHead != null)
        {
            this.caravanHead.caravanTail = null;
        }

        this.caravanHead = null;
    }

    public void joinCaravan(EntityImprovedLlama caravanHeadIn)
    {
        this.caravanHead = caravanHeadIn;
        this.caravanHead.caravanTail = this;
    }

    public boolean hasCaravanTrail()
    {
        return this.caravanTail != null;
    }

    public boolean inCaravan()
    {
        return this.caravanHead != null;
    }

    @Nullable
    public EntityImprovedLlama getCaravanHead()
    {
        return this.caravanHead;
    }

    @Override
	protected double followLeashSpeed()
    {
        return 2.0D;
    }

    @Override
	protected void followMother()
    {
        if (!this.inCaravan() && this.isChild())
        {
            super.followMother();
        }
    }

    @Override
	public boolean canEatGrass()
    {
        return false;
    }

    /**
     * Attack the specified entity using a ranged attack.
     */
    @Override
	public void attackEntityWithRangedAttack(EntityLivingBase target, float distanceFactor)
    {
        this.spit(target);
    }

    @Override
	public void setSwingingArms(boolean swingingArms)
    {
    }

    static class AIDefendTarget extends EntityAINearestAttackableTarget<EntityWolf>
        {
            public AIDefendTarget(EntityImprovedLlama llama)
            {
                super(llama, EntityWolf.class, 16, false, true, (Predicate)null);
            }

            /**
             * Returns whether the EntityAIBase should begin execution.
             */
            @Override
			public boolean shouldExecute()
            {
                if (super.shouldExecute() && this.targetEntity != null && !this.targetEntity.isTamed())
                {
                    return true;
                }
                else
                {
                    this.taskOwner.setAttackTarget((EntityLivingBase)null);
                    return false;
                }
            }

            @Override
			protected double getTargetDistance()
            {
                return super.getTargetDistance() * 0.25D;
            }
        }

    static class AIHurtByTarget extends EntityAIHurtByTarget
        {
            public AIHurtByTarget(EntityImprovedLlama llama)
            {
                super(llama, false);
            }

            /**
             * Returns whether an in-progress EntityAIBase should continue executing
             */
            @Override
			public boolean shouldContinueExecuting()
            {
                if (this.taskOwner instanceof EntityImprovedLlama)
                {
                    EntityImprovedLlama EntityImprovedLlama = (EntityImprovedLlama)this.taskOwner;

                    if (EntityImprovedLlama.didSpit)
                    {
                        EntityImprovedLlama.setDidSpit(false);
                        return false;
                    }
                }

                return super.shouldContinueExecuting();
            }
        }

    static class GroupData implements IEntityLivingData
        {
            public int variant;

            private GroupData(int variantIn)
            {
                this.variant = variantIn;
            }
        }
    
    @Override
	public List<ItemStack> onSheared(ItemStack item, IBlockAccess world, BlockPos pos, int fortune) 
    {
        this.setSheared(true);
        int i = 1 + this.rand.nextInt(3);

        java.util.List<ItemStack> ret = new java.util.ArrayList<ItemStack>();
        for (int j = 0; j < i; ++j)
        	if (Config.woolSystem)
			{
				ret.add(new ItemStack(ItemInit.WOOL_TUFT, 1 + this.rand.nextInt(3), 15));
			}
			else
            ret.add(new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 0));

        this.playSound(SoundEvents.ENTITY_SHEEP_SHEAR, 1.0F, 1.0F);
        return ret;

	}

    //////////
    @Override public boolean isShearable(ItemStack item, net.minecraft.world.IBlockAccess world, BlockPos pos){ return !this.getSheared() && !this.isChild(); }

    @Override
	public boolean getSheared()
    { 
        return this.dataManager.get(SHEARED).booleanValue();
    }
    
    public void setSheared(boolean shearedIn)
    {
        this.dataManager.set(SHEARED, Boolean.valueOf(shearedIn));
    }


       

}