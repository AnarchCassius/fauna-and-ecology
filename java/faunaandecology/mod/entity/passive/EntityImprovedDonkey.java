package faunaandecology.mod.entity.passive;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.collect.Sets;

import faunaandecology.mod.client.gui.inventory.ContainerImprovedHorseInventory;
import faunaandecology.mod.entity.IEntityAdvanced;
import faunaandecology.mod.entity.IEntityAdvancedHorse;
import faunaandecology.mod.entity.ai.EntityAIHerdMovement;
import faunaandecology.mod.entity.ai.EntityAIImprovedMate;
import faunaandecology.mod.entity.ai.EntityAIItemSearch;
import faunaandecology.mod.entity.ai.EntityAIRunAround;
import faunaandecology.mod.util.Config;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.IJumpingMount;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIEatGrass;
import net.minecraft.entity.ai.EntityAIFollowParent;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIPanic;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAITempt;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.ai.attributes.RangedAttribute;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.passive.AbstractChestHorse;
import net.minecraft.entity.passive.AbstractHorse;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.IInventoryChangedListener;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemMonsterPlacer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.network.play.server.SPacketOpenWindow;
import net.minecraft.server.management.PreYggdrasilConverter;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.datafix.DataFixer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityImprovedDonkey extends AbstractChestHorse implements IInventoryChangedListener, IJumpingMount, IEntityAdvancedHorse
{
	private static final IAttribute JUMP_STRENGTH = (new RangedAttribute((IAttribute)null, "horse.jumpStrength", 0.7D, 0.0D, 2.0D)).setDescription("Jump Strength").setShouldWatch(true);
	private static final DataParameter<Byte> STATUS = EntityDataManager.<Byte>createKey(EntityImprovedDonkey.class, DataSerializers.BYTE);

	private static final Set<Item> TEMPTATION_ITEMS = Sets.newHashSet(Items.WHEAT_SEEDS, Items.MELON_SEEDS, Items.PUMPKIN_SEEDS, Items.BEETROOT_SEEDS,
			Items.BREAD, Items.APPLE, Items.CARROT, Items.WHEAT, Items.BEETROOT, Items.GOLDEN_APPLE, Items.GOLDEN_CARROT, Items.MELON, Items.SPECKLED_MELON,
			Items.SUGAR, Items.REEDS, Item.getItemFromBlock(Blocks.YELLOW_FLOWER), Item.getItemFromBlock(Blocks.CACTUS), Item.getItemFromBlock(Blocks.DEADBUSH));
	
	private static final DataParameter<Optional<UUID>> OWNER_UNIQUE_ID = EntityDataManager.<Optional<UUID>>createKey(EntityImprovedDonkey.class, DataSerializers.OPTIONAL_UNIQUE_ID);

    //private static final DataParameter<Boolean> DATA_ID_CHEST = EntityDataManager.<Boolean>createKey(EntityImprovedDonkey.class, DataSerializers.BOOLEAN);
    //
    private static final DataParameter<Integer> SEX = EntityDataManager.<Integer>createKey(EntityImprovedDonkey.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> OLD_AGE = EntityDataManager.<Integer>createKey(EntityImprovedDonkey.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> TAMABILITY = EntityDataManager.<Integer>createKey(EntityImprovedDonkey.class, DataSerializers.VARINT);
    private static final DataParameter<Float> HUNGER = EntityDataManager.<Float>createKey(EntityImprovedDonkey.class, DataSerializers.FLOAT);
    private static final DataParameter<Integer> HAPPINESS = EntityDataManager.<Integer>createKey(EntityImprovedDonkey.class, DataSerializers.VARINT);
    private static final DataParameter<Boolean> SHEARED = EntityDataManager.<Boolean>createKey(EntityImprovedDonkey.class, DataSerializers.BOOLEAN);
    private static final DataParameter<Optional<UUID>> OTHER_PARENT_ID = EntityDataManager.<Optional<UUID>>createKey(EntityImprovedDonkey.class, DataSerializers.OPTIONAL_UNIQUE_ID);
    
    private int eatingHaystackCounter;
    private int jumpRearingCounter;

    private boolean hasReproduced;
    /** "The higher this value, the more likely the horse is to be tamed next time a player rides it." */
    protected int temper;
    
    private int parentTamability;
    private float parentMaxHealth;
    private float parentSpeed;
    private double parentJump;
    private String parentType = "Horse";
    private int parentVariant;
    //
    private int pregnancyTime = 0;
    private int groupTimer = 0;
    static public int pregnancyLength = 26000;
    static public float foodMax = 80.0F;
    static public int ageMax = 720000; //30 'years'
    static public int childAge = -96000; //2000 = 1 month

    private String texturePrefix;
    private final String[] horseTexturesArray = new String[3];
    
    //new ai
    private EntityAIAvoidEntity<EntityPlayer> avoidEntity;
    /** The tempt AI task for this mob, used to prevent taming while it is fleeing. */
    private EntityAITempt aiTempt;
    private EntityAIPanic aiPanic;
    private EntityAIHerdMovement aiHerd;
    

    public EntityImprovedDonkey(World worldIn)
    {
        super(worldIn);
        this.canGallop = false;

        //new
        this.setupTamedAI();
    }

    @Override
    protected void initEntityAI()
    {
        this.aiPanic = new EntityAIPanic(this, 1.2D);
        this.aiHerd = new EntityAIHerdMovement(this, 1.0D);
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(1, this.aiPanic);
        this.tasks.addTask(1, new EntityAIRunAround(this, 1.75D));
        this.tasks.addTask(2, this.aiHerd);
        this.tasks.addTask(3, new EntityAIImprovedMate(this, 1.0D, AbstractHorse.class));
        this.tasks.addTask(4, new EntityAITempt(this, 1.0D, false, TEMPTATION_ITEMS));
        
        this.tasks.addTask(7, new EntityAIFollowParent(this, 1.25D));
        this.tasks.addTask(9, new EntityAIItemSearch(this, 1.0D));
        this.tasks.addTask(9, new EntityAIEatGrass(this));
        this.tasks.addTask(10, new EntityAIWander(this, 1.0D));
        this.tasks.addTask(11, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
        this.tasks.addTask(12, new EntityAILookIdle(this));
    }
    
    protected void setupTamedAI()
    {
        if (this.avoidEntity == null)
        {
            this.avoidEntity = new EntityAIAvoidEntity(this, EntityPlayer.class, 16.0F, 1.0D, 2.0D);
        }
        if (this.aiPanic == null)
        {
            this.aiPanic = new EntityAIPanic(this, 1.2D);
        }
        if (this.aiHerd == null)
        {
            this.aiHerd =  new EntityAIHerdMovement(this, 1.0D);
        }

        this.tasks.removeTask(this.avoidEntity);
        this.tasks.removeTask(this.aiPanic);
        this.tasks.removeTask(this.aiHerd);
        
        
        this.tasks.addTask(2, this.aiHerd);
        
        if (!this.isTame())
        {
        	if (this.getHappiness() <= -50)
        	{
        		this.tasks.addTask(5, this.avoidEntity);        		
        	}
            
    		this.tasks.addTask(1, this.aiPanic);
        }
    }
    

    @Override
	protected void entityInit()
    {
        super.entityInit();
        this.dataManager.register(STATUS, Byte.valueOf((byte)0));
        this.dataManager.register(OLD_AGE, Integer.valueOf(0));
        this.dataManager.register(SEX, Integer.valueOf(0));
        this.dataManager.register(HUNGER, Float.valueOf(0));
        this.dataManager.register(HAPPINESS, Integer.valueOf(0));
        this.dataManager.register(TAMABILITY, Integer.valueOf(0));
        this.dataManager.register(SHEARED, Boolean.valueOf(false));
        this.dataManager.register(OWNER_UNIQUE_ID, Optional.<UUID>absent());

        //this.dataManager.register(DATA_ID_CHEST, Boolean.valueOf(false));
    }

    
    @Override
    public String getZoopediaName()
    {
    	return "Donkey";
    }

    @Override
	@Nullable
    protected ResourceLocation getLootTable()
    {
        return LootTableList.ENTITIES_DONKEY;
    }

    @Override
	protected SoundEvent getAmbientSound()
    {
        super.getAmbientSound();
        return SoundEvents.ENTITY_DONKEY_AMBIENT;
    }

    @Override
	protected SoundEvent getDeathSound()
    {
        super.getDeathSound();
        return SoundEvents.ENTITY_DONKEY_DEATH;
    }

    @Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
    {
        super.getHurtSound(damageSourceIn);
        return SoundEvents.ENTITY_DONKEY_HURT;
    }

    protected SoundEvent getAngrySound()
    {
        super.getAngrySound();
        return SoundEvents.ENTITY_DONKEY_ANGRY;
    }
    
    //@SideOnly(Side.CLIENT) 
    private void setHorseTexturePaths()
    {
        int i = 0;
        int j = (i & 255) % 7;
        int k = ((i & 65280) >> 8) % 5;
        String texture = null;
        this.horseTexturesArray[0] = "faunaandecology:textures/entity/horse/donkey.png";
        this.horseTexturesArray[1] = null;
        this.horseTexturesArray[2] = texture;
        this.texturePrefix = "horse/" + "don" + "don" + texture;
    }

    private net.minecraftforge.items.IItemHandler itemHandler = null; // Initialized by initHorseChest above.

    @SideOnly(Side.CLIENT)
    public String getHorseTexture()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.texturePrefix;
    }

    @SideOnly(Side.CLIENT)
    public String[] getVariantTexturePaths()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.horseTexturesArray;
    }

    @Override
	public double getHorseJumpStrength()
    {
        return this.getEntityAttribute(JUMP_STRENGTH).getAttributeValue();
    }
    
    @Override
	public int getMaxTemper()
    {
    	return 200 - ( this.getTamability() * 5 );
    }

    @Override
	public float getModifiedMaxHealth()
    {
    	return 14.0F + this.rand.nextInt(7) + this.rand.nextInt(7);
    }

    @Override
	public double getModifiedMovementSpeed()
    {
    	return (0.44999998807907104D + this.rand.nextDouble() * 0.3D + this.rand.nextDouble() * 0.3D + this.rand.nextDouble() * 0.3D) * 0.3D;
    }

    @Override
	public boolean wearsArmor()
    {
        return false;
    }

    public boolean isShearable(ItemStack item, net.minecraft.world.IBlockAccess world, BlockPos pos){ return false; }
    
    /**
     * Called only once on an entity when first time spawned, via egg, mob spawner, natural spawning etc, but not called
     * when entity is reloaded from nbt. Mainly used for initializing attributes and inventory
     */
    @Override
	@Nullable
    public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty, @Nullable IEntityLivingData livingdata)
    {
        //livingdata = super.onInitialSpawn(difficulty, livingdata);
        int i = 1;

        //this.setHorseVariant(i | 0 << 8);
            
        int randroll = this.rand.nextInt(2);
		if (Config.enableSexes == true)
		{
	        if (randroll == 1)
			{
	        	this.setSex(Integer.valueOf(1));	
			}
	        else
	        {
	            this.setSex(Integer.valueOf(0));
	        }
	        randroll = this.rand.nextInt(2);
        }
		
		randroll = this.rand.nextInt(2);
        if (randroll == 1)
		{
        	this.setTamability(Integer.valueOf(-4));
		}
        else
        {
    		this.setTamability(Integer.valueOf(-5));
        }

        this.setHunger(foodMax);
		this.setHappiness(0);
		

        if (this.rand.nextInt(5) == 0)
        {
            this.setGrowingAge(childAge);
            this.setOldAge(0);
        }
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.getModifiedMaxHealth());
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(this.getModifiedMovementSpeed());
        this.getEntityAttribute(JUMP_STRENGTH).setBaseValue(this.getModifiedJumpStrength());
        
        this.setHealth(this.getMaxHealth());
        this.setupTamedAI();
        this.setHorseTexturePaths();
        this.initHorseChest();
        return livingdata;
    }
    //////////////////////

    public static void registerFixesHorse(DataFixer fixer)
    {
    	AbstractChestHorse.registerFixesAbstractChestHorse(fixer, EntityImprovedDonkey.class);
    }

    public static void registerFixesDonkey(DataFixer fixer)
    {
    	AbstractChestHorse.registerFixesAbstractChestHorse(fixer, EntityImprovedDonkey.class);
    }
//
//    public boolean hasChest()
//    {
//        return ((Boolean)this.dataManager.get(DATA_ID_CHEST)).booleanValue();
//    }
//
//    public void setChested(boolean chested)
//    {
//        this.dataManager.set(DATA_ID_CHEST, Boolean.valueOf(chested));
//    }

    @Override
	protected int getInventorySize()
    {
        return this.hasChest() ? 17 : 2;
    }

    /**
     * Returns the Y offset from the entity's position for any entity riding this one.
     */
    @Override
	public double getMountedYOffset()
    {
        return super.getMountedYOffset() - 0.25D;
    }
    

    /**
     * Called when the mob's health reaches 0.
     */
    @Override
	public void onDeath(DamageSource cause)
    {
        super.onDeath(cause);

        if (this.hasChest())
        {
            if (!this.world.isRemote)
            {
                this.dropItem(Item.getItemFromBlock(Blocks.CHEST), 1);
            }

            this.setChested(false);
        }
    }
    

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    @Override
	public void writeEntityToNBT(NBTTagCompound compound)
    {
        super.writeEntityToNBT(compound);
        compound.setInteger("Temper", this.getTemper());

        compound.setInteger("Sex", this.getSex());
        compound.setFloat("Hunger", this.getHunger());
        compound.setInteger("Tamability", this.getTamability());
        compound.setInteger("Happiness", this.getHappiness());
        
        compound.setInteger("ParentTamability", this.parentTamability);
        compound.setFloat("ParentMaxHealth", this.parentMaxHealth);
        compound.setFloat("ParentSpeed", this.parentSpeed);
        compound.setDouble("ParentJump", this.parentJump);
        compound.setString("ParentType", this.parentType);
        compound.setInteger("ParentVariant", this.parentVariant);
        
        compound.setInteger("PregnancyTime", this.getPregnancyTime());
        compound.setBoolean("ChestedHorse", this.hasChest());
        

        compound.setBoolean("Tame", this.isTame());

        if (this.getOwnerId() != null)
        {
            compound.setString("OwnerUUID", this.getOwnerId().toString());
        }
        if (!this.horseChest.getStackInSlot(1).isEmpty())
        {
            compound.setTag("ArmorItem", this.horseChest.getStackInSlot(1).writeToNBT(new NBTTagCompound()));
        }

        if (this.hasChest())
        {
            NBTTagList nbttaglist = new NBTTagList();

            for (int i = 2; i < this.horseChest.getSizeInventory(); ++i)
            {
                ItemStack itemstack = this.horseChest.getStackInSlot(i);

                if (!itemstack.isEmpty())
                {
                    NBTTagCompound nbttagcompound = new NBTTagCompound();
                    nbttagcompound.setByte("Slot", (byte)i);
                    itemstack.writeToNBT(nbttagcompound);
                    nbttaglist.appendTag(nbttagcompound);
                }
            }

            compound.setTag("Items", nbttaglist);
        }
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    @Override
	public void readEntityFromNBT(NBTTagCompound compound)
    {
        super.readEntityFromNBT(compound);
        this.setTemper(compound.getInteger("Temper"));     
        
        this.setSex(compound.getInteger("Sex"));
        this.setHunger(compound.getFloat("Hunger"));
        this.setTamability(compound.getInteger("Tamability"));
        this.setHappiness(compound.getInteger("Happiness"));

        this.parentTamability = (compound.getInteger("ParentTamability"));
        this.parentMaxHealth =  (compound.getFloat("ParentMaxHealth"));
        this.parentJump =  (compound.getDouble("ParentJump"));
        this.parentSpeed =  (compound.getFloat("ParentSpeed"));
        this.parentType =  (compound.getString("ParentType"));
        this.parentVariant =  (compound.getInteger("ParentVariant"));

        this.pregnancyTime = (compound.getInteger("PregnancyTime"));

        compound.setInteger("PregnancyTime", this.getPregnancyTime());
        

        this.setHorseTamed(compound.getBoolean("Tame"));
        String s;

        if (compound.hasKey("OwnerUUID", 8))
        {
            s = compound.getString("OwnerUUID");
        }
        else
        {
            String s1 = compound.getString("Owner");
            s = PreYggdrasilConverter.convertMobOwnerIfNeeded(this.getServer(), s1);
        }

        if (!s.isEmpty())
        {
            this.setOwnerId(UUID.fromString(s));
        }
        
        this.setChested(compound.getBoolean("ChestedHorse"));

        if (this.hasChest())
        {
            NBTTagList nbttaglist = compound.getTagList("Items", 10);
            this.initHorseChest();

            for (int i = 0; i < nbttaglist.tagCount(); ++i)
            {
                NBTTagCompound nbttagcompound = nbttaglist.getCompoundTagAt(i);
                int j = nbttagcompound.getByte("Slot") & 255;

                if (j >= 2 && j < this.horseChest.getSizeInventory())
                {
                    this.horseChest.setInventorySlotContents(j, new ItemStack(nbttagcompound));
                }
            }
        }
        IAttributeInstance iattributeinstance = this.getAttributeMap().getAttributeInstanceByName("Speed");

        if (iattributeinstance != null)
        {
            this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(iattributeinstance.getBaseValue() * 0.25D);
        }

        this.setupTamedAI();


        this.updateHorseSlots();
    }

    
    public boolean isAdultHorse()
    {
        return !this.isChild();
    }

    public boolean isRidable()
    {
        return this.isAdultHorse();
    }

    @Override
	@Nullable
    public UUID getOwnerId()
    {
        return (UUID)((Optional)this.dataManager.get(OWNER_UNIQUE_ID)).orNull();
    }

    @Override
	public void setOwnerId(@Nullable UUID uniqueId)
    {
        this.dataManager.set(OWNER_UNIQUE_ID, Optional.fromNullable(uniqueId));
    }

    @Override
	protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.getModifiedMaxHealth());
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(this.getModifiedMovementSpeed());
        this.getEntityAttribute(JUMP_STRENGTH).setBaseValue(this.getModifiedJumpStrength());
    }

    @Override
    public void eatGrassBonus()
    {
        this.setHunger(this.getHunger() + 4);
        this.setHappiness(this.getHappiness() + 1);
        return;
    }

    @Override
    public EntityAgeable createChild(EntityAgeable ageable)
    {
    	EntityImprovedHorse entityhorse = (EntityImprovedHorse)ageable;
    	IEntityAdvancedHorse entityhorse1;
        
        //Domestication process
        int newTamability = (entityhorse.getTamability() + this.getTamability()) / 2;
        int randroll = this.rand.nextInt(2) + 1;
        if (this.isTame() || entityhorse.isTame())
        {
        	newTamability += randroll;	
        }
        else
        {
        	newTamability -= randroll;
        }
        newTamability = MathHelper.clamp(newTamability, -5, 5);
        newTamability = MathHelper.clamp(newTamability, -5, 5);
        //


    	if ((entityhorse.getZoopediaName() == "Donkey") || (entityhorse.getZoopediaName() == "Wild Ass") && (this.getZoopediaName() == "Donkey") || (this.getZoopediaName() == "Wild Ass"))
    	{
        	if (newTamability >= 0 || ((entityhorse.getZoopediaName() == "Donkey") && (this.getZoopediaName() == "Donkey") && (Config.reversion == false) ))
			{
        		entityhorse1 = new EntityImprovedDonkey(this.world);
			}
        	else
        	{
            	entityhorse1 = new EntityWildHorse(this.world);
        	}
    	}
    	else if ((entityhorse.getZoopediaName() == "Llama") || (entityhorse.getZoopediaName() == "Guanaco") && (this.getZoopediaName() == "Llama") || (this.getZoopediaName() == "Guanaco"))
    	{
        	if (newTamability >= 0  || ((entityhorse.getZoopediaName() == "Llama") && (this.getZoopediaName() == "Llama") && (Config.reversion == false) ) )
			{
        		entityhorse1 = new EntityImprovedLlama(this.world);
			}
        	else
        	{
            	entityhorse1 = new EntityGuanaco(this.world);
        	}
    	}
        else if ((this.getZoopediaName() == "Donkey") && (entityhorse.getZoopediaName() == "Zebra"))
        {
        	entityhorse1 = new EntityZebra(this.world);
        }
        else if ((this.getZoopediaName() == "Donkey") && (entityhorse.getZoopediaName() == "Horse"))
        {
        	entityhorse1 = new EntityImprovedMule(this.world);
        }         //failsafe         //failsafe
        else entityhorse1 = new EntityImprovedHorse(this.world);
        //
    	
        //check nearby players for taming
        List<EntityPlayer> players = this.world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(9.0D));
		for (Entity e : players)
		{
			if (e != null)
			{
				entityhorse1.setTamedByPlayer((EntityPlayer) e);
				//entityhorse1.setOwnerId(e.getPersistentID());
				//entityhorse1.setTamed(true);
			}
		}
        entityhorse1.setTamability(newTamability);
        

                
        int j = this.rand.nextInt(9);
        int i;

        if (j < 4)
        {
            i = 0 & 255;
            //if (this.getImprovedType() == ImprovedHorseType.ZEBRA) i = entityhorse.getHorseVariant() & 255; //zorses use horse parent's texture
        }
        else if (j < 8)
        {
            i = entityhorse.getHorseVariant() & 255;
            //if (entityhorse.getImprovedType() == ImprovedHorseType.ZEBRA) i = this.getHorseVariant() & 255; //zorses use horse parent's texture
        }
        else
        {
            i = this.rand.nextInt(7);
        }

        int k = this.rand.nextInt(5);

        if (k < 2)
        {
            i = i | 0 & 65280;
        }
        else if (k < 4)
        {
            i = i | entityhorse.getHorseVariant() & 65280;
        }
        else
        {
            i = i | this.rand.nextInt(5) << 8 & 65280;
        }
        
        ((EntityImprovedHorse)entityhorse1).setHorseVariant(i);

		if (Config.newHorseBreeding == true)
		{
	        //entityhorse1.setType(horsetype2);
	        
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue();
	        entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(Math.round(d1 / 1.5D + (this.rand.nextDouble() * 0.5D)));
	        if (entityhorse1 instanceof EntityImprovedDonkey)
	        {	
	        	if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 28.0D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(28.0D);
	        }
	        else if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 36.0D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(36.0D);
	        
	        double d2 = this.getEntityAttribute(JUMP_STRENGTH).getBaseValue() + ageable.getEntityAttribute(JUMP_STRENGTH).getBaseValue();
	        entityhorse1.getEntityAttribute(JUMP_STRENGTH).setBaseValue(d2 / (1.0D + (this.rand.nextDouble() * 1.0D)));
	        
        	if (entityhorse1.getEntityAttribute(JUMP_STRENGTH).getBaseValue() > 0.460000006D) entityhorse1.getEntityAttribute(JUMP_STRENGTH).setBaseValue(0.460000006D);
        	if ((entityhorse1 instanceof EntityImprovedDonkey) || (entityhorse1 instanceof EntityImprovedDonkey))	entityhorse1.getEntityAttribute(JUMP_STRENGTH).setBaseValue(0.5D);
        	
	        double d0 = this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue();
	        entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(d0 / (1.0D + (this.rand.nextDouble() * 1.0D)));
	        if  ((entityhorse1.getClass() == EntityImprovedHorse.class) ||  (entityhorse1 instanceof EntityWildHorse))
	        {
	        	if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() > 0.3375D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.3375D);	
	        }
	        else if (entityhorse1 instanceof EntityZebra)
	        {	
	        	if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() > 0.405D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.405D);
	        }
	        else entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.17499999701976776D);
	       
		}
		else
		{
	        //entityhorse1.setType(horsetype2);
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.getModifiedMaxHealth();
	        entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(d1 / 3.0D);
	        double d2 = this.getEntityAttribute(JUMP_STRENGTH).getBaseValue() + ageable.getEntityAttribute(JUMP_STRENGTH).getBaseValue() + this.getModifiedJumpStrength();
	        entityhorse1.getEntityAttribute(JUMP_STRENGTH).setBaseValue(d2 / 3.0D);
	        double d0 = this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() + this.getModifiedMovementSpeed();
	        entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(d0 / 3.0D);
		}

		EntityAgeable entityreturnhorse = (EntityAgeable) entityhorse1;

		if (Config.enableSexes == true)
		{
			entityhorse1.setSex(Integer.valueOf(this.rand.nextInt(2)));			
		}

		entityhorse1.setHunger(foodMax);
		entityhorse1.setHappiness(0);
        
        return entityreturnhorse;
    }
    

    public EntityAgeable createChild()
    {
    	IEntityAdvancedHorse entityhorse1;
        int newTamability = (this.parentTamability + this.getTamability()) / 2;

        int randroll = this.rand.nextInt(2) + 1;
        if (this.isTame())
        {
        	newTamability += randroll;	
        }
        else
        {
        	newTamability -= randroll;
        }
        newTamability = MathHelper.clamp(newTamability, -5, 5);
        //

    	if ((this.parentType == "Donkey") || (this.parentType == "Wild Ass") && (this.getZoopediaName() == "Donkey") || (this.getZoopediaName() == "Wild Ass"))
    	{
        	if (newTamability >= 0  || ((this.getClass() == EntityImprovedDonkey.class) && (Config.reversion == false) ))
			{
        		entityhorse1 = new EntityImprovedDonkey(this.world);
			}
        	else
        	{
            	entityhorse1 = new EntityWildAss(this.world);
        	}
    	}
    	else if ((this.parentType == "Llama") || (this.parentType == "Guanaco") && (this.getZoopediaName() == "Llama") || (this.getZoopediaName() == "Guanaco"))
    	{
        	if (newTamability >= 0  || ((this.getClass() == EntityImprovedLlama.class) && (Config.reversion == false) ))
			{
        		entityhorse1 = new EntityImprovedLlama(this.world);
			}
        	else
        	{
            	entityhorse1 = new EntityGuanaco(this.world);
        	}
    	}
        else if ((this.getZoopediaName() == "Donkey") && (this.parentType == "Zebra"))
        {
        	entityhorse1 = new EntityZebra(this.world);
        }
        else if ((this.getZoopediaName() == "Donkey") && (this.parentType == "Horse"))
        {
        	entityhorse1 = new EntityZebra(this.world);
        }        
        else entityhorse1 = new EntityImprovedDonkey(this.world);

        
        
        //check nearby players for taming
        List<EntityPlayer> players = this.world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(9.0D));
		for (Entity e : players)
		{
			if (e != null)
			{

				entityhorse1.setTamedByPlayer((EntityPlayer) e);
				//entityhorse1.setOwnerId(e.getPersistentID());
				//entityhorse1.setTamed(true);
			}
		}
        entityhorse1.setTamability(newTamability);
        
        
        int j = this.rand.nextInt(9);
        int i;

        if (j < 4)
        {
            i = 0 & 255;
            //if (this.getImprovedType() == ImprovedHorseType.ZEBRA) i = this.parentVariant & 255; //zorses use horse parent's texture
        }
        else if (j < 8)
        {
            i = this.parentVariant & 255;
            //if (horsetype1 == ImprovedHorseType.ZEBRA) i = this.getHorseVariant() & 255; //zorses use horse parent's texture
        }
        else
        {
            i = this.rand.nextInt(7);
        }

        int k = this.rand.nextInt(5);

        if (k < 2)
        {
            i = i | 0 & 65280;
        }
        else if (k < 4)
        {
            i = i |  this.parentVariant & 65280;
        }
        else
        {
            i = i | this.rand.nextInt(5) << 8 & 65280;
        }

        //((EntityImprovedHorse)entityhorse1).setHorseVariant(i);
        
        
        
		if (Config.newHorseBreeding == true)
		{
	        
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.parentMaxHealth;
	        entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(Math.round(d1 / 1.5D + (this.rand.nextDouble() * 0.5D)));
	        if (entityhorse1.getClass() == EntityImprovedDonkey.class)
	        {	
	        	if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 28.0D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(28.0D);
	        }
	        else if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 36.0D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(36.0D);
	        
	        double d2 = this.getEntityAttribute(JUMP_STRENGTH).getBaseValue() + this.parentJump;
	        entityhorse1.getEntityAttribute(JUMP_STRENGTH).setBaseValue(d2 / (1.0D + (this.rand.nextDouble() * 1.0D)));
	        
        	if (entityhorse1.getEntityAttribute(JUMP_STRENGTH).getBaseValue() > 0.460000006D) entityhorse1.getEntityAttribute(JUMP_STRENGTH).setBaseValue(0.460000006D);
        	if ((entityhorse1.getClass() == EntityImprovedDonkey.class) || (entityhorse1.getClass() == EntityImprovedDonkey.class))	entityhorse1.getEntityAttribute(JUMP_STRENGTH).setBaseValue(0.5D);
        	
	        double d0 = this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() + this.parentSpeed;
	        entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(d0 / (1.0D + (this.rand.nextDouble() * 1.0D)));
	        if  ((entityhorse1.getClass() == EntityImprovedHorse.class) ||  (entityhorse1 instanceof EntityWildHorse))
	        {
	        	if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() > 0.3375D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.3375D);	
	        }
	        else if (entityhorse1 instanceof EntityZebra)
	        {	
	        	if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() > 0.405D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.405D);
	        }
	        else entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.17499999701976776D);
	        
		}
		else
		{
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.parentMaxHealth + this.getModifiedMaxHealth();
	        entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(d1 / 3.0D);
	        double d2 = this.getEntityAttribute(JUMP_STRENGTH).getBaseValue() + this.parentJump + this.getModifiedJumpStrength();
	        entityhorse1.getEntityAttribute(JUMP_STRENGTH).setBaseValue(d2 / 3.0D);
	        double d0 = this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() + this.parentSpeed + this.getModifiedMovementSpeed();
	        entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(d0 / 3.0D);
		}
		
		EntityAgeable entityreturnhorse = (EntityAgeable) entityhorse1;

		if (Config.enableSexes == true)
		{
			entityhorse1.setSex(Integer.valueOf(this.rand.nextInt(2)));;			
		}

		entityhorse1.setHunger(foodMax);
		entityhorse1.setHappiness(0);
        
		if (entityhorse1 instanceof EntityImprovedDonkey) return entityreturnhorse; 
        return entityreturnhorse;
    }

    @Override
	public boolean replaceItemInInventory(int inventorySlot, ItemStack itemStackIn)
    {
        if (inventorySlot == 499)
        {
            if (this.hasChest() && itemStackIn.isEmpty())
            {
                this.setChested(false);
                this.initHorseChest();
                return true;
            }

            if (!this.hasChest() && itemStackIn.getItem() == Item.getItemFromBlock(Blocks.CHEST))
            {
                this.setChested(true);
                this.initHorseChest();
                return true;
            }
        }

        return super.replaceItemInInventory(inventorySlot, itemStackIn);
    }

    @Override
    public boolean processInteract(EntityPlayer player, EnumHand hand)
    {
        ItemStack itemstack = player.getHeldItem(hand);
        boolean flag = !itemstack.isEmpty();

        if (itemstack != null && itemstack.getItem() == Items.SPAWN_EGG)
        {
            if (!this.world.isRemote)
            {
                Class <? extends Entity > oclass = EntityList.getClass(ItemMonsterPlacer.getNamedIdFrom(itemstack));


                if (oclass != null && this.getClass() == oclass)
                {
                    EntityAgeable entityageable = this.createChild(this);

                    if (entityageable != null)
                    {
                        entityageable.setGrowingAge(childAge);
                        entityageable.setLocationAndAngles(this.posX, this.posY, this.posZ, 0.0F, 0.0F);
                        this.world.spawnEntity(entityageable);

                        if (itemstack.hasDisplayName())
                        {
                            entityageable.setCustomNameTag(itemstack.getDisplayName());
                        }

                        if (!player.capabilities.isCreativeMode)
                        {
                        	itemstack.setCount(itemstack.getCount() -1);
                        }
                    }
                }
            }

            return true;
        }
        else
        {
            if (!this.isChild())
            {

                //
                if (itemstack != null && !this.hasChest() && itemstack.getItem() == Item.getItemFromBlock(Blocks.CHEST))
                {
                    this.setChested(true);
                    this.playChestEquipSound();
                    this.initHorseChest();
                    return true;
                }
                //
                if (this.isTame() && player.isSneaking())
                {
                    this.openGUI(player);
                    return true;
                }

                if (this.isBeingRidden())
                {
                    return super.processInteract(player, hand);
                }
            }


            if (flag)
            {	//
            	if (Config.enableAdvancedStats == true)
            	{
            		if (this.getHunger() < foodMax - 0.5F)
            		{
    	        		if  (Config.enableWildBreeding == true)
    	        		{
    	                    if (this.isBreedingItem(itemstack)  && !(this.isChild()) && !(this.isInLove()) )
    	                    {
    	                        this.consumeItemFromStack(player, itemstack);
    	                        return true;
    	                    }
    	                    else if (this.isBreedingItem(itemstack))
    	                    {
    	                        this.consumeItemFromStack(player, itemstack);
    	                        return true;
    	                    }
    	        		}
    	        		else
    	        		{
    	                    if (this.isBreedingItem(itemstack) && !(this.isChild()) && !(this.isInLove()) )
    	                    {
    	                        this.consumeItemFromStack(player, itemstack);
    	                        this.setInLove(player);
    	                        return true;
    	                    }
    	                    else if (this.isBreedingItem(itemstack))
    	                    {
    	                        this.consumeItemFromStack(player, itemstack);
    	                        return true;
    	                    }
    	        		}
            		}
            	}
                
                if (this.isEdible(itemstack))
                {
                	if  (this.getHunger() <  foodMax - 0.5F)
                	{
    	                this.consumeItemFromStack(player, itemstack);
    	                return true;
                	}
                }
                //


                if (itemstack.interactWithEntity(player, this, hand))
                {
                    return true;
                }

                if (!this.isTame())
                {
                    this.makeMad();
                    return true;
                }

                boolean flag1 = ImprovedHorseArmorType.getByItemStack(itemstack) != ImprovedHorseArmorType.NONE;
                boolean flag2 = !this.isChild() && !this.isHorseSaddled() && itemstack.getItem() == Items.SADDLE;

                if (flag1 || flag2)
                {
                    this.openGUI(player);
                    return true;
                }
            }

            if (this.isChild())
            {
                return super.processInteract(player, hand);
            }
            else
            {
                this.mountTo(player);
                return true;
            }
        }
    }
    

    @Override
	protected void playChestEquipSound()
    {
        this.playSound(SoundEvents.ENTITY_DONKEY_CHEST, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
    }

    @Override
	public int getInventoryColumns()
    {
        return 5;
    }

    @Override
    public void openGUI(EntityPlayer playerEntity)
    {
        if (!this.world.isRemote && (!this.isBeingRidden() || this.isPassenger(playerEntity)) && this.isTame())
        {
        	if (playerEntity instanceof EntityPlayerMP)
        	{
        		EntityPlayerMP playerMP = (EntityPlayerMP) playerEntity; 
                this.horseChest.setCustomName(this.getName());
                //playerEntity.openGuiHorseInventory(this, this.horseChest);
                if (playerMP.openContainer != playerMP.inventoryContainer)
                {
                	playerMP.closeScreen();
                }

                playerMP.getNextWindowId();
                playerMP.connection.sendPacket(new SPacketOpenWindow(playerMP.currentWindowId, "EntityHorse", this.horseChest.getDisplayName(), this.horseChest.getSizeInventory(), this.getEntityId()));
                playerMP.openContainer = new ContainerImprovedHorseInventory(playerMP.inventory, this.horseChest, this, playerMP);
                playerMP.openContainer.windowId = playerMP.currentWindowId;
                playerMP.openContainer.addListener(playerMP);
        	}
        }
    }

    
    /**
     * Returns true if the mob is currently able to mate with the specified mob.
     */
    @Override
    public boolean canMateWith(EntityAnimal otherAnimal)
    {
        if (otherAnimal == this)
        {
            return false;
        }
        else if ((otherAnimal.getClass() == EntityImprovedDonkey.class || otherAnimal instanceof EntityImprovedDonkey) && (this.getClass() == EntityImprovedDonkey.class || this instanceof EntityImprovedDonkey))
        {
        	IEntityAdvancedHorse entitycow = (IEntityAdvancedHorse)otherAnimal;

            if ( this.isInLove() && otherAnimal.isInLove() && (this.getSex() != entitycow.getSex() || (Config.enableSexes == false) ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if ((otherAnimal.getClass() == EntityImprovedHorse.class && this.getClass() == EntityImprovedDonkey.class))
        {
        	IEntityAdvancedHorse entitycow = (IEntityAdvancedHorse)otherAnimal;

            if ( this.isInLove() && otherAnimal.isInLove() && (this.getSex() != entitycow.getSex() || (Config.enableSexes == false) ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if ((otherAnimal instanceof EntityZebra) && (this.getClass() == EntityImprovedDonkey.class))
        {
        	IEntityAdvancedHorse entitycow = (IEntityAdvancedHorse)otherAnimal;

            if ( this.isInLove() && otherAnimal.isInLove() && (this.getSex() != entitycow.getSex() || (Config.enableSexes == false) ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if ((otherAnimal instanceof EntityImprovedLlama) && (this instanceof EntityImprovedLlama))
        {
        	IEntityAdvancedHorse entitycow = (IEntityAdvancedHorse)otherAnimal;

            if ( this.isInLove() && otherAnimal.isInLove() && (this.getSex() != entitycow.getSex() || (Config.enableSexes == false) ) )
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        return false;
    }
    
    @Override
    protected boolean canMate()
    {
    	return !this.isBeingRidden() && !this.isRiding() && this.isTame() && !this.isChild() && this.getHealth() >= this.getMaxHealth() && this.isInLove();
    }
    
	@Override
	public void getOtherParentData(IEntityAdvanced parent)
	{

    	if (parent instanceof EntityImprovedHorse)
    	{
    		EntityImprovedHorse otherParent = (EntityImprovedHorse) parent;
            this.parentTamability = otherParent.getTamability();
            this.parentMaxHealth = otherParent.getMaxHealth();
            this.parentSpeed = otherParent.getAIMoveSpeed();
            this.parentJump = otherParent.getHorseJumpStrength();
            this.parentVariant = otherParent.getHorseVariant();
            this.parentType = otherParent.getZoopediaName();
    	}
    	else
    	{
            this.parentTamability = this.getTamability();
            this.parentMaxHealth = this.getMaxHealth();
            this.parentSpeed = this.getAIMoveSpeed();
            this.parentJump = this.getHorseJumpStrength();
            this.parentVariant = 0;
            this.parentType = this.getZoopediaName();
    	}
		
	}

	@Override
	public float getFoodMax() {
        return foodMax;
	}

    //	/
    @Override
    public int getSex()
    {
        return this.dataManager.get(SEX).intValue();
    }
    
    @Override
	public void setSex(int sexIn)
    {
        this.dataManager.set(SEX, Integer.valueOf(sexIn));
    }

    @Override
	public float getHunger()
    {
        return this.dataManager.get(HUNGER).floatValue();
    }
    
    @Override
	public void setHunger(float hungerIn)
    {
    	//hungerIn = MathHelper.clamp_float(hungerIn, 0.0F, foodMax);
    	if (hungerIn > foodMax) hungerIn = foodMax;
        this.dataManager.set(HUNGER, Float.valueOf(hungerIn));
    }

    @Override
	public int getTamability()
    {

        return this.dataManager.get(TAMABILITY).intValue();
    }
    
    @Override
	public void setTamability(int tamabilityIn)
    {
        this.dataManager.set(TAMABILITY, Integer.valueOf(tamabilityIn));
    }

    @Override
	public int getHappiness()
    {
        return this.dataManager.get(HAPPINESS).intValue();
    }
    
    @Override
	public void setHappiness(int happinessIn)
    {
    	happinessIn = MathHelper.clamp(happinessIn, -100, 100);
    	if (happinessIn > 100) happinessIn = 100;
    	if (happinessIn < -100) happinessIn = -100;
        this.dataManager.set(HAPPINESS, Integer.valueOf(happinessIn));
    }



    /**
     * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
     * use this to react to sunlight and start to burn.
     */
    @Override
    public void onLivingUpdate()
    {
    	if (Config.enablePregnancy && this.getPregnancyTime() > 0)
    	{
    		int p = this.getPregnancyTime();
    		if (p >= EntityImprovedDonkey.pregnancyLength)
    		{
    			//use alt child gen from stored data on other parent
    			EntityAgeable entityageable = this.createChild();
    	        final net.minecraftforge.event.entity.living.BabyEntitySpawnEvent event = new net.minecraftforge.event.entity.living.BabyEntitySpawnEvent(this, this, entityageable);
    	        final boolean cancelled = net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(event);
    	        if (cancelled) {
    	            //Reset the "inLove" state for the animals
    	            this.setGrowingAge(6000);
    	            this.resetInLove();
    	            return;
    	        }

    	        entityageable = event.getChild();

    	        if (entityageable != null)
    	        {
	        		this.setGrowingAge(6000);
    	            this.resetInLove();
    	            
    	            entityageable.setGrowingAge(this.getChildAge());
    	            entityageable.setLocationAndAngles(this.posX, this.posY, this.posZ, 0.0F, 0.0F);

    	            if (entityageable instanceof EntityImprovedHorse)
    	            {
    	            	EntityImprovedHorse spawned = (EntityImprovedHorse) entityageable;
        	            this.world.spawnEntity(spawned);
    	            }
    	            if (entityageable instanceof EntityImprovedDonkey)
    	            {
    	            	EntityImprovedDonkey spawned = (EntityImprovedDonkey) entityageable;
        	            this.world.spawnEntity(spawned);
    	            }
    	            
    	            Random random = this.getRNG();

    	            for (int i = 0; i < 7; ++i)
    	            {
    	                double d0 = random.nextGaussian() * 0.02D;
    	                double d1 = random.nextGaussian() * 0.02D;
    	                double d2 = random.nextGaussian() * 0.02D;
    	                double d3 = random.nextDouble() * this.width * 2.0D - this.width;
    	                double d4 = 0.5D + random.nextDouble() * this.height;
    	                double d5 = random.nextDouble() * this.width * 2.0D - this.width;
    	                this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + d3, this.posY + d4, this.posZ + d5, d0, d1, d2, new int[0]);
    	            }

    	            if (this.world.getGameRules().getBoolean("doMobLoot") && this.isTame())
    	            {
    	                this.world.spawnEntity(new EntityXPOrb(this.world, this.posX, this.posY, this.posZ, random.nextInt(7) + 1));
    	            }
    	        }
    	        
//    	        for (int e = 0; e < this.litterSize; e++)
//    	        {
//        			entityageable = this.createChild((EntityAgeable) parent);
//        	        final net.minecraftforge.event.entity.living.BabyEntitySpawnEvent event2 = new net.minecraftforge.event.entity.living.BabyEntitySpawnEvent(this, (EntityLiving) parent, entityageable);
//
//        	        entityageable = event.getChild();
//    	            entityageable.setGrowingAge(this.getChildAge());
//    	            entityageable.setLocationAndAngles(this.posX, this.posY, this.posZ, 0.0F, 0.0F);
//    	            this.world.spawnEntity(entityageable);
//    	        }
    	        
        		this.setPregnancyTime(0);
    		}
    		else this.setPregnancyTime(p+1);
    	}
    	
        super.onLivingUpdate();
        //new
		this.groupTimer += 1;

		if (this.groupTimer >= 750)
		{
	    	this.setupTamedAI();
	        Random random = this.getRNG();
	    	if (Config.enableAdvancedStats == true)
	    	{
	    		List<EntityAnimal> list = this.world.<EntityAnimal>getEntitiesWithinAABB(AbstractHorse.class, this.getEntityBoundingBox().grow(8.0D));
		        int breedchance = 0;
		        //group size happiness
		        if (list.size() == this.getPreferedGroupSize())
		        {
		        	breedchance = 50;
		        	this.setHappiness(this.getHappiness()+3);
		        }
		        else if (list.size() >= this.getPreferedGroupSize()-1 && list.size() <= this.getPreferedGroupSize()+1 )
		        {
		        	breedchance = 25;
		        	this.setHappiness(this.getHappiness()+1);
		        }
		        else if (list.size() <= this.getPreferedGroupSize()-3 || list.size() >= this.getPreferedGroupSize()+3 )
		        {
		        	this.setHappiness(this.getHappiness()-random.nextInt(2)); //new
		        }

		        //wild breeding
		        if (list.size() >= 2 && list.size() <= this.getPreferedGroupSize() * 2)
		        {
		        	if (Config.enableWildBreeding == true && !(this.isChild()) && !(this.isInLove()) && (this.getGrowingAge() == 0) )
		        	{
		        		if ( (this.getHappiness() + breedchance) >= random.nextInt(100)) this.setInLove(null);
		        	}
		        }	
	    	}
			this.groupTimer = random.nextInt(10);
		}
		if (Config.enableAdvancedStats == true)
		{
        	//aging process
            int i = this.getOldAge();

            ++i;
            this.setOldAge(i);

            if (i == ageMax && Config.enableOldAge == true)
            {
                this.setDead();
            }
        
	        //hungerprocess
	        this.setHunger((float) (this.getHunger() - ((this.getMaxHealth() + this.getHorseJumpStrength() + this.getAIMoveSpeed()) / 50000)) );
	        
	        if (this.getHunger() <= 0 && Config.enableStarvation == true)
	        {
	        	this.damageEntity(DamageSource.STARVE, 1.0F);
	        	this.setHunger(4.0F);
	        	this.setHappiness(this.getHappiness()-10);
	        }
		}
		
        if (!this.world.isRemote)
        {
            if (this.rand.nextInt(900) == 0 && this.deathTime == 0 && this.getHunger() > 3.0F && this.getHealth() < this.getMaxHealth())
            {
                this.heal(1.0F);
                this.setHunger(this.getHunger()-3.0F);
            }

            if (!this.isEatingHaystack() && !this.isBeingRidden() && this.rand.nextInt(300) == 0 && this.world.getBlockState(new BlockPos(MathHelper.floor(this.posX), MathHelper.floor(this.posY) - 1, MathHelper.floor(this.posZ))).getBlock() == Blocks.GRASS)
            {
                this.setEatingHaystack(true);
            }

            if (this.isEatingHaystack() && ++this.eatingHaystackCounter > 50)
            {
                this.eatingHaystackCounter = 0;
                this.setEatingHaystack(false);
            }

            if (this.isBreeding() && !this.isAdultHorse() && !this.isEatingHaystack())
            {
            	AbstractHorse entityhorse = this.getClosestHorse(this, 16.0D);

                if (entityhorse != null && this.getDistance(entityhorse) > 4.0D)
                {
                    this.navigator.getPathToEntityLiving(entityhorse);
                }
            }

        }
    }
    
    @Override
    public boolean setTamedBy(EntityPlayer player)
    {
        this.setOwnerId(player.getUniqueID());
        this.setHorseTamed(true);
        return true;
    }

	@Override
	public boolean getSheared() {
		// TODO Auto-generated method stub
		return false;
	}

    @Override
	public int getOldAge()
    {
        return this.dataManager.get(OLD_AGE).intValue();
    }
    
    @Override
	public void setOldAge(int age)
    {
        this.dataManager.set(OLD_AGE, Integer.valueOf(age));
    }    

    @Override
	public float getGrowthSize()
    {
    	if (this.isChild())
    	{
	    	float growth = EntityImprovedDonkey.childAge + this.getOldAge();
	    	float ratio = growth / EntityImprovedDonkey.childAge;
	    	return 1.0F - ratio;
    	}
    	return 1.0F;
    }
    
	@Override
	public int getPreferedGroupSize() {
		return 2;
	}

    @Override
    public boolean attackEntityFrom(DamageSource par1DamageSource, float par2)
    {
    	this.setHappiness(this.getHappiness()-50);
        return super.attackEntityFrom(par1DamageSource, par2);
    }

    @Override
    public boolean isTame()
    {
        return (this.getOwnerId() != null);
    }

    @Override
    public boolean isTamed()
    {
        return (this.getOwnerId() != null);
    }
    
    @Override
    protected void consumeItemFromStack(EntityPlayer player, ItemStack stack)
    {
    	//new
		if (Config.enableAdvancedStats == true)
		{
			float newHunger = 0;
			int newHappiness = 5;
	    	if (stack.getItem() instanceof ItemFood)
	    	{
	    		ItemFood eatenFood = (ItemFood)stack.getItem();
	    		newHunger = this.getHunger() + eatenFood.getHealAmount(stack) + (eatenFood.getHealAmount(stack)*eatenFood.getSaturationModifier(stack)); 
	    		newHappiness = (int) (this.getHappiness() + eatenFood.getHealAmount(stack) + (eatenFood.getHealAmount(stack)*eatenFood.getSaturationModifier(stack)) );	            
	    		this.setHunger(newHunger);
				this.setHappiness(newHappiness);
	    	}
	    	else
	    	{
				this.eatGrassBonus();
	    	}
	    	
            if (!this.isTame() && this.getTemper() < this.getMaxTemper())
            {
                this.increaseTemper(newHappiness+this.rand.nextInt(5)+5);
                if (this.getTemper() >= this.getMaxTemper())
                {
                	this.setTamedBy(player);
                    double d0 = this.rand.nextGaussian() * 0.025D;
                    double d1 = this.rand.nextGaussian() * 0.025D;
                    double d2 = this.rand.nextGaussian() * 0.025D;
                    this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                }
                else
                {
                    double d0 = this.rand.nextGaussian() * 0.025D;
                    double d1 = this.rand.nextGaussian() * 0.025D;
                    double d2 = this.rand.nextGaussian() * 0.025D;
                    this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
                	this.world.spawnParticle(EnumParticleTypes.VILLAGER_HAPPY, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	
                }
            }
            else
            {
                double d0 = this.rand.nextGaussian() * 0.025D;
                double d1 = this.rand.nextGaussian() * 0.025D;
                double d2 = this.rand.nextGaussian() * 0.025D;
                this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
            	this.world.spawnParticle(EnumParticleTypes.SPELL, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
            }
    	}
    	//
        if (!player.capabilities.isCreativeMode) stack.setCount(stack.getCount() -1);
    }
    
    public boolean isEdible(ItemStack stack)
    {
    	if (EntityImprovedDonkey.TEMPTATION_ITEMS.contains(stack.getItem())) return true;
    	return false;
    }
 
    @Override
	public boolean isEdible(EntityItem entityitem)
	{
    	if (EntityImprovedDonkey.TEMPTATION_ITEMS.contains(entityitem.getItem().getItem())) return true;
    	return false;
	}

	@Override
	public int getChildAge() 
	{
		return EntityImprovedDonkey.childAge;
	}
	
    @Override
    public float getZoopediaHealth()
    {
    	return this.getHealth();
    }
    
    @Override
    public float getZoopediaMaxHealth()
    {
    	return this.getMaxHealth();
    }
    

	@Override
	public ImprovedHorseArmorType getHorseImprovedArmorType() {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
	public int getPregnancyTime()
    {
        return this.pregnancyTime;
    }
    
    @Override
	public void setPregnancyTime(int age)
    {
        this.pregnancyTime = age;
    }


	@Override
	public void setTamed(boolean tamed) {
		// TODO Auto-generated method stub
		
	}    

    /**
     * Checks if the parameter is an item which this animal can be fed to breed it (wheat, carrots or seeds depending on
     * the animal type)
     */
	@Override
    public boolean isBreedingItem(@Nullable ItemStack stack)
    {
        return stack != null && TEMPTATION_ITEMS.contains(stack.getItem());
    }

	@Override
	public boolean setTamedByPlayer(EntityPlayer player) {
        this.setOwnerId(player.getUniqueID());
        this.setHorseTamed(true);
        return true;
	}
    
}

