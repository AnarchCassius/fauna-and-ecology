package faunaandecology.mod.entity.passive;

import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.base.Optional;

import faunaandecology.mod.entity.IEntityAdvancedHorse;
import faunaandecology.mod.util.Config;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.IJumpingMount;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.ai.attributes.RangedAttribute;
import net.minecraft.inventory.IInventoryChangedListener;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityWildHorse extends EntityImprovedHorse implements IInventoryChangedListener, IJumpingMount, IEntityAdvancedHorse, net.minecraftforge.common.IShearable
{
	private static final IAttribute JUMP_STRENGTH = (new RangedAttribute((IAttribute)null, "horse.jumpStrength", 0.7D, 0.0D, 2.0D)).setDescription("Jump Strength").setShouldWatch(true);
    //private static final DataParameter<Optional<UUID>> OWNER_UNIQUE_ID = EntityDataManager.<Optional<UUID>>createKey(EntityWildHorse.class, DataSerializers.OPTIONAL_UNIQUE_ID);

    private static final String[] WILD_HORSE_TEXTURES = new String[] {"faunaandecology:textures/entity/horse/wild_horse_dun.png",
    		"faunaandecology:textures/entity/horse/wild_horse_dun.png", "faunaandecology:textures/entity/horse/wild_horse_dun.png",
    		"faunaandecology:textures/entity/horse/wild_horse_brown.png", "faunaandecology:textures/entity/horse/wild_horse_grullo.png",
    		"faunaandecology:textures/entity/horse/wild_horse_grullo.png", "faunaandecology:textures/entity/horse/wild_horse_brown.png"};
    private static final String[] WILD_COLT_TEXTURES = new String[] {"faunaandecology:textures/entity/horse/wild_horse_dun_juvenile.png",
    		"faunaandecology:textures/entity/horse/wild_horse_dun_juvenile.png", "faunaandecology:textures/entity/horse/wild_horse_dun_juvenile.png",
    		"faunaandecology:textures/entity/horse/wild_horse_brown_juvenile.png", "faunaandecology:textures/entity/horse/wild_horse_grullo_juvenile.png",
    		"faunaandecology:textures/entity/horse/wild_horse_grullo_juvenile.png", "faunaandecology:textures/entity/horse/wild_horse_brown_juvenile.png"};
    private static final String[] HORSE_TEXTURES_ABBR = new String[] {"whwh", "whcr", "whch", "whbr", "whbl", "whgr", "whdb"};
    private static final String[] COLT_TEXTURES_ABBR = new String[] {"wcwh", "wccr", "wcch", "wcbr", "wcbl", "wcgr", "wcdb"};

    private static final String[] HORSE_MARKING_TEXTURES = new String[] {null, "faunaandecology:textures/entity/horse/horse_markings_white.png",
    		"faunaandecology:textures/entity/horse/horse_markings_whitefield.png", "faunaandecology:textures/entity/horse/horse_markings_whitedots.png",
    		"faunaandecology:textures/entity/horse/horse_markings_blackdots.png",
    		"faunaandecology:textures/entity/horse/horse_markings_stripes_0.png", "faunaandecology:textures/entity/horse/horse_markings_stripes_1.png",
    		"faunaandecology:textures/entity/horse/horse_markings_stripes_2.png", "faunaandecology:textures/entity/horse/horse_markings_stripes_3.png",
    		"faunaandecology:textures/entity/horse/horse_markings_stripes_4.png", "faunaandecology:textures/entity/horse/horse_markings_stripes_5.png",
    		"faunaandecology:textures/entity/horse/horse_markings_stripes_6.png", "faunaandecology:textures/entity/horse/horse_markings_stripes_7.png"};
    private static final String[] HORSE_MARKING_TEXTURES_ABBR = new String[] {"", "wo_", "wmo", "wdo", "bdo", "st0", "st1", "st2", "st3", "st4", "st5", "st6", "st7"}; 
    
    static public int pregnancyLength = 24000;
    static public float foodMax = 80.0F;
    static public int ageMax = 720000; //30 'years'
    static public int childAge = -96000; //2000 = 1 month
    //
    
    private String texturePrefix;
    private final String[] wildHorseTexturesArray = new String[3];


    public EntityWildHorse(World worldIn)
    {
        super(worldIn);
        this.setSize(1.3964844F, 1.6F);
        this.isImmuneToFire = false;
        this.stepHeight = 1.0F;
        this.initHorseChest();

        //new
        //this.setupTamedAI();
    }
    
    @Override
    public String getZoopediaName()
    {
    	return "Wild Horse";
    }

    @Override
	protected void entityInit()
    {
        super.entityInit();
        //this.dataManager.register(OWNER_UNIQUE_ID, Optional.<UUID>absent());

    }


    @Override
	public int getMaxTemper()
    {
    	return 150 - ( this.getTamability() * 5 );
    }

    @Override
	public float getModifiedMaxHealth()
    {
//    	 if (this.getImprovedType() == ImprovedHorseType.QUAGGA || this.getImprovedType() == ImprovedHorseType.DONKEY || this.getImprovedType() == ImprovedHorseType.WILD_ASS)
//        {
//    		return 14.0F + (float)this.rand.nextInt(7) + (float)this.rand.nextInt(7);
//        }
//    	else if (this.getImprovedType() == ImprovedHorseType.ZEBRA || this.getImprovedType() == ImprovedHorseType.WILD_HORSE || this.getImprovedType() == ImprovedHorseType.ZONKEY)
//        {
//    		return 15.0F + (float)this.rand.nextInt(8) + (float)this.rand.nextInt(9);
//        }
//    	else
//    	{
    	return 15.0F + this.rand.nextInt(8) + this.rand.nextInt(9);
    }

	@SideOnly(Side.CLIENT) 
    private void setHorseTexturePaths()
    {
        int i = this.getHorseVariant();
        int j = (i & 255) % 7;
        int k = ((i & 65280) >> 8) % 13;
        String texture = null; //If armorStack is empty, the server is vanilla so the texture should be determined the vanilla way
        if (this.isChild()) this.wildHorseTexturesArray[0] = WILD_COLT_TEXTURES[j];
        else this.wildHorseTexturesArray[0] = WILD_HORSE_TEXTURES[j];
        this.wildHorseTexturesArray[1] = HORSE_MARKING_TEXTURES[k];
        this.wildHorseTexturesArray[2] = texture;
        if (this.isChild()) this.texturePrefix = "horse/" + COLT_TEXTURES_ABBR[j] + HORSE_MARKING_TEXTURES_ABBR[k] + texture;
        else this.texturePrefix = "horse/" + HORSE_TEXTURES_ABBR[j] + HORSE_MARKING_TEXTURES_ABBR[k] + texture;
    }

    @Override
	@SideOnly(Side.CLIENT)
    public String getHorseTexture()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.texturePrefix;
    }

    @Override
	@SideOnly(Side.CLIENT)
    public String[] getVariantTexturePaths()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.wildHorseTexturesArray;
    }

    /**
     * Called only once on an entity when first time spawned, via egg, mob spawner, natural spawning etc, but not called
     * when entity is reloaded from nbt. Mainly used for initializing attributes and inventory
     */
    @Override
	@Nullable
    public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty, @Nullable IEntityLivingData livingdata)
    {
    	//livingdata = super.onInitialSpawn(difficulty, livingdata);
        //this.setHorseVariant(i | 0 << 8);

        int randroll = this.rand.nextInt(2);
		if (Config.enableSexes == true)
		{
	        if (randroll == 1)
			{
	        	this.setSex(Integer.valueOf(1));	
			}
	        else
	        {
	            this.setSex(Integer.valueOf(0));
	        }
	        randroll = this.rand.nextInt(2);
        }
		
		randroll = this.rand.nextInt(2);
        if (randroll == 1)
		{
        	this.setTamability(Integer.valueOf(-4));
		}
        else
        {
    		this.setTamability(Integer.valueOf(-5));
        }

        this.setHunger(foodMax);
		this.setHappiness(0);
		

        if (this.rand.nextInt(5) == 0)
        {
            this.setGrowingAge(childAge);
            this.setOldAge(0);
        }
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.getModifiedMaxHealth());
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(this.getModifiedMovementSpeed());
        this.getEntityAttribute(JUMP_STRENGTH).setBaseValue(this.getModifiedJumpStrength());
        
        this.setHealth(this.getMaxHealth());
        this.setupTamedAI();
        if (this.world.isRemote)
        {
        	this.setHorseTexturePaths();
        }
    	this.getBiomeHorseType();
        return livingdata;
    }
    //////////////////////

    @Override
	public boolean wearsArmor()
    {
        return false;
    }
 
    public int getBiomeHorseType()
    {
        Biome biome = this.world.getBiome(new BlockPos(this));
        
        if (BiomeDictionary.hasType(biome, Type.FOREST))
        {

               this.setHorseVariant(5 | this.rand.nextInt(5)+8 << 8);
            	return 5;
        }
        else if (BiomeDictionary.hasType(biome, Type.COLD))
        {

            this.setHorseVariant(6 | 5 << 8);
        	return 6;
        }
        else
        {
        	this.setHorseVariant(1 | this.rand.nextInt(5)+6 << 8);
            return 1;
        }
    }
 
    public int getBiomeHorseType(Biome biome)
    {
        //Biome biome = this.world.getBiome(new BlockPos(this));
        
        if (BiomeDictionary.hasType(biome, Type.FOREST))
        {

               this.setHorseVariant(5 | this.rand.nextInt(5)+8 << 8);
               return this.rand.nextInt(5)+8;
        }
        else if (BiomeDictionary.hasType(biome, Type.COLD))
        {

            this.setHorseVariant(6 | 5 << 8);
        	return 5;
        }
        else
        {
        	this.setHorseVariant(1 | this.rand.nextInt(5)+6 << 8);
            return this.rand.nextInt(5)+8;
        }
    }


    
}
