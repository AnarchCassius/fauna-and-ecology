package faunaandecology.mod.entity.passive;

import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityZonkey extends EntityImprovedDonkey
{
    private String texturePrefix;
    private final String[] horseTexturesArray = new String[3];

	public EntityZonkey(World worldIn) {
		super(worldIn);
		// TODO Auto-generated constructor stub
	}
	
    @Override
    public String getZoopediaName()
    {
    	return "Mule";
    }

    @SideOnly(Side.CLIENT) 
    private void setHorseTexturePaths()
    {
        String texture = null;
        this.horseTexturesArray[0] = "faunaandecology:textures/entity/horse/zonkey.png";
        this.horseTexturesArray[1] = null;
        this.horseTexturesArray[2] = texture;
        this.texturePrefix = "horse/" + "zon" + "zon" + texture;
    }


    @Override
	@SideOnly(Side.CLIENT)
    public String getHorseTexture()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.texturePrefix;
    }

    @Override
	@SideOnly(Side.CLIENT)
    public String[] getVariantTexturePaths()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.horseTexturesArray;
    }
    
    @Override
	public int getMaxTemper()
    {
    	return 175 - ( this.getTamability() * 5 );
    }

    @Override
	public float getModifiedMaxHealth()
    {
    	return 15.0F + this.rand.nextInt(8) + this.rand.nextInt(9);
    }


}
