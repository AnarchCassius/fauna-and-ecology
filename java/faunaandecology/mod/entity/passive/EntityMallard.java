package faunaandecology.mod.entity.passive;

import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.collect.Sets;

import faunaandecology.mod.entity.IEntityAdvanced;
import faunaandecology.mod.entity.ai.EntityAIFlyingTempt;
import faunaandecology.mod.entity.ai.EntityAIForager;
import faunaandecology.mod.entity.ai.EntityAIGrainEater;
import faunaandecology.mod.entity.ai.EntityAIHerdMovement;
import faunaandecology.mod.entity.ai.EntityAIImprovedFollowParent;
import faunaandecology.mod.entity.ai.EntityAIImprovedMate;
import faunaandecology.mod.entity.ai.EntityAIItemSearch;
import faunaandecology.mod.entity.ai.EntityAIWanderFlying;
import faunaandecology.mod.init.ItemInit;
import faunaandecology.mod.util.Config;
import faunaandecology.mod.util.handlers.SoundsHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIFollowParent;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIMate;
import net.minecraft.entity.ai.EntityAIPanic;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAITempt;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWanderAvoidWater;
import net.minecraft.entity.ai.EntityAIWanderAvoidWaterFlying;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityFlying;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class EntityMallard extends EntityJungleFowl implements EntityFlying
{
    private static final Set<Item> TEMPTATION_ITEMS = Sets.newHashSet(Items.WHEAT_SEEDS, Items.MELON_SEEDS, Items.PUMPKIN_SEEDS, Items.BEETROOT_SEEDS);

    private EntityAIPanic aiPanic;
    private EntityAIHerdMovement aiHerd;
    
    //private boolean sheared;
    private int groupTimer = 0;
    static public int litterSize = 4;
    static public int pregnancyLength = 15200;
	static public float foodMax = 50.0F;
    static public int ageMax = 480000; //20 'years' //2000 = 1 month
    static public int childAge = -36000; //18 'monthes' 

    //private static final DataParameter<Integer> OLD_AGE = EntityDataManager.<Integer>createKey(EntityMallard.class, DataSerializers.VARINT);
    //private static final DataParameter<Integer> SEX = EntityDataManager.<Integer>createKey(EntityMallard.class, DataSerializers.VARINT);
    //private static final DataParameter<Integer> TAMABILITY = EntityDataManager.<Integer>createKey(EntityMallard.class, DataSerializers.VARINT);
    //private static final DataParameter<Float> HUNGER = EntityDataManager.<Float>createKey(EntityMallard.class, DataSerializers.FLOAT);
    //private static final DataParameter<Integer> HAPPINESS = EntityDataManager.<Integer>createKey(EntityMallard.class, DataSerializers.VARINT);


    private int parentTamability;
    private float parentMaxHealth;
    
	public EntityMallard(World worldIn) {
		super(worldIn);
        //this.setSize(0.6F, 0.7F);
        //this.timeUntilRealEgg = this.rand.nextInt(40000) + 40000;
        //this.setPathPriority(PathNodeType.WATER, 0.0F);
    }

    @Override
	protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(3.0D);
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.20000000298023224D);
        //this.getAttributeMap().registerAttribute(SharedMonsterAttributes.FLYING_SPEED);
        this.getEntityAttribute(SharedMonsterAttributes.FLYING_SPEED).setBaseValue(0.95D);
    }
	
    @Override
	protected float getWaterSlowDown()
    {
        return 0.98F;
    }

    /**
     * Moves the entity based on the specified heading.
     */
    public void moveEntityWithHeading(float strafe, float forward)
    {
        this.stepHeight = 0.5F;
        if (this.isInWater()) this.jumpMovementFactor = 0.004F;
        else this.jumpMovementFactor = 0.03F;

        super.travel(strafe, 0.0F, forward);
    }

    @Override
    public String getZoopediaName()
    {
    	return "Mallard";
    }
    
    /**
     * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
     * use this to react to sunlight and start to burn.
     */
    @Override
	public void onLivingUpdate()
    {
        if (!this.isChild() && !this.isChickenJockey() && --this.timeUntilRealEgg <= 0)
        {
        	if (this.getSex() == 0) this.playSound(SoundEvents.ENTITY_CHICKEN_EGG, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
            if (this.getSex() == 0) this.dropItem(ItemInit.EGG_DUCK, 1);
            this.timeUntilRealEgg = this.rand.nextInt(40000) + 40000;
        }
        
        super.onLivingUpdate();
    }

    @Override
	protected void initEntityAI()
    {
        //this.tasks.addTask(0, new EntityAISwimming(this));
        this.aiPanic = new EntityAIPanic(this, 1.25D);
        this.aiHerd = new EntityAIHerdMovement(this, 1.25D);
        this.tasks.addTask(1, this.aiPanic);
        this.tasks.addTask(2, this.aiHerd);

        this.tasks.addTask(3, new EntityAIImprovedMate(this, 1.0D, EntityMallard.class));
        this.tasks.addTask(4, new EntityAIFlyingTempt(this, 1.2D, false, TEMPTATION_ITEMS));
        this.tasks.addTask(6, new EntityAIImprovedFollowParent(this, 1.1D, EntityMallard.class));
        this.tasks.addTask(7, new EntityAIForager(this));
        this.tasks.addTask(8, new EntityAIItemSearch(this, 1.0D));
        this.tasks.addTask(9, new EntityAIGrainEater(this));

        this.tasks.addTask(10, new EntityAIWanderFlying(this, 1.0D));
        
        this.tasks.addTask(11, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
        this.tasks.addTask(12, new EntityAILookIdle(this));
    }


    /**
     * Returns true if the mob is currently able to mate with the specified mob.
     */
    @Override
    public boolean canMateWith(EntityAnimal otherAnimal)
    {
        if (otherAnimal == this)
        {
            return false;
        }
        else if (otherAnimal.getClass() == EntityMallard.class || otherAnimal.getClass() == EntityDuck.class)
        {
        	IEntityAdvanced entitypig = (IEntityAdvanced)otherAnimal;

            if ( this.isInLove() && otherAnimal.isInLove() && (this.getSex() != entitypig.getSex() || (Config.enableSexes == false) ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    @Override
	public EntityChicken createChild(EntityAgeable ageable)
    {
    	IEntityAdvanced entitypig = (IEntityAdvanced)ageable;

        //Domestication process
        int newTamability = (entitypig.getTamability() + this.getTamability()) / 2;
        int randroll = this.rand.nextInt(2) + 1;
        if (this.isTame() || entitypig.isTamed())
        {
        	newTamability += randroll;	
        }
        else
        {
        	newTamability -= randroll;
        }
        newTamability = MathHelper.clamp(newTamability, -5, 5);
        

        IEntityAdvanced entitynewpig;
        if ( newTamability >= 0  || ((ageable.getClass() == EntityDuck.class) && (this.getClass() == EntityDuck.class) && (Config.reversion == false) ))
        {
            entitynewpig = new EntityDuck(this.world);
        }
        else
        {
        	entitynewpig = new EntityMallard(this.world);
        }
        //check nearby players for taming
        List<EntityPlayer> players = this.world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(9.0D));
		for (Entity e : players)
		{
			if (e != null)
			{
	    		entitynewpig.setOwnerId(e.getPersistentID());
	    		entitynewpig.setTamed(true);
			}
		}
        entitynewpig.setTamability(newTamability);
        
        EntityChicken entityreturnduck = (EntityChicken) entitynewpig;
        
        if (Config.newHorseBreeding == true)
		{   
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue();
	        entityreturnduck.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(Math.round(d1 / 1.5D + (this.rand.nextDouble() * 0.5D)));
	        if (entityreturnduck.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 4.0D) entityreturnduck.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(15.0D);
	        if (entityreturnduck.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() < 1.0D) entityreturnduck.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10.0D);

        }
		else
		{
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.getModifiedMaxHealth();
	        entityreturnduck.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(d1 / 3.0D);
		}
        

		if (Config.enableSexes == true)
		{
			entitynewpig.setSex(Integer.valueOf(this.rand.nextInt(2)));			
		}
		
		entitynewpig.setHunger(foodMax);
		entitynewpig.setHappiness(0);
		
		EntityChicken entityreturnpig = (EntityChicken) entitynewpig;
        
        return entityreturnpig;
    }

    public EntityChicken createChild()
    {
        //Domestication process
        int newTamability = (this.parentTamability + this.getTamability()) / 2;
        int randroll = this.rand.nextInt(2) + 1;
        if (this.isTame())
        {
        	newTamability += randroll;	
        }
        else
        {
        	newTamability -= randroll;
        }
        newTamability = MathHelper.clamp(newTamability, -5, 5);
        

        IEntityAdvanced entitynewpig;
        if ( newTamability >= 0 || ((this.getClass() == EntityDuck.class) && (Config.reversion == false) ))//(ageable instanceof EntityAurochs)
        {
            entitynewpig = new EntityDuck(this.world);
        }
        else
        {
        	entitynewpig = new EntityMallard(this.world);
        }
        
        //check nearby players for taming
        List<EntityPlayer> players = this.world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(9.0D));
		for (Entity e : players)
		{
			if (e != null)
			{
	    		entitynewpig.setOwnerId(e.getPersistentID());
	    		entitynewpig.setTamed(true);
			}
		}
        entitynewpig.setTamability(newTamability);
        
        EntityChicken entityreturnpig = (EntityChicken) entitynewpig;
        if (Config.newHorseBreeding == true)
		{   
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.parentMaxHealth;	        
	        entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(Math.round(Math.round(d1 / 1.5D + (this.rand.nextDouble() * 0.5D))));
	        if (entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 30.0D) entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(30.0D);
	        if (entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() < 20.0D) entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0D);
	        
        }
		else
		{
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.parentMaxHealth + this.getModifiedMaxHealth();
	        entityreturnpig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(d1 / 3.0D);
		}
        
        
        

		if (Config.enableSexes == true)
		{
			entitynewpig.setSex(Integer.valueOf(this.rand.nextInt(2)));			
		}
		
		entitynewpig.setHunger(foodMax);
		entitynewpig.setHappiness(0);
		//entitynewpig.oldAge = 0;
        
        return entityreturnpig;
    }

    @Override
	protected SoundEvent getAmbientSound()
    {
        super.getAmbientSound();
        return SoundsHandler.AMBIENT_DUCK;
    }

    @Override
	protected SoundEvent getDeathSound()
    {
        super.getDeathSound();
        return SoundsHandler.DEATH_DUCK;
    }

    @Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
    {
        super.getHurtSound(damageSourceIn);
        return SoundsHandler.HURT_DUCK;
    }

    protected SoundEvent getAngrySound()
    {
        //super.getAngrySound();
        return SoundsHandler.ANGRY_DUCK;
    }

    @Override
	public boolean isEdible(ItemStack stack)
    {
    	if (EntityMallard.TEMPTATION_ITEMS.contains(stack.getItem())) return true;
    	return false;
    }
 
    @Override
	public boolean isEdible(EntityItem entityitem)
	{
    	if (EntityMallard.TEMPTATION_ITEMS.contains(entityitem.getItem().getItem())) return true;
    	return false;
	}

    /**
     * Checks if the parameter is an item which this animal can be fed to breed it (wheat, carrots or seeds depending on
     * the animal type)
     */
    @Override
    public boolean isBreedingItem(@Nullable ItemStack stack)
    {
        return stack != null && TEMPTATION_ITEMS.contains(stack.getItem());
    }
}
