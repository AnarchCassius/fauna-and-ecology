package faunaandecology.mod.entity.passive;

import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class EntityMouflon extends EntityImprovedSheep
{
	public EntityMouflon(World worldIn) {
		super(worldIn);
		// TODO Auto-generated constructor stub
	}


    @Override
    public String getZoopediaName()
    {
    	return "Mouflon";
    }


    @Override
	public int getMaxTemper()
    {
        return 120 - ( this.getTamability() * 5 );        	
    }
    
   @Override public boolean isShearable(ItemStack item, net.minecraft.world.IBlockAccess world, BlockPos pos){ return false; }

}
