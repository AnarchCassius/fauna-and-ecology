package faunaandecology.mod.entity.passive;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;

import faunaandecology.mod.client.gui.inventory.ContainerImprovedHorseInventory;
import faunaandecology.mod.entity.IEntityAdvanced;
import faunaandecology.mod.entity.IEntityAdvancedHorse;
import faunaandecology.mod.entity.ai.EntityAIGrainEater;
import faunaandecology.mod.entity.ai.EntityAIHerdMovement;
import faunaandecology.mod.entity.ai.EntityAIImprovedFollowParent;
import faunaandecology.mod.entity.ai.EntityAIImprovedMate;
import faunaandecology.mod.entity.ai.EntityAIItemSearch;
import faunaandecology.mod.entity.ai.EntityAIRunAround;
import faunaandecology.mod.init.ItemInit;
import faunaandecology.mod.util.Config;
import net.minecraft.block.SoundType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.IJumpingMount;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIEatGrass;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIPanic;
//import net.minecraft.entity.ai.EntityAISkeletonRiders;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAITempt;
import net.minecraft.entity.ai.EntityAIWanderAvoidWater;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.ai.attributes.RangedAttribute;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.passive.AbstractHorse;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.IInventoryChangedListener;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemMonsterPlacer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.network.play.server.SPacketOpenWindow;
import net.minecraft.server.management.PreYggdrasilConverter;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.datafix.DataFixer;
import net.minecraft.util.datafix.FixTypes;
import net.minecraft.util.datafix.walkers.ItemStackData;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityImprovedHorse extends AbstractHorse implements IInventoryChangedListener, IJumpingMount, IEntityAdvancedHorse, net.minecraftforge.common.IShearable
{
    private static final Predicate<Entity> IS_HORSE_BREEDING = new Predicate<Entity>()
    {
        @Override
		public boolean apply(@Nullable Entity p_apply_1_)
        {
            return p_apply_1_ instanceof AbstractHorse && ((AbstractHorse)p_apply_1_).isBreeding();
        }
    };

	private static final Set<Item> TEMPTATION_ITEMS = Sets.newHashSet(Items.WHEAT_SEEDS, Items.MELON_SEEDS, Items.PUMPKIN_SEEDS, Items.BEETROOT_SEEDS,
			Items.BREAD, Items.APPLE, Items.CARROT, Items.WHEAT, Items.BEETROOT, Items.GOLDEN_APPLE, Items.GOLDEN_CARROT, Items.MELON, Items.SPECKLED_MELON,
			Items.SUGAR, Items.REEDS, Item.getItemFromBlock(Blocks.YELLOW_FLOWER));
	
	private static final IAttribute JUMP_STRENGTH = (new RangedAttribute((IAttribute)null, "horse.jumpStrength", 0.7D, 0.0D, 2.0D)).setDescription("Jump Strength").setShouldWatch(true);
    private static final UUID ARMOR_MODIFIER_UUID = UUID.fromString("556E1665-8B10-40C8-8F9D-CF9B1667F295");
    private static final DataParameter<Byte> STATUS = EntityDataManager.<Byte>createKey(EntityImprovedHorse.class, DataSerializers.BYTE);

    private static final DataParameter<Integer> HORSE_VARIANT = EntityDataManager.<Integer>createKey(EntityImprovedHorse.class, DataSerializers.VARINT);
    private static final DataParameter<Optional<UUID>> OWNER_UNIQUE_ID = EntityDataManager.<Optional<UUID>>createKey(EntityImprovedHorse.class, DataSerializers.OPTIONAL_UNIQUE_ID);

    private static final DataParameter<Integer> HORSE_ARMOR = EntityDataManager.<Integer>createKey(EntityImprovedHorse.class, DataSerializers.VARINT);
    private static final DataParameter<ItemStack> HORSE_ARMOR_STACK = EntityDataManager.<ItemStack>createKey(EntityImprovedHorse.class, DataSerializers.ITEM_STACK);
    
    protected static final DataParameter<Integer> SEX = EntityDataManager.<Integer>createKey(EntityImprovedHorse.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> OLD_AGE = EntityDataManager.<Integer>createKey(EntityImprovedHorse.class, DataSerializers.VARINT);
    protected static final DataParameter<Integer> TAMABILITY = EntityDataManager.<Integer>createKey(EntityImprovedHorse.class, DataSerializers.VARINT);
    private static final DataParameter<Float> HUNGER = EntityDataManager.<Float>createKey(EntityImprovedHorse.class, DataSerializers.FLOAT);
    private static final DataParameter<Integer> HAPPINESS = EntityDataManager.<Integer>createKey(EntityImprovedHorse.class, DataSerializers.VARINT);
    private static final DataParameter<Boolean> SHEARED = EntityDataManager.<Boolean>createKey(EntityImprovedHorse.class, DataSerializers.BOOLEAN);
    //private static final DataParameter<Optional<UUID>> OTHER_PARENT_ID = EntityDataManager.<Optional<UUID>>createKey(EntityImprovedHorse.class, DataSerializers.OPTIONAL_UNIQUE_ID);
    
    private static final String[] HORSE_TEXTURES = new String[] {"faunaandecology:textures/entity/horse/horse_white.png",
    		"faunaandecology:textures/entity/horse/horse_creamy.png", "faunaandecology:textures/entity/horse/horse_chestnut.png",
    		"faunaandecology:textures/entity/horse/horse_brown.png", "faunaandecology:textures/entity/horse/horse_black.png",
    		"faunaandecology:textures/entity/horse/horse_gray.png", "faunaandecology:textures/entity/horse/horse_darkbrown.png"};
    private static final String[] HORSE_TEXTURES_ABBR = new String[] {"hwh", "hcr", "hch", "hbr", "hbl", "hgr", "hdb"};
    private static final String[] HORSE_MARKING_TEXTURES = new String[] {null, "faunaandecology:textures/entity/horse/horse_markings_white.png",
    		"faunaandecology:textures/entity/horse/horse_markings_whitefield.png", "faunaandecology:textures/entity/horse/horse_markings_whitedots.png",
    		"faunaandecology:textures/entity/horse/horse_markings_blackdots.png",
    		"faunaandecology:textures/entity/horse/horse_markings_stripes_0.png", "faunaandecology:textures/entity/horse/horse_markings_stripes_1.png",
    		"faunaandecology:textures/entity/horse/horse_markings_stripes_2.png", "faunaandecology:textures/entity/horse/horse_markings_stripes_3.png",
    		"faunaandecology:textures/entity/horse/horse_markings_stripes_4.png", "faunaandecology:textures/entity/horse/horse_markings_stripes_5.png",
    		"faunaandecology:textures/entity/horse/horse_markings_stripes_6.png", "faunaandecology:textures/entity/horse/horse_markings_stripes_7.png"};
    private static final String[] HORSE_MARKING_TEXTURES_ABBR = new String[] {"", "wo_", "wmo", "wdo", "bdo", "st0", "st1", "st2", "st3", "st4", "st5", "st6", "st7"}; 


    private int eatingHaystackCounter;

    private boolean hasReproduced;
    /** "The higher this value, the more likely the horse is to be tamed next time a player rides it." */
    protected int temper;
    
    private int parentTamability;
    private float parentMaxHealth;
    private float parentSpeed;
    private double parentJump;
    private String parentType = "Horse";
    private int parentVariant;

    //
    private int pregnancyTime = 0;
    private int groupTimer = 0;
    static public int pregnancyLength = 24000;
    static public float foodMax = 80.0F;
    static public int ageMax = 720000; //30 'years'
    static public int childAge = -96000; //2000 = 1 month
    
    private String texturePrefix;
    private final String[] horseTexturesArray = new String[3];

    private EntityAIAvoidEntity<EntityPlayer> avoidEntity;
    /** The tempt AI task for this mob, used to prevent taming while it is fleeing. */
    //private EntityAITempt aiTempt;
    private EntityAIPanic aiPanic;
    private EntityAIHerdMovement aiHerd;


    public EntityImprovedHorse(World worldIn)
    {
        super(worldIn);

        //new
        //this.setupTamedAI();
    }
    
    @Override
	protected void entityInit()
    {
        super.entityInit();
        this.dataManager.register(STATUS, Byte.valueOf((byte)0));
        this.dataManager.register(OLD_AGE, Integer.valueOf(0));
        this.dataManager.register(SEX, Integer.valueOf(0));
        this.dataManager.register(HUNGER, Float.valueOf(0));
        this.dataManager.register(HAPPINESS, Integer.valueOf(0));
        this.dataManager.register(TAMABILITY, Integer.valueOf(0));
        this.dataManager.register(SHEARED, Boolean.valueOf(false));
        this.dataManager.register(OWNER_UNIQUE_ID, Optional.<UUID>absent());
        
        this.dataManager.register(HORSE_VARIANT, Integer.valueOf(0));
        this.dataManager.register(HORSE_ARMOR, Integer.valueOf(ImprovedHorseArmorType.NONE.getOrdinal()));
        this.dataManager.register(HORSE_ARMOR_STACK, ItemStack.EMPTY);
    }

    public static void registerFixesHorse(DataFixer fixer)
    {
        AbstractHorse.registerFixesAbstractHorse(fixer, EntityImprovedHorse.class);
        fixer.registerWalker(FixTypes.ENTITY, new ItemStackData(EntityImprovedHorse.class, new String[] {"ArmorItem"}));
    }


    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    @Override
	public void writeEntityToNBT(NBTTagCompound compound)
    {
        super.writeEntityToNBT(compound);
        compound.setInteger("Variant", this.getHorseVariant());
        compound.setInteger("Temper", this.getTemper());
        compound.setBoolean("Sheared", this.getSheared());

        compound.setInteger("Sex", this.getSex());
        compound.setFloat("Hunger", this.getHunger());
        compound.setInteger("Tamability", this.getTamability());
        compound.setInteger("Happiness", this.getHappiness());
        
        compound.setInteger("ParentTamability", this.parentTamability);
        compound.setFloat("ParentMaxHealth", this.parentMaxHealth);
        compound.setFloat("ParentSpeed", this.parentSpeed);
        compound.setDouble("ParentJump", this.parentJump);
        compound.setString("ParentType", this.parentType);
        compound.setInteger("ParentVariant", this.parentVariant);
        
        compound.setInteger("PregnancyTime", this.getPregnancyTime());

        
        compound.setBoolean("Tame", this.isTame());

        if (this.getOwnerUniqueId() != null)
        {
            compound.setString("OwnerUUID", this.getOwnerUniqueId().toString());
        }
        if (!this.horseChest.getStackInSlot(1).isEmpty())
        {
            compound.setTag("ArmorItem", this.horseChest.getStackInSlot(1).writeToNBT(new NBTTagCompound()));
        }

    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    @Override
	public void readEntityFromNBT(NBTTagCompound compound)
    {
        super.readEntityFromNBT(compound);
        this.setHorseVariant(compound.getInteger("Variant"));
        this.setTemper(compound.getInteger("Temper"));     
        this.setSheared( compound.getBoolean("Sheared"));         
        
        this.setSex(compound.getInteger("Sex"));
        this.setHunger(compound.getFloat("Hunger"));
        this.setTamability(compound.getInteger("Tamability"));
        this.setHappiness(compound.getInteger("Happiness"));

        this.parentTamability = (compound.getInteger("ParentTamability"));
        this.parentMaxHealth =  (compound.getFloat("ParentMaxHealth"));
        this.parentJump =  (compound.getDouble("ParentJump"));
        this.parentSpeed =  (compound.getFloat("ParentSpeed"));
        this.parentType =  (compound.getString("ParentType"));
        this.parentVariant =  (compound.getInteger("ParentVariant"));

        this.pregnancyTime = (compound.getInteger("PregnancyTime"));

        compound.setInteger("PregnancyTime", this.getPregnancyTime());

        
        this.setHorseTamed(compound.getBoolean("Tame"));
        String s;

        if (compound.hasKey("OwnerUUID", 8))
        {
            s = compound.getString("OwnerUUID");
        }
        else
        {
            String s1 = compound.getString("Owner");
            s = PreYggdrasilConverter.convertMobOwnerIfNeeded(this.getServer(), s1);
        }

        if (!s.isEmpty())
        {
            this.setOwnerUniqueId(UUID.fromString(s));
        }

        IAttributeInstance iattributeinstance = this.getAttributeMap().getAttributeInstanceByName("Speed");

        if (iattributeinstance != null)
        {
            this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(iattributeinstance.getBaseValue() * 0.25D);
        }

        if (compound.hasKey("ArmorItem", 10))
        {
            ItemStack itemstack = new ItemStack(compound.getCompoundTag("ArmorItem"));

            if (!itemstack.isEmpty() && isArmor(itemstack))
            {
                this.horseChest.setInventorySlotContents(1, itemstack);
            }
        }

        this.setupTamedAI();

        this.updateHorseSlots();
    }
    

    public void setHorseVariant(int variant)
    {
        this.dataManager.set(HORSE_VARIANT, Integer.valueOf(variant));
        this.resetTexturePrefix();
    }

    public int getHorseVariant()
    {
        return this.dataManager.get(HORSE_VARIANT).intValue();
    }

    private void resetTexturePrefix()
    {
        this.texturePrefix = null;
    }
    
    @SideOnly(Side.CLIENT) 
    private void setHorseTexturePaths()
    {
        int i = this.getHorseVariant();
        int j = (i & 255) % 7;
        int k = ((i & 65280) >> 8) % 13;
        //ItemStack armorStack = this.dataManager.get(HORSE_ARMOR_STACK);
        String texture = ImprovedHorseArmorType.getByOrdinal(this.dataManager.get(HORSE_ARMOR)).getTextureName(); //If armorStack is empty, the server is vanilla so the texture should be determined the vanilla way
        this.horseTexturesArray[0] = HORSE_TEXTURES[j];
        this.horseTexturesArray[1] = HORSE_MARKING_TEXTURES[k];
        this.horseTexturesArray[2] = texture;
        this.texturePrefix = "horse/" + HORSE_TEXTURES_ABBR[j] + HORSE_MARKING_TEXTURES_ABBR[k] + texture;
    }

    @SideOnly(Side.CLIENT)
    public String getHorseTexture()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.texturePrefix;
    }

    @SideOnly(Side.CLIENT)
    public String[] getVariantTexturePaths()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.horseTexturesArray;
    }

    /**
     * Updates the items in the saddle and armor slots of the horse's inventory.
     */
    @Override
	protected void updateHorseSlots()
    {
        super.updateHorseSlots();
        this.setHorseArmorStack(this.horseChest.getStackInSlot(1));
    }


    /**
     * Set horse armor stack (for example: new ItemStack(Items.iron_horse_armor))
     */
    public void setHorseArmorStack(ItemStack itemStackIn)
    {
    	ImprovedHorseArmorType horsearmortype = ImprovedHorseArmorType.getByItemStack(itemStackIn);
        this.dataManager.set(HORSE_ARMOR, Integer.valueOf(horsearmortype.getOrdinal()));
        this.dataManager.set(HORSE_ARMOR_STACK, itemStackIn);
        this.resetTexturePrefix();

        if (!this.world.isRemote)
        {
            this.getEntityAttribute(SharedMonsterAttributes.ARMOR).removeModifier(ARMOR_MODIFIER_UUID);
            int i = horsearmortype.getProtection();

            if (i != 0)
            {
                this.getEntityAttribute(SharedMonsterAttributes.ARMOR).applyModifier((new AttributeModifier(ARMOR_MODIFIER_UUID, "Horse armor bonus", i, 0)).setSaved(false));
            }
        }
    }
    
    @Override
	public ImprovedHorseArmorType getHorseImprovedArmorType()
    {
    	ImprovedHorseArmorType armor = ImprovedHorseArmorType.getByItemStack(this.dataManager.get(HORSE_ARMOR_STACK)); //First check the Forge armor DataParameter
        if (armor == ImprovedHorseArmorType.NONE) armor = ImprovedHorseArmorType.getByOrdinal(this.dataManager.get(HORSE_ARMOR)); //If the Forge armor DataParameter returns NONE, fallback to the vanilla armor DataParameter. This is necessary to prevent issues with Forge clients connected to vanilla servers.
        return armor;
    }

    /**
     * Called by InventoryBasic.onInventoryChanged() on a array that is never filled.
     */
    @Override
	public void onInventoryChanged(IInventory invBasic)
    {
    	ImprovedHorseArmorType horsearmortype = this.getHorseImprovedArmorType();
        super.onInventoryChanged(invBasic);
        ImprovedHorseArmorType horsearmortype1 = this.getHorseImprovedArmorType();

        if (this.ticksExisted > 20 && horsearmortype != horsearmortype1 && horsearmortype1 != ImprovedHorseArmorType.NONE)
        {
            this.playSound(SoundEvents.ENTITY_HORSE_ARMOR, 0.5F, 1.0F);
        }
    }

    @Override
	protected void playGallopSound(SoundType p_190680_1_)
    {
        super.playGallopSound(p_190680_1_);

        if (this.rand.nextInt(10) == 0)
        {
            this.playSound(SoundEvents.ENTITY_HORSE_BREATHE, p_190680_1_.getVolume() * 0.6F, p_190680_1_.getPitch());
        }
    }
    
    @Override
	protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.getModifiedMaxHealth());
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(this.getModifiedMovementSpeed());
        this.getEntityAttribute(JUMP_STRENGTH).setBaseValue(this.getModifiedJumpStrength());
    }


    @Override
    public boolean isTame()
    {
        return (this.getOwnerUniqueId() != null);
    }

    @Override
	public boolean isTamed()
    {
        return (this.getOwnerId() != null);
    }
    
    
    /**
     * Called to update the entity's position/logic.
     */
    @Override
	public void onUpdate()
    {
        super.onUpdate();

        if (this.world.isRemote && this.dataManager.isDirty())
        {
            this.dataManager.setClean();
            this.resetTexturePrefix();
        }
        ItemStack armor = this.horseChest.getStackInSlot(1);
        if (isArmor(armor)) armor.getItem().onHorseArmorTick(world, this, armor);
    }

    @Override
	protected SoundEvent getAmbientSound()
    {
        super.getAmbientSound();
        return SoundEvents.ENTITY_HORSE_AMBIENT;
    }

    @Override
	protected SoundEvent getDeathSound()
    {
        super.getDeathSound();
        return SoundEvents.ENTITY_HORSE_DEATH;
    }

    @Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
    {
        super.getHurtSound(damageSourceIn);
        return SoundEvents.ENTITY_HORSE_HURT;
    }

    @Override
	protected SoundEvent getAngrySound()
    {
        super.getAngrySound();
        return SoundEvents.ENTITY_HORSE_ANGRY;
    }

    @Override
	protected ResourceLocation getLootTable()
    {
        return LootTableList.ENTITIES_HORSE;
    }


    @Override
	public boolean wearsArmor()
    {
        return true;
    }
    
    @Override
	public boolean isArmor(ItemStack stack)
    {
        return ImprovedHorseArmorType.isHorseArmor(stack.getItem());
    }
    
    /**
     * Called only once on an entity when first time spawned, via egg, mob spawner, natural spawning etc, but not called
     * when entity is reloaded from nbt. Mainly used for initializing attributes and inventory
     */
    @Override
	@Nullable
    public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty, @Nullable IEntityLivingData livingdata)
    {
        livingdata = super.onInitialSpawn(difficulty, livingdata);
        int i = 0;

        i = this.rand.nextInt(7);

        this.setHorseVariant(i | this.rand.nextInt(5) << 8);

        int randroll = this.rand.nextInt(2);
		if (Config.enableSexes == true)
		{
	        if (randroll == 1)
			{
	            this.dataManager.set(SEX, Integer.valueOf(1));	
			}
	        else
	        {
	            this.dataManager.set(SEX, Integer.valueOf(0));
	        }
        }
		
		randroll = this.rand.nextInt(2);
        this.dataManager.set(TAMABILITY, Integer.valueOf(0));
        
        this.setHunger(foodMax);
		this.setHappiness(0);
		

        if (this.rand.nextInt(5) == 0)
        {
            this.setGrowingAge(childAge);
            this.setOldAge(0);
        }
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.getModifiedMaxHealth());
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(this.getModifiedMovementSpeed());
        this.getEntityAttribute(JUMP_STRENGTH).setBaseValue(this.getModifiedJumpStrength());
        
        this.setHealth(this.getMaxHealth());
        this.setupTamedAI();
        if (this.world.isRemote)
        {
        	this.setHorseTexturePaths();
        }
        
        this.getOtherParentData(this);
        
        return livingdata;
    }
    //////////////////////
    
    
    
    
    @Override
    public String getZoopediaName()
    {
    	return "Horse";
    }

    @Override
    public float getZoopediaHealth()
    {
    	return this.getHealth();
    }
    
    @Override
    public float getZoopediaMaxHealth()
    {
    	return this.getMaxHealth();
    }
    
    @Override
    protected void initEntityAI()
    {
        this.aiPanic = new EntityAIPanic(this, 1.5D);
        this.aiHerd = new EntityAIHerdMovement(this, 1.0D);
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(1, this.aiPanic);
        //this.tasks.addTask(3, this.aiHerd);
        this.tasks.addTask(1, new EntityAIRunAround(this, 1.5D));
        this.tasks.addTask(4, new EntityAIImprovedMate(this, 1.0D, AbstractHorse.class));
        this.tasks.addTask(5, new EntityAITempt(this, 1.0D, false, TEMPTATION_ITEMS));
        this.tasks.addTask(7, new  EntityAIImprovedFollowParent(this, 1.25D, EntityImprovedHorse.class));
        
        this.tasks.addTask(9, new EntityAIItemSearch(this, 1.0D));
        this.tasks.addTask(9, new EntityAIEatGrass(this));
        this.tasks.addTask(9, new EntityAIGrainEater(this));
        this.tasks.addTask(10, new EntityAIWanderAvoidWater(this, 1.0D));
        
        this.tasks.addTask(11, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
        this.tasks.addTask(12, new EntityAILookIdle(this));
    }
    
    protected void setupTamedAI()
    {
        if (this.avoidEntity == null)
        {
            this.avoidEntity = new EntityAIAvoidEntity<EntityPlayer>(this, EntityPlayer.class, 16.0F, 1.0D, 2.0D);
        }
        if (this.aiPanic == null)
        {
            this.aiPanic = new EntityAIPanic(this, 1.25D);
        }
        if (this.aiHerd == null)
        {
            this.aiHerd =  new EntityAIHerdMovement(this, 1.25D);
        }

        this.tasks.removeTask(this.avoidEntity);
        this.tasks.removeTask(this.aiPanic);
        this.tasks.removeTask(this.aiHerd);
        
        if (this.isChild())
    	{
        	this.tasks.addTask(1, this.aiPanic);
        	return;
    	}
        
        //this.tasks.addTask(3, this.aiHerd);
        
        if (!this.isTame())
        {
        	if (this.getHappiness() <= -50)
        	{
        		this.tasks.addTask(6, this.avoidEntity);        		
        	}
            
        	AbstractHorse nearpig = this.getClosestHorse(this, 16.0D);

        	if (this.getSex() == 0 || (nearpig != null && nearpig.isChild()) )
            {
            	this.tasks.addTask(1, this.aiPanic);
            }
        }
    }
    
    @Override
	public boolean getHorseWatchableBoolean(int p_110233_1_)
    {
        return (this.dataManager.get(STATUS).byteValue() & p_110233_1_) != 0;
    }

    @Override
	public void setHorseWatchableBoolean(int p_110208_1_, boolean p_110208_2_)
    {
        byte b0 = this.dataManager.get(STATUS).byteValue();

        if (p_110208_2_)
        {
            this.dataManager.set(STATUS, Byte.valueOf((byte)(b0 | p_110208_1_)));
        }
        else
        {
            this.dataManager.set(STATUS, Byte.valueOf((byte)(b0 & ~p_110208_1_)));
        }
    }
    

    
    public boolean isAdultHorse()
    {
        return !this.isChild();
    }

    public boolean isRidable()
    {
        return this.isAdultHorse();
    }

    @Nullable
    @Override
    public UUID getOwnerUniqueId()
    {
        return (UUID)((Optional<?>)this.dataManager.get(OWNER_UNIQUE_ID)).orNull();
    }

    @Override
    public void setOwnerUniqueId(@Nullable UUID uniqueId)
    {
        this.dataManager.set(OWNER_UNIQUE_ID, Optional.fromNullable(uniqueId));
    }

    @Nullable
    @Override
    public UUID getOwnerId()
    {
        return (UUID)((Optional<?>)this.dataManager.get(OWNER_UNIQUE_ID)).orNull();
    }

    @Override
    public void setOwnerId(@Nullable UUID uniqueId)
    {
        this.dataManager.set(OWNER_UNIQUE_ID, Optional.fromNullable(uniqueId));
    }

    @Override
	public float getHorseSize()
    {
    	if (this.isChild())
    	{
	    	float ratio = this.getOldAge() / EntityImprovedHorse.childAge;
	    	return ratio;
    	}
    	return 1.0F;
    }

    /**
     * "Sets the scale for an ageable entity according to the boolean parameter, which says if it's a child."
     */
    @Override
	public void setScaleForAge(boolean child)
    {
        if (child)
        {
            this.setScale(0.5F);
        }
        else
        {
        	this.setScale(1.0F);            
        }
    }

    @Override
	public boolean isHorseJumping()
    {
        return this.horseJumping;
    }

    @Override
	public void setHorseTamed(boolean tamed)
    {
        this.setHorseWatchableBoolean(2, tamed);
    }

    @Override
	public void setHorseJumping(boolean jumping)
    {
        this.horseJumping = jumping;
    }

    
    @Override
	protected void onLeashDistance(float p_142017_1_)
    {
        if (p_142017_1_ > 6.0F && this.isEatingHaystack())
        {
            this.setEatingHaystack(false);
        }
    }
    
    @Override
	public boolean isEatingHaystack()
    {
        return this.getHorseWatchableBoolean(32);
    }


    @Override
	public boolean isBreeding()
    {
        return this.getHorseWatchableBoolean(16);
    }

    public boolean getHasReproduced()
    {
        return this.hasReproduced;
    }
    

    @Override
	public void setBreeding(boolean breeding)
    {
        this.setHorseWatchableBoolean(16, breeding);
    }

    public void setChested(boolean chested)
    {
        this.setHorseWatchableBoolean(8, chested);
    }

    public void setHasReproduced(boolean hasReproducedIn)
    {
        this.hasReproduced = hasReproducedIn;
    }

    @Override
	public void setHorseSaddled(boolean saddled)
    {
        this.setHorseWatchableBoolean(4, saddled);
    }

    //	/
    @Override
    public int getSex()
    {
        return this.dataManager.get(SEX).intValue();
    }
    
    @Override
	public void setSex(int sexIn)
    {
        this.dataManager.set(SEX, Integer.valueOf(sexIn));
    }

    @Override
	public float getHunger()
    {
        return this.dataManager.get(HUNGER).floatValue();
    }
    
    @Override
	public void setHunger(float hungerIn)
    {
    	//hungerIn = MathHelper.clamp_float(hungerIn, 0.0F, foodMax);
    	if (hungerIn > foodMax) hungerIn = foodMax;
        this.dataManager.set(HUNGER, Float.valueOf(hungerIn));
    }
    
    @Override
	public int getTamability()
    {

        return this.dataManager.get(TAMABILITY).intValue();
    }
    
    @Override
	public void setTamability(int tamabilityIn)
    {
        this.dataManager.set(TAMABILITY, Integer.valueOf(tamabilityIn));
    }

    @Override
	public int getHappiness()
    {
        return this.dataManager.get(HAPPINESS).intValue();
    }
    
    @Override
	public void setHappiness(int happinessIn)
    {
    	happinessIn = MathHelper.clamp(happinessIn, -100, 100);
    	if (happinessIn > 100) happinessIn = 100;
    	if (happinessIn < -100) happinessIn = -100;
        this.dataManager.set(HAPPINESS, Integer.valueOf(happinessIn));
    }
    

    /**
     * Updates the items in the saddle and armor slots of the horse's inventory.
     */
    public void doUpdateHorseSlots()
    {
        this.updateHorseSlots();
    }
    

    
    /**
     * Checks if the entity's current position is a valid location to spawn this entity.
     */
    @Override
	public boolean getCanSpawnHere()
    {
        //this.prepareChunkForSpawn();
        return super.getCanSpawnHere();
    }

    @Override
	protected AbstractHorse getClosestHorse(Entity entityIn, double distance)
    {
        double d0 = Double.MAX_VALUE;
        Entity entity = null;

        for (Entity entity1 : this.world.getEntitiesInAABBexcluding(entityIn, entityIn.getEntityBoundingBox().expand(distance, distance, distance), IS_HORSE_BREEDING))
        {
            double d1 = entity1.getDistanceSq(entityIn.posX, entityIn.posY, entityIn.posZ);

            if (d1 < d0)
            {
                entity = entity1;
                d0 = d1;
            }
        }

        return (AbstractHorse)entity;
    }

    @Override
	public double getHorseJumpStrength()
    {
        return this.getEntityAttribute(JUMP_STRENGTH).getAttributeValue();
    }

   
    @Override
	public boolean isHorseSaddled()
    {
        return this.getHorseWatchableBoolean(4);
    }


    /**
     * Will return how many at most can spawn in a chunk at once.
     */
    @Override
	public int getMaxSpawnedInChunk()
    {
        return 8;
    }

    
    @Override
	public int getMaxTemper()
    {
//    	ImprovedHorseType horsetype = this.getImprovedType();
//    	if (horsetype == ImprovedHorseType.MULE)
//        {
//            return 140 - ( this.getTamability() * 5 );
//        }
//        else if (horsetype == ImprovedHorseType.DONKEY || horsetype == ImprovedHorseType.WILD_HORSE || horsetype == ImprovedHorseType.ZORSE  || horsetype == ImprovedHorseType.QUAGGA)
//        {
//            return 150 - ( this.getTamability() * 5 );
//        }
//        else if (horsetype == ImprovedHorseType.ZEBRA || horsetype == ImprovedHorseType.WILD_ASS)
//        {
//            return 200 - ( this.getTamability() * 5 );
//        }
//        else if (horsetype == ImprovedHorseType.ZONKEY)
//        {
//            return 175 - ( this.getTamability() * 5 );
//        }
//        else
//        {
            return 100 - ( this.getTamability() * 5 );
    }
	

    /**
     * Returns the volume for the sounds this mob makes.
     */
    @Override
	protected float getSoundVolume()
    {
        return 0.8F;
    }


    /**
     * Dead and sleeping entities cannot move
     */
    @Override
	protected boolean isMovementBlocked()
    {
        return this.isBeingRidden() && this.isHorseSaddled() ? true : this.isEatingHaystack() || this.isRearing();
    }

    /**
     * Checks if the parameter is an item which this animal can be fed to breed it (wheat, carrots or seeds depending on
     * the animal type)
     */
	@Override
    public boolean isBreedingItem(@Nullable ItemStack stack)
    {
        return stack != null && TEMPTATION_ITEMS.contains(stack.getItem());
    }

    /**
     * Called when the mob's health reaches 0.
     */
    @Override
	public void onDeath(DamageSource cause)
    {
        super.onDeath(cause);

        if (!this.world.isRemote && this.horseChest != null)
        {
            for (int i = 0; i < this.horseChest.getSizeInventory(); ++i)
            {
                ItemStack itemstack = this.horseChest.getStackInSlot(i);

                if (!itemstack.isEmpty())
                {
                    this.entityDropItem(itemstack, 0.0F);
                }
            }
        }
    }


    /**
     * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
     * use this to react to sunlight and start to burn.
     */
    @Override
	public void onLivingUpdate()
    {
    	if (Config.enablePregnancy && this.getPregnancyTime() > 0 && !this.world.isRemote)
    	{
    		int p = this.getPregnancyTime();
    		if (p >= EntityImprovedHorse.pregnancyLength)
    		{
    			//use alt child gen from stored data on other parent
    			EntityAgeable entityageable = this.createChild();
    	        final net.minecraftforge.event.entity.living.BabyEntitySpawnEvent event = new net.minecraftforge.event.entity.living.BabyEntitySpawnEvent(this, this, entityageable);
    	        final boolean cancelled = net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(event);
    	        if (cancelled) {
    	            //Reset the "inLove" state for the animals
    	            this.setGrowingAge(this.getChildAge()/-2);
    	            this.resetInLove();
    	            return;
    	        }

    	        entityageable = event.getChild();

    	        if (entityageable != null)
    	        {
	        		this.setGrowingAge(this.getChildAge()/-2);
    	            this.resetInLove();
    	            
    	            entityageable.setGrowingAge(this.getChildAge());
    	            entityageable.setLocationAndAngles(this.posX, this.posY, this.posZ, 0.0F, 0.0F);
    	            this.world.spawnEntity(entityageable);
    	            Random random = this.getRNG();

    	            for (int i = 0; i < 7; ++i)
    	            {
    	                double d0 = random.nextGaussian() * 0.02D;
    	                double d1 = random.nextGaussian() * 0.02D;
    	                double d2 = random.nextGaussian() * 0.02D;
    	                double d3 = random.nextDouble() * this.width * 2.0D - this.width;
    	                double d4 = 0.5D + random.nextDouble() * this.height;
    	                double d5 = random.nextDouble() * this.width * 2.0D - this.width;
    	                this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + d3, this.posY + d4, this.posZ + d5, d0, d1, d2, new int[0]);
    	            }

    	            if (this.world.getGameRules().getBoolean("doMobLoot") && this.isTame())
    	            {
    	                this.world.spawnEntity(new EntityXPOrb(this.world, this.posX, this.posY, this.posZ, random.nextInt(7) + 1));
    	            }
    	        }
    	        
//    	        for (int e = 0; e < this.litterSize; e++)
//    	        {
//        			entityageable = this.createChild((EntityAgeable) parent);
//        	        final net.minecraftforge.event.entity.living.BabyEntitySpawnEvent event2 = new net.minecraftforge.event.entity.living.BabyEntitySpawnEvent(this, (EntityLiving) parent, entityageable);
//
//        	        entityageable = event.getChild();
//    	            entityageable.setGrowingAge(this.getChildAge());
//    	            entityageable.setLocationAndAngles(this.posX, this.posY, this.posZ, 0.0F, 0.0F);
//    	            this.world.spawnEntity(entityageable);
//    	        }
    	        
        		this.setPregnancyTime(0);
    		}
    		else this.setPregnancyTime(p+1);
    	}
    	
        super.onLivingUpdate();
        //new
    	if (Config.enableAdvancedStats == true)
    	{
        	//aging process
            int i = this.getOldAge();

            ++i;
            this.setOldAge(i);

            if (i >= (ageMax) && Config.enableOldAge == true)
            {
                this.setDead();
            }
        
            //hungerprocess
            this.setHunger((float) (this.getHunger() - ((this.getMaxHealth() + this.getHorseJumpStrength() + this.getAIMoveSpeed()) / 40000)) );
            
            if (this.getHunger() <= 0 && Config.enableStarvation == true)
            {
            	this.damageEntity(DamageSource.STARVE, 1.0F);
            	this.setHunger(4.0F);
            	this.setHappiness(this.getHappiness()-10);
            }
    	}
        if (!this.world.isRemote)
        {
    		this.groupTimer += 1;
    		if (this.groupTimer >= 750)
    		{
    	    	this.setupTamedAI();
    	        Random random = this.getRNG();
    	    	if (Config.enableAdvancedStats == true)
    	    	{
    	    		List<EntityAnimal> list = this.world.<EntityAnimal>getEntitiesWithinAABB(AbstractHorse.class, this.getEntityBoundingBox().grow(16.0D));
    		        int breedchance = 0;
    		        //group size happiness
    		        if (list.size() == this.getPreferedGroupSize())
    		        {
    		        	breedchance = 50;
    		        	this.setHappiness(this.getHappiness()+3);
    		        }
    		        else if (list.size() >= this.getPreferedGroupSize()+2 ) //harder cap
    		        {
    		        	breedchance = -100;
    		        	this.setHappiness(this.getHappiness()-random.nextInt(2)); //new
    		        }
    		        else if (list.size() <= this.getPreferedGroupSize()-3)
    		        {
    		        	breedchance = -25;
    		        	this.setHappiness(this.getHappiness()-random.nextInt(2)); //new
    		        }
    		        else
    		        {
    		        	breedchance = 25;
    		        	this.setHappiness(this.getHappiness()+1);
    		        }
    		        
    		        if (this.inWater)
    		        {
    		        	breedchance = -100;
    		        }
    		        //wild breeding
    		        if (list.size() >= 2 && list.size() <= this.getPreferedGroupSize() + 2)
    		        {
    		        	if (Config.enableWildBreeding == true && !(this.isChild()) && !(this.isInLove()) && (this.getGrowingAge() == 0) )
    		        	{
    		        		if ( (this.getHappiness() + breedchance) >= random.nextInt(100)) this.setInLove(null);
    		        	}
    		        }	
    	    	}
    			this.groupTimer = random.nextInt(10);
    			
    		}
            if (this.rand.nextInt(900) == 0 && this.deathTime == 0 && this.getHunger() > 3.0F && this.getHealth() < this.getMaxHealth())
            {
                this.heal(1.0F);
                this.setHunger(this.getHunger()-3.0F);
            }

            if (!this.isEatingHaystack() && !this.isBeingRidden() && this.rand.nextInt(300) == 0 && this.world.getBlockState(new BlockPos(MathHelper.floor(this.posX), MathHelper.floor(this.posY) - 1, MathHelper.floor(this.posZ))).getBlock() == Blocks.GRASS)
            {
                this.setEatingHaystack(true);
            }

            if (this.isEatingHaystack() && ++this.eatingHaystackCounter > 50)
            {
                this.eatingHaystackCounter = 0;
                this.setEatingHaystack(false);
            }

            if (this.isBreeding() && !this.isAdultHorse() && !this.isEatingHaystack())
            {
            	AbstractHorse entityhorse = this.getClosestHorse(this, 16.0D);

                if (entityhorse != null && this.getDistance(entityhorse) > 4.0D)
                {
                    this.navigator.getPathToEntityLiving(entityhorse);
                }
            }

        }
    }


    @Override
	public void setEatingHaystack(boolean p_110227_1_)
    {
        this.setHorseWatchableBoolean(32, p_110227_1_);
    }

    @Override
    public boolean setTamedBy(EntityPlayer player)
    {
        this.setOwnerUniqueId(player.getUniqueID());
        this.setHorseTamed(true);
        return true;
    }


    ////////////////////////////////////////////////
    // FORGE
    private net.minecraftforge.items.IItemHandler itemHandler = null; // Initialized by initHorseChest above.


    /**
     * Returns randomized max health
     */
    @Override
	public float getModifiedMaxHealth()
    {
    		return 16.0F + this.rand.nextInt(10) + this.rand.nextInt(10);
    }

    /**
     * Returns randomized jump strength
     */
    @Override
	public double getModifiedJumpStrength()
    {
    	return 0.4000000059604645D + this.rand.nextDouble() * 0.2D + this.rand.nextDouble() * 0.2D + this.rand.nextDouble() * 0.2D;
    }

    /**
     * Returns randomized movement speed
     */
    @Override
	public double getModifiedMovementSpeed()
    {
        	return (0.44999998807907104D + this.rand.nextDouble() * 0.3D + this.rand.nextDouble() * 0.3D + this.rand.nextDouble() * 0.3D) * 0.25D;
    } 
    
    @Override
    public EntityAgeable createChild(EntityAgeable ageable)
    {
        EntityImprovedHorse entityhorse = (EntityImprovedHorse)ageable;
        IEntityAdvancedHorse entityhorse1;

        //Domestication process
        int newTamability = (entityhorse.getTamability() + this.getTamability()) / 2;
        int randroll = this.rand.nextInt(2) + 1;
        if (this.isTame() || entityhorse.isTame())
        {
        	newTamability += randroll;	
        }
        else
        {
        	newTamability -= randroll;
        }
        newTamability = MathHelper.clamp(newTamability, -5, 5);
        newTamability = MathHelper.clamp(newTamability, -5, 5);
        //

    	if ((entityhorse.getZoopediaName() == "Horse") || (entityhorse.getZoopediaName() == "Wild Horse") && (this.getZoopediaName() == "Horse") || (this.getZoopediaName() == "Wild Horse"))
    	{
        	if (newTamability >= 0 || ((entityhorse.getZoopediaName() == "Horse") && (this.getZoopediaName() == "Horse") && (Config.reversion == false) ))
			{
        		entityhorse1 = new EntityImprovedHorse(this.world);
			}
        	else
        	{
            	entityhorse1 = new EntityWildHorse(this.world);
        	}
    	}
        else if ((this.getZoopediaName() == "Zebra") && (entityhorse.getZoopediaName()== "Zebra"))
        {
        	entityhorse1 = new EntityZebra(this.world);
        }
        else if ((this.getZoopediaName() == "Quagga") && (entityhorse.getZoopediaName() == "Quagga"))
        {
        	entityhorse1 = new EntityQuagga(this.world);
        } 
        else if ((entityhorse.getZoopediaName() == "Donkey") && (this.getZoopediaName() == "Horse"))
        {
        	entityhorse1 = new EntityImprovedMule(this.world);
    	}
        else if ((entityhorse.getZoopediaName() == "Quagga") || (entityhorse.getZoopediaName() == "Zebra") && (this.getZoopediaName() == "Zebra") || (this.getZoopediaName() == "Quagga"))
        {
        	if (this.rand.nextInt(2) == 0) 
        			entityhorse1 = new EntityZebra(this.world);
        	else
        		entityhorse1 = new EntityQuagga(this.world);
        }         //failsafe
        else entityhorse1 = new EntityImprovedHorse(this.world);
        
        
        //check nearby players for taming
        List<EntityPlayer> players = this.world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(9.0D));
		for (Entity e : players)
		{
			if (e != null)
			{
				entityhorse1.setTamedByPlayer((EntityPlayer) e);
				//entityhorse1.setOwnerUniqueId(e.getPersistentID());
				//entityhorse1.setTamed(true);
			}
		}
        entityhorse1.setTamability(newTamability);
        //
                
        int j = this.rand.nextInt(9);
        int i;
        
        if (j < 4)
        {
            i = this.getHorseVariant() & 255;
            //if (this.getImprovedType() == ImprovedHorseType.ZEBRA) i = entityhorse.getHorseVariant() & 255; //zorses use horse parent's texture
        }
        else if (j < 8)
        {
            i = entityhorse.getHorseVariant() & 255;
            //if (entityhorse.getImprovedType() == ImprovedHorseType.ZEBRA) i = this.getHorseVariant() & 255; //zorses use horse parent's texture
        }
        else
        {
            i = this.rand.nextInt(7);
            ((EntityImprovedHorse)entityhorse1).getBiomeHorseType(this.world.getBiome(new BlockPos(this)));
        }

        int k = this.rand.nextInt(5);

        if (k < 2)
        {
            i = i | this.getHorseVariant() & 65280;
        }
        else if (k < 4)
        {
            i = i | entityhorse.getHorseVariant() & 65280;
        }
        else
        {
            i = i | this.rand.nextInt(5) << 8 & 65280;
            ((EntityImprovedHorse)entityhorse1).setHorseVariant(i);
            ((EntityImprovedHorse)entityhorse1).getBiomeHorseType(this.world.getBiome(new BlockPos(this)));
        }

        if (k < 4) ((EntityImprovedHorse)entityhorse1).setHorseVariant(i);

		if (Config.newHorseBreeding == true)
		{
	        //entityhorse1.setType(horsetype2);
	        
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue();
	        entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(Math.round(d1 / 1.5D + (this.rand.nextDouble() * 0.5D)));
	        if (entityhorse1 instanceof EntityZebra)
	        {	
	        	if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 32.0D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(32.0D);
	        }
//	        else if (horsetype2 == ImprovedHorseType.DONKEY  || horsetype2 == ImprovedHorseType.WILD_ASS)
//	        {
//	        	if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 28.0D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(28.0D);
//	        }
	        else if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 36.0D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(36.0D);
	        
	        double d2 = this.getEntityAttribute(JUMP_STRENGTH).getBaseValue() + ageable.getEntityAttribute(JUMP_STRENGTH).getBaseValue();
	        entityhorse1.getEntityAttribute(JUMP_STRENGTH).setBaseValue(d2 / (1.0D + (this.rand.nextDouble() * 1.0D)));
	        
        	if (entityhorse1.getEntityAttribute(JUMP_STRENGTH).getBaseValue() > 0.460000006D) entityhorse1.getEntityAttribute(JUMP_STRENGTH).setBaseValue(0.460000006D);
        	//if (entityhorse1.getImprovedType() == ImprovedHorseType.DONKEY || entityhorse1.getImprovedType() == ImprovedHorseType.WILD_ASS)	entityhorse1.getEntityAttribute(JUMP_STRENGTH).setBaseValue(0.5D);
        	
	        double d0 = this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue();
	        entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(d0 / (1.0D + (this.rand.nextDouble() * 1.0D)));
	        if  ((entityhorse1.getClass() == EntityImprovedHorse.class) ||  (entityhorse1 instanceof EntityWildHorse))
	        {
	        	if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() > 0.3375D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.3375D);	
	        }
	        else if (entityhorse1 instanceof EntityZebra)
	        {	
	        	if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() > 0.405D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.405D);
	        }
	        else entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.17499999701976776D);
	       
		}
		else
		{
	        //entityhorse1.setType(horsetype2);
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.getModifiedMaxHealth();
	        entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(d1 / 3.0D);
	        double d2 = this.getEntityAttribute(JUMP_STRENGTH).getBaseValue() + ageable.getEntityAttribute(JUMP_STRENGTH).getBaseValue() + this.getModifiedJumpStrength();
	        entityhorse1.getEntityAttribute(JUMP_STRENGTH).setBaseValue(d2 / 3.0D);
	        double d0 = this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() + ageable.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() + this.getModifiedMovementSpeed();
	        entityhorse1.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(d0 / 3.0D);
		}

		EntityAgeable entityreturnhorse = (EntityAgeable) entityhorse1;


		if (Config.enableSexes == true)
		{
			entityhorse1.setSex(Integer.valueOf(this.rand.nextInt(2)));			
		}

		entityhorse1.setHunger(foodMax);
		entityhorse1.setHappiness(0);
        
        return entityreturnhorse;
    }
    
    
    private void getBiomeHorseType() {
		
	}
    
    private void getBiomeHorseType(Biome biome) {
		
	}

	//////////////////
    @Override
    public boolean attackEntityFrom(DamageSource par1DamageSource, float par2)
    {
    	this.setHappiness(this.getHappiness()-50);
    	return super.attackEntityFrom(par1DamageSource, par2);
    }
    
    /**
     * Decreases ItemStack size by one
     */
    @Override
    protected void consumeItemFromStack(EntityPlayer player, ItemStack stack)
    {
    	//new
		if (Config.enableAdvancedStats == true)
		{
			float newHunger = 0;
			int newHappiness = 5;
	    	if (stack.getItem() instanceof ItemFood)
	    	{
	    		ItemFood eatenFood = (ItemFood)stack.getItem();
	    		newHunger = this.getHunger() + eatenFood.getHealAmount(stack) + (eatenFood.getHealAmount(stack)*eatenFood.getSaturationModifier(stack)); 
	    		newHappiness = (int) (this.getHappiness() + eatenFood.getHealAmount(stack) + (eatenFood.getHealAmount(stack)*eatenFood.getSaturationModifier(stack)) );	            
	    		this.setHunger(newHunger);
				this.setHappiness(newHappiness);
	    	}
	    	else
	    	{
				this.eatGrassBonus();
	    	}
	    	
            if (!this.isTame() && this.getTemper() < this.getMaxTemper())
            {
                this.increaseTemper(newHappiness+this.rand.nextInt(5)+5);
                if (this.getTemper() >= this.getMaxTemper())
                {
                	this.setTamedBy(player);
                    double d0 = this.rand.nextGaussian() * 0.025D;
                    double d1 = this.rand.nextGaussian() * 0.025D;
                    double d2 = this.rand.nextGaussian() * 0.025D;
                    this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	this.world.spawnParticle(EnumParticleTypes.HEART, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                }
                else
                {
                    double d0 = this.rand.nextGaussian() * 0.025D;
                    double d1 = this.rand.nextGaussian() * 0.025D;
                    double d2 = this.rand.nextGaussian() * 0.025D;
                    this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
                	this.world.spawnParticle(EnumParticleTypes.VILLAGER_HAPPY, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
                	
                }
            }
            else
            {
                double d0 = this.rand.nextGaussian() * 0.025D;
                double d1 = this.rand.nextGaussian() * 0.025D;
                double d2 = this.rand.nextGaussian() * 0.025D;
                this.playSound(SoundEvents.ENTITY_HORSE_EAT, 1.0F, (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F);
            	this.world.spawnParticle(EnumParticleTypes.SPELL, this.posX + this.rand.nextFloat() * this.width * 2.0F - this.width, this.posY + 0.5D + this.rand.nextFloat() * this.height, this.posZ + this.rand.nextFloat() * this.width * 2.0F - this.width, d0, d1, d2, new int[0]);
            }
    	}
    	//
        if (!player.capabilities.isCreativeMode) stack.setCount(stack.getCount() -1);
    }
    
    public boolean isEdible(ItemStack stack)
    {
    	if (EntityImprovedHorse.TEMPTATION_ITEMS.contains(stack.getItem())) return true;
    	return false;
    }
 
    @Override
	public boolean isEdible(EntityItem entityitem)
	{
    	if (EntityImprovedHorse.TEMPTATION_ITEMS.contains(entityitem.getItem().getItem())) return true;
    	return false;
	}


    /**
     * This function applies the benefits of growing back wool and faster growing up to the acting entity. (This
     * function is used in the AIEatGrass)
     */
    @Override
    public void eatGrassBonus()
    {
        this.setHunger(this.getHunger() + 4);
        this.setHappiness(this.getHappiness() + 1);
        return;
    }
    

    @Override
	public float getGrowthSize()
    {
    	if (this.isChild())
    	{
    		float growth = EntityImprovedHorse.childAge + this.getOldAge();
	    	float ratio = growth / EntityImprovedHorse.childAge;
	    	return 1.0F - ratio;
    	}
    	return 1.0F;
    }
    
    @Override
	public int getPreferedGroupSize()
    {
    	return 5;
    }
    
    @Override
	public float getFoodMax()
    {
        return foodMax;
    }

	@Override
	public void setTamed(boolean tamed)
	{
		// TODO Auto-generated method stub	
	}

    @Override
	public int getOldAge()
    {
        return this.dataManager.get(OLD_AGE).intValue();
    }
    
    @Override
	public void setOldAge(int age)
    {
        this.dataManager.set(OLD_AGE, Integer.valueOf(age));
    }    

	@Override
	public int getChildAge() 
	{
		return EntityImprovedHorse.childAge;
	}
	

    public EntityAgeable createChild()
    {
    	IEntityAdvancedHorse entityhorse1;
        int newTamability = (this.parentTamability + this.getTamability()) / 2;

        int randroll = this.rand.nextInt(2) + 1;
        if (this.isTame())
        {
        	newTamability += randroll;	
        }
        else
        {
        	newTamability -= randroll;
        }
        newTamability = MathHelper.clamp(newTamability, -5, 5);
        //

    	if ((this.parentType == "Horse") || (this.parentType == "Wild Horse") && (this.getZoopediaName() == "Horse") || (this.getZoopediaName() == "Wild Horse"))
    	{
        	if (newTamability >= 0  || ((this.getClass() == EntityImprovedHorse.class) && (Config.reversion == false) ))
			{
        		entityhorse1 = new EntityImprovedHorse(this.world);
			}
        	else
        	{
            	entityhorse1 = new EntityWildHorse(this.world);
        	}
    	}
        else if ((this.getZoopediaName() == "Zebra") && (this.parentType == "Zebra"))
        {
        	entityhorse1 = new EntityZebra(this.world);
        }
        else if ((this.getZoopediaName() == "Quagga") && (this.parentType == "Quagga"))
        {
        	entityhorse1 = new EntityZebra(this.world);
        } 
        else if ((this.parentType == "Donkey") && (this.getZoopediaName() == "Horse"))
        {
        	entityhorse1 = new EntityImprovedMule(this.world);
    	}
        else if ((this.parentType == "Quagga") || (this.parentType == "Zebra") && (this.getZoopediaName() == "Zebra") || (this.getZoopediaName() == "Quagga"))
        {
        	if (this.rand.nextInt(2) == 0) 
        			entityhorse1 = new EntityZebra(this.world);
        	else
        		entityhorse1 = new EntityQuagga(this.world);
        }         //failsafe
        else entityhorse1 = new EntityImprovedHorse(this.world);
                
        //check nearby players for taming
        List<EntityPlayer> players = this.world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(9.0D));
		for (Entity e : players)
		{
			if (e != null)
			{
				entityhorse1.setTamedByPlayer((EntityPlayer) e);
				//entityhorse1.setOwnerUniqueId(e.getPersistentID());
				//entityhorse1.setTamed(true);
			}
		}
        entityhorse1.setTamability(newTamability);
        
        int j = this.rand.nextInt(9);
        int i;

        if (j < 4)
        {
            i = this.getHorseVariant() & 255;
            if (this.getZoopediaName() == "Zebra") i = this.parentVariant & 255; //zorses use horse parent's texture
        }
        else if (j < 8)
        {
            i = this.parentVariant & 255;
            if (this.parentType == "Zebra") i = this.getHorseVariant() & 255; //zorses use horse parent's texture
        }
        else
        {
        	i = this.rand.nextInt(7);
        }

        int k = this.rand.nextInt(5);

        if (k < 2)
        {
            i = i | this.getHorseVariant() & 65280;
        }
        else if (k < 4)
        {
            i = i | this.parentVariant & 65280;
        }
        else
        {
            i = i | this.rand.nextInt(5) << 8 & 65280;
            ((EntityImprovedHorse)entityhorse1).setHorseVariant(i);
            if (this.getClass() == EntityWildHorse.class)
    		{
                ((EntityWildHorse)entityhorse1).getBiomeHorseType(this.world.getBiome(new BlockPos(this)));
    		}
        }

        if (k < 4) ((EntityImprovedHorse)entityhorse1).setHorseVariant(i);
        
		if (Config.newHorseBreeding == true)
		{
	        
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.parentMaxHealth;
	        ((AbstractHorse)entityhorse1).getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(Math.round(d1 / 1.5D + (this.rand.nextDouble() * 0.5D)));
	        if (entityhorse1 instanceof EntityZebra)
	        {	
	        	if (((AbstractHorse)entityhorse1).getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 32.0D) ((AbstractHorse)entityhorse1).getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(32.0D);
	        }
//	        else if (horsetype2 == ImprovedHorseType.DONKEY  || horsetype2 == ImprovedHorseType.WILD_ASS)
//	        {
//	        	if (entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 28.0D) entityhorse1.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(28.0D);
//	        }
	        else if (((AbstractHorse)entityhorse1).getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() > 36.0D) ((AbstractHorse)entityhorse1).getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(36.0D);
	        
	        double d2 = this.getEntityAttribute(JUMP_STRENGTH).getBaseValue() + this.parentJump;
	        ((AbstractHorse)entityhorse1).getEntityAttribute(JUMP_STRENGTH).setBaseValue(d2 / (1.0D + (this.rand.nextDouble() * 1.0D)));
	        
        	if (((AbstractHorse)entityhorse1).getEntityAttribute(JUMP_STRENGTH).getBaseValue() > 0.460000006D) ((AbstractHorse)entityhorse1).getEntityAttribute(JUMP_STRENGTH).setBaseValue(0.460000006D);
        	//if (entityhorse1.getImprovedType() == ImprovedHorseType.DONKEY || entityhorse1.getImprovedType() == ImprovedHorseType.WILD_ASS)	entityhorse1.getEntityAttribute(JUMP_STRENGTH).setBaseValue(0.5D);
        	
	        double d0 = this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() + this.parentSpeed;
	        ((AbstractHorse)entityhorse1).getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(d0 / (1.0D + (this.rand.nextDouble() * 1.0D)));
	        if  ((entityhorse1.getClass() == EntityImprovedHorse.class) ||  (entityhorse1 instanceof EntityWildHorse))
	        {
	        	if (((AbstractHorse)entityhorse1).getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() > 0.3375D) ((AbstractHorse)entityhorse1).getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.3375D);	
	        }
	        else if (entityhorse1 instanceof EntityZebra)
	        {	
	        	if (((AbstractHorse)entityhorse1).getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() > 0.405D) ((AbstractHorse)entityhorse1).getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.405D);
	        }
	        else ((AbstractHorse)entityhorse1).getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.17499999701976776D);
	        
		}
		else
		{
	        double d1 = this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() + this.parentMaxHealth + this.getModifiedMaxHealth();
	        ((AbstractHorse)entityhorse1).getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(d1 / 3.0D);
	        double d2 = this.getEntityAttribute(JUMP_STRENGTH).getBaseValue() + this.parentJump + this.getModifiedJumpStrength();
	        ((AbstractHorse)entityhorse1).getEntityAttribute(JUMP_STRENGTH).setBaseValue(d2 / 3.0D);
	        double d0 = this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue() + this.parentSpeed + this.getModifiedMovementSpeed();
	        ((AbstractHorse)entityhorse1).getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(d0 / 3.0D);
		}
		
		EntityAgeable entityreturnhorse = (EntityAgeable) entityhorse1;

		if (Config.enableSexes == true)
		{
			entityhorse1.setSex(Integer.valueOf(this.rand.nextInt(2)));			
		}

		entityhorse1.setHunger(foodMax);
		entityhorse1.setHappiness(0);
        
        return entityreturnhorse;
    }


    //////////
    @Override public boolean isShearable(ItemStack item, net.minecraft.world.IBlockAccess world, BlockPos pos){ return !this.getSheared() && !this.isChild(); }

    @Override
	public boolean getSheared()
    { 
        return this.dataManager.get(SHEARED).booleanValue();
    }
    
    public void setSheared(boolean shearedIn)
    {
        this.dataManager.set(SHEARED, Boolean.valueOf(shearedIn));
    }

    
    @Override
    public java.util.List<ItemStack> onSheared(ItemStack item, net.minecraft.world.IBlockAccess world, net.minecraft.util.math.BlockPos pos, int fortune)
    {

        java.util.List<ItemStack> ret = new java.util.ArrayList<ItemStack>();
        for (int i = 0; i < 2; ++i)
        {
            ret.add(new ItemStack(ItemInit.HAIR_HORSE));
        }

        this.setSheared(true);
        //        stack.damageItem(1, player);
        this.playSound(SoundEvents.ENTITY_MOOSHROOM_SHEAR, 1.0F, 1.0F);
        return ret;
    }
    
    //

    public static class GroupData implements IEntityLivingData
        {
            public int variant;

            public GroupData(int variantIn)
            {
                this.variant = variantIn;
            }
        }
    
    @Override
	public int getPregnancyTime()
    {
        return this.pregnancyTime;
    }
    
    @Override
	public void setPregnancyTime(int age)
    {
        this.pregnancyTime = age;
    }    

    @Override
    public boolean processInteract(EntityPlayer player, EnumHand hand)
    {
        ItemStack itemstack = player.getHeldItem(hand);
        boolean flag = !itemstack.isEmpty();

        if (itemstack != null && itemstack.getItem() == Items.SPAWN_EGG)
        {
            if (!this.world.isRemote)
            {
                Class <? extends Entity > oclass = EntityList.getClass(ItemMonsterPlacer.getNamedIdFrom(itemstack));


                if (oclass != null && this.getClass() == oclass)
                {
                    EntityAgeable entityageable = this.createChild();

                    if (entityageable != null)
                    {
                        entityageable.setGrowingAge(childAge);
                        entityageable.setLocationAndAngles(this.posX, this.posY, this.posZ, 0.0F, 0.0F);
                        this.world.spawnEntity(entityageable);

                        if (itemstack.hasDisplayName())
                        {
                            entityageable.setCustomNameTag(itemstack.getDisplayName());
                        }

                        if (!player.capabilities.isCreativeMode)
                        {
                        	itemstack.setCount(itemstack.getCount() -1);
                        }
                    }
                }
            }

            return true;
        }
        else
        {

            if (flag)
            {	//
            	if (Config.enableAdvancedStats == true)
            	{
            		if (this.getHunger() < foodMax - 0.5F)
            		{
    	        		if  (Config.enableWildBreeding == true)
    	        		{
    	                    if (this.isBreedingItem(itemstack)  && !(this.isChild()) && !(this.isInLove()) )
    	                    {
    	                        this.consumeItemFromStack(player, itemstack);
    	                        return true;
    	                    }
    	                    else if (this.isBreedingItem(itemstack))
    	                    {
    	                        this.consumeItemFromStack(player, itemstack);
    	                        return true;
    	                    }
    	        		}
    	        		else
    	        		{
    	                    if (this.isBreedingItem(itemstack) && !(this.isChild()) && !(this.isInLove()) )
    	                    {
    	                        this.consumeItemFromStack(player, itemstack);
    	                        this.setInLove(player);
    	                        return true;
    	                    }
    	                    else if (this.isBreedingItem(itemstack))
    	                    {
    	                        this.consumeItemFromStack(player, itemstack);
    	                        return true;
    	                    }
    	        		}
            		}
            	}
                
                if (this.isEdible(itemstack))
                {
                	if  (this.getHunger() <  foodMax - 0.5F)
                	{
    	                this.consumeItemFromStack(player, itemstack);
    	                return true;
                	}
                }
                //

                if (!this.isChild())
                {
                    if (this.isTame() && player.isSneaking())
                    {
                        this.openGUI(player);
                        return true;
                    }

                    if (this.isBeingRidden())
                    {
                        return super.processInteract(player, hand);
                    }
                }

                if (itemstack.interactWithEntity(player, this, hand))
                {
                    return true;
                }

                if (!this.isTame())
                {
                    this.makeMad();
                    return true;
                }

                boolean flag1 = ImprovedHorseArmorType.getByItemStack(itemstack) != ImprovedHorseArmorType.NONE;
                boolean flag2 = !this.isChild() && !this.isHorseSaddled() && itemstack.getItem() == Items.SADDLE;

                if (flag1 || flag2)
                {
                    this.openGUI(player);
                    return true;
                }
            }

            if (this.isChild())
            {
                return super.processInteract(player, hand);
            }
            else
            {
                this.mountTo(player);
                return true;
            }
        }
    }


    @Override
    public void openGUI(EntityPlayer playerEntity)
    {
        if (!this.world.isRemote && (!this.isBeingRidden() || this.isPassenger(playerEntity)) && this.isTame())
        {
        	if (playerEntity instanceof EntityPlayerMP)
        	{
        		EntityPlayerMP playerMP = (EntityPlayerMP) playerEntity; 
                this.horseChest.setCustomName(this.getName());
                //playerEntity.openGuiHorseInventory(this, this.horseChest);
                if (playerEntity.openContainer != playerEntity.inventoryContainer)
                {
                	playerEntity.closeScreen();
                }

                playerMP.getNextWindowId();
                playerMP.connection.sendPacket(new SPacketOpenWindow(playerMP.currentWindowId, "EntityHorse", this.horseChest.getDisplayName(), this.horseChest.getSizeInventory(), this.getEntityId()));
                playerEntity.openContainer = new ContainerImprovedHorseInventory(playerEntity.inventory, this.horseChest, this, playerEntity);
                playerEntity.openContainer.windowId = playerMP.currentWindowId;
                playerEntity.openContainer.addListener(playerMP);
        	}
        }
    }
    
    /**
     * Returns true if the mob is currently able to mate with the specified mob.
     */
    @Override
    public boolean canMateWith(EntityAnimal otherAnimal)
    {
        if (otherAnimal == this)
        {
            return false;
        }
        else if ((otherAnimal.getClass() == EntityImprovedHorse.class || otherAnimal instanceof EntityWildHorse) && (this.getClass() == EntityImprovedHorse.class || this instanceof EntityWildHorse))
        {
        	IEntityAdvancedHorse entitycow = (IEntityAdvancedHorse)otherAnimal;

            if ( this.isInLove() && otherAnimal.isInLove() && (this.getSex() != entitycow.getSex() || (Config.enableSexes == false) ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if ((this.getClass() == EntityImprovedHorse.class && otherAnimal.getClass() == EntityImprovedDonkey.class))
        {
        	IEntityAdvancedHorse entitycow = (IEntityAdvancedHorse)otherAnimal;

            if ( this.isInLove() && otherAnimal.isInLove() && (this.getSex() != entitycow.getSex() || (Config.enableSexes == false) ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if ((this instanceof EntityZebra) && (otherAnimal.getClass() == EntityImprovedDonkey.class))
        {
        	IEntityAdvancedHorse entitycow = (IEntityAdvancedHorse)otherAnimal;

            if ( this.isInLove() && otherAnimal.isInLove() && (this.getSex() != entitycow.getSex() || (Config.enableSexes == false) ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if ((otherAnimal instanceof EntityZebra) && (this instanceof EntityZebra))
        {
        	IEntityAdvancedHorse entitycow = (IEntityAdvancedHorse)otherAnimal;

            if ( this.isInLove() && otherAnimal.isInLove() && (this.getSex() != entitycow.getSex() || (Config.enableSexes == false) ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if ((otherAnimal instanceof EntityQuagga) && (this instanceof EntityQuagga))
        {
        	IEntityAdvancedHorse entitycow = (IEntityAdvancedHorse)otherAnimal;

            if ( this.isInLove() && otherAnimal.isInLove() && (this.getSex() != entitycow.getSex() || (Config.enableSexes == false) ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }
    
    @Override
    protected boolean canMate()
    {
    	return !this.isBeingRidden() && !this.isRiding() && this.isTame() && !this.isChild() && this.getHealth() >= this.getMaxHealth() && this.isInLove();
    }


	@Override
	public void getOtherParentData(IEntityAdvanced parent) 
	{
	    	if (parent instanceof EntityImprovedHorse)
	    	{
	    		EntityImprovedHorse otherParent = (EntityImprovedHorse) parent;
	            this.parentTamability = otherParent.getTamability();
	            this.parentMaxHealth = otherParent.getMaxHealth();
	            this.parentSpeed = otherParent.getAIMoveSpeed();
	            this.parentJump = otherParent.getHorseJumpStrength();
	            this.parentVariant = otherParent.getHorseVariant();
	            this.parentType = otherParent.getZoopediaName();
	    	}
	    	else
	    	{
	            this.parentTamability = this.getTamability();
	            this.parentMaxHealth = this.getMaxHealth();
	            this.parentSpeed = this.getAIMoveSpeed();
	            this.parentJump = this.getHorseJumpStrength();
	            this.parentVariant = this.getHorseVariant();
	            this.parentType = this.getZoopediaName();
	    	}
	}

	@Override
	public boolean setTamedByPlayer(EntityPlayer player) {
        this.setOwnerUniqueId(player.getUniqueID());
        this.setHorseTamed(true);
        return true;
	}
    
}
