package faunaandecology.mod.entity.passive;

import faunaandecology.mod.init.ItemInit;
import faunaandecology.mod.util.Config;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemMonsterPlacer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;


public class EntityGuanaco extends EntityImprovedLlama
{

    private String texturePrefix;
    private final String[] horseTexturesArray = new String[3];
    
    public EntityGuanaco(World worldIn)
    {
        super(worldIn);
        this.setSize(0.9F, 1.87F);
    }
    
    @Override
    public String getZoopediaName()
    {
    	return "Guanaco";
    }
    
    @Override
	@SideOnly(Side.CLIENT)
    public String getHorseTexture()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.texturePrefix;
    }

    @Override
	@SideOnly(Side.CLIENT)
    public String[] getVariantTexturePaths()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.horseTexturesArray;
    }

    
    @SideOnly(Side.CLIENT) 
    private void setHorseTexturePaths()
    {
        String texture = null;
        this.horseTexturesArray[0] = "faunaandecology:textures/entity/horse/guanaco.png";
        this.horseTexturesArray[1] = null;
        this.horseTexturesArray[2] = texture;
        this.texturePrefix = "horse/" + "gua" + "gua" + texture;
    }
    
    @Override
    public java.util.List<ItemStack> onSheared(ItemStack item, net.minecraft.world.IBlockAccess world, BlockPos pos, int fortune)
    {
        this.setSheared(true);

        java.util.List<ItemStack> ret = new java.util.ArrayList<ItemStack>();

        	if (Config.woolSystem)
			{
				ret.add(new ItemStack(ItemInit.WOOL_TUFT, 1 + this.rand.nextInt(3), 3));
			}
			else
            ret.add(new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 12));

        this.playSound(SoundEvents.ENTITY_SHEEP_SHEAR, 1.0F, 1.0F);
        return ret;
    }

    @Override
    public boolean processInteract(EntityPlayer player, EnumHand hand)
    {
        ItemStack itemstack = player.getHeldItem(hand);
        boolean flag = !itemstack.isEmpty();

        if (itemstack != null && itemstack.getItem() == Items.SPAWN_EGG)
        {
            if (!this.world.isRemote)
            {
                Class <? extends Entity > oclass = EntityList.getClass(ItemMonsterPlacer.getNamedIdFrom(itemstack));


                if (oclass != null && this.getClass() == oclass)
                {
                    EntityAgeable entityageable = this.createChild(this);

                    if (entityageable != null)
                    {
                        entityageable.setGrowingAge(childAge);
                        entityageable.setLocationAndAngles(this.posX, this.posY, this.posZ, 0.0F, 0.0F);
                        this.world.spawnEntity(entityageable);

                        if (itemstack.hasDisplayName())
                        {
                            entityageable.setCustomNameTag(itemstack.getDisplayName());
                        }

                        if (!player.capabilities.isCreativeMode)
                        {
                        	itemstack.setCount(itemstack.getCount() -1);
                        }
                    }
                }
            }

            return true;
        }
        else
        {
            if (!this.isChild())
            {

                //
                if (itemstack != null && !this.hasChest() && itemstack.getItem() == Item.getItemFromBlock(Blocks.CHEST))
                {
                    //this.setChested(true);
                    //this.playChestEquipSound();
                    //this.initHorseChest();
                    return false;
                }
                //
                if (this.isTame() && player.isSneaking())
                {
                    this.openGUI(player);
                    return true;
                }

                if (this.isBeingRidden())
                {
                    return super.processInteract(player, hand);
                }
            }


            if (flag)
            {	//
            	if (Config.enableAdvancedStats == true)
            	{
            		if (this.getHunger() < foodMax - 0.5F)
            		{
    	        		if  (Config.enableWildBreeding == true)
    	        		{
    	                    if (this.isBreedingItem(itemstack)  && !(this.isChild()) && !(this.isInLove()) )
    	                    {
    	                        this.consumeItemFromStack(player, itemstack);
    	                        return true;
    	                    }
    	                    else if (this.isBreedingItem(itemstack))
    	                    {
    	                        this.consumeItemFromStack(player, itemstack);
    	                        return true;
    	                    }
    	        		}
    	        		else
    	        		{
    	                    if (this.isBreedingItem(itemstack) && !(this.isChild()) && !(this.isInLove()) )
    	                    {
    	                        this.consumeItemFromStack(player, itemstack);
    	                        this.setInLove(player);
    	                        return true;
    	                    }
    	                    else if (this.isBreedingItem(itemstack))
    	                    {
    	                        this.consumeItemFromStack(player, itemstack);
    	                        return true;
    	                    }
    	        		}
            		}
            	}
                
                if (this.isEdible(itemstack))
                {
                	if  (this.getHunger() <  foodMax - 0.5F)
                	{
    	                this.consumeItemFromStack(player, itemstack);
    	                return true;
                	}
                }
                //


                if (itemstack.interactWithEntity(player, this, hand))
                {
                    return true;
                }

                if (!this.isTame())
                {
                    this.makeMad();
                    return true;
                }

                boolean flag1 = ImprovedHorseArmorType.getByItemStack(itemstack) != ImprovedHorseArmorType.NONE;
                boolean flag2 = !this.isChild() && !this.isHorseSaddled() && itemstack.getItem() == Items.SADDLE;

                if (flag1 || flag2)
                {
                    this.openGUI(player);
                    return true;
                }
            }

            if (this.isChild())
            {
                return super.processInteract(player, hand);
            }
            else
            {
                this.mountTo(player);
                return true;
            }
        }
    }
}