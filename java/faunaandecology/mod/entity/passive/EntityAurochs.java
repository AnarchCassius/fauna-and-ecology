package faunaandecology.mod.entity.passive;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;

import faunaandecology.mod.entity.IEntityAdvanced;
import faunaandecology.mod.entity.IEntityAdvancedMilkable;
import faunaandecology.mod.entity.ai.EntityAIGrainEater;
import faunaandecology.mod.entity.ai.EntityAIHerdMovement;
import faunaandecology.mod.entity.ai.EntityAIImprovedFollowParent;
import faunaandecology.mod.entity.ai.EntityAIImprovedMate;
import faunaandecology.mod.entity.ai.EntityAIItemSearch;
import faunaandecology.mod.util.Config;
import net.minecraft.block.Block;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIEatGrass;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIPanic;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAITempt;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.monster.EntityPolarBear;
import net.minecraft.entity.passive.AbstractHorse;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemMonsterPlacer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.server.management.PreYggdrasilConverter;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.datafix.DataFixer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;
import net.minecraft.world.storage.loot.LootTableList;

public class EntityAurochs extends EntityImprovedCow implements IEntityAdvancedMilkable
{
	///
    private static final DataParameter<Optional<UUID>> OWNER_UNIQUE_ID = EntityDataManager.<Optional<UUID>>createKey(EntityAurochs.class, DataSerializers.OPTIONAL_UNIQUE_ID);
	private static final Set<Item> TEMPTATION_ITEMS = Sets.newHashSet(Items.WHEAT_SEEDS, Items.MELON_SEEDS, Items.PUMPKIN_SEEDS, Items.BEETROOT_SEEDS,
			Items.REEDS, Item.getItemFromBlock(Blocks.YELLOW_FLOWER), Items.APPLE, Items.CARROT, Items.WHEAT, Items.BEETROOT, Items.GOLDEN_APPLE,
			Items.GOLDEN_CARROT, Items.MELON, Items.SPECKLED_MELON);
	
//    private static final DataParameter<Boolean> SADDLE = EntityDataManager.<Boolean>createKey(EntityAurochs.class, DataSerializers.BOOLEAN);
//    private static final DataParameter<Integer> OLD_AGE = EntityDataManager.<Integer>createKey(EntityAurochs.class, DataSerializers.VARINT);
//    protected static final DataParameter<Integer> SEX = EntityDataManager.<Integer>createKey(EntityAurochs.class, DataSerializers.VARINT);
//    protected static final DataParameter<Integer> TAMABILITY = EntityDataManager.<Integer>createKey(EntityAurochs.class, DataSerializers.VARINT);
//    private static final DataParameter<Float> HUNGER = EntityDataManager.<Float>createKey(EntityAurochs.class, DataSerializers.FLOAT);
//    private static final DataParameter<Integer> HAPPINESS = EntityDataManager.<Integer>createKey(EntityAurochs.class, DataSerializers.VARINT);
//    ///
//    private static final DataParameter<Integer> MAXMILK = EntityDataManager.<Integer>createKey(EntityAurochs.class, DataSerializers.VARINT);
//    private static final DataParameter<Integer> CURMILK = EntityDataManager.<Integer>createKey(EntityAurochs.class, DataSerializers.VARINT);
//    private static final DataParameter<Integer> MILK_TIMER = EntityDataManager.<Integer>createKey(EntityAurochs.class, DataSerializers.VARINT);
//
//    
//    private static final DataParameter<Integer> DATA_STRENGTH_ID = EntityDataManager.<Integer>createKey(EntityAurochs.class, DataSerializers.VARINT);

    //new ai

    private EntityAIAvoidEntity<EntityPlayer> avoidEntity;
    /** The tempt AI task for this mob, used to prevent taming while it is fleeing. */
    //private EntityAITempt aiTempt;

    private EntityAIPanic aiPanic;
    private EntityAIAttackMelee aiAttack;
    private EntityAIHerdMovement aiHerd;
    ///
    /** "The higher this value, the more likely the bovine is to be tamed." */
    protected int temper;
    
    private int parentTamability;
    private float parentMaxHealth;;
    private int parentMaxMilk;
    
    private int pregnancyTime;
    private int groupTimer = 0;
    static public int pregnancyLength = 18000;
    static public float foodMax = 75.0F;
    static public int ageMax = 528000; //22 'years'
    static public int childAge = -36000; //18 'monthes' 

    ////
    
    public EntityAurochs(World worldIn)
    {
        super(worldIn);
        this.setSize(0.9F, 1.35F);
        
    }


    @Override
    public String getZoopediaName()
    {
    	return "Aurochs";
    }

    @Override
    public float getZoopediaHealth()
    {
    	return this.getHealth();
    }
    
    @Override
    public float getZoopediaMaxHealth()
    {
    	return this.getMaxHealth();
    }
    
    public static void registerFixesCow(DataFixer fixer)
    {
    	EntityLiving.registerFixesMob(fixer, EntityAurochs.class);
    }


    @Override
	protected void initEntityAI()
    {
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.aiPanic = new EntityAIPanic(this, 1.75D);
        this.aiAttack = new EntityAIAttackMelee(this, 1.75D, false);
        this.aiHerd = new EntityAIHerdMovement(this, 1.0D);
        this.tasks.addTask(1, this.aiAttack);
        this.tasks.addTask(2, this.aiPanic);
        this.tasks.addTask(3, this.aiHerd);
        this.tasks.addTask(4, new EntityAIImprovedMate(this, 1.0D, EntityCow.class));
        //this.tasks.addTask(4, new EntityAITempt(this, 1.0D, ItemInit.WHEAT_ON_A_STICK, false));
        this.tasks.addTask(5, new EntityAITempt(this, 1.0D, false, TEMPTATION_ITEMS));

        this.tasks.addTask(7, new  EntityAIImprovedFollowParent(this, 1.25D, EntityCow.class));
        this.tasks.addTask(8, new EntityAIItemSearch(this, 1.0D));
        this.tasks.addTask(9, new EntityAIEatGrass(this));
        this.tasks.addTask(9, new EntityAIGrainEater(this));
        this.tasks.addTask(10, new EntityAIWander(this, 1.0D));
        this.tasks.addTask(11, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
        this.tasks.addTask(12, new EntityAILookIdle(this));
        this.targetTasks.addTask(2, new EntityAIHurtByTarget(this, true, new Class[0]));
    }

    @Override
	protected SoundEvent getAmbientSound()
    {
        return SoundEvents.ENTITY_COW_AMBIENT;
    }

    protected SoundEvent getHurtSound()
    {
        return SoundEvents.ENTITY_COW_HURT;
    }

    @Override
	protected SoundEvent getDeathSound()
    {
        return SoundEvents.ENTITY_COW_DEATH;
    }

    @Override
	protected void playStepSound(BlockPos pos, Block blockIn)
    {
        this.playSound(SoundEvents.ENTITY_COW_STEP, 0.15F, 1.0F);
    }

    /**
     * Returns the volume for the sounds this mob makes.
     */
    @Override
	protected float getSoundVolume()
    {
        return 0.4F;
    }

    @Override
	@Nullable
    protected ResourceLocation getLootTable()
    {
        return LootTableList.ENTITIES_COW;
    }


//    @Override
//    public boolean processInteract(EntityPlayer player, EnumHand hand)
//    {
//
//        ItemStack stack = player.getHeldItem(hand);
//        
//        //override of EntityAgeable to allow varied growth times
//        if (stack != null && stack.getItem() == Items.SPAWN_EGG)
//        {
//            ItemStack itemstack = player.getHeldItem(hand);
//            if (!this.world.isRemote)
//            {
//                Class <? extends Entity > oclass = EntityList.getClass(ItemMonsterPlacer.getNamedIdFrom(itemstack));
//
//                if (oclass != null && this.getClass() == oclass)
//                {
//                    EntityAgeable entityageable = this.createChild(this);
//
//                    if (entityageable != null)
//                    {
//                        entityageable.setGrowingAge(childAge);
//                        entityageable.setLocationAndAngles(this.posX, this.posY, this.posZ, 0.0F, 0.0F);
//                        this.world.spawnEntity(entityageable);
//
//                        if (stack.hasDisplayName())
//                        {
//                            entityageable.setCustomNameTag(stack.getDisplayName());
//                        }
//
//                        if (!player.capabilities.isCreativeMode)
//                        {
//                            stack.setCount(stack.getCount() -1);
//                        }
//                    }
//                }
//            }
//
//            return true;
//        } //
//        else
//        {
//
//        	//breeding item overrides
//            if (stack != null)
//            {
//            	if (Config.enableAdvancedStats == true)
//            	{
//            		if (this.getHunger() < foodMax - 0.5F)
//            		{
//    	        		if  (Config.enableWildBreeding == true)
//    	        		{
//    	                    if (this.isBreedingItem(stack)  && !(this.isChild()) && !(this.isInLove()) )
//    	                    {
//    	                        this.consumeItemFromStack(player, stack);
//    	                        return true;
//    	                    }
//    	                    else if (this.isBreedingItem(stack))
//    	                    {
//    	                        this.consumeItemFromStack(player, stack);
//    	                        return true;
//    	                    }
//    	        		}
//    	        		else
//    	        		{
//    	                    if (this.isBreedingItem(stack) && !(this.isChild()) && !(this.isInLove()) )
//    	                    {
//    	                        this.consumeItemFromStack(player, stack);
//    	                        this.setInLove(player);
//    	                        return true;
//    	                    }
//    	                    else if (this.isBreedingItem(stack))
//    	                    {
//    	                        this.consumeItemFromStack(player, stack);
//    	                        return true;
//    	                    }
//    	        		}
//            		}
//
//                    if (this.isEdible(stack))
//                    {
//                    	if  (this.getHunger() <  foodMax - 0.5F)
//                    	{
//        	                this.consumeItemFromStack(player, stack);
//        	                return true;
//                    	}
//                    }
//                    
//                    if (stack.getItem() == Items.BUCKET && !player.capabilities.isCreativeMode && !this.isChild())
//                    {
//                    	if ( this.getSex() == 0)
//                    	{
//                    	}
//                    	else	
//                    	{
//                    		player.playSound(SoundEvents.ENTITY_COW_HURT, 1.0F, 1.0F);
//                    		this.setHappiness(this.getHappiness()-3);
//                    	}
//                    	return false;
//                    }
//            	}
//                
//            }
//            
//            return super.processInteract(player, hand);
//        }
//        
//    }




    @Override
	public float getEyeHeight()
    {
        return this.isChild() ? this.height : 1.3F;
    }
    
    
    //Getters and setters
    @Override
	public int getSex()
    {
        return this.dataManager.get(SEX).intValue();
    }
    
    @Override
	public void setSex(int sexIn)
    {
        this.dataManager.set(SEX, Integer.valueOf(sexIn));
    }
    
    @Override
	public int getTamability()
    {

        return this.dataManager.get(TAMABILITY).intValue();
    }
    
    @Override
	public void setTamability(int tamabilityIn)
    {
        this.dataManager.set(TAMABILITY, Integer.valueOf(tamabilityIn));
    }
    

    @Override
	public int getMaxTemper()
    {
        return 75 - ( this.getTamability() * 5 );        	
    }

    @Override
	public boolean isTame()
    {
        return (this.getOwnerId() != null);
    }

    @Override
	public boolean isTamed()
    {
        return (this.getOwnerId() != null);
    }

    @Override
	public void setTamed(boolean tamed)
    {
        //new
        //this.setupTamedAI();
    }


    @Override
	public boolean setTamedBy(EntityPlayer player)
    {
        this.setOwnerId(player.getUniqueID());
        this.setTamed(true);
        return true;
    }

    protected void setupTamedAI()
    {
        if (this.avoidEntity == null)
        {
            this.avoidEntity = new EntityAIAvoidEntity<EntityPlayer>(this, EntityPlayer.class, 16.0F, 1.0D, 2.0D);
        }
        if (this.aiPanic == null)
        {
            this.aiPanic = new EntityAIPanic(this, 1.75D);
        }
        if (this.aiAttack == null)
        {
            this.aiAttack = new EntityAIAttackMelee(this, 1.75D, false);
        }
        if (this.aiHerd == null)
        {
            this.aiHerd =  new EntityAIHerdMovement(this, 1.0D);
        }

        this.tasks.removeTask(this.avoidEntity);
        this.tasks.removeTask(this.aiPanic);
        this.tasks.removeTask(this.aiAttack);
        this.tasks.removeTask(this.aiHerd);
        
        if (this.isChild())
    	{
        	this.tasks.addTask(2, this.aiPanic);
        	return;
    	}
        
        this.tasks.addTask(3, this.aiHerd);
        
        if (!this.isTame())
        {
        	if (this.getHappiness() <= -50)
        	{
        		this.tasks.addTask(6, this.avoidEntity);        		
        	}
            
    		this.tasks.addTask(1, this.aiAttack);
        }
    }

    
    //////////////////////////////
    ///init and save

    @Override
	protected void entityInit()
    {
        super.entityInit();
//        this.dataManager.register(DATA_STRENGTH_ID, Integer.valueOf(0));
//        this.dataManager.register(OLD_AGE, Integer.valueOf(0));
//        this.dataManager.register(SEX, Integer.valueOf(0));
//        this.dataManager.register(HUNGER, Float.valueOf(0));
//        this.dataManager.register(HAPPINESS, Integer.valueOf(0));
//        this.dataManager.register(TAMABILITY, Integer.valueOf(0));
//        this.dataManager.register(MAXMILK, Integer.valueOf(0));
//        this.dataManager.register(CURMILK, Integer.valueOf(0));
//        this.dataManager.register(MILK_TIMER, Integer.valueOf(0));
//        this.dataManager.register(OWNER_UNIQUE_ID, Optional.<UUID>absent());
        //this.dataManager.register(SADDLE, Boolean.valueOf(false));
    }
    

    /**
     * Returns true if the mob is currently able to mate with the specified mob.
     */
    @Override
	public boolean canMateWith(EntityAnimal otherAnimal)
    {
        if (otherAnimal == this)
        {
            return false;
        }
        else if (otherAnimal instanceof EntityImprovedCow || otherAnimal instanceof EntityAurochs)
        {
        	IEntityAdvanced entitycow = (IEntityAdvanced)otherAnimal;

            if ( this.isInLove() && otherAnimal.isInLove() && (this.getSex() != entitycow.getSex() || (Config.enableSexes == false) ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    ////generation
    

    /**
     * Called only once on an entity when first time spawned, via egg, mob spawner, natural spawning etc, but not called
     * when entity is reloaded from nbt. Mainly used for initializing attributes and inventory
     */
    @Override
	@Nullable
    public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty, @Nullable IEntityLivingData livingdata)
    {
    	super.onInitialSpawn(difficulty, livingdata);

        int randroll = this.rand.nextInt(3);
		if (Config.enableSexes == true)
		{
	        if (randroll == 1)
			{
	            this.dataManager.set(SEX, Integer.valueOf(1));	
			}
	        else
	        {
	            this.dataManager.set(SEX, Integer.valueOf(0));
	        }
	        randroll = this.rand.nextInt(2);
        }
		
		randroll = this.rand.nextInt(2);
		if (randroll == 1)
		{
    		this.dataManager.set(TAMABILITY, Integer.valueOf(-4));
		}
        else
        {
    		this.dataManager.set(TAMABILITY, Integer.valueOf(-5));
        }

		this.setHunger(foodMax);
		this.setHappiness(0);
		this.setMaxMilk(this.rand.nextInt(5) + this.rand.nextInt(6));
		this.setCurMilk(this.getMaxMilk());
		this.setMilkTimer(0);

        if (this.rand.nextInt(5) == 0)
        {
            this.setGrowingAge(childAge);
            this.setOldAge(0);
        }    	

        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.getModifiedMaxHealth());
        this.setHealth(this.getMaxHealth());
        //new
        this.setupTamedAI();
        return livingdata;
    }
    /////
    
    @Override
    public boolean attackEntityFrom(DamageSource par1DamageSource, float par2)
    {
    	this.setHappiness(this.getHappiness()-50);
        return super.attackEntityFrom(par1DamageSource, par2);
    }

    @Override
	public boolean attackEntityAsMob(Entity entityIn)
    {
        float f = (float)this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getAttributeValue();
        int i = 0;

        if (entityIn instanceof EntityLivingBase)
        {
            f += EnchantmentHelper.getModifierForCreature(this.getHeldItemMainhand(), ((EntityLivingBase)entityIn).getCreatureAttribute());
            i += EnchantmentHelper.getKnockbackModifier(this);
        }

        boolean flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), f);

        if (flag)
        {
            if (i > 0 && entityIn instanceof EntityLivingBase)
            {
                ((EntityLivingBase)entityIn).knockBack(this, i * 0.5F, MathHelper.sin(this.rotationYaw * 0.017453292F), (-MathHelper.cos(this.rotationYaw * 0.017453292F)));
                this.motionX *= 0.6D;
                this.motionZ *= 0.6D;
            }

            int j = EnchantmentHelper.getFireAspectModifier(this);

            if (j > 0)
            {
                entityIn.setFire(j * 4);
            }

            if (entityIn instanceof EntityPlayer)
            {
                EntityPlayer entityplayer = (EntityPlayer)entityIn;
                ItemStack itemstack = this.getHeldItemMainhand();
                ItemStack itemstack1 = entityplayer.isHandActive() ? entityplayer.getActiveItemStack() : null;

                if (itemstack != null && itemstack1 != null && itemstack.getItem() instanceof ItemAxe && itemstack1.getItem() == Items.SHIELD)
                {
                    float f1 = 0.25F + EnchantmentHelper.getEfficiencyModifier(this) * 0.05F;

                    if (this.rand.nextFloat() < f1)
                    {
                        entityplayer.getCooldownTracker().setCooldown(Items.SHIELD, 100);
                        this.world.setEntityState(entityplayer, (byte)30);
                    }
                }
            }

            this.applyEnchantments(this, entityIn);
        }

        return flag;
    }

    /**
     * This function applies the benefits of growing back wool and faster growing up to the acting entity. (This
     * function is used in the AIEatGrass)
     */
    @Override
	public void eatGrassBonus()
    {
        this.setHunger(this.getHunger() + 4);
        this.setHappiness(this.getHappiness() + 1);
        return;
    }
    
    public boolean isEdible(ItemStack stack)
    {
    	if (EntityAurochs.TEMPTATION_ITEMS.contains(stack.getItem())) return true;
    	return false;
    }
 
    @Override
	public boolean isEdible(EntityItem entityitem)
	{
    	if (EntityAurochs.TEMPTATION_ITEMS.contains(entityitem.getItem().getItem())) return true;
    	return false;
	}
	
    
	@Override
	public int getChildAge() 
	{
		return EntityAurochs.childAge;
	}
	
    @Override
	public float getGrowthSize()
    {
    	if (this.isChild())
    	{
    		float growth = EntityAurochs.childAge + this.getOldAge();
	    	float ratio = growth / EntityAurochs.childAge;
	    	return 1.0F - ratio;
    	}
    	return 1.0F;
    }
    
    @Override
	public int getPreferedGroupSize()
    {
    	return 5;
    }

    @Override
	public float getFoodMax()
    {
        return foodMax;
    }

    /////////////////////
 

    @Override
	public int getPregnancyTime()
    {
        return this.pregnancyTime;
    }
    
    @Override
	public void setPregnancyTime(int age)
    {
        this.pregnancyTime = age;
    }
    
    @Override
	public void getOtherParentData(IEntityAdvanced parent)
    {
    	if (parent instanceof EntityAurochs)
    	{
    		EntityAurochs otherParent = (EntityAurochs) parent;
            this.parentTamability = otherParent.getTamability();
            this.parentMaxHealth = otherParent.getMaxHealth();
            this.parentMaxMilk = otherParent.getMaxMilk();
    	}
    	else if (parent instanceof EntityImprovedCow)
    	{
			EntityImprovedCow otherParent = (EntityImprovedCow) parent;
            this.parentTamability = otherParent.getTamability();
            this.parentMaxHealth = otherParent.getMaxHealth();
            this.parentMaxMilk = otherParent.getMaxMilk();
    	}
    	else //failsafe
    	{
            this.parentTamability = this.getTamability();
            this.parentMaxHealth = this.getMaxHealth();
            this.parentMaxMilk = this.getMaxMilk();
    	}
        return;
    }
    

    /**
     * Checks if the parameter is an item which this animal can be fed to breed it (wheat, carrots or seeds depending on
     * the animal type)
     */
    @Override
    public boolean isBreedingItem(@Nullable ItemStack stack)
    {
        return stack != null && TEMPTATION_ITEMS.contains(stack.getItem());
    }


    private float getModifiedMaxHealth()
    {
        return 20.0F + this.rand.nextInt(5) + this.rand.nextInt(5);
    }
    
    

    class AIAttackPlayer extends EntityAINearestAttackableTarget<EntityPlayer>
    {
        public AIAttackPlayer()
        {
            super(EntityAurochs.this, EntityPlayer.class, 20, true, true, (Predicate)null);
        }

        /**
         * Returns whether the EntityAIBase should begin execution.
         */
        @Override
		public boolean shouldExecute()
        {
            if (EntityAurochs.this.isChild())
            {
                return false;
            }
            else
            {
                if (super.shouldExecute())
                {
                    for (EntityPolarBear entitypolarbear : EntityAurochs.this.world.getEntitiesWithinAABB(EntityPolarBear.class, EntityAurochs.this.getEntityBoundingBox().grow(8.0D, 4.0D, 8.0D)))
                    {
                        if (entitypolarbear.isChild())
                        {
                            return true;
                        }
                    }
                }

                EntityAurochs.this.setAttackTarget((EntityLivingBase)null);
                return false;
            }
        }

        @Override
		protected double getTargetDistance()
        {
            return super.getTargetDistance() * 0.5D;
        }
    }
    

}