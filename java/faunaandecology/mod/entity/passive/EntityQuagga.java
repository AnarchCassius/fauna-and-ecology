package faunaandecology.mod.entity.passive;

import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityQuagga extends EntityZebra
{
    private String texturePrefix;
    private String parentType = "Quagga";
    private final String[] horseTexturesArray = new String[3];

	public EntityQuagga(World worldIn) {
		super(worldIn);
		// TODO Auto-generated constructor stub
	}
	
    @Override
    public String getZoopediaName()
    {
    	return "Quagga";
    }

    private void setHorseTexturePaths()
    {
        int i = 0;
        int j = (i & 255) % 7;
        int k = ((i & 65280) >> 8) % 5;
        String texture = null;
        this.horseTexturesArray[0] = "faunaandecology:textures/entity/horse/quagga.png";
        this.horseTexturesArray[1] = null;
        this.horseTexturesArray[2] = texture;
        this.texturePrefix = "horse/" + "qua" + "qua" + texture;
    }


    @Override
    public String getHorseTexture()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.texturePrefix;
    }

    @Override
    public String[] getVariantTexturePaths()
    {
        if (this.texturePrefix == null)
        {
            this.setHorseTexturePaths();
        }

        return this.horseTexturesArray;
    }
    
    @Override
	public int getMaxTemper()
    {
    	return 150 - ( this.getTamability() * 5 );
    }

    @Override
	public float getModifiedMaxHealth()
    {
    	return 15.0F + this.rand.nextInt(8) + this.rand.nextInt(9);
    }


}
