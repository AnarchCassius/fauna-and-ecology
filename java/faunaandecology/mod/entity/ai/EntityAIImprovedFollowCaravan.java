package faunaandecology.mod.entity.ai;

import java.util.List;

import faunaandecology.mod.entity.passive.EntityImprovedLlama;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.util.math.Vec3d;

public class EntityAIImprovedFollowCaravan extends EntityAIBase
{
    public EntityImprovedLlama llama;
    private double speedModifier;
    private int distCheckCounter;

    public EntityAIImprovedFollowCaravan(EntityImprovedLlama llamaIn, double speedModifierIn)
    {
        this.llama = llamaIn;
        this.speedModifier = speedModifierIn;
        this.setMutexBits(1);
    }

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    @Override
	public boolean shouldExecute()
    {
        if (!this.llama.getLeashed() && !this.llama.inCaravan())
        {
            List<EntityImprovedLlama> list = this.llama.world.<EntityImprovedLlama>getEntitiesWithinAABB(this.llama.getClass(), this.llama.getEntityBoundingBox().grow(9.0D, 4.0D, 9.0D));
            EntityImprovedLlama EntityImprovedLlama = null;
            double d0 = Double.MAX_VALUE;

            for (EntityImprovedLlama EntityImprovedLlama1 : list)
            {
                if (EntityImprovedLlama1.inCaravan() && !EntityImprovedLlama1.hasCaravanTrail())
                {
                    double d1 = this.llama.getDistanceSq(EntityImprovedLlama1);

                    if (d1 <= d0)
                    {
                        d0 = d1;
                        EntityImprovedLlama = EntityImprovedLlama1;
                    }
                }
            }

            if (EntityImprovedLlama == null)
            {
                for (EntityImprovedLlama EntityImprovedLlama2 : list)
                {
                    if (EntityImprovedLlama2.getLeashed() && !EntityImprovedLlama2.hasCaravanTrail())
                    {
                        double d2 = this.llama.getDistanceSq(EntityImprovedLlama2);

                        if (d2 <= d0)
                        {
                            d0 = d2;
                            EntityImprovedLlama = EntityImprovedLlama2;
                        }
                    }
                }
            }

            if (EntityImprovedLlama == null)
            {
                return false;
            }
            else if (d0 < 4.0D)
            {
                return false;
            }
            else if (!EntityImprovedLlama.getLeashed() && !this.firstIsLeashed(EntityImprovedLlama, 1))
            {
                return false;
            }
            else
            {
                this.llama.joinCaravan(EntityImprovedLlama);
                return true;
            }
        }
        else
        {
            return false;
        }
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    @Override
	public boolean shouldContinueExecuting()
    {
        if (this.llama.inCaravan() && this.llama.getCaravanHead().isEntityAlive() && this.firstIsLeashed(this.llama, 0))
        {
            double d0 = this.llama.getDistanceSq(this.llama.getCaravanHead());

            if (d0 > 676.0D)
            {
                if (this.speedModifier <= 3.0D)
                {
                    this.speedModifier *= 1.2D;
                    this.distCheckCounter = 40;
                    return true;
                }

                if (this.distCheckCounter == 0)
                {
                    return false;
                }
            }

            if (this.distCheckCounter > 0)
            {
                --this.distCheckCounter;
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Reset the task's internal state. Called when this task is interrupted by another one
     */
    @Override
	public void resetTask()
    {
        this.llama.leaveCaravan();
        this.speedModifier = 2.1D;
    }

    /**
     * Keep ticking a continuous task that has already been started
     */
    @Override
	public void updateTask()
    {
        if (this.llama.inCaravan())
        {
            EntityImprovedLlama EntityImprovedLlama = this.llama.getCaravanHead();
            double d0 = this.llama.getDistance(EntityImprovedLlama);
            Vec3d vec3d = (new Vec3d(EntityImprovedLlama.posX - this.llama.posX, EntityImprovedLlama.posY - this.llama.posY, EntityImprovedLlama.posZ - this.llama.posZ)).normalize().scale(Math.max(d0 - 2.0D, 0.0D));
            this.llama.getNavigator().tryMoveToXYZ(this.llama.posX + vec3d.x, this.llama.posY + vec3d.y, this.llama.posZ + vec3d.z, this.speedModifier);
        }
    }

    private boolean firstIsLeashed(EntityImprovedLlama p_190858_1_, int p_190858_2_)
    {
        if (p_190858_2_ > 8)
        {
            return false;
        }
        else if (p_190858_1_.inCaravan())
        {
            if (p_190858_1_.getCaravanHead().getLeashed())
            {
                return true;
            }
            else
            {
                EntityImprovedLlama EntityImprovedLlama = p_190858_1_.getCaravanHead();
                ++p_190858_2_;
                return this.firstIsLeashed(EntityImprovedLlama, p_190858_2_);
            }
        }
        else
        {
            return false;
        }
    }
}