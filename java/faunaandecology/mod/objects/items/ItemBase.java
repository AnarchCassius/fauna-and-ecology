package faunaandecology.mod.objects.items;

import faunaandecology.mod.Main;
import faunaandecology.mod.init.ItemInit;
import faunaandecology.mod.util.IHasModel;
import faunaandecology.mod.util.Reference;
import net.minecraft.item.Item;

public class ItemBase extends Item implements IHasModel
{

	public ItemBase(String name)
	{
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(Reference.FaunaAndEcology);
		
		ItemInit.ITEMS.add(this);
	}

	@Override
	public void registerModels() 
	{
		// TODO Auto-generated method stub
		Main.proxy.registerItemRenderer(this, 0, "inventory");		
	}
	
}
