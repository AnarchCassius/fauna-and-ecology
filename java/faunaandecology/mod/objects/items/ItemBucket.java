package faunaandecology.mod.objects.items;

import faunaandecology.mod.Main;
import faunaandecology.mod.init.ItemInit;
import faunaandecology.mod.util.IHasModel;
import faunaandecology.mod.util.Reference;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class ItemBucket extends Item implements IHasModel
{

	protected int maxStackSize = 1;
	
	public ItemBucket(String name)
	{
        this.setMaxStackSize(1);
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(Reference.FaunaAndEcology);
		
		ItemInit.ITEMS.add(this);
	}

	@Override
	public void registerModels() 
	{
		// TODO Auto-generated method stub
		Main.proxy.registerItemRenderer(this, 0, "inventory");
	}
	

	@Override
	public Item getContainerItem() 
	{
		return Items.BUCKET;
	}
	
	
	@Override
	public boolean hasContainerItem() 
	{
		return true;
	}
	
}
