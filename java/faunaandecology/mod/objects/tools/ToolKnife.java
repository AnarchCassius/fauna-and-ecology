package faunaandecology.mod.objects.tools;

import java.util.Set;

import com.google.common.collect.Sets;

import faunaandecology.mod.Main;
import faunaandecology.mod.init.ItemInit;
import faunaandecology.mod.objects.blocks.BlockHide;
import faunaandecology.mod.util.IHasModel;
import faunaandecology.mod.util.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.EnchantmentDamage;
import net.minecraft.enchantment.EnchantmentFireAspect;
import net.minecraft.enchantment.EnchantmentMending;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.util.math.MathHelper;

public class ToolKnife extends ItemTool implements IHasModel 
{
    private static final Set<Block> EFFECTIVE_ON = Sets.newHashSet(new Block[] {Blocks.WEB, Blocks.LEAVES, Blocks.LOG, Blocks.LOG2, Blocks.PUMPKIN, Blocks.LIT_PUMPKIN, Blocks.MELON_BLOCK});

    private String toolClass;
	public ToolKnife(String name, ToolMaterial material)
	{
        super(material, EFFECTIVE_ON);
		setUnlocalizedName(name);
		setRegistryName(name);
		this.toolClass = "knife";
		setCreativeTab(CreativeTabs.TOOLS);
		
		ItemInit.ITEMS.add(this);
	}

    protected ToolKnife(String name, Item.ToolMaterial material, float damage, float speed)
    {
        super(material, EFFECTIVE_ON);
        setUnlocalizedName(name);
		setRegistryName(name);
		this.toolClass = "knife";
		setCreativeTab(Reference.FaunaAndEcology);
        this.attackDamage = damage + material.getAttackDamage();
        this.attackSpeed = speed;
		
		ItemInit.ITEMS.add(this);
	}

    @Override
	public float getDestroySpeed(ItemStack stack, IBlockState state)
    {
        Material material = state.getMaterial();

        if (state.getBlock() == Blocks.WEB)
        {
            return 15.0F;
        }
        else if (material == Material.LEAVES || material == Material.VINE || material == Material.CLOTH || material == Material.CARPET  || material == Material.WEB || material == Material.PLANTS) return this.efficiency;
        return material != Material.WOOD ? ToolKnife.EFFECTIVE_ON.contains(state.getBlock()) ? this.efficiency : 1.0F : Math.max(this.efficiency/2, 1.0F);
    }
    
	@Override
	public void registerModels() 
	{
		// TODO Auto-generated method stub
		Main.proxy.registerItemRenderer(this, 0, "inventory");
	}
	
    /**
     * Check whether this Item can harvest the given Block
     */
    @Override
	public boolean canHarvestBlock(IBlockState blockIn)
    {
        return (blockIn.getBlock() == Blocks.WEB || blockIn.getBlock().getClass() == BlockHide.class);
    }

    @Override
    public int getHarvestLevel(ItemStack stack, String toolClass, @javax.annotation.Nullable net.minecraft.entity.player.EntityPlayer player, @javax.annotation.Nullable IBlockState blockState)
    {
        int level = super.getHarvestLevel(stack, toolClass,  player, blockState);
        if (level == -1 && toolClass.equals(this.toolClass))
        {
            return this.toolMaterial.getHarvestLevel();
        }
        else
        {
            return level;
        }
    }

    @Override
    public Set<String> getToolClasses(ItemStack stack)
    {
        return toolClass != null ? com.google.common.collect.ImmutableSet.of(toolClass) : super.getToolClasses(stack);
    }

    /**
     * Checks whether an item can be enchanted with a certain enchantment. This applies specifically to enchanting an item in the enchanting table and is called when retrieving the list of possible enchantments for an item.
     * Enchantments may additionally (or exclusively) be doing their own checks in {@link net.minecraft.enchantment.Enchantment#canApplyAtEnchantingTable(ItemStack)}; check the individual implementation for reference.
     * By default this will check if the enchantment type is valid for this item type.
     * @param stack the item stack to be enchanted
     * @param enchantment the enchantment to be applied
     * @return true if the enchantment can be applied to this item
     */
    @Override
	public boolean canApplyAtEnchantingTable(ItemStack stack, net.minecraft.enchantment.Enchantment enchantment)
    {
    	if (enchantment instanceof EnchantmentDamage) return true;
    	if (enchantment instanceof EnchantmentFireAspect) return true;
    	if (enchantment instanceof EnchantmentMending) return true;
        return enchantment.type.canEnchantItem(stack.getItem());
    }
    
    @Override
    public boolean hasContainerItem()
    {
    	 return true;
    }
    /**
     * Current implementations of this method in child classes do not use the entry argument beside ev. They just raise
     * the damage on the stack.
     */
    @Override
	public boolean hitEntity(ItemStack stack, EntityLivingBase target, EntityLivingBase attacker)
    {
        if (target instanceof EntityLivingBase)
        {
            target.knockBack(attacker, -0.1F, MathHelper.sin(attacker.rotationYaw * 0.017453292F), (-MathHelper.cos(attacker.rotationYaw * 0.017453292F)));
        }
        
        stack.damageItem(1, attacker);
        return true;
    }
    
    
    
}
