package faunaandecology.mod;

import java.io.File;

import faunaandecology.mod.init.EntityInit;
import faunaandecology.mod.proxy.CommonProxy;
import faunaandecology.mod.util.Config;
import faunaandecology.mod.util.Reference;
import faunaandecology.mod.util.handlers.CraftingHandler;
import faunaandecology.mod.util.handlers.EventsHandler;
import faunaandecology.mod.util.handlers.FuelHandler;
import faunaandecology.mod.util.handlers.GUIHandler;
import faunaandecology.mod.util.handlers.RecipeHandler;
import faunaandecology.mod.util.handlers.RegistryHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = Reference.MODID, name = Reference.NAME,  version = Reference.VERSION)

public class Main {


	public static boolean isFAMMloaded;

	@Mod.Instance
	public static Main instance;
	
	@SidedProxy(clientSide = Reference.CLIENTPROXY, serverSide = Reference.COMMONPROXY)
	public static CommonProxy proxy;
	
	@EventHandler
	public static void preInit(FMLPreInitializationEvent event)
	{
		MinecraftForge.EVENT_BUS.register(new EventsHandler());
		
		//config
        File directory = event.getModConfigurationDirectory();
        CommonProxy.config = new Configuration(new File(directory.getPath(), "f&e.cfg"));
        Config.readConfig();

		CraftingHandler.removeRecipes();

        EntityInit.init();
        proxy.initModels();
     
  		GameRegistry.registerFuelHandler(new FuelHandler());   
	}
	
	@EventHandler
	public static void init(FMLInitializationEvent event) 
	{

		RegistryHandler.initRegistries();
		

        NetworkRegistry.INSTANCE.registerGuiHandler(Main.instance, new GUIHandler());

	}
	
	@EventHandler
	public static void postInit(FMLPostInitializationEvent event) 
	{

		//BiomeDictionary.registerAllBiomes();
		
		if (Loader.isModLoaded("famm")){
			isFAMMloaded = true;
		}
			
		RecipeHandler.RegisterCrafting();
		RecipeHandler.RegisterSmelting();
		
        EntityInit.postInit();
		//config
        if (CommonProxy.config.hasChanged()) {
        	CommonProxy.config.save();
        }
        
	}
	
	
}
