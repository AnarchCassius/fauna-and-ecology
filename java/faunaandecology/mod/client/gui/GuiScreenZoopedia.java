package faunaandecology.mod.client.gui;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import faunaandecology.mod.entity.IEntityAdvanced;
import faunaandecology.mod.util.Reference;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiScreenZoopedia extends GuiScreen
{
    private final int bookImageHeight = 160;
    private final int bookImageWidth = 256;
    private int currPage = 0;
    private static final int bookTotalPages = 1;
    private static ResourceLocation[] bookPageTextures = 
          new ResourceLocation[bookTotalPages];
    private static String[] stringPageText = new String[bookTotalPages];
    private GuiButton buttonDone;
    private NextPageButton buttonNextPage;
    private NextPageButton buttonPreviousPage;
    private IEntityAdvanced thisAnimal;
    public GuiScreenZoopedia(IEntityAdvanced examinedAnimal)
    {
        bookPageTextures[0] = new ResourceLocation(
              Reference.MODID+":textures/gui/zoopedia/zoopedia_page.png");
        //bookPageTextures[1] = new ResourceLocation(
//        		Reference.MODID+":textures/gui/zoopedia/zoopedia_page.png");
        stringPageText[0] = "";
        //stringPageText[1] = "";
        this.thisAnimal = examinedAnimal;
 }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    @Override
    public void initGui() 
    {
     // DEBUG
     System.out.println("GuiMysteriousStranger initGUI()");
        buttonList.clear();
        Keyboard.enableRepeatEvents(true);

        buttonDone = new GuiButton(0, width / 2 + 2, 4 + bookImageHeight, 
              98, 20, I18n.format("gui.done", new Object[0]));
  
        buttonList.add(buttonDone);
        int offsetFromScreenLeft = (width - bookImageWidth) / 2;
        buttonList.add(buttonNextPage = new NextPageButton(1, 
              offsetFromScreenLeft + 120, 156, true));
        buttonList.add(buttonPreviousPage = new NextPageButton(2, 
              offsetFromScreenLeft + 38, 156, false));
    }

    /**
     * Called from the main game loop to update the screen.
     */
    @Override
    public void updateScreen() 
    {
        buttonDone.visible = false;
        buttonNextPage.visible = false;
        buttonPreviousPage.visible = false;
        buttonDone.visible = (currPage == bookTotalPages - 1);
        buttonNextPage.visible = (currPage < bookTotalPages - 1);
        buttonPreviousPage.visible = currPage > 0;
    }
 
    /**
     * Draws the screen and all the components in it.
     */
    @Override
    public void drawScreen(int parWidth, int parHeight, float p_73863_3_)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         mc.getTextureManager().bindTexture(bookPageTextures[0]);
        int offsetFromScreenLeft = (width - bookImageWidth ) / 2;
        drawTexturedModalRect(offsetFromScreenLeft, 2, 0, 0, bookImageWidth, 
              bookImageHeight);
        
        int widthOfString;
        String stringPageIndicator = I18n.format("book.pageIndicator", 
              new Object[] {Integer.valueOf(currPage + 1), bookTotalPages});
        widthOfString = fontRenderer.getStringWidth(stringPageIndicator);
        

        
        fontRenderer.drawString(""+this.thisAnimal.getZoopediaName(), 
                offsetFromScreenLeft - widthOfString + bookImageWidth - 185, 
                15, 0);
       
        fontRenderer.drawString(stringPageIndicator, 
              offsetFromScreenLeft - widthOfString + bookImageWidth - 44, 
              18, 0);
        
        fontRenderer.drawString("Age: "+this.thisAnimal.getOldAge()/24000, 
                offsetFromScreenLeft - widthOfString + bookImageWidth - 185, 
                35, 0);
        
        fontRenderer.drawString("Health: "+this.thisAnimal.getZoopediaHealth()+"/"+this.thisAnimal.getZoopediaMaxHealth(), 
                offsetFromScreenLeft - widthOfString + bookImageWidth - 185, 
                45, 0);

        fontRenderer.drawString("Hunger: "+(int)(this.thisAnimal.getHunger()+0.5F)+"/"+(int)this.thisAnimal.getFoodMax(), 
                offsetFromScreenLeft - widthOfString + bookImageWidth - 185, 
                55, 0);
        
        String diet;
    	diet = "Diet: Herbivore";    
    	if (thisAnimal.getZoopediaName() == "Pig" || thisAnimal.getZoopediaName() == "Wild Boar") diet = "Diet: Omnivore";
        fontRenderer.drawString(diet, offsetFromScreenLeft - widthOfString + bookImageWidth - 185, 
                65, 0);
        
        String temperment;
        temperment = "Temperament: Passive";        	
        fontRenderer.drawString(temperment, offsetFromScreenLeft - widthOfString + bookImageWidth - 185, 
                75, 0);

        String sex;
        if (this.thisAnimal.getSex() == 0)
		{
			sex = "Sex: Female";        	
        	if (thisAnimal.getPregnancyTime() > 0) sex = "Sex: Female, pregnant";
		}
        else
        {
        	sex = "Sex: Male";
        }
        
        fontRenderer.drawString(sex, 
                offsetFromScreenLeft - widthOfString + bookImageWidth - 185, 
                85, 0);

        String tamed;
    	EntityAnimal animalb = (EntityAnimal) this.thisAnimal;
    	IEntityAdvanced animal = this.thisAnimal;
        
        if (animal.isTamed() == true && animalb.world.getPlayerEntityByUUID(this.thisAnimal.getOwnerId()) != null)
		{
			tamed = "Owner: "+ animalb.world.getPlayerEntityByUUID(this.thisAnimal.getOwnerId()).getName();        	
		}
        else
        {
        	tamed = "Untamed";
        }
        
        fontRenderer.drawString(tamed, 
                offsetFromScreenLeft - widthOfString + bookImageWidth - 185, 
                95, 0);
        
        fontRenderer.drawString("Happiness: "+this.thisAnimal.getHappiness(), 
                offsetFromScreenLeft - widthOfString + bookImageWidth - 185, 
                105, 0);

//        fontRenderer.drawSplitString(stringPageText[currPage], 
//              offsetFromScreenLeft + 36, 34, 116, 0);
        
        super.drawScreen(parWidth, parHeight, p_73863_3_);

    }


    /**
     * Called when a mouse button is pressed and the mouse is moved around. 
     * Parameters are : mouseX, mouseY, lastButtonClicked & 
     * timeSinceMouseClick.
     */
    @Override
    protected void mouseClickMove(int parMouseX, int parMouseY, 
          int parLastButtonClicked, long parTimeSinceMouseClick) 
    {
     
    }

    @Override
    protected void actionPerformed(GuiButton parButton) 
    {
     if (parButton == buttonDone)
     {
         // You can send a packet to server here if you need server to do 
         // something
         mc.displayGuiScreen((GuiScreen)null);
     }
        else if (parButton == buttonNextPage)
        {
            if (currPage < bookTotalPages - 1)
            {
                ++currPage;
            }
        }
        else if (parButton == buttonPreviousPage)
        {
            if (currPage > 0)
            {
                --currPage;
            }
        }
   }

    /**
     * Called when the screen is unloaded. Used to disable keyboard repeat 
     * events
     */
    @Override
    public void onGuiClosed() 
    {
     
    }

    /**
     * Returns true if this GUI should pause the game when it is displayed in 
     * single-player
     */
    @Override
    public boolean doesGuiPauseGame()
    {
        return false;
    }
    
    @SideOnly(Side.CLIENT)
    static class NextPageButton extends GuiButton
    {
        public NextPageButton(int parButtonId, int parPosX, int parPosY, 
              boolean parIsNextButton)
        {
            super(parButtonId, parPosX, parPosY, 23, 13, "");
        }

//        /**
//         * Draws this button to the screen.
//         */
//        @Override
//        public void drawButton(Minecraft mc, int parX, int parY)
//        {
//            if (visible)
//            {
//                boolean isButtonPressed = (parX >= xPosition 
//                      && parY >= yPosition 
//                      && parX < xPosition + width 
//                      && parY < yPosition + height);
//                GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//                mc.getTextureManager().bindTexture(bookPageTextures[1]);
//                int textureX = 0;
//                int textureY = 192;
//
//                if (isButtonPressed)
//                {
//                    textureX += 28;
//                }
//
//                if (!isNextButton)
//                {
//                    textureY += 16;
//                }
//
//                drawTexturedModalRect(xPosition, yPosition, 
//                      textureX, textureY, 
//                      23, 13);
//            }
//        }
        
    }
}