package faunaandecology.mod.util.handlers;

import faunaandecology.mod.Main;
import faunaandecology.mod.entity.IEntityAdvanced;
import faunaandecology.mod.entity.passive.EntityAfricanAurochs;
import faunaandecology.mod.entity.passive.EntityAurochs;
import faunaandecology.mod.entity.passive.EntityDuck;
import faunaandecology.mod.entity.passive.EntityGuanaco;
import faunaandecology.mod.entity.passive.EntityImprovedCow;
import faunaandecology.mod.entity.passive.EntityImprovedDonkey;
import faunaandecology.mod.entity.passive.EntityImprovedHorse;
import faunaandecology.mod.entity.passive.EntityImprovedLlama;
import faunaandecology.mod.entity.passive.EntityImprovedMooshroom;
import faunaandecology.mod.entity.passive.EntityImprovedMule;
import faunaandecology.mod.entity.passive.EntityImprovedPig;
import faunaandecology.mod.entity.passive.EntityImprovedSheep;
import faunaandecology.mod.entity.passive.EntityJungleFowl;
import faunaandecology.mod.entity.passive.EntityMallard;
import faunaandecology.mod.entity.passive.EntityMouflon;
import faunaandecology.mod.entity.passive.EntityQuagga;
import faunaandecology.mod.entity.passive.EntitySanga;
import faunaandecology.mod.entity.passive.EntityWildAss;
import faunaandecology.mod.entity.passive.EntityWildBoar;
import faunaandecology.mod.entity.passive.EntityWildHorse;
import faunaandecology.mod.entity.passive.EntityWildMooshroom;
import faunaandecology.mod.entity.passive.EntityZebra;
import faunaandecology.mod.init.BlockInit;
import faunaandecology.mod.init.ItemInit;
import faunaandecology.mod.objects.blocks.BlockHide;
import faunaandecology.mod.objects.tools.ToolKnife;
import faunaandecology.mod.util.Config;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.passive.AbstractChestHorse;
import net.minecraft.entity.passive.AbstractHorse;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.passive.EntityDonkey;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.passive.EntityLlama;
import net.minecraft.entity.passive.EntityMooshroom;
import net.minecraft.entity.passive.EntityMule;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.passive.EntityRabbit;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.entity.passive.EntitySquid;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.item.ItemExpireEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.EntityInteract;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class EventsHandler 
{
/*	@SubscribeEvent
	public void onSpawnAttempt(WorldEvent.PotentialSpawns event)
	{
		World world = event.getWorld();
		BlockPos oldpos = event.getPos();
		IBlockState state = world.getBlockState(oldpos);
		
		if (world.getBlockState(oldpos).getBlock() instanceof BlockBush || world.getBlockState(oldpos).getBlock() instanceof IPlantable )
		{
			BlockPos pos = new BlockPos(oldpos.getX(),oldpos.getY(),oldpos.getZ());
			pos.add(world.rand.nextInt(17) - 8,world.rand.nextInt(17) - 8,world.rand.nextInt(17) - 8);
			world.setBlockState(pos, state);
		}
		
	}*/
	//This method gets an extra item when crafting this one.
	@SubscribeEvent
	public void onCrafting(PlayerEvent.ItemCraftedEvent event)
	{
		ItemStack itemstack = event.crafting;
		EntityPlayer player = event.player;
		World world = player.world;
		if (itemstack.getItem() == ItemInit.BONE_MARROW)
		{
			ItemStack extraitem = new ItemStack(ItemInit.SHARDS_BONE, 2);
			if (Config.shardsFromBone == false) extraitem = new ItemStack(Items.DYE, 3, 15); 
			 
			if (!player.inventory.addItemStackToInventory(extraitem))
				    {
						    world.spawnEntity(new EntityItem(world, (double)player.posX + 0.5D, (double)player.posY + 1.5D, (double)player.posZ + 0.5D, extraitem));
				    }
		}
	
	}
	
	//Horse Hide drop, temporary
	@SubscribeEvent
	public void onLivingDropsEvent(LivingDropsEvent event) 
	{
		Item bonetype = ItemInit.BONE_MEATY;
		if (Config.meatyBones == false) bonetype = Items.BONE;
		
		Entity entity = event.getEntity();
//		World world = event.getEntity().getEntityWorld();
//		BlockPos pos = event.getEntity().getPosition();
		if (entity instanceof EntityDuck)
		{
			event.getDrops().clear();
			EntityMallard deadSquid = (EntityMallard) entity;
			event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.FEATHER, deadSquid.getRNG().nextInt(2) + 1, 0)));
			event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.DUCK_RAW)));
		}
		else if (entity instanceof EntityMallard)
		{
			event.getDrops().clear();
			EntityMallard deadSquid = (EntityMallard) entity;
			event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.FEATHER, deadSquid.getRNG().nextInt(2) + 1, 0)));
			event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.MALLARD_RAW)));
		}
		else if (entity instanceof EntityJungleFowl)
		{
			event.getDrops().clear();
			EntityJungleFowl deadSquid = (EntityJungleFowl) entity;
			event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.FEATHER, deadSquid.getRNG().nextInt(2) + 1, 0)));
			event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.JUNGLEFOWL_RAW)));
		}
		
		if (Config.fewerDrops == false)
		{
			int amount = 1;
			if (entity instanceof EntitySquid)
			{
				event.getDrops().clear();
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.DYE, amount, 0)));
				EntitySquid deadSquid = (EntitySquid) entity;
				amount = deadSquid.getRNG().nextInt(3) + 2;
				if (deadSquid.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_CALAMARI, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.CALAMARI, amount)));
			}
			else if (entity instanceof EntityBat)
			{
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.RAW_BAT, 1)));
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.PELT_BAT, 1)));
			}
			else if (entity instanceof EntityRabbit)
			{
				EntityRabbit deadRabbit = (EntityRabbit) entity;
				event.getDrops().clear();
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.COOKED_RABBIT, 1)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.RABBIT, 1)));
				amount = deadRabbit.getRNG().nextInt(2);
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.RABBIT_FOOT, amount)));
				if (!deadRabbit.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.PELT_RABBIT, 1)));
			}
			else if (event.getEntity().getClass() == EntityImprovedPig.class || event.getEntity().getClass() == EntityPig.class)
		    {
				EntityPig pig = (EntityPig) entity;
				event.getDrops().clear();
				amount = pig.getRNG().nextInt(2) + 2;
				if (pig.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = pig.getRNG().nextInt(2) + 1;
				if (pig.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = (int) (pig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 9) / 2;
				amount += 2;
				if (pig.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.COOKED_PORKCHOP, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.PORKCHOP, amount)));
				if (!pig.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_PIG, 1)));
		    }
			else if (event.getEntity() instanceof EntityWildBoar)
		    {
				EntityPig pig = (EntityPig) entity;
				event.getDrops().clear();
				amount = pig.getRNG().nextInt(2) + 1;
				if (pig.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = pig.getRNG().nextInt(2) + 1;
				if (pig.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = (int) (pig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 9) / 2;
				amount += 2;
				if (pig.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.COOKED_PORKCHOP, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.RAW_BOAR_PORKCHOP, amount)));
				if (!pig.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_WILD_BOAR, 1)));
		    }
			else if (event.getEntity().getClass() == EntityImprovedCow.class || event.getEntity().getClass() == EntityCow.class)
		    {
				EntityCow cow = (EntityCow) entity;
				event.getDrops().clear();
				amount = cow.getRNG().nextInt(2) + 3;
				if (cow.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = cow.getRNG().nextInt(2) + 3;
				if (cow.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = (int) (cow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 20) / 3;
				amount += 4;
				if (cow.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.COOKED_BEEF, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.BEEF, amount)));
				if (!cow.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_COW, 1)));
		    }
			else if (event.getEntity().getClass() == EntitySanga.class)
		    {
				EntitySanga cow = (EntitySanga) entity;
				event.getDrops().clear();
				amount = cow.getRNG().nextInt(2) + 3;
				if (cow.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = cow.getRNG().nextInt(2) + 3;
				if (cow.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = (int) (cow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 20) / 3;
				amount += 4;
				if (cow.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.COOKED_BEEF, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.BEEF, amount)));
				if (cow.getSex() == 1 && !cow.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_SANGA_MALE, 1)));
				if (cow.getSex() == 0 && !cow.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_SANGA_FEMALE, 1)));

		    }
			else if (event.getEntity().getClass() == EntityMooshroom.class || event.getEntity().getClass() == EntityImprovedMooshroom.class)
		    {
				EntityCow cow = (EntityCow) entity;
				event.getDrops().clear();
				amount = cow.getRNG().nextInt(2) + 1;
				if (cow.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = cow.getRNG().nextInt(2) + 2;
				if (cow.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = (int) (cow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 10) / 2;
				amount += 2;
				if (cow.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.COOKED_BEEF, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.MOOSHROOM_MEAT, amount)));
				if (!cow.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_MOOSHROOM, 1)));
		    }
			else if (event.getEntity() instanceof EntityWildMooshroom)
		    {
				EntityWildMooshroom cow = (EntityWildMooshroom) entity;
				event.getDrops().clear();
				amount = cow.getRNG().nextInt(2) + 1;
				if (cow.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = cow.getRNG().nextInt(2) + 2;
				if (cow.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = (int) (cow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 10) / 2;
				amount += 2;
				if (cow.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.COOKED_BEEF, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.WILD_MOOSHROOM_MEAT, amount)));
				if (cow.getSex() == 1 && !cow.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_MUSHROOM_AUROCHS_MALE, 1)));
				if (cow.getSex() == 0 && !cow.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_MUSHROOM_AUROCHS_FEMALE, 1)));
		    }
			else if (event.getEntity().getClass() == EntityAfricanAurochs.class)
		    {
				EntityAfricanAurochs cow = (EntityAfricanAurochs) entity;
				event.getDrops().clear();
				amount = cow.getRNG().nextInt(2) + 2;
				if (cow.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = cow.getRNG().nextInt(2) + 3;
				if (cow.getSex() == 1) amount += 1;
				if (cow.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = (int) (cow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 20) / 3;
				amount += 4;
				if (cow.getSex() == 1) amount += 1;
				if (cow.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.COOKED_BEEF, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.AUROCHS_MEAT, amount)));
				if (cow.getSex() == 1 && !cow.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_SCRUB_AUROCHS_MALE, 1)));
				if (cow.getSex() == 0 && !cow.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_SCRUB_AUROCHS_FEMALE, 1)));
		    }
			else if (event.getEntity().getClass() ==  EntityAurochs.class)
		    {
				EntityAurochs cow = (EntityAurochs) entity;
				event.getDrops().clear();
				amount = cow.getRNG().nextInt(2) + 2;
				if (cow.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = cow.getRNG().nextInt(2) + 3;
				if (cow.getSex() == 1) amount += 1;
				if (cow.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = (int) (cow.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 20) / 3;
				amount += 4;
				if (cow.getSex() == 1) amount += 1;
				if (cow.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.COOKED_BEEF, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.AUROCHS_MEAT, amount)));
				if (cow.getSex() == 1 && !cow.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_AUROCHS_MALE, 1)));
				if (cow.getSex() == 0 && !cow.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_AUROCHS_FEMALE, 1)));
		    }
			else if (event.getEntity().getClass() == EntityImprovedHorse.class || event.getEntity().getClass() == EntityHorse.class)
			{
					AbstractHorse deadHorse = (AbstractHorse) entity;
					event.getDrops().clear();
					amount = deadHorse.getRNG().nextInt(3) + 2;
					if (deadHorse.isChild()) amount /= 2;
					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HAIR_HORSE, amount)));
					amount = deadHorse.getRNG().nextInt(3) + 1;
					if (deadHorse.isChild()) amount /= 2;
					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
					amount = deadHorse.getRNG().nextInt(3) + 3;
					if (deadHorse.isChild()) amount /= 2;
					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
					amount = deadHorse.getRNG().nextInt(3) + 3;
					if (deadHorse.isChild()) amount /= 2;
					if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_HORSE_MEAT, amount)));
					else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HORSE_MEAT, amount)));
					amount = deadHorse.getRNG().nextInt(4) + 2;
					if (!deadHorse.isChild() && event.getEntity().getClass() == EntityHorse.class)
					{
						EntityHorse variantHorse = (EntityHorse) deadHorse;
						if (variantHorse.getHorseVariant() == 0) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_WHITE, 1)));
						else if (variantHorse.getHorseVariant() == 1) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_CREAMY, 1)));
						else if (variantHorse.getHorseVariant() == 2) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_CHESTNUT, 1)));
						else if (variantHorse.getHorseVariant() == 3) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_BROWN, 1)));
						else if (variantHorse.getHorseVariant() == 4) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_BLACK, 1)));
						else if (variantHorse.getHorseVariant() == 5) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_GRAY, 1)));
						else if (variantHorse.getHorseVariant() == 6) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_DARKBROWN, 1)));
					}
					else if (!deadHorse.isChild() && event.getEntity().getClass() == EntityImprovedHorse.class)
					{
						EntityImprovedHorse variantHorse = (EntityImprovedHorse) deadHorse;
						if (variantHorse.getHorseVariant() == 0) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_WHITE, 1)));
						else if (variantHorse.getHorseVariant() == 1) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_CREAMY, 1)));
						else if (variantHorse.getHorseVariant() == 2) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_CHESTNUT, 1)));
						else if (variantHorse.getHorseVariant() == 3) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_BROWN, 1)));
						else if (variantHorse.getHorseVariant() == 4) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_BLACK, 1)));
						else if (variantHorse.getHorseVariant() == 5) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_GRAY, 1)));
						else if (variantHorse.getHorseVariant() == 6) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_DARKBROWN, 1)));
					}
			}
			else if (event.getEntity().getClass() == EntityDonkey.class)	
			{
				EntityDonkey deadHorse = (EntityDonkey) entity;
				event.getDrops().clear();
				amount = deadHorse.getRNG().nextInt(2) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HAIR_HORSE, amount)));
				amount = deadHorse.getRNG().nextInt(2) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = deadHorse.getRNG().nextInt(2) + 2;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = deadHorse.getRNG().nextInt(3) + 2;
				if (deadHorse.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_DONKEY_MEAT, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.DONKEY_MEAT, amount)));
				amount = deadHorse.getRNG().nextInt(3) + 1;
				if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_DONKEY, 1)));
			}
			else if (event.getEntity().getClass() == EntityImprovedDonkey.class)	
			{
				EntityImprovedDonkey deadHorse = (EntityImprovedDonkey) entity;
				event.getDrops().clear();
				amount = deadHorse.getRNG().nextInt(2) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HAIR_HORSE, amount)));
				amount = deadHorse.getRNG().nextInt(2) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = deadHorse.getRNG().nextInt(2) + 2;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = deadHorse.getRNG().nextInt(3) + 2;
				if (deadHorse.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_DONKEY_MEAT, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.DONKEY_MEAT, amount)));
				amount = deadHorse.getRNG().nextInt(3) + 1;
				if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_DONKEY, 1)));
			}
			else if (event.getEntity().getClass() == EntityMule.class)		
			{
				EntityMule deadHorse = (EntityMule) entity;
				event.getDrops().clear();
				amount = deadHorse.getRNG().nextInt(3) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HAIR_HORSE, amount)));
				amount = deadHorse.getRNG().nextInt(3) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = deadHorse.getRNG().nextInt(3) + 2;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
		        int randroll = deadHorse.getRNG().nextInt(2);
			    if (randroll == 1)
				{	
					amount = deadHorse.getRNG().nextInt(3) + 3;
					if (deadHorse.isChild()) amount /= 2;
					if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_DONKEY_MEAT, amount)));
					else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.DONKEY_MEAT, amount)));
					amount = deadHorse.getRNG().nextInt(3) + 2;
					if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_DONKEY, 1)));
				}
		        else
		        {
					amount = deadHorse.getRNG().nextInt(3) + 3;
					if (deadHorse.isChild()) amount /= 2;
					if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_HORSE_MEAT, amount)));
					else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HORSE_MEAT, amount)));
					amount = deadHorse.getRNG().nextInt(3) + 2;
					if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_BROWN,1)));
		        }	
			}
			else if (event.getEntity().getClass() == EntityImprovedMule.class)		
			{
				EntityImprovedMule deadHorse = (EntityImprovedMule) entity;
				event.getDrops().clear();
				amount = deadHorse.getRNG().nextInt(3) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HAIR_HORSE, amount)));
				amount = deadHorse.getRNG().nextInt(3) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = deadHorse.getRNG().nextInt(3) + 2;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
		        int randroll = deadHorse.getRNG().nextInt(2);
			    if (randroll == 1)
				{	
					amount = deadHorse.getRNG().nextInt(3) + 3;
					if (deadHorse.isChild()) amount /= 2;
					if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_DONKEY_MEAT, amount)));
					else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.DONKEY_MEAT, amount)));
					amount = deadHorse.getRNG().nextInt(3) + 2;
					if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_DONKEY, 1)));
				}
		        else
		        {
					amount = deadHorse.getRNG().nextInt(3) + 3;
					if (deadHorse.isChild()) amount /= 2;
					if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_HORSE_MEAT, amount)));
					else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HORSE_MEAT, amount)));
					amount = deadHorse.getRNG().nextInt(3) + 2;
					if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE_BROWN,1)));
		        }	
			}

			else if (entity instanceof EntityWildHorse)		
			{
				EntityWildHorse deadHorse = (EntityWildHorse) entity;
				event.getDrops().clear();
				amount = deadHorse.getRNG().nextInt(3) + 2;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HAIR_HORSE, amount)));
				amount = deadHorse.getRNG().nextInt(3) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = deadHorse.getRNG().nextInt(3) + 3;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = (int) (deadHorse.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 20) / 3;
				amount += 3;
				if (deadHorse.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_HORSE_MEAT, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.WILD_HORSE_MEAT, amount)));
				amount = deadHorse.getRNG().nextInt(4) + 2;
				if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_WILD_HORSE, 1)));
			}
			else if (entity instanceof EntityWildAss)		
			{
				EntityWildAss deadHorse = (EntityWildAss) entity;
				event.getDrops().clear();
				amount = deadHorse.getRNG().nextInt(2) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HAIR_HORSE, amount)));
				amount = deadHorse.getRNG().nextInt(2) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = deadHorse.getRNG().nextInt(2) + 2;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = (int) (deadHorse.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 20) / 3;
				amount += 2;
				if (deadHorse.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_HORSE_MEAT, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.WILD_ASS_MEAT, amount)));
				amount = deadHorse.getRNG().nextInt(3) + 1;
				if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_WILD_ASS, amount)));
			}
			else if (entity instanceof EntityQuagga)		
			{
				EntityQuagga deadHorse = (EntityQuagga) entity;
				event.getDrops().clear();
				amount = deadHorse.getRNG().nextInt(2) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HAIR_HORSE, amount)));
				amount = deadHorse.getRNG().nextInt(2) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = deadHorse.getRNG().nextInt(2) + 2;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = (int) (deadHorse.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 20) / 3;
				amount += 2;
				if (deadHorse.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_ZEBRA_MEAT, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.QUAGGA_MEAT, amount)));
				amount = deadHorse.getRNG().nextInt(3) + 1;
				if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_QUAGGA, amount)));
			}
			else if (entity instanceof EntityZebra)		
			{
				EntityZebra deadHorse = (EntityZebra) entity;
				event.getDrops().clear();
				amount = deadHorse.getRNG().nextInt(2) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HAIR_HORSE, amount)));
				amount = deadHorse.getRNG().nextInt(2) + 1;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
				amount = deadHorse.getRNG().nextInt(2) + 2;
				if (deadHorse.isChild()) amount /= 2;
				event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
				amount = (int) (deadHorse.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 20) / 3;
				amount += 2;
				if (deadHorse.isChild()) amount /= 2;
				if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_ZEBRA_MEAT, amount)));
				else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.ZEBRA_MEAT, amount)));
				amount = deadHorse.getRNG().nextInt(3) + 1;
				if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_ZEBRA, 1)));
			}
				else if (event.getEntity().getClass() == EntityLlama.class || event.getEntity().getClass() == EntityImprovedLlama.class)
			    {
					AbstractChestHorse pig = (AbstractChestHorse) entity;
					event.getDrops().clear();
					amount = pig.getRNG().nextInt(2);
					if (pig.isChild()) amount /= 2;
					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
					if ( event.getEntity().getClass() == EntityImprovedLlama.class) 
					{
						EntityImprovedLlama woolyLlama = (EntityImprovedLlama) entity;
						if ( !woolyLlama.getSheared() )
						{
							amount = pig.getRNG().nextInt(2)+1;
							if (pig.isChild()) amount /= 2;
							if (Config.woolSystem)
							{
								event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.WOOL_TUFT, amount * pig.getRNG().nextInt(4) + 1)));
							}
							else
							event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Blocks.WOOL, amount)));	
						}
					}
					amount = pig.getRNG().nextInt(2) + 1;
					if (pig.isChild()) amount /= 2;
					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
					amount = (int) (pig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 10) / 2;
					amount += 2;
					if (pig.isChild()) amount /= 2;
					if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_LLAMA_MEAT, amount)));
					else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.LLAMA_MEAT, amount)));
					if (!pig.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_LLAMA_WHITE, 1)));
			    }
				else if (event.getEntity().getClass() == EntityGuanaco.class)
			    {
					AbstractChestHorse pig = (AbstractChestHorse) entity;
					event.getDrops().clear();
					amount = pig.getRNG().nextInt(2);
					if (pig.isChild()) amount /= 2;
					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
					EntityImprovedLlama woolyLlama = (EntityImprovedLlama) entity;
					if ( !woolyLlama.getSheared() )
					{
						if (Config.woolSystem)
						{
							if (!pig.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.WOOL_TUFT, pig.getRNG().nextInt(4) + 1, 3)));
						}
						else
						if (!pig.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Blocks.WOOL, 1, 12)));	
					}
					amount = pig.getRNG().nextInt(2) + 1;
					if (pig.isChild()) amount /= 2;
					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
					amount = (int) (pig.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 10) / 2;
					amount += 2;
					if (pig.isChild()) amount /= 2;
					if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_LLAMA_MEAT, amount)));
					else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.GUANACO_MEAT, amount)));
					if (!pig.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_GUANACO, 1)));
			    }
				else if (event.getEntity().getClass() == EntityMouflon.class)
			    {
					EntityMouflon sheep = (EntityMouflon) entity;
					event.getDrops().clear();
					amount = sheep.getRNG().nextInt(2);
					if (sheep.isChild()) amount /= 2;
					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
					amount = sheep.getRNG().nextInt(2) + 1;
					if (sheep.isChild()) amount /= 2;
					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
					amount = (int) (sheep.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 5) / 2;
					amount += 2;
					if (sheep.isChild()) amount /= 2;
					if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.COOKED_MUTTON, amount)));
					else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.MOUFLON_MEAT, amount)));
					if (sheep.getSex() == 1 && !sheep.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_MOUFLON_MALE, 1)));
					if (sheep.getSex() == 0 && !sheep.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_MOUFLON_FEMALE, 1)));
			    }
				else if (event.getEntity().getClass() == EntitySheep.class || event.getEntity().getClass() == EntityImprovedSheep.class)
			    {
					EntitySheep sheep = (EntitySheep) entity;
					event.getDrops().clear();
					amount = sheep.getRNG().nextInt(2);
					if (sheep.isChild()) amount /= 2;
					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
					EntitySheep woolyLlama = (EntitySheep) entity;
					if ( !woolyLlama.getSheared() )
					{
						amount = sheep.getRNG().nextInt(3)+1;
						if (sheep.isChild()) amount /= 2;
						if (Config.woolSystem)
						{						
							event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.WOOL_TUFT, amount * sheep.getRNG().nextInt(4) + 1, 15 - sheep.getFleeceColor().getMetadata())));
						}
						else
						event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Blocks.WOOL, amount, sheep.getFleeceColor().getMetadata())));	
					}
					amount = sheep.getRNG().nextInt(2) + 1;
					if (sheep.isChild()) amount /= 2;
					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
					amount = (int) (sheep.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 5) / 2;
					amount += 2;
					if (sheep.isChild()) amount /= 2;
					if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.COOKED_MUTTON, amount)));
					else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(Items.MUTTON, amount)));
					if (!sheep.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_SHEEP_WHITE, 1)));
			    }
//				else if (deadHorse.getImprovedType() == ImprovedHorseType.ZONKEY)		
//				{
//					event.getDrops().clear();
//					amount = deadHorse.getRNG().nextInt(2) + 1;
//					if (deadHorse.isChild()) amount /= 2;
//					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HAIR_HORSE, amount)));
//					amount = deadHorse.getRNG().nextInt(2) + 1;
//					if (deadHorse.isChild()) amount /= 2;
//					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
//					amount = deadHorse.getRNG().nextInt(2) + 2;
//					if (deadHorse.isChild()) amount /= 2;
//					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
//			        int randroll = deadHorse.getRNG().nextInt(2);
//				    if (randroll == 1)
//					{	
//				    	amount = (int) (deadHorse.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 20) / 3;
//						amount += 2;
//						if (deadHorse.isChild()) amount /= 2;
//						if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_DONKEY_MEAT, amount)));
//						else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.DONKEY_MEAT, amount)));
//						amount = deadHorse.getRNG().nextInt(3) + 1;
//						if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_DONKEY, amount)));
//					}
//			        else
//			        {
//			        	amount = (int) (deadHorse.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 20) / 3;
//						amount += 2;
//						if (deadHorse.isChild()) amount /= 2;
//						if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_DONKEY_MEAT, amount)));
//						else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.ZEBRA_MEAT, amount)));
//						amount = deadHorse.getRNG().nextInt(3) + 1;
//						if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_ZEBRA, amount)));
//			        }	
//				}	
//				else if (deadHorse.getImprovedType() == ImprovedHorseType.ZORSE)		
//				{
//					event.getDrops().clear();
//					amount = deadHorse.getRNG().nextInt(3) + 1;
//					if (deadHorse.isChild()) amount /= 2;
//					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HAIR_HORSE, amount)));
//					amount = deadHorse.getRNG().nextInt(3) + 1;
//					if (deadHorse.isChild()) amount /= 2;
//					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.FAT, amount)));
//					amount = deadHorse.getRNG().nextInt(3) + 2;
//					if (deadHorse.isChild()) amount /= 2;
//					event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(bonetype, amount)));
//			        int randroll = deadHorse.getRNG().nextInt(2);
//				    if (randroll == 1)
//					{	
//				    	amount = (int) (deadHorse.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 20) / 3;
//						amount += 2;
//						if (deadHorse.isChild()) amount /= 2;
//						if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_HORSE_MEAT, amount)));
//						else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.HORSE_MEAT, amount)));
//						amount = deadHorse.getRNG().nextInt(3) + 2;
//						if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_HORSE, amount)));
//					}
//			        else
//			        {
//			        	amount = (int) (deadHorse.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue() - 20) / 3;
//						amount += 2;						
//						if (deadHorse.isChild()) amount /= 2;
//						if (event.getSource().isFireDamage()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.COOKED_ZEBRA_MEAT, amount)));
//						else event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(ItemInit.ZEBRA_MEAT, amount)));
//						amount = deadHorse.getRNG().nextInt(3) + 2;
//						if (!deadHorse.isChild()) event.getDrops().add(new EntityItem(entity.world, entity.posX, entity.posY, entity.posZ, new ItemStack(BlockInit.HIDE_ZEBRA, amount)));
//			        }	
//				}
		}
	}
	
	@SubscribeEvent
	public void onEntitySpawn(EntityJoinWorldEvent event)
	{
		
		{
//			if(event.getEntity() instanceof EntitySkeleton || event.getEntity() instanceof EntityZombie || event.getEntity() instanceof EntitySpider || event.getEntity() instanceof EntitySlime  || event.getEntity() instanceof EntityWitch)
//			{
//		    	event.setCanceled(true);
//		    }
		}
		
	}
	
	

	@SubscribeEvent
	public void onEntityIntereact(EntityInteract event)
	{
		if (event.getEntity() instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) event.getEntity();
			ItemStack itemstack = player.inventory.getCurrentItem();
			//use Zoopedia on new animals
			if (event.getTarget() != null && event.getTarget() instanceof IEntityAdvanced && itemstack != null && itemstack.getItem() == ItemInit.ZOOPEDIA)
			{
			        player.openGui(Main.instance, GUIHandler.ZOOPEDIA_GUI, player.world, event.getTarget().getEntityId(),(int)  player.posY,(int)  player.posZ);
			}
		}
		return;
	}	
		
	@SubscribeEvent
	public void harvestDrops(BlockEvent.HarvestDropsEvent event) 
	{

		if(event.getHarvester() !=null)
		{
			if(event.getHarvester().getHeldItemMainhand().getItem() !=null)
			{
				if (event.getHarvester().getHeldItemMainhand().getItem() instanceof ToolKnife)
				{
					if (event.getState().getBlock() == Blocks.VINE || event.getState().getBlock() == Blocks.REEDS ) 
					{
						event.getDrops().clear();
						event.getDrops().add(new ItemStack(ItemInit.FIBER, 1));
					}
					if (event.getState().getBlock() == Blocks.DOUBLE_PLANT && event.getState().getBlock().getMetaFromState(event.getState()) == 2)
					{
						event.getDrops().clear();
						event.getDrops().add(new ItemStack(ItemInit.FIBER, 1));
					}
				}			
			}
			
		}
			
		//Leaves drop sticks
		if (Config.leavesDropSticks == true)
		{
			if (event.getState().getBlock().isLeaves(event.getState(), event.getWorld(), event.getPos()) && Math.random() > 0.9D) 
			{
				event.getDrops().add(new ItemStack(Items.STICK, 1));
			}
		}

		if (Config.leavesDropResin == true)
		{
			if (event.getState().getBlock().isLeaves(event.getState(), event.getWorld(), event.getPos()) && Math.random() > 0.96D) 
			{
				event.getDrops().add(new ItemStack(ItemInit.RESIN, 1));
			}
		}
		

		//Stop players from harvesting logs without the correct tool.
		if (Config.noPunchingWood == true)
		{
			if (isPlayerHarvestingLogWithoutCorrectTool(event.getState(), event.getWorld(), event.getPos(), event.getHarvester())) 
			{
				event.getDrops().clear();
			}
		}
		
	}
	

    @SubscribeEvent
    public void onItemDecay(ItemExpireEvent event) {
        if (Config.replantBlocks == true)
        {
            EntityItem ent = event.getEntityItem();
            if (ent.motionX < 0.001 && ent.motionZ < 0.001) // stationary 
            {
                ItemStack item = ent.getItem();
                if(item!=null && item.getCount() > 0 && item.getItem() instanceof ItemBlock)
                {
                    Block id = ((ItemBlock) item.getItem()).getBlock();
                    if (!(id instanceof BlockHide))
                    {
                        BlockPos pos = new BlockPos(ent);
                        if(id.canPlaceBlockAt(ent.world, pos)) {
                            IBlockState state = id.getStateFromMeta(item.getItem().getMetadata(item.getMetadata()));
                            if(ent.world.setBlockState(pos, state)){
                                item.setCount(item.getCount() - 1);
                            }
                        }
                    }
                }
            }
        }
    }

                        
    @SubscribeEvent
    public void onItemUsed(PlayerInteractEvent.RightClickItem event) 
    {
    	EntityLivingBase playerIn = event.getEntityLiving();
    	
    	if (playerIn instanceof EntityPlayer)
    	{
    		ItemStack items = event.getItemStack();
    		EntityPlayer player = (EntityPlayer) playerIn;
        	
        	if (items.getItem() == Items.BOWL)
        	{
        		World worldIn = playerIn.getEntityWorld();
        	    RayTraceResult raytraceresult = getNearestBlockWithDefaultReachDistance(event.getWorld(), event.getEntityPlayer(), true, false, false);
                if (raytraceresult != null)
                {
                    if (raytraceresult.typeOfHit == RayTraceResult.Type.BLOCK)
                    {
                        BlockPos blockpos = raytraceresult.getBlockPos();
                        if (event.getEntity().getEntityWorld().getBlockState(blockpos).getMaterial() == Material.WATER)
                        {
                            worldIn.playSound(player, player.posX, player.posY, player.posZ, SoundEvents.BLOCK_WATERLILY_PLACE, SoundCategory.NEUTRAL, 1.0F, 1.0F);
                        	items.shrink(1);
                        	//player.addStat(StatList.getObjectUseStats(Items.BOWL));
                            
                        	ItemStack stack = new ItemStack(ItemInit.BOWL_WATER);
                        	
                        	EnumHand hand = event.getHand();
                        	if (stack.isEmpty())
                            {
                                playerIn.setHeldItem(hand, stack);
                            }
                            else if (!player.inventory.addItemStackToInventory(stack))
                            {
                            	   player.dropItem(stack, false);
                            }
                            else if (playerIn instanceof EntityPlayerMP)
                            {
                                ((EntityPlayerMP)playerIn).sendContainerToPlayer(player.inventoryContainer);
                            }

                        }
                        
                        if (event.getEntity().getEntityWorld().getBlockState(blockpos).getBlock() == Blocks.CAULDRON)
                        {
                        	if( Blocks.CAULDRON.getMetaFromState(event.getEntity().getEntityWorld().getBlockState(blockpos)) > 0)
                        	{
                                player.addStat(StatList.CAULDRON_USED);
                                
                            	items.shrink(1);
                            	ItemStack stack = new ItemStack(ItemInit.BOWL_WATER);
                            	
                            	EnumHand hand = event.getHand();
                            	if (stack.isEmpty())
                                {
                                    playerIn.setHeldItem(hand, stack);
                                }
                                else if (!player.inventory.addItemStackToInventory(stack))
                                {
                                	   player.dropItem(stack, false);
                                }
                                else if (playerIn instanceof EntityPlayerMP)
                                {
                                    ((EntityPlayerMP)playerIn).sendContainerToPlayer(player.inventoryContainer);
                                }

                                worldIn.playSound((EntityPlayer)null, blockpos, SoundEvents.ITEM_BOTTLE_FILL, SoundCategory.BLOCKS, 1.0F, 1.0F);
                                Blocks.CAULDRON.setWaterLevel(worldIn, blockpos, event.getEntity().getEntityWorld().getBlockState(blockpos), Blocks.CAULDRON.getMetaFromState(event.getEntity().getEntityWorld().getBlockState(blockpos)) - 1);
                        		
                        	}
                        }


                    }
                }
        	}
        	
        	//refill cauldron
        	if (items.getItem() == ItemInit.BOWL_WATER)
        	{
        		World worldIn = playerIn.getEntityWorld();
        	    RayTraceResult raytraceresult = getNearestBlockWithDefaultReachDistance(event.getWorld(), event.getEntityPlayer(), true, false, false);
                if (raytraceresult != null)
                {
                    if (raytraceresult.typeOfHit == RayTraceResult.Type.BLOCK)
                    {
                        BlockPos blockpos = raytraceresult.getBlockPos();
                        if (event.getEntity().getEntityWorld().getBlockState(blockpos).getBlock() == Blocks.CAULDRON)
                        {
                        	if( Blocks.CAULDRON.getMetaFromState(event.getEntity().getEntityWorld().getBlockState(blockpos)) < 3)
                        	{
                                player.addStat(StatList.CAULDRON_USED);
                                
                            	items.shrink(1);
                            	ItemStack stack = new ItemStack(Items.BOWL);
                            	
                            	EnumHand hand = event.getHand();
                            	if (stack.isEmpty())
                                {
                                    playerIn.setHeldItem(hand, stack);
                                }
                                else if (!player.inventory.addItemStackToInventory(stack))
                                {
                                	   player.dropItem(stack, false);
                                }
                                else if (playerIn instanceof EntityPlayerMP)
                                {
                                    ((EntityPlayerMP)playerIn).sendContainerToPlayer(player.inventoryContainer);
                                }

                                worldIn.playSound((EntityPlayer)null, blockpos, SoundEvents.ITEM_BOTTLE_FILL, SoundCategory.BLOCKS, 1.0F, 1.0F);
                                Blocks.CAULDRON.setWaterLevel(worldIn, blockpos, event.getEntity().getEntityWorld().getBlockState(blockpos), Blocks.CAULDRON.getMetaFromState(event.getEntity().getEntityWorld().getBlockState(blockpos)) + 1);
                        		
                        	}
                        }


                    }
                }
        	}
    	}
    }
    
	private static RayTraceResult getMovingObjectPosWithReachDistance(World world, EntityPlayer player, double distance, boolean p1, boolean p2, boolean p3) {
		float f = player.rotationPitch;
		float f1 = player.rotationYaw;
		double d0 = player.posX;
		double d1 = player.posY + player.getEyeHeight();
		double d2 = player.posZ;
		Vec3d vec3 = new Vec3d(d0, d1, d2);
		float f2 = MathHelper.cos(-f1 * 0.017453292F - (float) Math.PI);
		float f3 = MathHelper.sin(-f1 * 0.017453292F - (float) Math.PI);
		float f4 = -MathHelper.cos(-f * 0.017453292F);
		float f5 = MathHelper.sin(-f * 0.017453292F);
		float f6 = f3 * f4;
		float f7 = f2 * f4;
		Vec3d vec31 = vec3.addVector(f6 * distance, f5 * distance, f7 * distance);
		return world.rayTraceBlocks(vec3, vec31, p1, p2, p3);
	}

	public static RayTraceResult getNearestBlockWithDefaultReachDistance(World world, EntityPlayer player) {
		return getNearestBlockWithDefaultReachDistance(world, player, false, true, false);
	}

	public static RayTraceResult getNearestBlockWithDefaultReachDistance(World world, EntityPlayer player, boolean stopOnLiquids, boolean ignoreBlockWithoutBoundingBox, boolean returnLastUncollidableBlock) {
		return getMovingObjectPosWithReachDistance(world, player, player.getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue(), stopOnLiquids, ignoreBlockWithoutBoundingBox, returnLastUncollidableBlock);
	}
    
	
	/**
	 * Can the tool harvest the block?
	 * <p>
	 * Adapted from ForgeHooks.canToolHarvestBlock, but has an IBlockState parameter instead of getting the IBlockState
	 * from an IBlockAccess and a BlockPos.
	 *
	 * @param state The block's state
	 * @param stack The tool ItemStack
	 * @return True if the tool can harvest the block
	 */
	private boolean canToolHarvestBlock(IBlockState state, ItemStack stack, EntityPlayer player) {
		String tool = state.getBlock().getHarvestTool(state);
		return stack != null && tool != null
				&& stack.getItem().getHarvestLevel(stack, tool, player, state) >= state.getBlock().getHarvestLevel(state);
	}

	/**
	 * Is the player harvesting a log block without the correct tool?
	 *
	 * @param state       The block's state
	 * @param blockAccess The world that the block is in
	 * @param pos         The position of the block
	 * @param player      The player harvesting the block
	 * @return True if the block is a log, the player isn't in creative mode and the player doesn't have the correct tool equipped
	 */
	private boolean isPlayerHarvestingLogWithoutCorrectTool(IBlockState state, IBlockAccess blockAccess, BlockPos pos, EntityPlayer player)
	{
		return player != null && !player.capabilities.isCreativeMode
				&& state.getBlock().isWood(blockAccess, pos)
				&& !canToolHarvestBlock(state, player.getHeldItemMainhand(), player);
	}



}
