package faunaandecology.mod.util.handlers;

import faunaandecology.mod.init.BlockInit;
import faunaandecology.mod.init.ItemInit;
import faunaandecology.mod.util.Config;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;

public class RecipeHandler 
{
	public static void RegisterCrafting()
	{
		
		 ItemInit.ACORN = Item.getByNameOrId("dynamictrees:oakseed");
		 ItemInit.ACORN_DARK = Item.getByNameOrId("dynamictrees:darkoakseed");
		 ItemInit.ACORN_AUTUMN = Item.getByNameOrId("dynamictreesbop:orangeautumnseed");
		 ItemInit.ACORN_DYING = Item.getByNameOrId("dynamictreesbop:oakdyingseed");
		 ItemInit.ACORN_FLOWERING = Item.getByNameOrId("dynamictreesbop:floweringoakseed");
		 ItemInit.ACORN_HELL = Item.getByNameOrId("dynamictreesbop:hellbarkseed");
		 ItemInit.BAMBOO_SHOOT = Item.getByNameOrId("dynamictreesbop:bambooseed");
		 ItemInit.PEAR = Item.getByNameOrId("bop:pear");
		 ItemInit.PEACH = Item.getByNameOrId("bop:peach");
		 ItemInit.PERSIMMON = Item.getByNameOrId("bop:persimmon");
		 
		 ItemInit.SPRUCE_CONE = Item.getByNameOrId("dynamictrees:spruceseed");
		 ItemInit.FIR_CONE = Item.getByNameOrId("dynamictreesbop:firseed");
		 ItemInit.PINE_CONE = Item.getByNameOrId("dynamictreesbop:pineseed");
		 ItemInit.REDWOOD_CONE = Item.getByNameOrId("dynamictreesbop:redwoodseed");
		 
		 ItemInit.TAR_DROP= Item.getByNameOrId("fossil:tardrop");

		 ItemInit.BOP_LOG0 = Item.getByNameOrId("biomesoplenty:log_0"); //4
		 ItemInit.BOP_LOG1 = Item.getByNameOrId("biomesoplenty:log_1"); //6
		 ItemInit.BOP_LOG2 = Item.getByNameOrId("biomesoplenty:log_2"); //2
		 ItemInit.BOP_LOG3 = Item.getByNameOrId("biomesoplenty:log_3");
		 //ItemInit.BOP_LOG4 = Item.getByNameOrId("biomesplenty:log_4");
		 

		OreDictionary.registerOre("treeCone", new ItemStack(ItemInit.SPRUCE_CONE));
		OreDictionary.registerOre("treeCone", new ItemStack(ItemInit.FIR_CONE));
		OreDictionary.registerOre("treeCone", new ItemStack(ItemInit.PINE_CONE));
		OreDictionary.registerOre("treeCone", new ItemStack(ItemInit.REDWOOD_CONE));
		

		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_WILD_BOAR));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_SHEEP_WHITE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_SCRUB_AUROCHS_MALE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_SCRUB_AUROCHS_FEMALE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_SANGA_MALE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_SANGA_FEMALE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_PIG));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_MUSHROOM_AUROCHS_MALE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_MUSHROOM_AUROCHS_FEMALE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_MOUFLON_FEMALE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_MOUFLON_FEMALE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_MOOSHROOM));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_LLAMA_WHITE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_HORSE_WHITE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_HORSE_GRAY));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_HORSE_DARKBROWN));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_HORSE_CREAMY));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_HORSE_CHESTNUT));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_HORSE_BROWN));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_HORSE_BLACK));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_GUANACO));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_AUROCHS_FEMALE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_AUROCHS_MALE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_COW));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_DONKEY));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_QUAGGA));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_ZEBRA));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_WILD_ASS));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_WILD_HORSE));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.HIDE_PIG));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.PELT_BAT));
		OreDictionary.registerOre("hideRaw", new ItemStack(BlockInit.PELT_RABBIT));

		OreDictionary.registerOre("resinTree", new ItemStack(ItemInit.RESIN));
		
		OreDictionary.registerOre("tallow", new ItemStack(ItemInit.TALLOW));

		OreDictionary.registerOre("egg", new ItemStack(ItemInit.EGG));
		OreDictionary.registerOre("egg", new ItemStack(ItemInit.EGG_DUCK));
		
		//harvestcraft comp
		OreDictionary.registerOre("listAllegg", new ItemStack(ItemInit.EGG));
		OreDictionary.registerOre("listAllegg", new ItemStack(ItemInit.EGG_DUCK));
		OreDictionary.registerOre("foodCheese", new ItemStack(ItemInit.CHEESE_COW));
		OreDictionary.registerOre("listAllduckraw", new ItemStack(ItemInit.DUCK_RAW));
		OreDictionary.registerOre("listAllduckraw", new ItemStack(ItemInit.MALLARD_RAW));
		OreDictionary.registerOre("listAllporkraw", new ItemStack(ItemInit.RAW_BOAR_PORKCHOP));
		OreDictionary.registerOre("listAllchickenraw", new ItemStack(ItemInit.JUNGLEFOWL_RAW));
		OreDictionary.registerOre("listAllchickenraw", new ItemStack(ItemInit.JUNGLEFOWL_RAW));
		OreDictionary.registerOre("listAllbeefraw", new ItemStack(ItemInit.AUROCHS_MEAT));
		OreDictionary.registerOre("listAllbeefraw", new ItemStack(ItemInit.MOOSHROOM_MEAT));
		OreDictionary.registerOre("listAllbeefraw", new ItemStack(ItemInit.WILD_MOOSHROOM_MEAT));
		//

		OreDictionary.registerOre("furCut", new ItemStack(ItemInit.FUR_WHITE));
		OreDictionary.registerOre("furCut", new ItemStack(ItemInit.FUR_BLACK));
		OreDictionary.registerOre("furCut", new ItemStack(ItemInit.FUR_LIGHT_GRAY));
		OreDictionary.registerOre("furCut", new ItemStack(ItemInit.FUR_BROWN));
		OreDictionary.registerOre("furCut", new ItemStack(ItemInit.FUR_RED));
		OreDictionary.registerOre("furCut", new ItemStack(ItemInit.FUR_DARK_GRAY));
		
		OreDictionary.registerOre("hideCut", new ItemStack(ItemInit.FUR_WHITE));
		OreDictionary.registerOre("hideCut", new ItemStack(ItemInit.FUR_BLACK));
		OreDictionary.registerOre("hideCut", new ItemStack(ItemInit.FUR_LIGHT_GRAY));
		OreDictionary.registerOre("hideCut", new ItemStack(ItemInit.FUR_BROWN));
		OreDictionary.registerOre("hideCut", new ItemStack(ItemInit.FUR_RED));
		OreDictionary.registerOre("hideCut", new ItemStack(ItemInit.FUR_DARK_GRAY));
		
		OreDictionary.registerOre("hideCut", new ItemStack(ItemInit.PIG_SKIN));
		
		OreDictionary.registerOre("tanninRich",  new ItemStack(Blocks.LOG, 1, 0));
		OreDictionary.registerOre("tanninPoor",  new ItemStack(Blocks.LOG, 1, 1));
		OreDictionary.registerOre("tanninRich",  new ItemStack(Blocks.LOG, 1, 2));
		OreDictionary.registerOre("tanninPoor",  new ItemStack(Blocks.LOG, 1, 3));
		
		OreDictionary.registerOre("tanninPoor",  new ItemStack(Blocks.LOG2, 1, 0));
		OreDictionary.registerOre("tanninRich",  new ItemStack(Blocks.LOG2, 1, 1));

		OreDictionary.registerOre("tanninRich",  new ItemStack(ItemInit.BOP_LOG0, 1, 4));
		OreDictionary.registerOre("tanninPoor",  new ItemStack(ItemInit.BOP_LOG0, 1, 5));
		OreDictionary.registerOre("tanninPoor",  new ItemStack(ItemInit.BOP_LOG0, 1, 6));
		OreDictionary.registerOre("tanninPoor",  new ItemStack(ItemInit.BOP_LOG0, 1, 7));

		OreDictionary.registerOre("tanninPoor",  new ItemStack(ItemInit.BOP_LOG1, 1, 4));
		OreDictionary.registerOre("tanninPoor",  new ItemStack(ItemInit.BOP_LOG1, 1, 5));
		OreDictionary.registerOre("tanninRich",  new ItemStack(ItemInit.BOP_LOG1, 1, 6));
		OreDictionary.registerOre("tanninPoor",  new ItemStack(ItemInit.BOP_LOG1, 1, 7));

		OreDictionary.registerOre("tanninRich",  new ItemStack(ItemInit.BOP_LOG2, 1, 4));
		OreDictionary.registerOre("tanninPoor",  new ItemStack(ItemInit.BOP_LOG2, 1, 5));
		OreDictionary.registerOre("tanninPoor",  new ItemStack(ItemInit.BOP_LOG2, 1, 6));
		OreDictionary.registerOre("tanninPoor",  new ItemStack(ItemInit.BOP_LOG2, 1, 7));
		
		OreDictionary.registerOre("tanninPoor",  new ItemStack(ItemInit.BOP_LOG3, 1, 4));
		OreDictionary.registerOre("tanninPoor",  new ItemStack(ItemInit.BOP_LOG3, 1, 5));
		OreDictionary.registerOre("tanninPoor",  new ItemStack(ItemInit.BOP_LOG3, 1, 6));
		OreDictionary.registerOre("tanninPoor",  new ItemStack(ItemInit.BOP_LOG3, 1, 7));
		
		GameRegistry.addShapedRecipe(new ResourceLocation("bone_meal_from_bone_shards"), new ResourceLocation("recipes"), new ItemStack(Items.DYE, 1, 15), new Object[]{"B",'B', ItemInit.SHARDS_BONE});

		if (Config.enableNewTorches)
		{
			GameRegistry.addShapedRecipe(new ResourceLocation("resinfibertorch"), new ResourceLocation("resinfibertorch"), new ItemStack(Blocks.TORCH, 2), new Object[]{"B","W","S",'B', "resinTree", 'W', ItemInit.FIBER, 'S', "stickWood" });
			GameRegistry.addShapedRecipe(new ResourceLocation("resinwooltorch"), new ResourceLocation("resinwooltorch"), new ItemStack(Blocks.TORCH, 4), new Object[]{"B","W","S",'B', "resinTree",'W',Blocks.WOOL,'S',"stickWood"});
			if (ItemInit.SPRUCE_CONE != null) GameRegistry.addShapedRecipe(new ResourceLocation("resinconetorch"), new ResourceLocation("resinconetorch"), new ItemStack(Blocks.TORCH, 3), new Object[]{"B","W","S",'B', "resinTree", 'W', "treeCone", 'S', "stickWood" });
	
			GameRegistry.addShapedRecipe(new ResourceLocation("fibertorch"), new ResourceLocation("fibertorch"), new ItemStack(Blocks.TORCH, 2), new Object[]{"B","W","S",'B', ItemInit.OIL_BOTTLE, 'W', ItemInit.FIBER, 'S', "stickWood" });
			GameRegistry.addShapedRecipe(new ResourceLocation("wooltorch"), new ResourceLocation("wooltorch"), new ItemStack(Blocks.TORCH, 4), new Object[]{"B","W","S",'B', ItemInit.OIL_BOTTLE, 'W', Blocks.WOOL, 'S', "stickWood" });
			if (ItemInit.SPRUCE_CONE != null) GameRegistry.addShapedRecipe(new ResourceLocation("conetorch"), new ResourceLocation("conetorch"), new ItemStack(Blocks.TORCH, 3), new Object[]{"B","W","S",'B', ItemInit.OIL_BOTTLE, 'W', "treeCone", 'S', "stickWood" });
		
			if (ItemInit.TAR_DROP != null)
			{	
				GameRegistry.addShapedRecipe(new ResourceLocation("tarfibertorch"), new ResourceLocation("tarfibertorch"), new ItemStack(Blocks.TORCH, 2), new Object[]{"B","W","S",'B', ItemInit.TAR_DROP, 'W', ItemInit.FIBER, 'S', "stickWood" });			
				GameRegistry.addShapedRecipe(new ResourceLocation("tarwooltorch"), new ResourceLocation("tarwooltorch"), new ItemStack(Blocks.TORCH, 4), new Object[]{"B","W","S",'B', ItemInit.TAR_DROP, 'W', Blocks.WOOL, 'S', "stickWood" });
				if (ItemInit.SPRUCE_CONE != null) GameRegistry.addShapedRecipe(new ResourceLocation("tarconetorch"), new ResourceLocation("tarconetorch"), new ItemStack(Blocks.TORCH, 3), new Object[]{"B","W","S",'B', ItemInit.TAR_DROP, 'W', "treeCone", 'S', "stickWood" });
			}
			
		}
			

		if ( Config.shardsFromBone == true && Config.marrowFromBone == false)
		{
			GameRegistry.addShapedRecipe(new ResourceLocation("bone_meal_from"), new ResourceLocation("bone_meal_from"), new ItemStack(ItemInit.SHARDS_BONE, 2), new Object[]{"B",'B', Items.BONE});
			GameRegistry.addShapedRecipe(new ResourceLocation("bone_meal_from_block"), new ResourceLocation("bone_meal_from_block"), new ItemStack(Items.DYE, 9, 15), new Object[]{"B",'B', Blocks.BONE_BLOCK});
		}
		else if (Config.marrowFromBone == true)
		{
			GameRegistry.addShapedRecipe(new ResourceLocation("bone_meal_from"), new ResourceLocation("bone_meal_from"), new ItemStack(ItemInit.BONE_MARROW, 1), new Object[]{"B",'B', Items.BONE});
			GameRegistry.addShapedRecipe(new ResourceLocation("bone_meal_from_block"), new ResourceLocation("bone_meal_from_block"), new ItemStack(Items.DYE, 9, 15), new Object[]{"B",'B', Blocks.BONE_BLOCK});
		}
					
		if (Config.boneArrows) GameRegistry.addShapedRecipe(new ResourceLocation("BoneArrows"), new ResourceLocation("Arrow"),new ItemStack(Items.ARROW, 4), "B", "S", "F", 'F', Items.FEATHER, 'S', Items.STICK, 'B', ItemInit.SHARDS_BONE);

		if (Config.bedRecipes)
		{
			GameRegistry.addShapedRecipe(new ResourceLocation("NewBedHideB"), new ResourceLocation("NewBeds"), new ItemStack(Items.BED, 1, 15),new Object[]{ "FFF", "FFF", "WWW", 'F', ItemInit.FUR_BLACK, 'W', "plankWood"});
			GameRegistry.addShapedRecipe(new ResourceLocation("NewBedHideBr"), new ResourceLocation("NewBeds"), new ItemStack(Items.BED, 1, 12),new Object[]{ "FFF", "FFF", "WWW", 'F', ItemInit.FUR_BROWN, 'W', "plankWood"});
			GameRegistry.addShapedRecipe(new ResourceLocation("NewBedHideDG"), new ResourceLocation("NewBeds"), new ItemStack(Items.BED, 1, 7),new Object[]{ "FFF", "FFF", "WWW", 'F', ItemInit.FUR_DARK_GRAY, 'W', "plankWood"});
			GameRegistry.addShapedRecipe(new ResourceLocation("NewBedHideLG"), new ResourceLocation("NewBeds"), new ItemStack(Items.BED, 1, 8),new Object[]{ "FFF", "FFF", "WWW", 'F', ItemInit.FUR_LIGHT_GRAY, 'W', "plankWood"});
			GameRegistry.addShapedRecipe(new ResourceLocation("NewBedHideR"), new ResourceLocation("NewBeds"), new ItemStack(Items.BED, 1, 14),new Object[]{ "FFF", "FFF", "WWW", 'F', ItemInit.FUR_RED, 'W', "plankWood"});
			GameRegistry.addShapedRecipe(new ResourceLocation("NewBedHideW"), new ResourceLocation("NewBeds"), new ItemStack(Items.BED, 1, 0),new Object[]{ "FFF", "FFF", "WWW", 'F', ItemInit.FUR_WHITE, 'W', "plankWood"});
			GameRegistry.addShapedRecipe(new ResourceLocation("NewBedFeather"), new ResourceLocation("NewBedsF"), new ItemStack(Items.BED, 1),new Object[]{ "FFF", "WWW", 'F', BlockInit.FEATHER_BLOCK, 'W', "plankWood"});
			//default to brown
			GameRegistry.addShapedRecipe(new ResourceLocation("NewBedHide"), new ResourceLocation("NewBeds"), new ItemStack(Items.BED, 1, 12),new Object[]{ "FFF", "FFF", "WWW", 'F', "furCut", 'W', "plankWood"});
		}
		
		if (Config.woolSystem)
		{
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockWhite"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 15) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 0)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockOrange"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 14) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 1)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockMagenta"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 13) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 2)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockLightBlue"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 12) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 3)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockYellow"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 11) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 4)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockLime"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 10) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 5)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockPink"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 9) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 6)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockGray"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 8) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 7)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockLightGray"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 7) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 8)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockCyan"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 6) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 9)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockPurple"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 5) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 10)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockBlue"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 4) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 11)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockBrown"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 3) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 12)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockGreen"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 2) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 13)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockRed"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 1) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 14)});
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlockBlack"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 0) ,new Object[]{ "WW", "WW", 'W', new ItemStack(ItemInit.WOOL_TUFT, 1, 15)});
			//default to brown
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolBlock"), new ResourceLocation("WoolBlock"), new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 12) ,new Object[]{ "WW", "WW", 'W', ItemInit.WOOL_TUFT});
			
			GameRegistry.addShapedRecipe(new ResourceLocation("WoolString"), new ResourceLocation("WoolString"), new ItemStack(Items.STRING, 2) ,new Object[]{ "W", "W", 'W', ItemInit.WOOL_TUFT});
		}
		
		if (Config.enableFowl)
		{
			GameRegistry.addShapedRecipe(new ResourceLocation("EggCake"), new ResourceLocation("recipes"), new ItemStack(Item.getItemFromBlock(Blocks.CAKE), 1), new Object[]{ "BBB", "SES", "FFF", 'B', Items.MILK_BUCKET, 'S', Items.SUGAR, 'E', "egg", 'F', Items.WHEAT});
		}

		if (Config.paperFromWood)
		{
			GameRegistry.addShapedRecipe(new ResourceLocation("PaperWood"), new ResourceLocation("recipes"),  new ItemStack(Items.PAPER, 2), new Object[]{ "WWW", 'W', "logWood"});
		}
		
		if (Config.sugarFromBeets) GameRegistry.addShapedRecipe(new ResourceLocation("recipes"), new ResourceLocation("SugarBeets"), new ItemStack(Items.SUGAR, 2), "BBB", 'B', Items.BEETROOT);
		if (Config.craftableNametags) GameRegistry.addShapedRecipe(new ResourceLocation("recipes"), new ResourceLocation("NameTag"),new ItemStack(Items.NAME_TAG, 1), " S ", " PI", " P ", 'S', Items.STRING, 'P', Items.PAPER, 'I', new ItemStack(Items.DYE, 1, EnumDyeColor.BLACK.getDyeDamage()));
		if (Config.craftableSaddles) GameRegistry.addShapedRecipe(new ResourceLocation("Saddle"), new ResourceLocation("Saddle"),new ItemStack(Items.SADDLE, 1), "LLL", "LSL", " I ", 'L', Items.LEATHER, 'S', Items.STRING, 'I', Items.IRON_INGOT);
		if (Config.craftableHorseArmor == true)
		{
			GameRegistry.addShapedRecipe(new ResourceLocation("IronHorseArmor"), new ResourceLocation("IronHorseArmor"), new ItemStack(Items.IRON_HORSE_ARMOR), "  I", "IWI", "IWI", 'I', Item.getItemFromBlock(Blocks.IRON_BLOCK), 'W', new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 15));
			GameRegistry.addShapedRecipe(new ResourceLocation("GoldHorseArmor"), new ResourceLocation("GoldHorseArmor"), new ItemStack(Items.GOLDEN_HORSE_ARMOR), "  G", "GWG", "GWG", 'G', Item.getItemFromBlock(Blocks.GOLD_BLOCK), 'W', new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 14));
			GameRegistry.addShapedRecipe(new ResourceLocation("DiamondHorseArmor"), new ResourceLocation("DiamondHorseArmor"), new ItemStack(Items.DIAMOND_HORSE_ARMOR), "  D", "DWD", "DWD", 'D', Item.getItemFromBlock(Blocks.DIAMOND_BLOCK), 'W', new ItemStack(Item.getItemFromBlock(Blocks.WOOL), 1, 4));
		}

//        if (Config.pumpkinRecipes == true)
//		{
//        	 GameRegistry.addShapedRecipe(new ItemStack(ItemInit.PUMPKIN_SLICE, 4), "P", 'P', new ItemStack(Blocks.PUMPKIN, 1));
//        	 GameRegistry.addShapedRecipe(new ItemStack(Items.PUMPKIN_SEEDS, 1), "P", 'P', ItemInit.PUMPKIN_SLICE);
//		}
		
		if (Config.enableFlintTools == true)
		{
			GameRegistry.addShapedRecipe(new ResourceLocation("AxeFlint"), new ResourceLocation("AxeFlint"),  new ItemStack(ItemInit.AXE_FLINT), new Object[] {
		            "F ",
		            "FS",
		            'S', "stickWood",   // can use ordinary items, blocks, itemstacks in ShapedOreRecipe
		            'F', Items.FLINT,  // look in OreDictionary for vanilla definitions
		    });
			GameRegistry.addShapedRecipe(new ResourceLocation("ShovelFlint"), new ResourceLocation("ShovelFlint"),  new ItemStack(ItemInit.SHOVEL_FLINT), new Object[] {
		            "F",
		            "S",
		            'S', "stickWood",   // can use ordinary items, blocks, itemstacks in ShapedOreRecipe
		            'F', Items.FLINT,  // look in OreDictionary for vanilla definitions
		    });
			GameRegistry.addShapedRecipe(new ResourceLocation("PickaxeFlint"), new ResourceLocation("PickaxeFlint"),  new ItemStack(ItemInit.PICKAXE_FLINT), new Object[] {
		            "FF",
		            " S",
		            'S', "stickWood",   // can use ordinary items, blocks, itemstacks in ShapedOreRecipe
		            'F', Items.FLINT,  // look in OreDictionary for vanilla definitions
		    });
			if (Config.enableKnives == true) GameRegistry.addShapedRecipe(new ResourceLocation("KnifeFlint"), new ResourceLocation("KnifeFlint"), new ItemStack(ItemInit.KNIFE_FLINT), new Object[] {
		            "F ",
		            " S",
		            'S', "stickWood",   // can use ordinary items, blocks, itemstacks in ShapedOreRecipe
		            'F', Items.FLINT,  // look in OreDictionary for vanilla definitions
		    });
		}
		
		if (Config.enableKnives == true)
		{
		    GameRegistry.addShapedRecipe(new ResourceLocation("KnifeWood"), new ResourceLocation("KnifeWood"), new ItemStack(ItemInit.KNIFE_WOOD), new Object[] {
		            "W ",
		            " S",
		            'S', "stickWood",   // can use ordinary items, blocks, itemstacks in ShapedOreRecipe
		            'W', "plankWood",  // look in OreDictionary for vanilla definitions
		    });
		    GameRegistry.addShapedRecipe(new ResourceLocation("KnifeStone"), new ResourceLocation("KnifeStone"), new ItemStack(ItemInit.KNIFE_STONE), new Object[] {
		            "C ",
		            " S",
		            'S', "stickWood",   // can use ordinary items, blocks, itemstacks in ShapedOreRecipe
		            'C', Blocks.COBBLESTONE,  // look in OreDictionary for vanilla definitions
		    });
		    GameRegistry.addShapedRecipe(new ResourceLocation("KnifeIron"), new ResourceLocation("KnifeIron"), new ItemStack(ItemInit.KNIFE_IRON), new Object[] {
		            "I ",
		            " S",
		            'S', "stickWood",   // can use ordinary items, blocks, itemstacks in ShapedOreRecipe
		            'I', "ingotIron",  // look in OreDictionary for vanilla definitions
		    });
		    GameRegistry.addShapedRecipe(new ResourceLocation("KnifeGold"), new ResourceLocation("KnifeGold"), new ItemStack(ItemInit.KNIFE_GOLD), new Object[] {
		            "G ",
		            " S",
		            'S', "stickWood",   // can use ordinary items, blocks, itemstacks in ShapedOreRecipe
		            'G', "ingotGold",  // look in OreDictionary for vanilla definitions
		    });
		    GameRegistry.addShapedRecipe(new ResourceLocation("KnifeDiamond"), new ResourceLocation("KnifeDiamond"), new ItemStack(ItemInit.KNIFE_DIAMOND), new Object[] {
		            "D ",
		            " S",
		            'S', "stickWood",   // can use ordinary items, blocks, itemstacks in ShapedOreRecipe
		            'D', Items.DIAMOND,  // look in OreDictionary for vanilla definitions
		    });
		}
		
	}
	
	
	
	public static void RegisterSmelting()
	{
		GameRegistry.addSmelting(ItemInit.DUCK_RAW, new ItemStack(ItemInit.DUCK_COOKED), 1);
		GameRegistry.addSmelting(ItemInit.MALLARD_RAW, new ItemStack(ItemInit.DUCK_COOKED), 1);
		GameRegistry.addSmelting(ItemInit.LLAMA_MEAT, new ItemStack(ItemInit.COOKED_LLAMA_MEAT), 1);
		GameRegistry.addSmelting(ItemInit.MOUFLON_MEAT, new ItemStack(Items.COOKED_MUTTON), 1);
		GameRegistry.addSmelting(ItemInit.AUROCHS_MEAT, new ItemStack(Items.COOKED_BEEF), 1);
		GameRegistry.addSmelting(ItemInit.WILD_MOOSHROOM_MEAT, new ItemStack(Items.COOKED_BEEF), 1);
		GameRegistry.addSmelting(ItemInit.MOOSHROOM_MEAT, new ItemStack(Items.COOKED_BEEF), 1);
		GameRegistry.addSmelting(ItemInit.BONE_MARROW, new ItemStack(ItemInit.COOKED_BONE_MARROW), 1);
		GameRegistry.addSmelting(ItemInit.CALAMARI, new ItemStack(ItemInit.COOKED_CALAMARI), 1);
		GameRegistry.addSmelting(ItemInit.FAT_BOTTLE, new ItemStack(ItemInit.OIL_BOTTLE), 1);
		GameRegistry.addSmelting(ItemInit.DONKEY_MEAT, new ItemStack(ItemInit.COOKED_DONKEY_MEAT), 1);
		GameRegistry.addSmelting(ItemInit.HORSE_MEAT, new ItemStack(ItemInit.COOKED_HORSE_MEAT), 1);
		GameRegistry.addSmelting(ItemInit.WILD_ASS_MEAT, new ItemStack(ItemInit.COOKED_DONKEY_MEAT), 1);
		GameRegistry.addSmelting(ItemInit.WILD_HORSE_MEAT, new ItemStack(ItemInit.COOKED_HORSE_MEAT), 1);
		GameRegistry.addSmelting(ItemInit.ZEBRA_MEAT, new ItemStack(ItemInit.COOKED_ZEBRA_MEAT), 1);
		GameRegistry.addSmelting(ItemInit.QUAGGA_MEAT, new ItemStack(ItemInit.COOKED_ZEBRA_MEAT), 1);
		GameRegistry.addSmelting(ItemInit.RAW_BAT, new ItemStack(ItemInit.COOKED_BAT), 1);
		GameRegistry.addSmelting(Items.PUMPKIN_SEEDS, new ItemStack(ItemInit.COOKED_PUMPKIN_SEEDS), 1);
		GameRegistry.addSmelting(ItemInit.PIG_SKIN, new ItemStack(ItemInit.RIND_PORK), 1);
		GameRegistry.addSmelting(Items.EGG, new ItemStack(ItemInit.FRIED_EGG), 1);
		GameRegistry.addSmelting(ItemInit.EGG, new ItemStack(ItemInit.FRIED_EGG), 1);
		GameRegistry.addSmelting(ItemInit.EGG_DUCK, new ItemStack(ItemInit.FRIED_EGG), 1);
		GameRegistry.addSmelting(ItemInit.RAW_BOAR_PORKCHOP, new ItemStack(Items.COOKED_PORKCHOP), 1);

		GameRegistry.addSmelting(Items.MILK_BUCKET, new ItemStack(ItemInit.BUCKET_CURDLE), 1);

		GameRegistry.addSmelting(ItemInit.BAT_STEW_RAW, new ItemStack(ItemInit.BAT_STEW), 1);
		GameRegistry.addSmelting(ItemInit.RABBIT_STEW_RAW, new ItemStack(Items.RABBIT_STEW), 1);
		GameRegistry.addSmelting(ItemInit.MUSHROOM_STEW_RAW, new ItemStack(Items.MUSHROOM_STEW), 1);

	}

}


