package faunaandecology.mod.util.handlers;

import faunaandecology.mod.init.ItemInit;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.IFuelHandler;

public class FuelHandler implements IFuelHandler
{

	@Override
	public int getBurnTime(ItemStack fuel) 
	{
		if (fuel.getItem() == ItemInit.FAT){
			return 200;
		}
		if (fuel.getItem() == ItemInit.FAT_BOTTLE){
			return 200;
		}
		if (fuel.getItem() == ItemInit.RESIN){
			return 400;
		}
		if (fuel.getItem() == ItemInit.TALLOW){
			return 1200;
		}
		if (fuel.getItem() == ItemInit.OIL_BOTTLE){
			return 1600;
		}
		return 0;
	}

}