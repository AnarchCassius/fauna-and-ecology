package faunaandecology.mod.util;

import faunaandecology.mod.init.ItemInit;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class Reference {

	//public static final CreativeTabs endextab = new EndExTabs("prehistoricworldtab");

	public static final String MODID = "faunaandecology";
	public static final String NAME = "Fauna and Ecology";
	public static final String VERSION = "0.1.3.2";
	
	public static final String CLIENTPROXY = "faunaandecology.mod.proxy.ClientProxy";
	public static final String COMMONPROXY = "faunaandecology.mod.proxy.CommonProxy";
	
	public static final CreativeTabs FaunaAndEcology = new CreativeTabs("myMod") {
	    @Override public ItemStack getTabIconItem() {
	        return new ItemStack(ItemInit.ZOOPEDIA, 1);
	    }
	};
}
