package faunaandecology.mod.util;

public interface IHasModel {

	public void registerModels();
}
