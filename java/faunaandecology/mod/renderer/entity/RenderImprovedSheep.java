package faunaandecology.mod.renderer.entity;

import faunaandecology.mod.entity.passive.EntityImprovedSheep;
import faunaandecology.mod.model.ModelImprovedSheep;
import faunaandecology.mod.renderer.entity.layers.LayerImprovedSheepWool;
import faunaandecology.mod.util.Config;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderImprovedSheep extends RenderLiving<EntityImprovedSheep>
{
    public static final Factory FACTORY = new Factory();

    private static final ResourceLocation SHEEP_TEXTURES_WHITE = new ResourceLocation("faunaandecology:textures/entity/sheep/sheep_white.png");
    private static final ResourceLocation SHEEP_TEXTURES_BLACK = new ResourceLocation("faunaandecology:textures/entity/sheep/sheep_black.png");
    private static final ResourceLocation SHEEP_TEXTURES_BROWN = new ResourceLocation("faunaandecology:textures/entity/sheep/sheep_brown.png");

    public RenderImprovedSheep(RenderManager rendermanagerIn)
    {
        super(rendermanagerIn, new ModelImprovedSheep(), 0.5F);
        this.addLayer(new LayerImprovedSheepWool(this));
        //this.addLayer(new LayerCowSaddle(this));
    }

    /**
     * Allows the render to do state modifications necessary before the model is rendered.
     */
    @Override
	protected void preRenderCallback(EntityImprovedSheep entitylivingbaseIn, float partialTickTime)
    {
        float f = 0.9F;


        //child scaling
        if (entitylivingbaseIn.isChild())
        {
        	f = 0.45f;
        	if (Config.smoothGrowth == true) f += 0.55F * entitylivingbaseIn.getGrowthSize();
        }
        
        //size variance
        if (Config.sizeVariance == true) f *= 1.0F + ((-25.0F + entitylivingbaseIn.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue()) / 40.0F);
        
        //larger males
		if (Config.enableSexes == true)
		{
	        if (entitylivingbaseIn.getSex() == 1)	f *= 1.10F;
        }

        GlStateManager.scale(f, f, f);
        super.preRenderCallback(entitylivingbaseIn, partialTickTime);
    }
    
    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
	@Override
    protected ResourceLocation getEntityTexture(EntityImprovedSheep entity)
    {
        switch (entity.getNaturalColor())
        {
            case WHITE:
            default:
                return SHEEP_TEXTURES_WHITE;
            case PINK:
            	return SHEEP_TEXTURES_BROWN;
            case GRAY:
            	return SHEEP_TEXTURES_BLACK;
            case SILVER:
            	return SHEEP_TEXTURES_WHITE;
            case BROWN:
            	return SHEEP_TEXTURES_BROWN;
            case BLACK:
            	return SHEEP_TEXTURES_BLACK;
        }
    }
    
    public static class Factory implements IRenderFactory<EntityImprovedSheep> {

        @Override
        public Render<? super EntityImprovedSheep> createRenderFor(RenderManager manager) {
            return new RenderImprovedSheep(manager);
        }

    }
}