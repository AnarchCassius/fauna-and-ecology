package faunaandecology.mod.renderer.entity;

import java.util.Map;

import javax.annotation.Nullable;

import com.google.common.collect.Maps;

import faunaandecology.mod.entity.passive.EntityImprovedHorse;
import faunaandecology.mod.model.ModelImprovedHorse;
import faunaandecology.mod.util.Config;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.LayeredTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderImprovedHorse extends RenderLiving<EntityImprovedHorse> {

    public static final Factory FACTORY = new Factory();


    private static final Map<String, ResourceLocation> LAYERED_LOCATION_CACHE = Maps.<String, ResourceLocation>newHashMap();

    public RenderImprovedHorse(RenderManager rendermanagerIn)
    {
        super(rendermanagerIn, new ModelImprovedHorse(), 0.5F);
        //setEntityTexture();        
    }

    /**
     * Allows the render to do state modifications necessary before the model is rendered.
     */
    @Override
	protected void preRenderCallback(EntityImprovedHorse entitylivingbaseIn, float partialTickTime)
    {
        float f = 1.0F;
        //ImprovedHorseType horsetype = entitylivingbaseIn.getImprovedType();

        //child scaling
        if (entitylivingbaseIn.isChild())
        {
        	f = 0.7f;
        	if (Config.smoothGrowth == true) f += 0.3F * entitylivingbaseIn.getGrowthSize();
        }
        
//        if (horsetype == ImprovedHorseType.DONKEY)
//        {
//            f *= 0.87F;
//        }
//        else if (horsetype == ImprovedHorseType.WILD_HORSE || horsetype == ImprovedHorseType.ZEBRA)
//        {
//            f *= 0.96F;
//        }
//        else if (horsetype == ImprovedHorseType.MULE || horsetype == ImprovedHorseType.ZONKEY || horsetype == ImprovedHorseType.WILD_ASS  || horsetype == ImprovedHorseType.QUAGGA)
//        {
//            f *= 0.92F;
//        }
//        
        //larger males

		if (Config.enableSexes == true)
		{
	        if (entitylivingbaseIn.getSex() == 1)	f *= 1.05F;
        }

        GlStateManager.scale(f, f, f);
        super.preRenderCallback(entitylivingbaseIn, partialTickTime);
    }

    @Nullable
    private ResourceLocation getOrCreateLayeredResourceLoc(EntityImprovedHorse p_188328_1_)
    {
        String s = p_188328_1_.getHorseTexture();

        ResourceLocation resourcelocation = LAYERED_LOCATION_CACHE.get(s);

        if (resourcelocation == null)
        {
            resourcelocation = new ResourceLocation(s);
            Minecraft.getMinecraft().getTextureManager().loadTexture(resourcelocation, new LayeredTexture(p_188328_1_.getVariantTexturePaths()));
            LAYERED_LOCATION_CACHE.put(s, resourcelocation);
        }

        return resourcelocation;

    }
    
    

    public static class Factory implements IRenderFactory<EntityImprovedHorse> {

        @Override
        public Render<? super EntityImprovedHorse> createRenderFor(RenderManager manager) {
            return new RenderImprovedHorse(manager);
        }

    }

	@Override
	protected ResourceLocation getEntityTexture(EntityImprovedHorse entity) {
		// TODO Auto-generated method stub
		return getOrCreateLayeredResourceLoc(entity);
	}
}