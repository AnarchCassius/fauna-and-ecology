package faunaandecology.mod.renderer.entity;

import faunaandecology.mod.entity.passive.EntityJungleFowl;
import faunaandecology.mod.model.ModelImprovedChicken;
import faunaandecology.mod.util.Config;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderImprovedChicken extends RenderLiving<EntityJungleFowl>
{
    public static final Factory FACTORY = new Factory();

    private static final ResourceLocation COW_TEXTURES = new ResourceLocation("faunaandecology:textures/entity/chicken/chicken_female.png");
    private static final ResourceLocation BULL_TEXTURES = new ResourceLocation("faunaandecology:textures/entity/chicken/chicken_male.png");
    private static final ResourceLocation CALF_TEXTURES = new ResourceLocation("faunaandecology:textures/entity/chicken/chicken_juvenile.png");

    public RenderImprovedChicken(RenderManager rendermanagerIn)
    {
        super(rendermanagerIn, new ModelImprovedChicken(), 0.5F);
    }

    /**
     * Defines what float the third param in setRotationAngles of ModelBase is
     */

    @Override
    protected float handleRotationFloat(EntityJungleFowl livingBase, float partialTicks)
    {
        float f = livingBase.oFlap + (livingBase.wingRotation - livingBase.oFlap) * partialTicks;
        float f1 = livingBase.oFlapSpeed + (livingBase.destPos - livingBase.oFlapSpeed) * partialTicks;
        return (MathHelper.sin(f) + 1.0F) * f1;
    }

    /**
     * Allows the render to do state modifications necessary before the model is rendered.
     */
    @Override
	protected void preRenderCallback(EntityJungleFowl entitylivingbaseIn, float partialTickTime)
    {
        float f = 1.0F;


        //child scaling
        if (entitylivingbaseIn.isChild())
        {
        	f = 0.45f;
        	if (Config.smoothGrowth == true) f += 0.55F * entitylivingbaseIn.getGrowthSize();
        }
        
        //size variance
        if (Config.sizeVariance == true) f *= 1.0F + ((-25.0F + entitylivingbaseIn.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue()) / 40.0F);
        
        //larger males
		if (Config.enableSexes == true)
		{
	        if (entitylivingbaseIn.getSex() == 1)	f *= 1.10F;
        }

        GlStateManager.scale(f, f, f);
        super.preRenderCallback(entitylivingbaseIn, partialTickTime);
    }
    
    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
	@Override
    protected ResourceLocation getEntityTexture(EntityJungleFowl entity)
    {
        if (entity.isChild())
        {
            return CALF_TEXTURES;
        }

		if (Config.enableSexes == true)
		{
	        if (entity.getSex() == 1) return BULL_TEXTURES;
        }
        return COW_TEXTURES;
    }
    
    public static class Factory implements IRenderFactory<EntityJungleFowl> {

        @Override
        public Render<? super EntityJungleFowl> createRenderFor(RenderManager manager) {
            return new RenderImprovedChicken(manager);
        }

    }
    

}