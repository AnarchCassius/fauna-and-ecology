package faunaandecology.mod.renderer.entity.layers;

import faunaandecology.mod.entity.passive.EntityImprovedCow;
import faunaandecology.mod.model.ModelImprovedCow;
import faunaandecology.mod.renderer.entity.RenderImprovedCow;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class LayerCowSaddle implements LayerRenderer<EntityImprovedCow>
{
    private static final ResourceLocation TEXTURE = new ResourceLocation("faunaandecology:textures/entity/cow/cow_saddle.png");
    private final RenderImprovedCow cowRenderer;
    private final ModelImprovedCow cowModel = new ModelImprovedCow();

    public LayerCowSaddle(RenderImprovedCow cowRendererIn)
    {
        this.cowRenderer = cowRendererIn;
    }

    @Override
	public void doRenderLayer(EntityImprovedCow entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale)
    {
        if (entitylivingbaseIn.getSaddled())
        {
            this.cowRenderer.bindTexture(TEXTURE);
            this.cowModel.setModelAttributes(this.cowRenderer.getMainModel());
            this.cowModel.render(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
        }
    }

    @Override
	public boolean shouldCombineTextures()
    {
        return false;
    }
}