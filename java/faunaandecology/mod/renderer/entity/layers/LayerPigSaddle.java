package faunaandecology.mod.renderer.entity.layers;

import faunaandecology.mod.entity.passive.EntityImprovedPig;
import faunaandecology.mod.model.ModelImprovedPig;
import faunaandecology.mod.renderer.entity.RenderImprovedPig;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class LayerPigSaddle implements LayerRenderer<EntityImprovedPig>
{
    private static final ResourceLocation TEXTURE = new ResourceLocation("textures/entity/pig/pig_saddle.png");
    private final RenderImprovedPig pigRenderer;
    private final ModelImprovedPig pigModel = new ModelImprovedPig();

    public LayerPigSaddle(RenderImprovedPig pigRendererIn)
    {
        this.pigRenderer = pigRendererIn;
    }

    @Override
	public void doRenderLayer(EntityImprovedPig entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale)
    {
        if (entitylivingbaseIn.getSaddled())
        {
            this.pigRenderer.bindTexture(TEXTURE);
            this.pigModel.setModelAttributes(this.pigRenderer.getMainModel());
            this.pigModel.render(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
        }
    }

    @Override
	public boolean shouldCombineTextures()
    {
        return false;
    }
}