package faunaandecology.mod.model;

import faunaandecology.mod.entity.passive.EntityImprovedPig;
import faunaandecology.mod.util.Config;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelImprovedPig extends ModelBase {
    public ModelRenderer body;
    public ModelRenderer leg_hind_right;
    public ModelRenderer leg_front_right;
    public ModelRenderer leg_hind_left;
    public ModelRenderer leg_front_left;
    public ModelRenderer shape10;
    public ModelRenderer shape10_1;
    public ModelRenderer shape12;
    public ModelRenderer neck;
    public ModelRenderer saddle;
    public ModelRenderer head;
    public ModelRenderer snout;
    public ModelRenderer ear_right;
    public ModelRenderer ear_left;

    public ModelImprovedPig() {
        this.textureWidth = 64;
        this.textureHeight = 64;
        this.leg_hind_left = new ModelRenderer(this, 38, 0);
        this.leg_hind_left.mirror = true;
        this.leg_hind_left.setRotationPoint(-2.0F, 18.0F, 7.5F);
        this.leg_hind_left.addBox(-2.0F, 0.0F, -2.0F, 3, 6, 3, 0.0F);
        this.shape10 = new ModelRenderer(this, 22, 0);
        this.shape10.setRotationPoint(2.5F, 11.0F, -14.0F);
        this.shape10.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1, 0.0F);
        this.setRotateAngle(shape10, 0.0F, 0.0F, 0.5235987755982988F);
        this.head = new ModelRenderer(this, 1, 1);
        this.head.setRotationPoint(0.0F, 0.1F, 1.0F);
        this.head.addBox(-4.0F, -4.0F, -8.0F, 8, 8, 5, 0.0F);
        this.setRotateAngle(head, -0.03490658503988659F, 0.0F, 0.0F);
        this.body = new ModelRenderer(this, 26, 15);
        this.body.setRotationPoint(0.5F, 11.5F, 2.0F);
        this.body.addBox(-5.0F, -10.0F, -7.0F, 9, 17, 10, 0.0F);
        this.setRotateAngle(body, 1.5707963267948966F, 0.0F, 0.0F);
        this.ear_right = new ModelRenderer(this, 0, 0);
        this.ear_right.mirror = true;
        this.ear_right.setRotationPoint(3.0F, -5.9F, -5.0F);
        this.ear_right.addBox(0.0F, 0.0F, 0.0F, 2, 3, 1, 0.0F);
        this.setRotateAngle(ear_right, 0.2617993877991494F, 0.0F, 0.2617993877991494F);
        this.leg_front_left = new ModelRenderer(this, 38, 0);
        this.leg_front_left.mirror = true;
        this.leg_front_left.setRotationPoint(-2.0F, 18.0F, -5.5F);
        this.leg_front_left.addBox(-2.0F, 0.0F, -2.0F, 3, 6, 3, 0.0F);
        this.shape10_1 = new ModelRenderer(this, 22, 0);
        this.shape10_1.setRotationPoint(-3.4F, 11.4F, -14.0F);
        this.shape10_1.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1, 0.0F);
        this.setRotateAngle(shape10_1, 0.0F, 0.0F, -0.5235987755982988F);
        this.leg_hind_right = new ModelRenderer(this, 38, 0);
        this.leg_hind_right.setRotationPoint(3.0F, 18.0F, 7.5F);
        this.leg_hind_right.addBox(-2.0F, 0.0F, -2.0F, 3, 6, 3, 0.0F);
        this.neck = new ModelRenderer(this, 43, 25);
        this.neck.setRotationPoint(0.0F, 12.6F, -7.3F);
        this.neck.addBox(-3.5F, -4.0F, -3.0F, 7, 8, 4, 0.0F);
        this.setRotateAngle(neck, 0.06981317007977318F, 0.0F, 0.0F);
        this.snout = new ModelRenderer(this, 15, 15);
        this.snout.setRotationPoint(0.0F, -1.0F, -1.5F);
        this.snout.addBox(-2.0F, 0.0F, -9.0F, 4, 4, 3, 0.0F);
        this.leg_front_right = new ModelRenderer(this, 38, 0);
        this.leg_front_right.setRotationPoint(3.0F, 18.0F, -5.5F);
        this.leg_front_right.addBox(-2.0F, 0.0F, -2.0F, 3, 6, 3, 0.0F);
        this.saddle = new ModelRenderer(this, 35, 45);
        this.saddle.setRotationPoint(-5.0F, 8.4F, -4.0F);
        this.saddle.addBox(0.0F, 0.0F, 0.0F, 10, 8, 9, 0.0F);
        this.ear_left = new ModelRenderer(this, 0, 0);
        this.ear_left.setRotationPoint(-4.9F, -5.4F, -5.0F);
        this.ear_left.addBox(0.0F, 0.0F, 0.0F, 2, 3, 1, 0.0F);
        this.setRotateAngle(ear_left, 0.2617993877991494F, 0.0F, -0.2617993877991494F);
        this.shape12 = new ModelRenderer(this, 31, 0);
        this.shape12.setRotationPoint(-0.5F, 9.0F, 6.0F);
        this.shape12.addBox(0.0F, 0.0F, 0.0F, 1, 6, 1, 0.0F);
        this.setRotateAngle(shape12, 0.3490658503988659F, 0.0F, 0.0F);
        this.neck.addChild(this.head);
        this.head.addChild(this.ear_right);
        this.head.addChild(this.snout);
        this.head.addChild(this.ear_left);
    }
    
    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) 
    { 
    	EntityImprovedPig entitycow = (EntityImprovedPig)entity;
        boolean flag = !(entitycow.isChild());

        this.leg_hind_right.render(f5);
        this.leg_hind_left.render(f5);
        this.leg_front_left.render(f5);
        this.leg_front_right.render(f5);
        this.body.render(f5);
        if (!flag)
        {
        	float size = 1.3F;
        	float growthSize = f5;
        	if (Config.smoothGrowth == true) size -= 0.3F * growthSize;
        	growthSize = (1.0F - growthSize);
            GlStateManager.pushMatrix();
            GlStateManager.scale(size, size, size);
            GlStateManager.translate(0.0F, -0.1F * growthSize, 0.2F * growthSize);
        }
        this.neck.render(f5);
        if (!flag)
        {
            GlStateManager.popMatrix();
        }
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
    @Override
	public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn)
    {
        this.neck.rotateAngleX = (headPitch * 0.008453292F) + 0.05f;
        this.neck.rotateAngleY = netHeadYaw * 0.008453292F;
        this.head.rotateAngleX = (headPitch * 0.009F) - 0.025f;
        this.head.rotateAngleY = netHeadYaw * 0.009F;
        this.body.rotateAngleX = ((float)Math.PI / 2F);
        this.leg_front_right.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        this.leg_front_left.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
        this.leg_hind_right.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
        this.leg_hind_left.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        //this.tail_top.rotateAngleZ = MathHelper.cos(limbSwing * 0.6662F) * 1.1F * limbSwingAmount;
    }
}
