package faunaandecology.mod.model;

import faunaandecology.mod.entity.passive.EntityImprovedSheep;
import faunaandecology.mod.util.Config;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

/**
 * ModelCow - Either Mojang or a mod author
 * Created using Tabula 6.0.0
 */
public class ModelImprovedSheep2 extends ModelBase {
    public ModelRenderer body;
    public ModelRenderer neck;
    public ModelRenderer shape59;
    public ModelRenderer leg_front_right;
    public ModelRenderer leg_front_left;
    public ModelRenderer tail;
    public ModelRenderer tail_wool;
    public ModelRenderer leg_back_right;
    public ModelRenderer leg_back_left;
    public ModelRenderer wool_body;
    public ModelRenderer shape53;
    public ModelRenderer shape53_1;
    public ModelRenderer shape53_2;
    public ModelRenderer shape53_3;
    public ModelRenderer head;
    public ModelRenderer shape57;
    public ModelRenderer snout;
    public ModelRenderer ear_short_right;
    public ModelRenderer ear_short_left;
    public ModelRenderer shape58;
    
    private float headRotationAngleX;
    private float headRotationAngleY;

    public ModelImprovedSheep2() {
        this.textureWidth = 128;
        this.textureHeight = 128;
        this.textureWidth = 128;
        this.textureHeight = 128;
        this.leg_front_right = new ModelRenderer(this, 44, 29);
        this.leg_front_right.setRotationPoint(2.0F, 0.5F, -16.5F);
        this.leg_front_right.addBox(-1.5F, 0.0F, -2.0F, 3, 10, 3, 0.0F);
        this.body = new ModelRenderer(this, 0, 32);
        this.body.setRotationPoint(0.5F, 13.5F, 9.0F);
        this.body.addBox(-5.0F, -8.0F, -19.0F, 9, 9, 20, 0.0F);
        this.shape53_3 = new ModelRenderer(this, 3, 71);
        this.shape53_3.setRotationPoint(-2.0F, 0.0F, -2.5F);
        this.shape53_3.addBox(0.0F, 0.0F, 0.0F, 4, 5, 4, 0.0F);
        this.shape59 = new ModelRenderer(this, 25, 0);
        this.shape59.setRotationPoint(-2.0F, 14.0F, 3.0F);
        this.shape59.addBox(0.0F, 0.0F, 0.0F, 4, 4, 3, 0.0F);
        this.leg_back_right = new ModelRenderer(this, 59, 29);
        this.leg_back_right.setRotationPoint(3.0F, 0.5F, -1.0F);
        this.leg_back_right.addBox(-2.5F, 0.0F, -2.0F, 3, 10, 3, 0.0F);
        this.wool_body = new ModelRenderer(this, 0, 63);
        this.wool_body.setRotationPoint(-5.5F, -8.5F, -19.5F);
        this.wool_body.addBox(0.0F, 0.0F, 0.0F, 10, 10, 21, 0.0F);
        this.ear_short_right = new ModelRenderer(this, 0, 0);
        this.ear_short_right.mirror = true;
        this.ear_short_right.setRotationPoint(1.4F, -5.9F, -8.5F);
        this.ear_short_right.addBox(0.0F, 0.0F, 4.0F, 3, 2, 1, 0.0F);
        this.setRotateAngle(ear_short_right, 0.0F, 0.0F, -0.17453292519943295F);
        this.shape53 = new ModelRenderer(this, 3, 71);
        this.shape53.setRotationPoint(-2.0F, 0.0F, -2.5F);
        this.shape53.addBox(0.0F, 0.0F, 0.0F, 4, 5, 4, 0.0F);
        this.shape53_1 = new ModelRenderer(this, 3, 71);
        this.shape53_1.mirror = true;
        this.shape53_1.setRotationPoint(-2.0F, 0.0F, -2.5F);
        this.shape53_1.addBox(0.0F, 0.0F, 0.0F, 4, 5, 4, 0.0F);
        this.neck = new ModelRenderer(this, 1, 13);
        this.neck.setRotationPoint(0.0F, 10.5F, -7.5F);
        this.neck.addBox(-2.05F, -10.0F, -3.0F, 4, 12, 5, 0.0F);
        this.setRotateAngle(neck, 0.6108652381980153F, 0.0F, 0.0F);
        this.leg_back_left = new ModelRenderer(this, 59, 29);
        this.leg_back_left.mirror = true;
        this.leg_back_left.setRotationPoint(-3.0F, 0.5F, -1.0F);
        this.leg_back_left.addBox(-1.5F, 0.0F, -2.0F, 3, 10, 3, 0.0F);
        this.shape53_2 = new ModelRenderer(this, 3, 71);
        this.shape53_2.mirror = true;
        this.shape53_2.setRotationPoint(-3.0F, 0.0F, -2.5F);
        this.shape53_2.addBox(0.0F, 0.0F, 0.0F, 4, 5, 4, 0.0F);
        this.head = new ModelRenderer(this, 3, 1);
        this.head.setRotationPoint(0.0F, -3.5F, 2.4F);
        this.head.addBox(-2.5F, -6.0F, -8.0F, 5, 5, 5, 0.0F);
        this.setRotateAngle(head, -0.4363323129985824F, 0.0F, 0.0F);
        this.shape58 = new ModelRenderer(this, 43, 70);
        this.shape58.setRotationPoint(-3.0F, -6.5F, -6.7F);
        this.shape58.addBox(0.0F, 0.0F, 0.0F, 6, 6, 5, 0.0F);
        this.snout = new ModelRenderer(this, 27, 21);
        this.snout.setRotationPoint(0.0F, 4.7F, -4.0F);
        this.snout.addBox(-2.0F, -10.0F, -7.0F, 4, 4, 3, 0.0F);
        this.tail = new ModelRenderer(this, 105, 10);
        this.tail.setRotationPoint(0.0F, -7.9F, 0.5F);
        this.tail.addBox(-1.0F, -1.0F, 0.0F, 1, 1, 7, 0.0F);
        this.setRotateAngle(tail, -1.4311699866353502F, 0.0F, 0.0F);
        this.shape57 = new ModelRenderer(this, 67, 75);
        this.shape57.setRotationPoint(-2.5F, -9.9F, -3.5F);
        this.shape57.addBox(0.0F, 0.0F, 0.0F, 5, 12, 7, 0.0F);
        this.tail_wool = new ModelRenderer(this, 104, 0);
        this.tail_wool.setRotationPoint(-1.5F, -1.5F, 0.0F);
        this.tail_wool.addBox(0.0F, 0.0F, 0.0F, 2, 2, 7, 0.0F);
        this.ear_short_left = new ModelRenderer(this, 0, 0);
        this.ear_short_left.setRotationPoint(-1.4F, -5.9F, -8.5F);
        this.ear_short_left.addBox(-3.0F, 0.0F, 4.0F, 3, 2, 1, 0.0F);
        this.setRotateAngle(ear_short_left, 0.0F, 0.0F, 0.17453292519943295F);
        this.leg_front_left = new ModelRenderer(this, 44, 29);
        this.leg_front_left.mirror = true;
        this.leg_front_left.setRotationPoint(-3.0F, 0.5F, -16.5F);
        this.leg_front_left.addBox(-1.5F, 0.0F, -2.0F, 3, 10, 3, 0.0F);
        this.body.addChild(this.leg_front_right);
        this.leg_back_left.addChild(this.shape53_3);
        this.body.addChild(this.leg_back_right);
        this.body.addChild(this.wool_body);
        this.head.addChild(this.ear_short_right);
        this.leg_front_right.addChild(this.shape53);
        this.leg_front_left.addChild(this.shape53_1);
        this.body.addChild(this.leg_back_left);
        this.leg_back_right.addChild(this.shape53_2);
        this.neck.addChild(this.head);
        this.head.addChild(this.shape58);
        this.head.addChild(this.snout);
        this.body.addChild(this.tail);
        this.neck.addChild(this.shape57);
        this.tail.addChild(this.tail_wool);
        this.head.addChild(this.ear_short_left);
        this.body.addChild(this.leg_front_left);
    }
    
    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) 
    { 
    	EntityImprovedSheep entitycow = (EntityImprovedSheep)entity;
        boolean flag = !(entitycow.isChild());
        boolean flag2 = entitycow.getSheared();
        float size2 = f5 * 1.0f;
        
        if (flag2)
    	{
        	this.shape57.isHidden = true;
        	this.shape58.isHidden = true;
        	this.wool_body.isHidden = true;	
    	}
        else
        {
        	this.shape57.isHidden = false;
        	this.shape58.isHidden = false;
        	this.wool_body.isHidden = false;
        }
        

        this.body.render(size2);
        
        
        if (!flag)
        {
        	float size = 1.3F;
        	float growthSize = f5;
        	if (Config.smoothGrowth == true) size += 0.3F * growthSize;
        	growthSize = (1.0F - growthSize);
            GlStateManager.pushMatrix();
            GlStateManager.scale(size, size, size);
            GlStateManager.translate(0.0F, -0.1F * growthSize, 0.15F * growthSize);
        }
        
        this.neck.render(size2);
      
        if (!flag)
        {
            GlStateManager.popMatrix();
        }
        
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
    @Override
	public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn)
    {
    	this.neck.rotateAngleX = (headPitch * 0.016F) + 0.35F;
    	this.neck.rotateAngleY = netHeadYaw * 0.016F;
    //	this.shape57.rotateAngleX = (headPitch * 0.01F) + 0.30f;
    //	this.shape57.rotateAngleY = netHeadYaw * 0.01F;
    	this.head.rotateAngleX = (headPitch * 0.001353292F) - 0.385F;
    	this.head.rotateAngleY = netHeadYaw * 0.001353292F;
    	
        //this.shape58.rotateAngleX = (headPitch * 0.007453292F) - 0.15f;
        //this.shape58.rotateAngleY = netHeadYaw * 0.007453292F;
        //this.body.rotateAngleX = ((float)Math.PI / 2F);
        this.leg_front_right.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        this.leg_back_right.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
        this.leg_front_left.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
        this.leg_back_left.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        this.tail.rotateAngleZ = MathHelper.cos(limbSwing * 0.6662F) * 1.1F * limbSwingAmount;
    }
    
}
