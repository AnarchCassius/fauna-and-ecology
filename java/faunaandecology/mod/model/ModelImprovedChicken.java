package faunaandecology.mod.model;

import faunaandecology.mod.entity.passive.EntityJungleFowl;
import faunaandecology.mod.util.Config;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

/**
 * ModelCow - Either Mojang or a mod author
 * Created using Tabula 6.0.0
 */
public class ModelImprovedChicken extends ModelJungleFowl {

    public ModelRenderer wing_left;
    public ModelRenderer leg_right;
    public ModelRenderer wing_right;
    public ModelRenderer neck;
    public ModelRenderer body;
    public ModelRenderer leg_left;
    public ModelRenderer tail_small;
    public ModelRenderer shape15;
    public ModelRenderer foot_right;
    public ModelRenderer head;
    public ModelRenderer beak;
    public ModelRenderer comb;
    public ModelRenderer wattle_male;
    public ModelRenderer foot_left;

    public ModelImprovedChicken() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.beak = new ModelRenderer(this, 19, 3);
        this.beak.setRotationPoint(0.5F, 3.0F, -3.0F);
        this.beak.addBox(-1.0F, -2.0F, -3.0F, 1, 2, 2, 0.0F);
        this.head = new ModelRenderer(this, 2, 1);
        this.head.setRotationPoint(0.0F, -6.9F, 1.6F);
        this.head.addBox(-1.5F, 0.0F, -4.0F, 3, 3, 3, 0.0F);
        this.setRotateAngle(head, -0.08726646259971647F, 0.0F, 0.0F);
        this.foot_left = new ModelRenderer(this, 31, 1);
        this.foot_left.setRotationPoint(-1.0F, 4.0F, -3.0F);
        this.foot_left.addBox(0.0F, 0.0F, 0.0F, 3, 0, 3, 0.0F);
        this.body = new ModelRenderer(this, 1, 10);
        this.body.setRotationPoint(0.5F, 17.0F, 0.0F);
        this.body.addBox(-3.0F, -4.0F, -3.0F, 5, 8, 6, 0.0F);
        this.setRotateAngle(body, 1.5707963267948966F, 0.0F, 0.0F);
        this.wing_right = new ModelRenderer(this, 24, 22);
        this.wing_right.setRotationPoint(3.5F, 14.0F, 0.0F);
        this.wing_right.addBox(-1.0F, 0.0F, -3.0F, 1, 4, 6, 0.0F);
        this.leg_right = new ModelRenderer(this, 39, 3);
        this.leg_right.setRotationPoint(1.0F, 20.0F, 0.5F);
        this.leg_right.addBox(-1.0F, 0.0F, 0.0F, 3, 4, 0, 0.0F);
        this.wattle_male = new ModelRenderer(this, 20, 8);
        this.wattle_male.setRotationPoint(-0.5F, 3.0F, -4.5F);
        this.wattle_male.addBox(0.0F, 0.0F, 0.0F, 1, 2, 2, 0.0F);
        this.shape15 = new ModelRenderer(this, 47, 22);
        this.shape15.setRotationPoint(-1.0F, 14.5F, 0.5F);
        this.shape15.addBox(0.0F, 0.0F, 0.0F, 2, 4, 6, 0.0F);
        this.setRotateAngle(shape15, 0.5235987755982988F, 0.0F, 0.0F);
        this.neck = new ModelRenderer(this, 36, 14);
        this.neck.setRotationPoint(0.0F, 17.0F, -3.5F);
        this.neck.addBox(-1.0F, -6.0F, -2.0F, 2, 7, 3, 0.0F);
        this.setRotateAngle(neck, 0.08726646259971647F, 0.0F, 0.0F);
        this.foot_right = new ModelRenderer(this, 31, 1);
        this.foot_right.setRotationPoint(-1.0F, 4.0F, -3.0F);
        this.foot_right.addBox(0.0F, 0.0F, 0.0F, 3, 0, 3, 0.0F);
        this.comb = new ModelRenderer(this, 28, 5);
        this.comb.mirror = true;
        this.comb.setRotationPoint(-1.0F, -2.5F, -4.5F);
        this.comb.addBox(0.0F, 0.0F, 0.0F, 1, 3, 4, 0.0F);
        this.wing_left = new ModelRenderer(this, 24, 22);
        this.wing_left.mirror = true;
        this.wing_left.setRotationPoint(-3.5F, 14.0F, 0.0F);
        this.wing_left.addBox(0.0F, 0.0F, -3.0F, 1, 4, 6, 0.0F);
        this.tail_small = new ModelRenderer(this, 44, 5);
        this.tail_small.setRotationPoint(-0.5F, 14.0F, 1.0F);
        this.tail_small.addBox(0.0F, 0.0F, 0.0F, 1, 2, 9, 0.0F);
        this.setRotateAngle(tail_small, 0.6108652381980153F, 0.0F, 0.0F);
        this.leg_left = new ModelRenderer(this, 39, 3);
        this.leg_left.mirror = true;
        this.leg_left.setRotationPoint(-2.0F, 20.0F, 0.5F);
        this.leg_left.addBox(-1.0F, 0.0F, 0.0F, 3, 4, 0, 0.0F);
        this.head.addChild(this.beak);
        this.neck.addChild(this.head);
        this.leg_left.addChild(this.foot_left);
        this.head.addChild(this.wattle_male);
        this.leg_right.addChild(this.foot_right);
        this.head.addChild(this.comb);
    }

}
