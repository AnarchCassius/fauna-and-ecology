package faunaandecology.mod.model;

import faunaandecology.mod.entity.IEntityAdvancedHorse;
import faunaandecology.mod.entity.passive.ImprovedHorseArmorType;
import faunaandecology.mod.util.Config;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.passive.AbstractChestHorse;
import net.minecraft.entity.passive.AbstractHorse;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelImprovedLlama extends ModelBase
{

    public ModelRenderer body;
    public ModelRenderer neck;
    public ModelRenderer saddle;
    public ModelRenderer chest_right;
    public ModelRenderer chest_left;
    public ModelRenderer leg_front_right;
    public ModelRenderer leg_front_left;
    public ModelRenderer tail_top;
    public ModelRenderer leg_back_right;
    public ModelRenderer leg_back_left;
    public ModelRenderer wool_body;
    public ModelRenderer shape48;
    public ModelRenderer shape49;
    public ModelRenderer shape48_1;
    public ModelRenderer shape48_2;
    public ModelRenderer head;
    public ModelRenderer wool_neck;
    public ModelRenderer snout;
    public ModelRenderer ear_short_right;
    public ModelRenderer ear_short_left;

    public ModelImprovedLlama() {
        this.textureWidth = 128;
        this.textureHeight = 128;
        this.neck = new ModelRenderer(this, 1, 14);
        this.neck.setRotationPoint(0.0F, 4.5F, -9.5F);
        this.neck.addBox(-2.05F, -10.0F, -3.0F, 4, 15, 5, 0.0F);
        this.setRotateAngle(neck, 0.08726646259971647F, 0.0F, 0.0F);
        this.chest_right = new ModelRenderer(this, 101, 50);
        this.chest_right.setRotationPoint(4.5F, 3.1F, 9.0F);
        this.chest_right.addBox(0.0F, 0.0F, 0.0F, 8, 8, 3, 0.0F);
        this.setRotateAngle(chest_right, 0.0F, 1.5707963267948966F, 0.0F);
        this.shape48 = new ModelRenderer(this, 45, 75);
        this.shape48.mirror = true;
        this.shape48.setRotationPoint(-2.0F, 2.5F, -2.5F);
        this.shape48.addBox(0.0F, 0.0F, 0.0F, 4, 6, 4, 0.0F);
        this.shape48_1 = new ModelRenderer(this, 45, 75);
        this.shape48_1.mirror = true;
        this.shape48_1.setRotationPoint(-3.0F, 2.5F, -2.5F);
        this.shape48_1.addBox(0.0F, 0.0F, 0.0F, 4, 6, 4, 0.0F);
        this.saddle = new ModelRenderer(this, 74, 30);
        this.saddle.setRotationPoint(-5.0F, 3.0F, -5.0F);
        this.saddle.addBox(0.0F, 0.0F, 0.0F, 10, 8, 9, 0.02F);
        this.wool_neck = new ModelRenderer(this, 65, 70);
        this.wool_neck.setRotationPoint(-2.5F, -6.0F, -3.5F);
        this.wool_neck.addBox(0.0F, 0.0F, 0.0F, 5, 12, 5, 0.0F);
        this.wool_body = new ModelRenderer(this, 0, 65);
        this.wool_body.mirror = true;
        this.wool_body.setRotationPoint(-5.5F, -8.5F, -19.5F);
        this.wool_body.addBox(0.0F, 0.0F, 0.0F, 11, 11, 21, 0.0F);
        this.body = new ModelRenderer(this, 0, 34);
        this.body.setRotationPoint(0.0F, 11.0F, 9.0F);
        this.body.addBox(-5.0F, -8.0F, -19.0F, 10, 10, 20, 0.0F);
        this.ear_short_right = new ModelRenderer(this, 0, 0);
        this.ear_short_right.setRotationPoint(0.4F, 3.7F, -8.5F);
        this.ear_short_right.addBox(0.45F, -12.0F, 4.0F, 2, 3, 1, 0.0F);
        this.shape49 = new ModelRenderer(this, 45, 75);
        this.shape49.setRotationPoint(-2.0F, 2.5F, -2.5F);
        this.shape49.addBox(0.0F, 0.0F, 0.0F, 4, 6, 4, 0.0F);
        this.shape48_2 = new ModelRenderer(this, 45, 75);
        this.shape48_2.setRotationPoint(-2.0F, 2.5F, -2.5F);
        this.shape48_2.addBox(0.0F, 0.0F, 0.0F, 4, 6, 4, 0.0F);
        this.leg_front_left = new ModelRenderer(this, 44, 29);
        this.leg_front_left.mirror = true;
        this.leg_front_left.setRotationPoint(-3.0F, -1.0F, -16.5F);
        this.leg_front_left.addBox(-1.5F, 0.0F, -2.0F, 3, 14, 3, 0.0F);
        this.chest_left = new ModelRenderer(this, 77, 50);
        this.chest_left.setRotationPoint(-7.5F, 3.1F, 9.0F);
        this.chest_left.addBox(0.0F, 0.0F, 0.0F, 8, 8, 3, 0.0F);
        this.setRotateAngle(chest_left, 0.0F, 1.5707963267948966F, 0.0F);
        this.snout = new ModelRenderer(this, 27, 21);
        this.snout.setRotationPoint(0.0F, 4.5F, -4.0F);
        this.snout.addBox(-2.0F, -10.0F, -7.0F, 4, 4, 3, 0.0F);
        this.tail_top = new ModelRenderer(this, 104, 0);
        this.tail_top.setRotationPoint(0.0F, -7.8F, 1.0F);
        this.tail_top.addBox(-1.0F, -1.0F, 0.0F, 2, 2, 6, 0.0F);
        this.setRotateAngle(tail_top, -1.4311699866353502F, 0.0F, 0.0F);
        this.leg_back_left = new ModelRenderer(this, 59, 29);
        this.leg_back_left.mirror = true;
        this.leg_back_left.setRotationPoint(-3.0F, -1.0F, -1.0F);
        this.leg_back_left.addBox(-1.5F, 0.0F, -2.0F, 3, 14, 3, 0.0F);
        this.ear_short_left = new ModelRenderer(this, 0, 0);
        this.ear_short_left.mirror = true;
        this.ear_short_left.setRotationPoint(-0.4F, 3.7F, -8.5F);
        this.ear_short_left.addBox(-2.45F, -12.0F, 4.0F, 2, 3, 1, 0.0F);
        this.leg_front_right = new ModelRenderer(this, 44, 29);
        this.leg_front_right.setRotationPoint(3.0F, -1.0F, -16.5F);
        this.leg_front_right.addBox(-1.5F, 0.0F, -2.0F, 3, 14, 3, 0.0F);
        this.leg_back_right = new ModelRenderer(this, 59, 29);
        this.leg_back_right.setRotationPoint(4.0F, -1.0F, -1.0F);
        this.leg_back_right.addBox(-2.5F, 0.0F, -2.0F, 3, 14, 3, 0.0F);
        this.head = new ModelRenderer(this, 1, 1);
        this.head.setRotationPoint(0.0F, -5.0F, 4.5F);
        this.head.addBox(-2.5F, -6.0F, -8.0F, 5, 5, 5, 0.0F);
        this.leg_front_right.addChild(this.shape48);
        this.leg_back_right.addChild(this.shape48_1);
        this.neck.addChild(this.wool_neck);
        this.body.addChild(this.wool_body);
        this.head.addChild(this.ear_short_right);
        this.leg_front_left.addChild(this.shape49);
        this.leg_back_left.addChild(this.shape48_2);
        this.body.addChild(this.leg_front_left);
        this.head.addChild(this.snout);
        this.body.addChild(this.tail_top);
        this.body.addChild(this.leg_back_left);
        this.head.addChild(this.ear_short_left);
        this.body.addChild(this.leg_front_right);
        this.body.addChild(this.leg_back_right);
        this.neck.addChild(this.head);
    }
    /**
     * Sets the models various rotation angles then renders the model.
     */
    @Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
    {
        AbstractHorse abstracthorse = (AbstractHorse)entityIn;
    	IEntityAdvancedHorse advancedHorse = (IEntityAdvancedHorse)entityIn;
        float f = abstracthorse.getGrassEatingAmount(0.0F);
        boolean flag = !abstracthorse.isChild();
        boolean flag1 = flag && abstracthorse.isHorseSaddled();

        boolean flag2 = abstracthorse instanceof AbstractChestHorse;
        boolean flag3 = flag && flag2 && ((AbstractChestHorse)abstracthorse).hasChest();

        boolean armored = (advancedHorse.getHorseImprovedArmorType() != ImprovedHorseArmorType.NONE);
        float f1 = abstracthorse.getHorseSize();
        boolean flag4 = abstracthorse.isBeingRidden();
        
        this.neck.render(scale);
        //this.wool_neck.render(scale);
        
        if (advancedHorse.getSheared() == true) this.wool_neck.isHidden = true;
        else this.wool_neck.isHidden = false;

        if (advancedHorse.getSheared() == true) this.wool_body.isHidden = true;
        else this.wool_body.isHidden = false;

        if (advancedHorse.getSheared() == true) this.shape48_1.isHidden = true;
        else this.shape48_1.isHidden = false;

        if (advancedHorse.getSheared() == true) this.shape48_2.isHidden = true;
        else this.shape48_2.isHidden = false;

        if (advancedHorse.getSheared() == true) this.shape48.isHidden = true;
        else this.shape48.isHidden = false;

        if (advancedHorse.getSheared() == true) this.shape49.isHidden = true;
        else this.shape49.isHidden = false;
        
        
        if (!flag)
        {
        	float size = 0.7F;
        	if (Config.smoothGrowth == true) size += 0.3F * advancedHorse.getGrowthSize();
            GlStateManager.pushMatrix();
            GlStateManager.scale(size, 1.0F, 0.5F + size * 0.5F);
            GlStateManager.translate(0.0F, 0.0F, -0.30F * (1.0F - size));
        }

        this.body.render(scale);
        //this.wool_body.render(scale);

        
        if (flag1)
        {

        	this.saddle.render(scale);

        }

        if (flag3)
        {
            this.chest_left.render(scale);
            this.chest_right.render(scale);
        }

        if (!flag)
        {
            GlStateManager.popMatrix();
        }        

    }

    /**
     * Fixes and offsets a rotation in the ModelHorse class.
     */
    private float updateHorseRotation(float p_110683_1_, float p_110683_2_, float p_110683_3_)
    {
        float f;

        for (f = p_110683_2_ - p_110683_1_; f < -180.0F; f += 360.0F)
        {
            ;
        }

        while (f >= 180.0F)
        {
            f -= 360.0F;
        }

        return p_110683_1_ + p_110683_3_ * f;
    }

    /**
     * Used for easily adding entity-dependent animations. The second and third float params here are the same second
     * and third as in the setRotationAngles method.
     */
    @Override
	public void setLivingAnimations(EntityLivingBase entitylivingbaseIn, float p_78086_2_, float p_78086_3_, float partialTickTime)
    {
        super.setLivingAnimations(entitylivingbaseIn, p_78086_2_, p_78086_3_, partialTickTime);
        float f = this.updateHorseRotation(entitylivingbaseIn.prevRenderYawOffset, entitylivingbaseIn.renderYawOffset, partialTickTime);
        float f1 = this.updateHorseRotation(entitylivingbaseIn.prevRotationYawHead, entitylivingbaseIn.rotationYawHead, partialTickTime);
        float f2 = entitylivingbaseIn.prevRotationPitch + (entitylivingbaseIn.rotationPitch - entitylivingbaseIn.prevRotationPitch) * partialTickTime;
        float f3 = f1 - f;
        float f4 = f2 * 0.017453292F;

        if (f3 > 20.0F)
        {
            f3 = 20.0F;
        }

        if (f3 < -20.0F)
        {
            f3 = -20.0F;
        }

        if (p_78086_3_ > 0.2F)
        {
            f4 += MathHelper.cos(p_78086_2_ * 0.4F) * 0.15F * p_78086_3_;
        }

        AbstractHorse entityhorse = (AbstractHorse)entitylivingbaseIn;
        float f5 = entityhorse.getGrassEatingAmount(partialTickTime);
        float f6 = entityhorse.getRearingAmount(partialTickTime);
        float f7 = 1.0F - f6;
        float f8 = entityhorse.getMouthOpennessAngle(partialTickTime);
        boolean flag = entityhorse.tailCounter != 0;
        boolean flag1 = entityhorse.isHorseSaddled();
        boolean flag2 = entityhorse.isBeingRidden();
        float f9 = entitylivingbaseIn.ticksExisted + partialTickTime;
        float f10 = MathHelper.cos(p_78086_2_ * 0.6662F + (float)Math.PI);
        float f11 = f10 * 0.8F * p_78086_3_;
        
        this.neck.rotationPointY = 4.0F;
        this.neck.rotationPointZ = -10.0F;
        
        this.body.rotateAngleX = 0.0F;
        this.neck.rotateAngleX = 0.0235988F + f4; //5235988F
        this.neck.rotateAngleY = f3 * 0.017453292F;
        this.neck.rotateAngleX = f6 * (0.2617994F + f4) + f5 * 2.18166F + (1.0F - Math.max(f6, f5)) * this.neck.rotateAngleX;
        this.neck.rotateAngleY = f6 * f3 * 0.017453292F + (1.0F - Math.max(f6, f5)) * this.neck.rotateAngleY;
        this.neck.rotationPointY = f6 * -6.0F + f5 * 11.0F + (1.0F - Math.max(f6, f5)) * this.neck.rotationPointY;
        this.neck.rotationPointZ = f6 * -1.0F + f5 * -10.0F + (1.0F - Math.max(f6, f5)) * this.neck.rotationPointZ;
        this.body.rotateAngleX = f6 * -45.0F * 0.017453292F + f7 * this.body.rotateAngleX;
        
//        
//        this.wool_neck.rotationPointY = 4.0F;
//        this.wool_neck.rotationPointZ = -10.0F;
//        
//        this.wool_body.rotateAngleX = 0.0F;
//        this.wool_neck.rotateAngleX = 0.0235988F + f4; //5235988F
//        this.wool_neck.rotateAngleY = f3 * 0.017453292F;
//        this.wool_neck.rotateAngleX = f6 * (0.2617994F + f4) + f5 * 2.18166F + (1.0F - Math.max(f6, f5)) * this.neck.rotateAngleX;
//        this.wool_neck.rotateAngleY = f6 * f3 * 0.017453292F + (1.0F - Math.max(f6, f5)) * this.neck.rotateAngleY;
//        this.wool_neck.rotationPointY = f6 * -6.0F + f5 * 11.0F + (1.0F - Math.max(f6, f5)) * this.neck.rotationPointY;
//        this.wool_neck.rotationPointZ = f6 * -1.0F + f5 * -10.0F + (1.0F - Math.max(f6, f5)) * this.neck.rotationPointZ;
//        this.wool_body.rotateAngleX = f6 * -45.0F * 0.017453292F + f7 * this.body.rotateAngleX;
//        
        
        this.saddle.rotateAngleX = this.body.rotateAngleX;
        
        this.chest_left.rotateAngleX = f11 / 5.0F;
        this.chest_right.rotateAngleX = -f11 / 5.0F;
        float f12 = ((float)Math.PI / 2F);
        float f13 = ((float)Math.PI * 3F / 2F);
        float f14 = -1.0471976F;
        float f15 = 0.2617994F * f6;
        float f16 = MathHelper.cos(f9 * 0.6F + (float)Math.PI);
        float f17 = (-1.0471976F + f16) * f6 + f11 * f7;
        float f18 = (-1.0471976F + -f16) * f6 + -f11 * f7;
        this.leg_back_left.rotateAngleX = f15 + -f10 * 0.5F * p_78086_3_ * f7;
        this.leg_back_right.rotateAngleX = f15 + f10 * 0.5F * p_78086_3_ * f7;
        this.leg_front_left.rotateAngleX = f17;
        this.leg_front_right.rotateAngleX = f18;


        f12 = -1.3089F + p_78086_3_ * 1.5F;

        if (f12 > 0.0F)
        {
            f12 = 0.0F;
        }

        if (flag)
        {
            this.tail_top.rotateAngleY = MathHelper.cos(f9 * 0.7F);
            //this.tail_flowing.rotateAngleY = MathHelper.cos(f9 * 0.7F);
            f12 = 0.0F;
        }
        else
        {
            this.tail_top.rotateAngleY = 0.0F;
            //this.tail_flowing.rotateAngleY = 0.0F;
        }

        //this.tail_tuft.rotateAngleY = this.tail_top.rotateAngleY;
        this.tail_top.rotateAngleX = f12;
        //this.tail_flowing.rotateAngleX = f12;
    }
    

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}