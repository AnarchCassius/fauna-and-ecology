package faunaandecology.mod.model;

import faunaandecology.mod.entity.IEntityAdvancedHorse;
import faunaandecology.mod.entity.passive.ImprovedHorseArmorType;
import faunaandecology.mod.util.Config;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.passive.AbstractChestHorse;
import net.minecraft.entity.passive.AbstractHorse;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelImprovedHorse extends ModelBase
{

    public ModelRenderer rein_right;
    public ModelRenderer rein_left;
    public ModelRenderer body;
    public ModelRenderer neck;
    public ModelRenderer leg_back_right;
    public ModelRenderer leg_back_left;
    public ModelRenderer leg_front_right;
    public ModelRenderer leg_front_left;
    public ModelRenderer tail_top;
    public ModelRenderer chest_left;
    public ModelRenderer chest_right;
    public ModelRenderer saddle;
    public ModelRenderer tail_tuft;
    public ModelRenderer tail_flowing;
    public ModelRenderer mane_short;
    public ModelRenderer mane_mid;
    public ModelRenderer head;
    public ModelRenderer snout;
    public ModelRenderer ear_short_right;
    public ModelRenderer ear_short_left;
    public ModelRenderer ear_long_right;
    public ModelRenderer ear_long_left;
    public ModelRenderer rein_head;

    public ModelImprovedHorse() {
        this.textureWidth = 128;
        this.textureHeight = 128;
        this.leg_front_left = new ModelRenderer(this, 44, 29);
        this.leg_front_left.setRotationPoint(-3.4F, -1.0F, -16.5F);
        this.leg_front_left.addBox(-1.5F, 0.0F, -2.0F, 3, 14, 4, 0.0F);
        this.neck = new ModelRenderer(this, 1, 14);
        this.neck.setRotationPoint(-0.5F, 7.9F, -6.8F);
        this.neck.addBox(-2.05F, -13.0F, -3.0F, 4, 14, 7, 0.0F);
        this.setRotateAngle(neck, 0.6108652381980153F, 0.0F, 0.0F);
        this.leg_back_left = new ModelRenderer(this, 59, 29);
        this.leg_back_left.setRotationPoint(-3.4F, 10.0F, 11.5F);
        this.leg_back_left.addBox(-1.5F, 0.0F, -2.0F, 3, 14, 4, 0.0F);
        this.leg_back_right = new ModelRenderer(this, 59, 29);
        this.leg_back_right.mirror = true;
        this.leg_back_right.setRotationPoint(3.4F, 10.0F, 11.5F);
        this.leg_back_right.addBox(-2.5F, 0.0F, -2.0F, 3, 14, 4, 0.0F);
        this.rein_head = new ModelRenderer(this, 88, 15);
        this.rein_head.setRotationPoint(-0.5F, 3.8F, -1.0F);
        this.rein_head.addBox(-2.5F, -10.0F, -7.0F, 6, 7, 7, 0.0F);
        this.chest_left = new ModelRenderer(this, 0, 35);
        this.chest_left.setRotationPoint(-7.5F, -7.9F, 1.0F);
        this.chest_left.addBox(-3.0F, 0.0F, 0.0F, 8, 8, 3, 0.0F);
        this.setRotateAngle(chest_left, 0.0F, 1.5707963267948966F, 0.0F);
        this.mane_mid = new ModelRenderer(this, 70, 0);
        this.mane_mid.setRotationPoint(-1.0F, -11.0F, 3.5F);
        this.mane_mid.addBox(0.0F, -3.0F, 0.0F, 2, 15, 3, 0.0F);
        this.tail_top = new ModelRenderer(this, 104, 0);
        this.tail_top.setRotationPoint(0.0F, -7.8F, 4.5F);
        this.tail_top.addBox(-1.0F, -1.0F, 0.0F, 1, 1, 10, 0.0F);
        this.setRotateAngle(tail_top, -1.4311699866353502F, 0.0F, 0.0F);
        this.mane_short = new ModelRenderer(this, 60, 0);
        this.mane_short.setRotationPoint(0.0F, 0.5F, -1.5F);
        this.mane_short.addBox(-1.0F, -14.5F, 5.0F, 2, 15, 2, 0.0F);
        this.chest_right = new ModelRenderer(this, 0, 47);
        this.chest_right.setRotationPoint(3.5F, -7.9F, 1.0F);
        this.chest_right.addBox(-3.0F, 0.0F, 0.0F, 8, 8, 3, 0.0F);
        this.setRotateAngle(chest_right, 0.0F, 1.5707963267948966F, 0.0F);
        this.ear_long_right = new ModelRenderer(this, 22, 0);
        this.ear_long_right.setRotationPoint(0.0F, 4.5F, -5.5F);
        this.ear_long_right.addBox(-2.0F, -16.0F, 4.0F, 2, 6, 1, 0.0F);
        this.setRotateAngle(ear_long_right, 0.0F, 0.0F, 0.2617993877991494F);
        this.ear_short_left = new ModelRenderer(this, 0, 0);
        this.ear_short_left.mirror = true;
        this.ear_short_left.setRotationPoint(0.0F, 4.0F, -5.5F);
        this.ear_short_left.addBox(-2.45F, -12.0F, 4.0F, 2, 3, 1, 0.0F);
        this.tail_tuft = new ModelRenderer(this, 116, 0);
        this.tail_tuft.setRotationPoint(-1.5F, -1.5F, 9.5F);
        this.tail_tuft.addBox(0.0F, 0.0F, 0.0F, 2, 2, 5, 0.0F);
        this.tail_flowing = new ModelRenderer(this, 68, 49);
        this.tail_flowing.setRotationPoint(-0.5F, 0.0F, -0.05F);
        this.tail_flowing.addBox(-1.5F, -3.0F, 0.0F, 3, 4, 15, 0.0F);
        this.setRotateAngle(tail_flowing, -0.02617993877991494F, 0.0F, 0.0F);
        this.ear_short_right = new ModelRenderer(this, 0, 0);
        this.ear_short_right.setRotationPoint(0.0F, 4.0F, -5.5F);
        this.ear_short_right.addBox(0.45F, -12.0F, 4.0F, 2, 3, 1, 0.0F);
        this.rein_right = new ModelRenderer(this, 44, 10);
        this.rein_right.setRotationPoint(0.1F, 5.5F, -10.0F);
        this.rein_right.addBox(2.6F, -6.0F, -6.0F, 0, 3, 16, 0.0F);
        this.head = new ModelRenderer(this, 1, 1);
        this.head.setRotationPoint(0.0F, -7.1F, 3.0F);
        this.head.addBox(-2.5F, -6.0F, -8.0F, 5, 6, 8, 0.0F);
        this.setRotateAngle(head, -0.17453292519943295F, 0.0F, 0.0F);
        this.snout = new ModelRenderer(this, 27, 21);
        this.snout.setRotationPoint(0.0F, 4.3F, -4.0F);
        this.snout.addBox(-2.0F, -10.0F, -7.0F, 4, 5, 3, 0.0F);
        this.ear_long_left = new ModelRenderer(this, 22, 0);
        this.ear_long_left.mirror = true;
        this.ear_long_left.setRotationPoint(0.0F, 4.5F, -5.5F);
        this.ear_long_left.addBox(0.0F, -16.0F, 4.0F, 2, 6, 1, 0.0F);
        this.setRotateAngle(ear_long_left, 0.0F, 0.0F, -0.2617993877991494F);
        this.rein_left = new ModelRenderer(this, 44, 5);
        this.rein_left.setRotationPoint(-1.1F, 5.5F, -10.0F);
        this.rein_left.addBox(-2.6F, -6.0F, -6.0F, 0, 3, 16, 0.0F);
        this.body = new ModelRenderer(this, 0, 34);
        this.body.setRotationPoint(0.0F, 11.0F, 9.0F);
        this.body.addBox(-5.0F, -8.0F, -19.0F, 9, 10, 24, 0.0F);
        this.saddle = new ModelRenderer(this, 74, 30);
        this.saddle.setRotationPoint(-5.5F, -8.1F, -11.0F);
        this.saddle.addBox(0.0F, 0.0F, 0.0F, 10, 8, 9, 0.0F);
        this.leg_front_right = new ModelRenderer(this, 44, 29);
        this.leg_front_right.mirror = true;
        this.leg_front_right.setRotationPoint(2.4F, -1.0F, -16.5F);
        this.leg_front_right.addBox(-1.5F, 0.0F, -2.0F, 3, 14, 4, 0.0F);
        this.body.addChild(this.leg_front_left);
        this.head.addChild(this.rein_head);
        this.body.addChild(this.chest_left);
        this.neck.addChild(this.mane_mid);
        this.body.addChild(this.tail_top);
        this.neck.addChild(this.mane_short);
        this.body.addChild(this.chest_right);
        this.head.addChild(this.ear_long_right);
        this.head.addChild(this.ear_short_left);
        this.tail_top.addChild(this.tail_tuft);
        this.tail_top.addChild(this.tail_flowing);
        this.head.addChild(this.ear_short_right);
        this.neck.addChild(this.head);
        this.head.addChild(this.snout);
        this.head.addChild(this.ear_long_left);
        this.body.addChild(this.saddle);
        this.body.addChild(this.leg_front_right);
    }

    /**
     * Sets the models various rotation angles then renders the model.
     */
    @Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
    {
        AbstractHorse abstracthorse = (AbstractHorse)entityIn;
    	IEntityAdvancedHorse advancedHorse = (IEntityAdvancedHorse)entityIn;
        float f = abstracthorse.getGrassEatingAmount(0.0F);
        boolean flag = !abstracthorse.isChild();
        boolean flag1 = flag && abstracthorse.isHorseSaddled();

        boolean flag2 = abstracthorse instanceof AbstractChestHorse;
        boolean flag3 = flag && flag2 && ((AbstractChestHorse)abstracthorse).hasChest();

        boolean armored = (advancedHorse.getHorseImprovedArmorType() != ImprovedHorseArmorType.NONE);
        float f1 = abstracthorse.getHorseSize();
        boolean flag4 = abstracthorse.isBeingRidden();
        
        if (advancedHorse.getSheared() == true) this.tail_flowing.isHidden = true;
        else this.tail_flowing.isHidden = false;

        if (flag1) 	this.saddle.isHidden = false;
        else this.saddle.isHidden = true;
        
        if (flag3) this.chest_left.isHidden = false;
        else this.chest_left.isHidden = true;
        if (flag3) this.chest_right.isHidden = false;
        else this.chest_right.isHidden = true;

        if (flag1) this.rein_head.isHidden = false;
        else this.rein_head.isHidden = true;

        this.body.render(scale);
        this.leg_back_right.render(scale);
        this.leg_back_left.render(scale); 
        if (!flag)
        {
        	float size = 1.2F;
        	float growthSize = scale;
        	if (Config.smoothGrowth == true) size -= 0.2F * growthSize;
        	growthSize = (1.0F - growthSize);
            GlStateManager.pushMatrix();
            GlStateManager.scale(size, size, size);
            //lr, up, fb
            GlStateManager.translate(0.0F, 0.0F, 0.1F * growthSize);
        }
        this.neck.render(scale);
        if (!flag)
        {
            GlStateManager.popMatrix();
        }        

    }

    /**
     * Fixes and offsets a rotation in the ModelHorse class.
     */
    private float updateHorseRotation(float p_110683_1_, float p_110683_2_, float p_110683_3_)
    {
        float f;

        for (f = p_110683_2_ - p_110683_1_; f < -180.0F; f += 360.0F)
        {
            ;
        }

        while (f >= 180.0F)
        {
            f -= 360.0F;
        }

        return p_110683_1_ + p_110683_3_ * f;
    }

    /**
     * Used for easily adding entity-dependent animations. The second and third float params here are the same second
     * and third as in the setRotationAngles method.
     */
    @Override
	public void setLivingAnimations(EntityLivingBase entitylivingbaseIn, float p_78086_2_, float p_78086_3_, float partialTickTime)
    {
        super.setLivingAnimations(entitylivingbaseIn, p_78086_2_, p_78086_3_, partialTickTime);
        float f = this.updateHorseRotation(entitylivingbaseIn.prevRenderYawOffset, entitylivingbaseIn.renderYawOffset, partialTickTime);
        float f1 = this.updateHorseRotation(entitylivingbaseIn.prevRotationYawHead, entitylivingbaseIn.rotationYawHead, partialTickTime);
        float f2 = entitylivingbaseIn.prevRotationPitch + (entitylivingbaseIn.rotationPitch - entitylivingbaseIn.prevRotationPitch) * partialTickTime;
        float f3 = f1 - f;
        float f4 = f2 * 0.017453292F;

        if (f3 > 20.0F)
        {
            f3 = 20.0F;
        }

        if (f3 < -20.0F)
        {
            f3 = -20.0F;
        }

        if (p_78086_3_ > 0.2F)
        {
            f4 += MathHelper.cos(p_78086_2_ * 0.4F) * 0.15F * p_78086_3_;
        }

        AbstractHorse entityhorse = (AbstractHorse)entitylivingbaseIn;
        float f5 = entityhorse.getGrassEatingAmount(partialTickTime);
        float f6 = entityhorse.getRearingAmount(partialTickTime);
        float f7 = 1.0F - f6;
        boolean flag = entityhorse.tailCounter != 0;
        boolean flag1 = entityhorse.isHorseSaddled();
        boolean flag2 = entityhorse.isBeingRidden();
        float f9 = entitylivingbaseIn.ticksExisted + partialTickTime;
        float f10 = MathHelper.cos(p_78086_2_ * 0.6662F + (float)Math.PI);
        float f11 = f10 * 0.8F * p_78086_3_;
        
        this.neck.rotationPointY = 6.0F;
        this.neck.rotationPointZ = -7.0F;
        
        this.body.rotateAngleX = 0.0F;
        //this.neck.rotateAngleX = 0.5235988F + f4;
        this.neck.rotateAngleX = 0.49F + f4;
        this.neck.rotateAngleY = f3 * 0.017453292F;
        this.neck.rotateAngleX = f6 * (0.2617994F + f4) + f5 * 2.18166F + (1.0F - Math.max(f6, f5)) * this.neck.rotateAngleX;
        this.neck.rotateAngleY = f6 * f3 * 0.017453292F + (1.0F - Math.max(f6, f5)) * this.neck.rotateAngleY;
        this.neck.rotationPointY = f6 * -6.0F + f5 * 11.0F + (1.0F - Math.max(f6, f5)) * this.neck.rotationPointY;
        this.neck.rotationPointZ = f6 * -1.0F + f5 * -10.0F + (1.0F - Math.max(f6, f5)) * this.neck.rotationPointZ;
        this.body.rotateAngleX = f6 * -45.0F * 0.017453292F + f7 * this.body.rotateAngleX;
      
        
        this.chest_left.rotateAngleX = f11 / 5.0F;
        this.chest_right.rotateAngleX = -f11 / 5.0F;
        float f12 = ((float)Math.PI / 2F);
        float f13 = ((float)Math.PI * 3F / 2F);
        float f14 = -1.0471976F;
        float f15 = 0.2617994F * f6;
        float f16 = MathHelper.cos(f9 * 0.6F + (float)Math.PI);
        float f17 = (-1.0471976F + f16) * f6 + f11 * f7;
        float f18 = (-1.0471976F + -f16) * f6 + -f11 * f7;
        this.leg_back_left.rotateAngleX = -f10 * 0.5F * p_78086_3_ * f7;
        this.leg_back_right.rotateAngleX = f10 * 0.5F * p_78086_3_ * f7;
        this.leg_front_left.rotateAngleX = f17;
        this.leg_front_right.rotateAngleX = f18;

        f12 = -1.3089F + p_78086_3_ * 1.5F;

        if (f12 > 0.0F)
        {
            f12 = 0.0F;
        }

        if (flag)
        {
            this.tail_top.rotateAngleY = MathHelper.cos(f9 * 0.7F);
            //this.tail_flowing.rotateAngleY = MathHelper.cos(f9 * 0.7F);
            f12 = 0.0F;
        }
        else
        {
            this.tail_top.rotateAngleY = 0.0F;
            //this.tail_flowing.rotateAngleY = 0.0F;
        }

        //this.tail_tuft.rotateAngleY = this.tail_top.rotateAngleY;
        this.tail_top.rotateAngleX = f12;
        //this.tail_flowing.rotateAngleX = f12;
    }
    

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}