package faunaandecology.mod.model;

import faunaandecology.mod.entity.passive.EntityImprovedMooshroom;
import faunaandecology.mod.util.Config;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelImprovedMooshroom extends ModelBase {
    public ModelRenderer body;
    public ModelRenderer udder;
    public ModelRenderer right_hind_leg;
    public ModelRenderer right_front_leg;
    public ModelRenderer left_hind_leg;
    public ModelRenderer left_front_leg;
    public ModelRenderer chest_right;
    public ModelRenderer chest_left;
    public ModelRenderer tail_middle;
    public ModelRenderer neck;
    public ModelRenderer saddle;
    public ModelRenderer shape26;
    public ModelRenderer shape26_1;
    public ModelRenderer shape26_2;
    public ModelRenderer shape26_3;
    public ModelRenderer tail_tuft;
    public ModelRenderer head;
    public ModelRenderer snout;
    public ModelRenderer ear_right;
    public ModelRenderer ear_left;
    public ModelRenderer horn_male_left;
    public ModelRenderer horn_male_right;
    public ModelRenderer shape26_4;
    public ModelRenderer left_horn_female;
    public ModelRenderer right_horn_male;
    public ModelRenderer shape26_5;

    public ModelImprovedMooshroom() {
        this.textureWidth = 128;
        this.textureHeight = 128;
        this.chest_left = new ModelRenderer(this, 48, 0);
        this.chest_left.setRotationPoint(-7.0F, 2.1F, 13.0F);
        this.chest_left.addBox(0.0F, 0.0F, 0.0F, 8, 8, 3, 0.0F);
        this.setRotateAngle(chest_left, 0.0F, 1.5707963267948966F, 0.0F);
        this.shape26_2 = new ModelRenderer(this, 0, 96);
        this.shape26_2.setRotationPoint(-8.0F, 0.0F, 8.0F);
        this.shape26_2.addBox(0.0F, 0.0F, 0.0F, 0, 16, 16, 0.0F);
        this.setRotateAngle(shape26_2, 0.0F, 1.5707963267948966F, 0.0F);
        this.neck = new ModelRenderer(this, 62, 35);
        this.neck.setRotationPoint(0.5F, 7.4F, -5.7F);
        this.neck.addBox(-3.5F, -4.0F, -5.0F, 7, 8, 6, 0.0F);
        this.setRotateAngle(neck, -0.2617993877991494F, 0.0F, 0.0F);
        this.tail_middle = new ModelRenderer(this, 2, 51);
        this.tail_middle.setRotationPoint(0.5F, 3.0F, 14.0F);
        this.tail_middle.addBox(-0.5F, -0.5F, 0.0F, 1, 1, 12, 0.0F);
        this.setRotateAngle(tail_middle, -1.48352986419518F, 0.0F, 0.0F);
        this.shape26_4 = new ModelRenderer(this, 0, 96);
        this.shape26_4.setRotationPoint(-5.5F, -20.0F, -8.0F);
        this.shape26_4.addBox(0.0F, 0.0F, 0.0F, 0, 16, 16, 0.0F);
        this.setRotateAngle(shape26_4, 0.0F, 0.7853981633974483F, 0.0F);
        this.horn_male_left = new ModelRenderer(this, 38, 1);
        this.horn_male_left.setRotationPoint(-5.3F, -5.8F, -4.5F);
        this.horn_male_left.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1, 0.0F);
        this.setRotateAngle(horn_male_left, 0.17453292519943295F, 0.0F, -0.5235987755982988F);
        this.ear_right = new ModelRenderer(this, 23, 1);
        this.ear_right.setRotationPoint(3.0F, -2.5F, -3.0F);
        this.ear_right.addBox(0.0F, 0.0F, 0.0F, 3, 2, 1, 0.0F);
        this.setRotateAngle(ear_right, 0.0F, 0.0F, -0.08726646259971647F);
        this.shape26_1 = new ModelRenderer(this, 0, 96);
        this.shape26_1.setRotationPoint(2.0F, -8.5F, 21.0F);
        this.shape26_1.addBox(0.0F, 0.0F, 0.0F, 0, 16, 16, 0.0F);
        this.setRotateAngle(shape26_1, 4.71238898038469F, 0.0F, 0.7853981633974483F);
        this.left_horn_female = new ModelRenderer(this, 43, 1);
        this.left_horn_female.setRotationPoint(0.0F, 1.0F, 0.0F);
        this.left_horn_female.addBox(-0.0F, -0.0F, -0.0F, 1, 2, 1, 0.0F);
        this.snout = new ModelRenderer(this, 29, 6);
        this.snout.setRotationPoint(-2.5F, -1.0F, -9.0F);
        this.snout.addBox(0.0F, 0.0F, 0.0F, 5, 5, 3, 0.0F);
        this.left_front_leg = new ModelRenderer(this, 47, 34);
        this.left_front_leg.mirror = true;
        this.left_front_leg.setRotationPoint(-2.5F, 14.0F, -5.5F);
        this.left_front_leg.addBox(-2.0F, -1.0F, -2.0F, 3, 11, 4, 0.0F);
        this.chest_right = new ModelRenderer(this, 48, 12);
        this.chest_right.setRotationPoint(5.0F, 2.1F, 13.0F);
        this.chest_right.addBox(0.0F, 0.0F, 0.0F, 8, 8, 3, 0.0F);
        this.setRotateAngle(chest_right, 0.0F, 1.5707963267948966F, 0.0F);
        this.left_hind_leg = new ModelRenderer(this, 47, 34);
        this.left_hind_leg.setRotationPoint(-2.5F, 14.0F, 11.5F);
        this.left_hind_leg.addBox(-2.0F, -1.0F, -2.0F, 3, 11, 4, 0.0F);
        this.saddle = new ModelRenderer(this, 71, 0);
        this.saddle.setRotationPoint(-5.5F, 1.9F, -2.0F);
        this.saddle.addBox(0.0F, 0.0F, 0.0F, 12, 8, 9, 0.0F);
        this.shape26_5 = new ModelRenderer(this, 0, 96);
        this.shape26_5.setRotationPoint(-8.0F, 0.0F, 8.0F);
        this.shape26_5.addBox(0.0F, 0.0F, 0.0F, 0, 16, 16, 0.0F);
        this.setRotateAngle(shape26_5, 0.0F, 1.5707963267948966F, 0.0F);
        this.horn_male_right = new ModelRenderer(this, 38, 1);
        this.horn_male_right.setRotationPoint(4.5F, -6.3F, -4.5F);
        this.horn_male_right.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1, 0.0F);
        this.setRotateAngle(horn_male_right, 0.17453292519943295F, 0.0F, 0.5235987755982988F);
        this.body = new ModelRenderer(this, 0, 15);
        this.body.setRotationPoint(1.0F, 8.0F, 2.0F);
        this.body.addBox(-6.0F, -10.0F, -7.0F, 11, 22, 12, 0.0F);
        this.setRotateAngle(body, 1.5707963267948966F, 0.0F, 0.0F);
        this.right_horn_male = new ModelRenderer(this, 43, 1);
        this.right_horn_male.setRotationPoint(-0.0F, 1.0F, 0.0F);
        this.right_horn_male.addBox(0.0F, 0.0F, -0.0F, 1, 2, 1, 0.0F);
        this.udder = new ModelRenderer(this, 0, 50);
        this.udder.setRotationPoint(0.5F, 8.5F, 4.5F);
        this.udder.addBox(-2.0F, 2.0F, -8.0F, 4, 4, 2, 0.0F);
        this.setRotateAngle(udder, 1.5707963267948966F, 0.0F, 0.0F);
        this.ear_left = new ModelRenderer(this, 23, 1);
        this.ear_left.mirror = true;
        this.ear_left.setRotationPoint(-6.0F, -2.8F, -3.0F);
        this.ear_left.addBox(0.0F, 0.0F, 0.0F, 3, 2, 1, 0.0F);
        this.setRotateAngle(ear_left, 0.0F, 0.0F, 0.08726646259971647F);
        this.tail_tuft = new ModelRenderer(this, 16, 50);
        this.tail_tuft.setRotationPoint(-1.0F, -1.0F, 11.9F);
        this.tail_tuft.addBox(0.0F, 0.0F, 0.0F, 2, 2, 5, 0.0F);
        this.head = new ModelRenderer(this, 0, 0);
        this.head.setRotationPoint(0.0F, -0.3F, -3.6F);
        this.head.addBox(-4.0F, -4.0F, -6.0F, 8, 8, 6, 0.0F);
        this.setRotateAngle(head, 0.3490658503988659F, 0.0F, 0.0F);
        this.right_hind_leg = new ModelRenderer(this, 47, 34);
        this.right_hind_leg.mirror = true;
        this.right_hind_leg.setRotationPoint(4.5F, 14.0F, 11.5F);
        this.right_hind_leg.addBox(-2.0F, -1.0F, -2.0F, 3, 11, 4, 0.0F);
        this.shape26_3 = new ModelRenderer(this, 0, 96);
        this.shape26_3.setRotationPoint(-8.0F, 0.0F, 8.0F);
        this.shape26_3.addBox(0.0F, 0.0F, 0.0F, 0, 16, 16, 0.0F);
        this.setRotateAngle(shape26_3, 0.0F, 1.5707963267948966F, 0.0F);
        this.shape26 = new ModelRenderer(this, 0, 96);
        this.shape26.setRotationPoint(2.0F, 1.0F, 21.0F);
        this.shape26.addBox(0.0F, 0.0F, 0.0F, 0, 16, 16, 0.0F);
        this.setRotateAngle(shape26, 4.71238898038469F, 0.0F, 0.0F);
        this.right_front_leg = new ModelRenderer(this, 47, 34);
        this.right_front_leg.setRotationPoint(4.5F, 14.0F, -5.5F);
        this.right_front_leg.addBox(-2.0F, -1.0F, -2.0F, 3, 11, 4, 0.0F);
        this.shape26.addChild(this.shape26_2);
        this.head.addChild(this.shape26_4);
        this.head.addChild(this.horn_male_left);
        this.head.addChild(this.ear_right);
        this.body.addChild(this.shape26_1);
        this.horn_male_left.addChild(this.left_horn_female);
        this.head.addChild(this.snout);
        this.shape26_4.addChild(this.shape26_5);
        this.head.addChild(this.horn_male_right);
        this.horn_male_right.addChild(this.right_horn_male);
        this.head.addChild(this.ear_left);
        this.tail_middle.addChild(this.tail_tuft);
        this.neck.addChild(this.head);
        this.shape26_1.addChild(this.shape26_3);
        this.body.addChild(this.shape26);
    }
    
    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) 
    { 
    	EntityImprovedMooshroom entitycow = (EntityImprovedMooshroom)entity;
        boolean flag = !(entitycow.isChild());

        this.tail_middle.render(f5);
        this.right_hind_leg.render(f5);
        this.left_hind_leg.render(f5);
        this.left_front_leg.render(f5);
        this.right_front_leg.render(f5);
        this.udder.render(f5);
        this.body.render(f5);
        
        if (entitycow.getSheared() == true) this.shape26_1.isHidden = true;
        else this.shape26_1.isHidden = false;
        if (entitycow.getSheared() == true) this.shape26.isHidden = true;
        else this.shape26.isHidden = false;
        if (entitycow.getSheared() == true) this.shape26_4.isHidden = true;
        else this.shape26_4.isHidden = false;
        
        if (!flag)
        {
        	float size = 0.6F;
        	if (Config.smoothGrowth == true) size -= 0.4F * entitycow.getGrowthSize();
            GlStateManager.pushMatrix();
            GlStateManager.scale(size, 0.5F + size * 0.5F, 0.5F + size * 0.5F);
            GlStateManager.translate(0.0F, 0.15F * (1.0F - size), -0.15F * (1.0F - size));
        }
        
        this.neck.render(f5);
        if (!flag)
        {
            GlStateManager.popMatrix();
        }
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
    @Override
	public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn)
    {
        this.neck.rotateAngleX = (headPitch * 0.01F) - 0.25f;
        this.neck.rotateAngleY = netHeadYaw * 0.01F;
        this.head.rotateAngleX = (headPitch * 0.007453292F) + 0.25f;
        this.head.rotateAngleY = netHeadYaw * 0.007453292F;
        this.body.rotateAngleX = ((float)Math.PI / 2F);
        this.right_front_leg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        this.left_front_leg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
        this.right_hind_leg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
        this.left_hind_leg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        this.tail_middle.rotateAngleZ = MathHelper.cos(limbSwing * 0.6662F) * 1.1F * limbSwingAmount;
    }
}
