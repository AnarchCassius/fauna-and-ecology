package faunaandecology.mod.model;

import faunaandecology.mod.entity.passive.EntityJungleFowl;
import faunaandecology.mod.entity.passive.EntityMallard;
import faunaandecology.mod.util.Config;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelDuck extends ModelBase {
    public ModelRenderer wing_left;
    public ModelRenderer wing_right;
    public ModelRenderer neck;
    public ModelRenderer body;
    public ModelRenderer tail_small;
    public ModelRenderer leg_right;
    public ModelRenderer foot_left;
    public ModelRenderer head;
    public ModelRenderer beak;
    public ModelRenderer foot_right;
    public ModelRenderer foot_right_1;

    public ModelDuck() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.wing_right = new ModelRenderer(this, 24, 21);
        this.wing_right.setRotationPoint(3.5F, 15.1F, 0.3F);
        this.wing_right.addBox(-1.0F, 0.0F, -3.0F, 1, 4, 7, 0.0F);
        this.setRotateAngle(wing_right, -0.08726646259971647F, 0.0F, -0.0F);
        this.tail_small = new ModelRenderer(this, 48, 13);
        this.tail_small.setRotationPoint(-2.0F, 15.4F, 3.9F);
        this.tail_small.addBox(0.0F, 0.0F, 0.0F, 4, 5, 2, 0.0F);
        this.setRotateAngle(tail_small, 0.17453292519943295F, 0.0F, 0.0F);
        this.leg_right = new ModelRenderer(this, 39, 4);
        this.leg_right.setRotationPoint(0.0F, 21.0F, 1.0F);
        this.leg_right.addBox(0.0F, 0.0F, 0.0F, 3, 3, 0, 0.0F);
        this.neck = new ModelRenderer(this, 36, 14);
        this.neck.setRotationPoint(1.0F, 16.7F, -3.5F);
        this.neck.addBox(-2.0F, -6.0F, -2.0F, 2, 8, 2, 0.0F);
        this.setRotateAngle(neck, -0.08726646259971647F, 0.0F, 0.0F);
        this.foot_left = new ModelRenderer(this, 39, 4);
        this.foot_left.mirror = true;
        this.foot_left.setRotationPoint(-3.0F, 21.0F, 1.0F);
        this.foot_left.addBox(0.0F, 0.0F, 0.0F, 3, 3, 0, 0.0F);
        this.body = new ModelRenderer(this, 1, 10);
        this.body.setRotationPoint(0.5F, 18.0F, 0.0F);
        this.body.addBox(-3.0F, -4.0F, -3.0F, 5, 9, 6, 0.0F);
        this.setRotateAngle(body, 1.48352986419518F, 0.0F, 0.0F);
        this.head = new ModelRenderer(this, 2, 1);
        this.head.setRotationPoint(-1.0F, -5.0F, -0.4F);
        this.head.addBox(-1.5F, -1.5F, -3.0F, 3, 3, 3, 0.0F);
        this.setRotateAngle(head, 0.08726646259971647F, 0.0F, 0.0F);
        this.foot_right_1 = new ModelRenderer(this, 31, 1);
        this.foot_right_1.setRotationPoint(0.0F, 3.0F, -3.0F);
        this.foot_right_1.addBox(0.0F, 0.0F, 0.0F, 3, 0, 3, 0.0F);
        this.beak = new ModelRenderer(this, 19, 3);
        this.beak.setRotationPoint(0.0F, 0.4F, -2.5F);
        this.beak.addBox(-1.0F, -1.0F, -3.0F, 2, 2, 3, 0.0F);
        this.wing_left = new ModelRenderer(this, 24, 21);
        this.wing_left.mirror = true;
        this.wing_left.setRotationPoint(-3.5F, 15.1F, 0.3F);
        this.wing_left.addBox(0.0F, 0.0F, -3.0F, 1, 4, 7, 0.0F);
        this.setRotateAngle(wing_left, -0.08726646259971647F, 0.0F, 0.0F);
        this.foot_right = new ModelRenderer(this, 31, 1);
        this.foot_right.setRotationPoint(0.0F, 3.0F, -3.0F);
        this.foot_right.addBox(0.0F, 0.0F, 0.0F, 3, 0, 3, 0.0F);
        this.neck.addChild(this.head);
        this.foot_left.addChild(this.foot_right_1);
        this.head.addChild(this.beak);
        this.leg_right.addChild(this.foot_right);
    }

    
    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) 
    { 
    	EntityJungleFowl entitycow = (EntityJungleFowl)entity;
        boolean flag = !(entitycow.isChild());

        this.wing_right.render(f5);
        this.wing_left.render(f5);
        this.foot_left.render(f5);
        this.leg_right.render(f5);
        this.body.render(f5);
        this.tail_small.render(f5);
        
        if (!flag)
        {
        	float size = 1.15F;
        	float growthSize = f5;
        	if (Config.smoothGrowth == true) size -= 0.15F * growthSize;
        	growthSize = (1.0F - growthSize);
            GlStateManager.pushMatrix();
            GlStateManager.scale(size, size, size);
            GlStateManager.translate(0.0F, -0.1F * growthSize, 0.1F * growthSize);
        }
        
        this.neck.render(f5);
        
        if (!flag)
        {
            GlStateManager.popMatrix();
        }
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
    @Override
	public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn)
    {

    	EntityMallard fowl =  (EntityMallard) entityIn;
            this.neck.rotateAngleX = (headPitch * 0.01F) + MathHelper.cos(limbSwing * 2.0F) * 0.25F * limbSwingAmount;
            this.neck.rotateAngleY = netHeadYaw * 0.01F;
            this.head.rotateAngleX = (headPitch * 0.007453292F) - 0.05f;
            this.head.rotateAngleY = netHeadYaw * 0.002053292F; //0.007453292F;
            this.body.rotateAngleX = ((float)Math.PI / 2F);
            this.leg_right.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
            this.foot_left.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
            
            if (fowl.isInWater() == false) this.wing_right.rotateAngleZ = -ageInTicks;
            else this.wing_right.rotateAngleZ = 0;
            if (fowl.isInWater() == false) this.wing_left.rotateAngleZ = ageInTicks;
            else this.wing_left.rotateAngleZ = 0;
    }
}
